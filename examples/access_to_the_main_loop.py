from pynimation.viewer.viewer import Viewer
from pynimation.viewer.inout import mesh_importer as importer
from pynimation.viewer.scene import Scene
from pynimation.common import data as data_
from pynimation.viewer.loop import CallableLoop

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")
fbx_file = data_.getDataPath("data/animations/ybot@IdleLoop.fbx")

v = PynimationApp().viewer
scene = v.defaultScene()

[fbx_character] = importer.loadCharacters(fbx_file)
[bvh_character] = importer.loadCharacters(bvh_file)

scene.addInLine([fbx_character, bvh_character])


# callable loop
class Display(CallableLoop):
    # the default initialization is the scene
    def __init__(self, viewer: Viewer, scene: Scene, priority: int):
        self.scene: Scene = scene
        # initialization of the father, only the priority is needed
        CallableLoop.__init__(self, viewer, priority)

    # the before display
    def beforeDisplay(self):
        print("before")

    # the after display
    def afterDisplay(self):
        print("after")


display = Display(v, scene, priority=5)
v.displayScene(scene)
