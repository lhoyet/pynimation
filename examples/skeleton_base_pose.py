from pynimation.anim.animation import Animation
import pynimation.common.data as data_

from pynimation.viewer.ui import PynimationApp

# /!\ In PyNimation 2.0, the skeleton base Pose is included in the animation
# modifying the base pose will have no effect until it is removed from the
# animation, at which point updating the globals is no longer possible until
# it is added back

bipedFbxFile = data_.getDataPath("data/animations/biped.fbx")

# Here is an animation with a skeleton base pose equal to the first frame
# of the animnation
animationOrig = Animation.load(bipedFbxFile)

maleSkeletonFile = data_.getDataPath("data/animations/maleskeleton.npz")

# And another blank animation with the same skeleton but in a T pose
maleSkeleton = Animation.load(maleSkeletonFile)

# The objective is to set the base pose of the skeleton of the first animation
# to the T pose of the second

animationTpose = animationOrig.copy()

animationTpose.skeleton.basePose = maleSkeleton.skeleton.basePose.copy()

# animationTpose can now be compared to other animations with the same skeleton
# and base pose by removing the base pose from it

animationTpose.removeBasePose()

# updating globals will no longer work until the base pose is added back

animationTpose.addBasePose()


def getBasePoseAnim(anim: "Animation"):
    anim = anim.copy()
    anim.resize((1, *anim.shape[1:]))
    anim[0] = anim.skeleton.basePose
    return anim


origBasePose = getBasePoseAnim(animationOrig)
tPoseBasePose = getBasePoseAnim(animationTpose)

PynimationApp().viewer.display(
    [origBasePose, tPoseBasePose, animationOrig, animationTpose]
)
