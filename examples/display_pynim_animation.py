from pynimation.anim.animation import Animation
from pynimation.common import data as data_

from pynimation.viewer.ui import PynimationApp


pynim_file = data_.getDataPath("data/animations/ybot@IdleLoop.pynim")

animation = Animation.load(pynim_file)

f = "test.pynim"
animation.save(f)
a = Animation.load(f)

PynimationApp().viewer.display([animation, a])
