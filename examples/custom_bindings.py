from qtpy.QtCore import Qt
import pygame as pg

from pynimation.common import data as data_
from pynimation.anim.animation import Animation
from pynimation.viewer.ui.event import (
    KeyPressEvent,
    KeyHoldEvent,
)


# change this value if you want to test it with the qt ui or the pygame ui
test_with_qt = False

if test_with_qt:
    from pynimation.viewer.ui.qt_ui.qt_application import PynimationApp
else:
    from pynimation.viewer.ui.pygame_ui.pygame_application import PynimationApp


def key(k: str):
    # translation of keyboard events between qt and pygame
    if k == "a":
        return Qt.Key.Key_A if test_with_qt else pg.K_a
    elif k == "q":
        return Qt.Key.Key_Q if test_with_qt else pg.K_q
    elif k == "j":
        return Qt.Key.Key_J if test_with_qt else pg.K_j
    elif k == "r":
        return Qt.Key.Key_R if test_with_qt else pg.K_r
    elif k == "s":
        return Qt.Key.Key_S if test_with_qt else pg.K_s
    elif k == "z":
        return str(int(Qt.Key.Key_Z)) if test_with_qt else "z"


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")
animation = Animation.load(bvh_file)


def myAction():
    print("action")


def otherAction(param):
    print("other action %s" % param)


pynimation = PynimationApp()
pynimation.viewer.addAnimations([animation])

# get the actions manager of pynimation
actionManager = pynimation.tools.actions
# get the component you are interested in
viewerComponent = pynimation.window.viewerComponent
# get its binding manager
bindings = viewerComponent.bindings

# register new actions in the actions manager
actionManager.add("newAction", myAction)
actionManager.add("newOtherAction", otherAction, args=[2])

# bind newAction to an event
# (check the key function to understand how Key events work with Qt and Pygame)
bindings.bind(KeyPressEvent(key("a")), "newAction")

# bind an already existing action with another event
bindings.bind(KeyHoldEvent(key("z")), "previousFrame")

# add a new action for an already binded event
bindings.bind(KeyPressEvent(key("j")), "newOtherAction")

# unbind an event
# you can't record anymore with the "r" key
bindings.unbind(KeyPressEvent(key("r")))

# the screenshot key "s" is rebinded the "q" key
bindings.rebind(KeyPressEvent(key("s")), KeyPressEvent(key("q")))

# bind multiple actions in one call
bindings.bindMultipleAction(
    KeyPressEvent(key("s")), ["newAction", "newOtherAction"]
)

# run the application
pynimation.run()
