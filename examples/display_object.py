from pynimation.common.vec import Vec3
from pynimation.viewer.objects.cube import Cube

from pynimation.viewer.ui.pygame_ui.pygame_application import PynimationApp


cube = Cube()
# cube and its descendants globalTransforms are invalidated
cube.transform.translate(Vec3.y)
# invalid globalTransforms are recomputed
PynimationApp().viewer.display(cube)
