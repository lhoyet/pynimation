import pynimation.common.data as data_
from pynimation.anim.animation import Animation

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)

legs = animation.skeleton.legs
knees = animation.skeleton.knees

# do not update animation.globals
# while in this block
with animation.holdUpdate() as a:
    a.joint[legs].rotation.eulerDeg.x += 30
    a.joint[knees].rotation.eulerDeg.x -= 30

# do not update animation from skeleton
# while in this block
with animation.holdUpdateGraph() as a:
    for leg in legs:
        leg.children = (leg.children[0].children[0],)

PynimationApp().viewer.display(animation)
