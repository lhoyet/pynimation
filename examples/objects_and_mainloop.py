import math
import numpy as np

from pynimation.viewer.viewer import Viewer
from pynimation.viewer.inout import mesh_importer as importer
from pynimation.viewer.scene import Scene
from pynimation.common import data as data_
from pynimation.viewer.loop import CallableLoop
from pynimation.viewer.objects.plane import Plane
from pynimation.viewer.objects.cylinder import Cylinder
from pynimation.viewer.objects.sphere import Sphere
from pynimation.viewer.objects.cube import Cube
from pynimation.viewer.objects.arrow import Arrow
from pynimation.viewer.objects.reference_system import ReferenceSystem
from pynimation.viewer.objects.triangle import Triangle
from pynimation.common.vec import Vec3

from pynimation.viewer.ui.pygame_ui.pygame_application import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")
fbx_file = data_.getDataPath("data/animations/ybot@IdleLoop.fbx")

scene = Viewer.defaultScene()
[fbx_character] = importer.loadCharacters(fbx_file)
[bvh_character] = importer.loadCharacters(bvh_file)
# characters and their descendants globalTransforms are invalidated
fbx_character.transform.position = Vec3.x
bvh_character.transform.position = [-0.5, 0, -1]
scene.add(fbx_character)
scene.add(bvh_character)


# callable loop
class Display(CallableLoop):
    # the default initialization is the scene
    def __init__(self, viewer: Viewer, scene: Scene, priority: int):
        self.numberOfLoops: int = 100
        self.loop_i: int = 0
        self.scene: Scene = scene

        # quad
        self.quad: Plane = Plane(
            width=0.5,
            height=0.5,
            normalDirectionAx="x",
            color=[50 / 250, 120 / 250, 120 / 250, 1],
            textureFilename="floor_blue.bmp",
            fromFloorPosition=0,
        )
        self.targetPos = np.array([1, 1, -2])
        self.scene.add(self.quad)

        # cylinder
        self.cylinder: Cylinder = Cylinder(
            height=1.0,
            radiusBase=0.4,
            radiusTop=0.1,
            numberOfSectors=30,
            numberOfStacks=3,
            center=True,
            color=[1, 0.5, 0.4, 1],
            height_direction="y",
            phong=True,
        )
        self.scene.add(self.cylinder)

        # cube
        self.cube: Cube = Cube(1.0, color=[0.2, 0.5, 0.1])
        self.scene.add(self.cube)

        # arrow
        self.arrow1 = Arrow()
        scene.add(self.arrow1)
        self.arrow2 = Arrow(color=[0.9, 0.2, 0.3])
        scene.add(self.arrow2)
        self.arrow2.position = np.array([2, 2, 2])

        # sphere
        self.sphere = Sphere(0.055, [1, 1, 1, 0.8])
        self.scene.add(self.sphere)

        # reference system
        self.referenceSystem1 = ReferenceSystem()
        self.referenceSystem2 = ReferenceSystem()
        self.scene.add(self.referenceSystem1)
        self.scene.add(self.referenceSystem2)

        # triangle
        self.triangle: Triangle = Triangle()
        self.scene.add(self.triangle)

        # initialization of the father, only the priority is needed
        CallableLoop.__init__(self, viewer, priority)

    # the before display
    def beforeDisplay(self):
        k = math.sin(self.loop_i * math.pi / self.numberOfLoops)
        self.loop_i = self.loop_i + 1

        # quad
        currentPos = self.targetPos + np.array([0, k, 0])
        self.quad.transform.position += currentPos
        self.quad.transform.rotation.rotvec = Vec3.y * k
        self.quad.setMixParameter(k)

        # cylinder
        currentPos = self.targetPos + np.array([-0.6, 0, 0])
        self.cylinder.transform.position = currentPos
        transVector: np.ndarray = np.array([0, k / 10, 0])
        self.cylinder.transform.translate(transVector)
        self.cylinder.transform.rotation.rotvec = Vec3.y * k

        # cube
        currentPose = np.array(self.targetPos) + np.array([1, 0, -1])
        self.cube.transform.position = currentPose
        translatonVector: np.ndarray = np.array([0, 0, k])
        self.cube.transform.translate(translatonVector)

        # arrow
        direction: np.ndarray = np.array([1, 1 + k, 1])
        self.arrow1.pointAt(direction)
        self.arrow2.pointAt(direction)

        # sphere
        self.sphere.transform.position = direction

        # reference system
        direction: np.ndarray = np.array([-1, 1 + k, 1])
        self.referenceSystem1.transform.position = direction

        # triangle
        direction: np.ndarray = np.array([-1, 1.5 - k / 2, -1])
        self.triangle.transform.rotation.rotvec = Vec3.xy * k
        self.triangle.transform.position = direction

    # if not used still need to be defined
    def afterDisplay(self):
        pass


v = PynimationApp().viewer
display = Display(v, scene, priority=5)
v.displayScene(scene)
