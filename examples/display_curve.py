import numpy as np
from pynimation.viewer.objects.curve import Curve

from pynimation.viewer.ui.pygame_ui.pygame_application import PynimationApp


angl = np.linspace(0, np.pi / 2, 5)
points = np.stack([np.cos(angl), np.sin(angl), np.zeros(angl.shape)]).T


curve = Curve(points, width=4, color=[0, 1, 0])

PynimationApp().viewer.display(curve)
