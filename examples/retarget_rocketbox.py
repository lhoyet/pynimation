from pynimation.viewer.stickfigure import StickFigure
from pynimation.common import data as data_
from pynimation.anim.animation import Animation
from pynimation.viewer.character import Character

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

source = Animation.load(bvh_file)

rocketbox_file = data_.getDataPath("data/animations/maleskeleton.npz")

target = Animation.load(rocketbox_file)

goal_file = data_.getDataPath("data/animations/biped.fbx")

goal = Animation.load(goal_file)

retargeted = source.copy()

target.set(target[[0]])

# keep animations on spot
for anim in [source, target, retargeted, goal]:
    anim.root.position.z = anim.skeleton.root.transform.position.z

retargeted.retarget(
    target.skeleton,
    "kulpa",  # use kulpa implementation
    spineCurve=False,  # don't retarget the Spine with a Curve
    limbsExtendRatio=True,  # retarget limbs to keep extension ratio
)

# synchronise goal and retargeted animations
goal.setFramerate(120)
goal.timewarp(retargeted.nframes)

# retarget back to source
retargeted_inv = retargeted.copy()

retargeted_inv.retarget(
    source.skeleton,
    "kulpa",  # use kulpa implementation
    spineCurve=False,  # don't retarget the Spine with a Curve
    limbsExtendRatio=True,  # retarget limbs to keep extension ratio
)

# display characters
animations = [source, target, retargeted_inv, retargeted, goal]
colors = [
    [0.9, 0.0, 0.0, 1],
    [0.0, 0.9, 0.0, 1],
    [0, 0.0, 0.9, 1],
    [0.0, 0.9, 0.9, 1],
    [0.9, 0.0, 0.9, 1],
]
characters = [
    Character(a, [StickFigure(a.skeleton, color=color)])
    for (a, color) in zip(animations, colors)
]

for i, c in enumerate(characters[:2]):
    c.transform.translate([i, 0, 0])
characters[2].transform.translate([0, 0, 0])
for c in characters[3:]:
    c.transform.translate([i + 1, 0, 0])

PynimationApp().viewer.display(characters, inLine=False)
