from pynimation.viewer.objects.cube import Cube

from pynimation.viewer.ui.pygame_ui.pygame_application import PynimationApp


cube = Cube()

v = PynimationApp().viewer


# define callback function and register it
# with Viewer.callback() decorator
@v.callback(args=[cube])
def moveCube(cube: Cube):
    cube.globalTransform.position.y += 0.001
    cube.globalTransform.position.y %= 2


# (other way to register callback function)
# v.addCallback(moveCube, args=[cube], when=v.CallbackTime.BEFORE_DISPLAY)

s = v.defaultScene()
s.add(cube)

v.displayScene(s)
