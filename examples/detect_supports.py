import numpy as np

from pynimation.viewer.colored_character import ColoredCharacter
from pynimation.anim.animation import Animation
from pynimation.common import data as data_
import pynimation.anim.detect_supports.utils as utils

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walking_female.bvh")
xml_file = data_.getDataPath("data/animations/walking_female.xml")

animation = Animation.load(bvh_file)
skeleton = animation.skeleton

# joints, maxHeight and maxSpeeds parameters can be given
# supports = MenardaisSupportsDetecter(animation).detectSupports(
#     joints = skeleton.toes, maxHeight=0.14, maxSpeed=0.019
# )

# or they will default to usual values
# skeleton.toes
# MenardaisSupportsDetecter.defaultMaxHeight
# MenardaisSupportsDetecter.defaultMaxSpeed
supportsMenardais = animation.detectSupports("menardais")

# compute supports of toes using circle fit method
joints = [animation.skeleton.leftToe, animation.skeleton.rightToe]
supportsCircleFit = animation.detectSupports("circlefit", joints)

# compute merged supports of ankles and toes using circle fit method
joints = [
    [skeleton.leftToe, skeleton.leftAnkle],
    [skeleton.rightToe, skeleton.rightAnkle],
]
supportsCircleFitMerged = animation.detectSupports("circlefit", joints)

# you can also read supports from an xml file with the following syntax:
# <SupportPhases>
#     <LeftSupport frame="0" />
#     <DoubleSupport frame="40" />
#     <RightSupport frame="61" />
# ...
# </SupportPhases>
supportsXML = utils.readXMLSupports(xml_file)

# supports are masks arrays for each support joint
# indicating whether this joint is a support at this frame
# shape: nsupports x nframes
allsupports = [
    supportsMenardais,
    supportsCircleFit,
    supportsCircleFitMerged,
    supportsXML,
]

# a more readable way to print supports is with frame ranges
for support in allsupports:
    print(utils.supportsToRange(support))

chars = []

# to visualise supports, support arrays can be passed to
# ColoredCharacter.colorSupportsJoints  to color specific joints
# during support phases (left and right legs are colored by
# default but other joints can be specified, see relevant
# section of the API documentation)
for support in allsupports[:-1]:
    c = ColoredCharacter(animation)
    c.colorSupportsJoints(support)
    chars.append(c)

# supports can also be visualised using colored spheres
# added to joints with ColoredCharacter.colorSupportsSpheres
c = ColoredCharacter(animation)
c.colorSupportsSpheres(
    allsupports[-1],
    defaultColors=np.ones(4),
    colors=np.array([0.0, 0.9, 0.0, 1.0]),
)
chars.append(c)

# display the characters
PynimationApp().viewer.display(chars)
