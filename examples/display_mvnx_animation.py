from pynimation.anim.animation import Animation
from pynimation.common import data as data_

from pynimation.viewer.ui import PynimationApp


mvnx_file = data_.getDataPath("data/animations/walk.mvnx")

animation = Animation.load(mvnx_file)
PynimationApp().viewer.display(animation)
