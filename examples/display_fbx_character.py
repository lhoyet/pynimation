from pynimation.viewer.character import Character
from pynimation.common import data as data_

from pynimation.viewer.ui import PynimationApp


fbx_file = data_.getDataPath("data/animations/ybot@IdleLoop.fbx")
fbx_model_file = data_.getDataPath("data/models/ybot.fbx")

character = Character.load(fbx_file)

# or with model
character = Character.load(fbx_file, fbx_model_file)

PynimationApp().viewer.display(character)
