from pynimation.viewer.character import Character
from pynimation.common import data as data_
from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

character = Character.load(bvh_file)
PynimationApp().viewer.display(character)
