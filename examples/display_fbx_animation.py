from pynimation.anim.animation import Animation
from pynimation.common import data as data_

from pynimation.viewer.ui import PynimationApp


fbx_file = data_.getDataPath("data/animations/ybot@IdleLoop.fbx")

animation = Animation.load(fbx_file)
PynimationApp().viewer.display(animation)
