from pynimation.common import data as data_
from pynimation.anim.animation import Animation
from pynimation.viewer.character import Character

from pynimation.viewer.ui.pygame_ui.pygame_application import PynimationApp


walkLoop = data_.getDataPath("data/animations/walk_loop.bvh")
walkingFemale = data_.getDataPath("data/animations/walking_female.bvh")

walkLoopAnim = Animation.load(walkLoop)
walkingFemaleAnim = Animation.load(walkingFemale)
character = Character.load(walkLoop)

# modify the first animation
walkLoopAnim.rightKnee.rotation.eulerDeg.x += 90
walkLoopAnim.framerate = 60

PynimationApp().viewer.display(walkLoopAnim, walkingFemaleAnim, character)
