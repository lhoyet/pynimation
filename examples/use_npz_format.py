import pynimation.common.data as data_
from pynimation.anim.animation import Animation

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)

# The .npz format can be used to load and save animations to and from
# PyNimation and other python scripts

# It is the fastest and produces the lightest files if you enable the
# compress option (see examples/io_perf.py for more)

testFile = "test.npz"
animation.save(testFile, compress=True)
test = animation.load(testFile)

PynimationApp().viewer.display(animation, test)
