import pynimation.common.data as data_
from pynimation.anim.animation import Animation

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)

proxyAnim = animation.copy()
proxyAnim.createProxy()

noProxy = proxyAnim.copy()
noProxy.destroyProxy()

PynimationApp().viewer.display([animation, proxyAnim, noProxy])
