from pynimation.anim.animation import Animation
from pynimation.common import data

from pynimation.viewer.ui import PynimationApp


# obtain absolute path to provided example BVH animation file
# (this is only necessary for files provided with PyNimation)
bvh_file = data.getDataPath("data/animations/walk_loop.bvh")

# load the animation
animation: Animation = Animation.load(bvh_file)

# keep a copy of the original
animationPost = animation.copy()

# animation shape is (nframes, nfjoints, 4, 4)
print("animation shape ", animation.shape)

# they can be manipulated like normal numpy arrays
print(animation[0, 1, :3, 3])

# or with the the PyNimation API (equivalent to the line above)
print(animation.root[1].position)

# rotate legs by 10 degrees on x axis
animation.legs.rotation.eulerDeg.x += 10

# export the modified animation
animation.save("modified.fbx")

# display original and modified animations side by side
PynimationApp().viewer.display([animation, animationPost])
