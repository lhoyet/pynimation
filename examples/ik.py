from pynimation.viewer.character import Character
import pynimation.common.data as data_
from pynimation.common.vec import Vec3
from pynimation.anim.animation import Animation
from pynimation.viewer.objects.sphere import Sphere
from pynimation.anim.ik.ik_factory import ikFactory

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)
animation_orig = animation.copy()

target = animation.skeleton.rightAnkle
root = target.parent.parent
ik = ikFactory(target, root, settings={"debug": True}).solve(
    animation.joint[target].globals.position + Vec3.y * 0.1, animation
)

v = PynimationApp().viewer
scene = v.defaultScene()
characters = [Character(a) for a in [animation, animation_orig]]

scene.addMultiple(characters)

target_sphere = Sphere(0.02, color=[0, 0.9, 0, 1])
end_sphere = Sphere(0.02, color=[0.9, 0.0, 0, 1])
characters[0].children[0].children[target.id].addChild(end_sphere)
scene.add(target_sphere)


@v.callback(args=[target_sphere])
def moveTargetSphere(sphere):
    p = (
        characters[1].children[0].children[target.id].globalTransform.position
        + Vec3.y * 0.1
    )
    sphere.transform.position = p


v.displayScene(scene)
