import sys
from pynimation.anim.animation import Animation
from pynimation.common import data as data_
from pynimation.anim.motiongraph.motiongraph import MotionGraph
from pynimation.anim.motiongraph.randomwalk import RandomWalk
from pynimation.anim.metrics.global_position_velocity_no_root import (
    GlobalPositionVelocityNoRootMetric as CurrentMetric,
)

from pynimation.viewer.player import Player
from pynimation.viewer.stickfigure import StickFigure
from pynimation.viewer.scene_graph import SceneNode
from pynimation.viewer.animated import Animated

from pynimation.viewer.ui import PynimationApp


mographFileName = (
    data_.getDataPath("data/animations/mograph/") + "test.mograph"
)

# example of how to include animation files for the motion graph creation.
# These files can be downloaded from https://gitlab.inria.fr/lhoyet/pynimation/uploads/da4b31c28cdb0d6cf383ab55a6d7d5c3/motion_graph_data.zip
# They are also available from mixamo for instance
animFilenames = [
    "data/animations/mograph/ybot@WalkingForward.npz",
    "data/animations/mograph/ybot@WalkingBackwardLoop.npz",
    "data/animations/mograph/ybot@WalkingLeftTurnLoop.npz",
    "data/animations/mograph/ybot@WalkingRightTurnLoop.npz",
]


def animFilesToNpz() -> None:
    animFilenames = [
        "ybot@WalkingForward",
        "ybot@WalkingForwardLoop",
        "ybot@IdleLoop",
        "ybot@WalkingBackwardLoop",
        "ybot@WalkingLeftTurnLoop",
        "ybot@WalkingRightTurnLoop",
    ]

    # animFilenames = ["LongWalk"]

    for fn in animFilenames:
        path = data_.getDataPath("data/animations/mograph/") + fn
        anim_file = Animation.load(path + ".fbx")
        anim_file.setFramerate(60)
        anim_file.save(
            path + ".npz",
        )


def createMotionGraph(mographFilename: str) -> None:
    anim_files = [
        Animation.load(data_.getDataPath(fn)) for fn in animFilenames
    ]
    for anim in anim_files:
        anim.skeleton.mapHumanoidSkeleton()
    mograph = MotionGraph(
        CurrentMetric,
        anim_files,
        skeletonMap=anim_files[0].skeleton.SkeletalPart.defaultSkeleton(),
        framerate=30,
        threshold=0.5,
    )
    mograph.save(mographFileName)


# callable loop
class MotionGraphCharacter(SceneNode, Animated):
    def __init__(self, filename: str):
        """
        MyCharacter (see Character class for more complex, mesh-based example)
        Loads animations from :attr:`filename`
        A :class:`~pynimation.viewer.stickfigure.StickFigure` mesh will be created and used as a mesh for the character

        Parameters
        ----------
        filename:
            file to load animations from
        """
        super().__init__("")
        self.mograph = MotionGraph()
        self.mograph.load(filename)
        self.randomwalk = RandomWalk(self.mograph)

        self.player = Player()

        self.children = (StickFigure(self.mograph.skeleton),)

    def animate(self) -> None:
        deltaTime = self.player.getDeltaTime()
        self.randomwalk.update(deltaTime)
        self.children[0].animate(self.randomwalk.getCurrentFrame())


try:
    animFilesToNpz()
    createMotionGraph(mographFileName)
except Exception as e:
    print(e)
    print(
        """
Following files
%s
are probably missing in %s
They can be downloaded at https://gitlab.inria.fr/lhoyet/pynimation/uploads/da4b31c28cdb0d6cf383ab55a6d7d5c3/motion_graph_data.zip
"""
        % (
            "\n".join(animFilenames),
            sys.modules["pynimation"].__path__,
        )
    )
    sys.exit(1)

PynimationApp().viewer.display(MotionGraphCharacter(mographFileName))
