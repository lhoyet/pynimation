import time
from pynimation.anim.metrics.similarity import Similarity
import pynimation.common.data as data_
from pynimation.anim.animation import Animation

from pynimation.anim.metrics.global_position_velocity_no_root import (
    GlobalPositionVelocityNoRootMetric as Metric1,
)

from pynimation.anim.metrics.global_position_no_root import (
    GlobalPositionNoRootMetric as Metric2,
)

from pynimation.anim.metrics.local_orientation_velocity_no_root import (
    LocalOrientationVelocityNoRootMetric as Metric3,
)

from pynimation.anim.metrics.local_orientation_no_root import (
    LocalOrientationNoRootMetric as Metric4,
)

metrics = [Metric1, Metric2, Metric3, Metric4]

bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)


def timeit(t0):
    t = time.time_ns() * 1e-6
    print("%.5f ms" % (t - t0))
    return t


t0 = time.time_ns() * 1e-6
s = []
for metric in metrics:
    sim = Similarity(metric, animation)
    sim.getLocalMinima()
    s.append(sim.similarities)
    t0 = timeit(t0)
    sim.toImage()
    print("metric %s" % metric.__name__)
