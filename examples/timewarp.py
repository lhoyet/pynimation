import pynimation.common.data as data_
from pynimation.anim.animation import Animation

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animationOrig = Animation.load(bvh_file)


animationSlower = animationOrig.copy()

# slowing down the animation can be done
# by timewarping to a larger number of frames
slowerSpeedRatio = 1.5
largerFrameNumber = round(animationOrig.nframes * slowerSpeedRatio)
animationSlower.timewarp(largerFrameNumber)


animationFaster = animationOrig.copy()

# speeding up the animation can be done
# by timewarping to a smaller number of frames
fasterSpeedRatio = 0.5
smallerFrameNumber = round(animationOrig.nframes * fasterSpeedRatio)
animationFaster.timewarp(smallerFrameNumber)

PynimationApp().viewer.display(animationOrig, animationSlower, animationFaster)
