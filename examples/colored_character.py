import numpy as np

from pynimation.anim.animation import Animation
from pynimation.viewer.colored_character import ColoredCharacter
from pynimation.common import data as data_

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)

# color the whole character in red
red = [0.9, 0.0, 0.0, 1.0]

# color information per joint and frame
# colors.shape = (frame_count, joint_count, RGBA)
colors = np.full((animation.nframes, animation.njoints, 4), red)

# color some joints in blue
blue = [0.0, 0.0, 0.9, 1.0]

skeleton = animation.skeleton
jointsToColor = list(
    map(
        lambda j: j.id,
        [
            *skeleton.wrists,
            skeleton.head,
        ],
    )
)


# range of frames to color
framesToColor = slice(100, 200)

# set colors of chosen joints and frames
colors[framesToColor, jointsToColor] = blue

char = ColoredCharacter(
    animation,
    colors,
)

# display colored character
PynimationApp().viewer.display(char)
