from typing import TYPE_CHECKING

from qtpy.QtCore import Qt

from pynimation.viewer.viewer import Viewer
from pynimation.anim.animation import Animation
from pynimation.common import data as data_
from pynimation.viewer.ui.event import (
    KeyPressEvent,
)

if TYPE_CHECKING:
    from pynimation.viewer.third_person_camera import ThirdPersonCamera

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)

# add third person camera
app = PynimationApp()
viewer = app.viewer

# make the camera orbit around character root at first frame
camera = viewer.viewport.mainCamera
camera.center = animation[0].globals.root.position


def centerOnCharacter(
    camera: "ThirdPersonCamera", animation: "Animation", viewer: Viewer
):
    fid = viewer.player.getCurrentFrameId(animation)
    camera.center = animation[fid].globals.root.position


# center on character

# every frame
# viewer.addCallback(
#     centerOnCharacter,
#     Viewer.CallbackTime.BEFORE_DISPLAY,
#     args=[camera, animation, viewer],
# )

# get the ActionManager and the BindingManager
actions = app.tools.actions
bindings = app.window.viewerComponent.bindings

# on key press
actions.add(
    "centerOnCharacter",
    centerOnCharacter,
    args=[camera, animation, viewer],
)
bindings.bind(KeyPressEvent(Qt.Key.Key_A), "centerOnCharacter")

viewer.display(animation)
