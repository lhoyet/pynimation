import numpy as np
import pynimation.common.data as data_
from pynimation.anim.animation import Animation

from pynimation.anim.metrics.global_position_velocity_no_root import (
    GlobalPositionVelocityNoRootMetric as Metric1,
)

from pynimation.anim.metrics.global_position_no_root import (
    GlobalPositionNoRootMetric as Metric2,
)

from pynimation.anim.metrics.local_orientation_velocity_no_root import (
    LocalOrientationVelocityNoRootMetric as Metric3,
)

from pynimation.anim.metrics.local_orientation_no_root import (
    LocalOrientationNoRootMetric as Metric4,
)

metrics = [Metric1, Metric2, Metric3, Metric4]

bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)

# compute similarity metric between first ten and last ten frames
for metric in metrics:
    s = metric.evaluate(
        metric.prepare(animation, np.arange(10)),
        metric.prepare(
            animation, np.arange(animation.nframes - 10, animation.nframes)
        ),
    )
    print(
        "metric %s: %s" % (metric.__name__, ",".join(["%.4f" % f for f in s]))
    )
