from pynimation.anim.animation import Animation
from pynimation.common import data as data_

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)

animation.rightKnee.rotation.eulerDeg.x += 90

PynimationApp().viewer.display(animation)
