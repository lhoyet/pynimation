from pynimation.anim.animation import Animation
from pynimation.inout.fbx.importer import FBXImporter
from pynimation.inout.fbx.exporter import FBXExporter
import pynimation.common.data as data_

from pynimation.viewer.ui import PynimationApp


bipedFbxFile = data_.getDataPath("data/animations/biped.fbx")

animationOrig = Animation.load(bipedFbxFile)

animationFixed = animationOrig.copy()

# the fixBiped method can be called directly
# it reparents the upper body to the root and leaves the
# legs as children of the pelvis
# this fixes the floating joint that can be seen when
# importing the animation
FBXImporter.fixBiped(animationFixed)

# it is also available as an import option
animationFixed = Animation.load(bipedFbxFile, fixBiped=True)

animationRestored = animationFixed.copy()

# the restoreBiped method is used to restore the original hierarchy
FBXExporter.restoreBiped(animationRestored)

# it is also available as an export option
# /!\ this method will modify the animation passed to save
# save(animationFixed.copy(), "bipedFixed.fbx", restoreBiped=True)

PynimationApp().viewer.display(
    animationOrig,
    animationFixed,
    animationRestored,
)
