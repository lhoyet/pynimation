import timeit
import os
from pynimation.inout.importer import load
from pynimation.inout.exporter import save

from pynimation.common import data as data_


def humanSize(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, "Yi", suffix)


def fileSize(filename):
    return humanSize(os.path.getsize(filename))


def timeFormat(format: str, animation, filename="remove_me", saveOptions={}):
    if "file" in saveOptions:
        filename = saveOptions["file"]
        savetime = 0.0
    else:
        filename = filename + "." + format
        savetime = timeit.timeit(
            lambda: save(animation, filename, **saveOptions),
            number=1,
            globals=globals(),
        )
    loadtime = timeit.timeit(
        lambda: load(filename), number=1, globals=globals()
    )
    size = fileSize(filename)
    if "file" not in saveOptions:
        os.remove(filename)
    formatName = format + (
        " " + list(saveOptions.keys())[0] if len(saveOptions) > 0 else ""
    )
    return [
        formatName,
        size,
        loadtime,
        savetime,
    ]


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")
[animation] = load(bvh_file)

header = ["format", "size", "load time", "save time"]
data = [
    timeFormat(f, animation, saveOptions=s)
    for (f, s) in [
        ("fbx", {}),
        ("pynim", {}),
        ("npz", {}),
        ("npz", {"compress": True}),
        ("bvh", {"file": bvh_file}),
    ]
]

print("Number of Frames: %d" % animation.nframes)
print("Number of joints: %d" % animation.skeleton.njoints)
print(
    "\n".join(
        ["{:<15}{:>15}{:>16}{:>16}".format(*header)]
        + ["{:<15}{:>15}{:>15.4f}s{:>15.4f}s".format(*d) for d in data]
    )
)
