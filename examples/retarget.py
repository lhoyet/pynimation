from itertools import cycle
from pynimation.anim.animation import Animation
from pynimation.common import data as data_
from pynimation.viewer.character import Character
from pynimation.viewer.stickfigure import StickFigure

from pynimation.viewer.ui import PynimationApp


bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")

animation = Animation.load(bvh_file)

newSkeleton = animation.skeleton.copy()

# lengthen legs and arms of new skeleton by ratios
ratios = [0.6, 1.2]
limbJoints = [
    i
    for j in [*newSkeleton.legs, newSkeleton.rightArm, newSkeleton.leftArm]
    for i in zip(j.descendants()[:2], ratios)
]
for limbJoint, ratio in limbJoints:
    limbJoint.transform.position *= ratio

# retargeting fails if skeletons have different topologies
# differentSkel = newSkeleton.copy()
# differentSkel.root.addChild(Joint("tail"))

# # retarget animation to new skeleton
animRetarget = animation.copy()
animRetarget.retarget(newSkeleton)

# display original animation on new skeleton without retargeting for comparison
animWoRetarget = animation.copy()
animWoRetarget.skeleton = newSkeleton
animWoRetarget[...] = (
    newSkeleton.basePose
    @ animation.skeleton.basePose.inverse()
    @ animWoRetarget
)

# create, place, add characters
#           red                 green               blue
colors = [[0.9, 0.0, 0.0, 1], [0.0, 0.9, 0.0, 1], [0, 0.0, 0.9, 1]]
animations = [animation, animRetarget, animWoRetarget]
characters = [
    Character(animation, [StickFigure(animation.skeleton, color)])
    for (animation, color) in zip(animations, cycle(colors))
]

characters[0].transform.position.x -= 1

PynimationApp().viewer.display(characters, inLine=False)
