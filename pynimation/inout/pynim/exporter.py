from typing import Any

import pynimation.inout.common as common
from pynimation.inout.exporter import Exporter
from pynimation.inout.exporter import handleSaveOptions
from pynimation.inout.binarywriter import BinaryWriter
from pynimation.anim.animation import Animation
from pynimation.anim.skeleton import Skeleton


@common.io("pynim")
class PynimExporter(Exporter):
    """
    Exporter class for Pynim binary files (internal file format).
    """

    def __init__(self, **options: Any) -> None:
        common.IO.__init__(self, **options)

    @handleSaveOptions
    def save(
        self, animation: Animation, filename: str, **options: Any
    ) -> None:
        """
        Export `animation` to Pynim file `filename`

        Parameters
        ----------
        animation:
            animation to export

        filename:
            path to save the animation to

        options:
            options of the exporter, see ExporterOptions, IOOptions and options
            classes of exporter implementations
        """

        with BinaryWriter(filename) as bn:
            self.saveSkeleton(animation.skeleton, bn)

            # FIXME: add implem when metrics to 2.0 is done
            # bn.writeBool(animation.isCyclic())
            # not needed anymore though, we don't read it anymore
            # just for compatibility
            bn.writeBool(False)
            bn.writeFloat(animation.framerate)
            bn.writeInt(animation.nframes)

            a = animation.copy()
            a[...] = a.skeleton.basePose.inverse() @ a

            for frame in a:
                self.saveFrame(frame, bn)

    @staticmethod
    def saveSkeleton(skeleton: Skeleton, bn: BinaryWriter) -> None:
        """
        Export `skeleton` to binarywriter handle `bn`

        Parameters
        ----------
        skeleton:
            skeleton to export

        bn:
            Binary writer handle

        """

        bn.writeUnsignedShortInt(skeleton.root.id)
        bn.writeUnsignedShortInt(len(skeleton.joints))
        bn.writeShortInt(skeleton.proxyBone.id if skeleton.proxyBone else -1)

        for joint in skeleton.joints:
            bn.writeString(joint.name)
            bn.writeUnsignedShortInt(joint.id)
            if joint.parent is None:
                parent = -1
            else:
                parent = joint.parent.id
            bn.writeShortInt(parent)
            bn.writeVector(joint.transform.position)
            bn.writeVector(joint.transform.rotation.quat.wFirst)

    @staticmethod
    def saveFrame(frame, bn: BinaryWriter) -> None:
        """
        Export `frame` to binarywriter handle `bn`

        Parameters
        ----------
        frame:
            frame to export

        bn:
            Binary writer handle

        """

        for trans in frame:
            bn.writeVector(trans.position)
            bn.writeVector(trans.rotation.quat.wFirst)
