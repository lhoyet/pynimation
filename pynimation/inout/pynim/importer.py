import numpy as np
from typing import Dict, Any, List, TYPE_CHECKING


from pynimation.inout.importer import Importer
from pynimation.inout.binaryreader import BinaryReader
from pynimation.anim.skeleton import Skeleton
from pynimation.anim.joint import Joint
from pynimation.anim.animation import Animation
from pynimation.inout.importer import handleLoadOptions
import pynimation.inout.common as common

if TYPE_CHECKING:
    from pynimation.common.localglobal import LocalGlobal


@common.io("pynim")
class PynimImporter(Importer):
    """
    Importer class for Pynim binary files (internal file format).
    """

    def __init__(self, **options: Any) -> None:
        common.IO.__init__(self, **options)

    @handleLoadOptions
    def load(self, filename: str, **options: Any) -> List[Animation]:
        """
        Load animation from ``.pynim`` file `filename`

        Parameters
        ----------
        filename:
            ``.pynim`` file to Load
        options:
            dict of import options

        Returns
        -------
        List[Animation]:
            animation loaded from `filename`
        """
        with BinaryReader(filename) as bn:
            skeleton = self.loadSkeleton(bn)

            bn.readBool()  # isCyclic
            # animation.isCyclic = isCyclic REMOVED FROM CURRENT ANIMATION IMPLEMENTATION
            # REMOVING THE READBOOL/WRITEBOOL WOULD REQUIRE TO REGENERATE THE PYNIM FILES
            framerate = bn.readFloat()
            frameNumber = bn.readInt()

            animation = Animation.fromGraph(
                skeleton, nframes=frameNumber, framerate=framerate
            )

            for fr in animation:
                self.loadFrame(bn, skeleton, fr)

        animation[...] = skeleton.basePose @ animation

        return [animation]

    @staticmethod
    def loadSkeleton(bn: BinaryReader) -> Skeleton:
        """
        Load the skeleton found in the ``.pynim`` file opened by
        :class:`~pynimation.inout.binarywriter.BinaryWriter` `bn`

        Parameters
        ----------
        bn:
            Binary reader handle

        Returns
        -------
        Skeleton:
            skeleton loaded from opened file of `bn`
        """

        rootJointId = bn.readUnsignedShortInt()
        jointNumber = bn.readUnsignedShortInt()
        proxyId = bn.readShortInt()

        joints: Dict[int, Joint] = {}

        for j in range(jointNumber):
            jointName = bn.readString()
            jointID = bn.readUnsignedShortInt()
            jointParent = bn.readShortInt()
            jointPosition = bn.readVector(3)
            jointQuaternion = bn.readVector(4)

            parent = None if jointParent == -1 else joints[jointParent]
            joint = Joint(jointName, parent=parent)
            joint.transform.position = np.array(jointPosition)
            joint.transform.rotation.quat.wFirst = np.array(jointQuaternion)
            joints[jointID] = joint
            # if jointID == rootJointId:
            #     skel = Skeleton(joints[rootJointId])

        # import pdb

        # pdb.set_trace()

        skel = Skeleton(joints[rootJointId])

        if proxyId >= 0:
            skel.proxyBone = skel.nodes[proxyId]

        return skel

    @staticmethod
    def loadFrame(
        bn: BinaryReader,
        skeleton: Skeleton,
        frame: "LocalGlobal",
    ) -> None:
        """
        Load the next frame found at the current cursor position in
        the ``.pynim`` file opened by :class:`~pynimation.inout.binarywriter.BinaryWriter`
        `bn`

        Parameters
        ----------
        bn:
            Binary reader handle

        skeleton:
            Skeleton representing the frame

        frame:
            if provided, load the data in the frame, otherwise create and return a new frame

        Returns
        -------
        Skeleton:
            skeleton loaded from opened file of `bn`
        """

        for jj in skeleton.joints:
            frame.joint[jj].position = np.array(bn.readVector(3))
            frame.joint[jj].rotation.quat.wFirst = np.array(bn.readVector(4))
