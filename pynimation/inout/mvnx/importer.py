import time
from typing import List, Optional, Any
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element

import numpy as np

from pynimation.anim.skeleton import Skeleton
from pynimation.anim.animation import Animation
from pynimation.anim.joint import Joint
from pynimation.common.transform import Transform
from pynimation.common.vec import Vec3
from pynimation.common.quaternion import Quaternion
from pynimation.inout.importer import Importer
from pynimation.common import data as data_
import pynimation.inout.common as common
from pynimation.inout.importer import handleLoadOptions


@common.io("mvnx")
class MVNXImporter(Importer):
    """
    Import animations from MVNX files
    """

    def __init__(self, **options: Any) -> None:
        common.IO.__init__(self, **options)
        self._clear()
        self._ns = {"mvnx": "http://www.xsens.com/mvn/mvnx"}
        self._skelStructure = ET.parse(
            data_.getDataPath("data/animations/mvnxSkeleton.xml")
        ).getroot()
        self._frameRate: int = 0
        self.numberOfFrames: int = 0

    def _clear(self) -> None:
        self._skeleton: Optional[Skeleton] = None
        self._animation: Animation = None

    @handleLoadOptions
    def load(self, filename: str) -> List["Animation"]:
        self._clear()

        rootMVNX = ET.parse(filename).getroot()

        for subject in rootMVNX.findall("mvnx:subject", self._ns):
            self._loadSubject(subject, self.numberOfFrames)
        return [self._animation]

    def _loadSubject(
        self, subject: Element, numberOfFrames: int = None
    ) -> None:
        frames = subject.find("mvnx:frames", self._ns)
        self._frameRate = int(subject.attrib["frameRate"])
        if frames is None:
            raise Exception(
                "Error while reading MVNX file : could not find frames element"
            )
        identity = frames[0]

        self._skeleton = self._loadSkeleton(identity)

        self._loadAnimation(frames, self._skeleton)
        self._animation[...] = self._skeleton.basePose @ self._animation

    def _loadSkeleton(self, identityFrame: Element) -> Skeleton:
        root = self._skelStructure.find("Joint")
        if root is None:
            raise Exception(
                "Error while reading squeleton file : could not find root joint element"
            )
        self._loadJointMVNX(None, root, None)

        assert self._skeleton is not None

        globTr = self._loadGlobalTransforms(identityFrame)

        for joint in self._skeleton.joints:
            if joint.parent is not None:
                joint.transform.position = (
                    globTr[joint.parent.id].inverse() @ globTr[joint.id]
                ).position

        return self._skeleton

    def _loadGlobalTransforms(self, frame: Element) -> List[Transform]:
        frPos = frame.find("mvnx:position", self._ns)
        frRot = frame.find("mvnx:orientation", self._ns)

        if frPos is None or frRot is None:
            raise Exception(
                "Error while reading MVNX file : could not find rotation or position element"
            )
        elif frPos.text is None or frRot.text is None:
            raise Exception(
                "Error while reading MVNX file : empty rotation or position element"
            )

        r = np.asarray([float(a) for a in (frRot.text.split())])
        p = np.asarray([float(a) for a in (frPos.text.split())])

        rotations = Quaternion(np.split(r, len(r) // 4))
        positions = Vec3(np.split(p, len(p) // 3))

        # SI changement de repère à faire

        t = Transform.identity(positions.shape[:-1])
        t.position = positions.yzx
        t.rotation.quat = rotations.zwyx

        return t

    def _loadAnimation(self, frames: Element, skeleton: Skeleton) -> None:
        nframes = len(frames) - 3
        self._animation = Animation.fromGraph(
            skeleton, nframes=nframes, framerate=self._frameRate
        )
        t = time.perf_counter() * 1000
        for fr in frames:
            if "index" in fr.attrib and fr.attrib["index"]:
                fid = int(fr.attrib["index"])
                globTr = self._loadGlobalTransforms(fr)
                for joint in skeleton.joints:
                    if joint.parent is not None:
                        self._animation[fid, joint.id].rotation = (
                            globTr[joint.parent.id].inverse()
                            @ globTr[joint.id]
                        ).rotation
                    else:
                        self._animation[fid, joint.id] = globTr[joint.id]

        print("Finished: " + str(time.perf_counter() * 1000 - t))

    def _loadJointMVNX(
        self,
        skeleton: Optional[Skeleton],
        currentNode: Element,
        parent: Optional[Joint],
    ):
        joint = Joint(currentNode.attrib["name"])
        if skeleton is None:
            self._skeleton = Skeleton(joint)
            skeleton = self._skeleton
            assert parent is None

        skeleton.add(joint, parent=parent)

        for child in currentNode.findall("Joint"):
            self._loadJointMVNX(skeleton, child, joint)
