from typing import Any, List

import numpy as np

from pynimation.inout.importer import Importer
from pynimation.inout.importer import handleLoadOptions
import pynimation.inout.common as common
from pynimation.anim.joint import Joint
from pynimation.anim.skeleton import Skeleton
from pynimation.anim.animation import Animation


@common.io("npz")
class NpzImporter(Importer):
    """
    Importer class for files saved with :code:`numpy.savez`
    """

    def __init__(self, **options: Any) -> None:
        common.IO.__init__(self, **options)

    @staticmethod
    def _skelFromAdjacency(
        adjacency: np.ndarray, names: List[str], basePose: np.ndarray
    ):
        joints = np.array([Joint(n) for n in names], dtype=Joint)
        for j, a in zip(joints, adjacency):
            j.children = joints[a]

        skel = Skeleton(joints[0])
        skel.basePose[...] = basePose
        return skel

    @handleLoadOptions
    def load(self, filename: str, **options: Any) -> "Animation":
        """
        Load animation from ``.npz`` file `filename`

        Parameters
        ----------
        filename:
            ``.npz`` file to Load

        Returns
        -------
        Animation:
            animation loaded from `filename`
        """
        arr = np.load(filename)
        skel = self._skelFromAdjacency(
            arr["skeleton"], arr["jointNames"], arr["basePose"]
        )
        animation = Animation.fromGraph(
            skeleton=skel,
            nframes=len(arr["rot"]),
            framerate=arr["framerate"][0],
        )
        animation.rotation.quat = arr["rot"]
        animation.position = arr["pos"]
        return [animation]
