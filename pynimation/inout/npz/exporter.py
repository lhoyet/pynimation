from typing import Any, TYPE_CHECKING

import numpy as np

from pynimation.inout.exporter import Exporter
from pynimation.inout.exporter import handleSaveOptions
import pynimation.inout.common as common

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation


@common.io("npz")
class NpzExporter(Exporter):
    """
    Exporter class for animation files saved with numpy.savez
    """

    def __init__(self, **options: Any) -> None:
        common.IO.__init__(self, **options)

    @handleSaveOptions
    def save(
        self, animation: "Animation", filename: str, **options: Any
    ) -> None:
        """
        Load animation from ``.npz`` file `filename`

        Parameters
        ----------
        filename:
            ``.npz`` file to Load

        Returns
        -------
        Animation:
            animation loaded from `filename`
        """
        if "compress" in options and options["compress"]:
            func = np.savez_compressed
        else:
            func = np.savez
        func(
            filename,
            rot=animation.rotation.quat,
            pos=animation.position,
            skeleton=animation.skeleton.adjacency,
            basePose=animation.skeleton.basePose,
            jointNames=[j.name for j in animation.skeleton],
            framerate=[animation.framerate],
        )

    @staticmethod
    @common.ioOption("compress")
    def compress(animation: "Animation", compress: bool):
        pass
