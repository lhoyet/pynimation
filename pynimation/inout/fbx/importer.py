from typing import List, Optional, Any

import fbx
import time

from pynimation.common.transform import Transform
from pynimation.anim.skeleton import Skeleton
from pynimation.anim.animation import Animation
from pynimation.anim.joint import Joint
from pynimation.inout.importer import Importer
import pynimation.inout.common as common
from pynimation.inout.importer import handleLoadOptions


TIME_INFINITY = fbx.FbxTime(0x7FFFFFFFFFFFFFFF)


@common.io("fbx")
class FBXImporter(Importer):
    """
    Importer class for FBX files. Only imports animations. See
    :class:`~pynimation.viewer.inout.fbx.FBXMeshImporter` to import meshes from
    FBX files
    """

    def __init__(self, **options: Any):
        common.IO.__init__(self, **options)
        self.manager = fbx.FbxManager.Create()
        self.scene = fbx.FbxScene.Create(self.manager, "MyScene")
        self.importer = fbx.FbxImporter.Create(self.manager, "MyImporter")
        self.animations: List[Animation] = []
        self.skeletonObjects: List[Skeleton] = []
        self.rootNode = None

    def _loadFile(self, filename: str) -> None:
        status = self.importer.Initialize(filename)

        if not status:
            err = "Error: " + self.importer.GetStatus().GetErrorString()
            raise Exception(err)

        self.importer.Import(self.scene)
        self.importer.Destroy()

        self.rootNode = self.scene.GetRootNode()

        self.systemUnit = self.scene.GetGlobalSettings().GetSystemUnit()
        self.systemUnitScale = self.systemUnit.GetConversionFactorTo(
            fbx.FbxSystemUnit.m
        )

        lTimeMode = self.scene.GetGlobalSettings().GetTimeMode()
        if lTimeMode == fbx.FbxTime.EMode.eCustom:
            self.framerate = int(
                self.scene.GetGlobalSettings().GetCustomFrameRate()
            )
            fbx.FbxTime.SetGlobalTimeMode(lTimeMode, float(self.framerate))
        else:
            self.framerate = int(fbx.FbxTime.GetFrameRate(lTimeMode))
            fbx.FbxTime.SetGlobalTimeMode(lTimeMode)

        self.timespan = (
            self.scene.GetGlobalSettings().GetTimelineDefaultTimeSpan()
        )
        self.startTime = self.timespan.GetStart()
        self.stopTime = self.timespan.GetStop()

    @handleLoadOptions
    def load(self, filename: str, **options: Any) -> List[Animation]:
        """
        Imports animations from FBX file `filename`

        Parameters
        ----------
        filename:
            file to import animations from
        options:
            dict of import options


        Returns
        -------
        Animation
            Imported animations
        """
        if self.rootNode is None:
            self._loadFile(filename)

        self._loadAnimations()
        return self.animations

    def _loadSkeletons(self) -> None:
        if self.skeletonObjects:
            print("Skeleton data already loaded")
            return

        self.skeletonObjects = []
        self.skeletonNodes: Any = {}
        self._parseSceneSkeletons(
            self.rootNode, fbx.FbxNodeAttribute.EType.eSkeleton
        )
        if len(self.skeletonObjects) == 0:
            # we didn't find any skeletons
            # try with nulls (supposedly exports from houdini)
            self._parseSceneSkeletons(
                self.rootNode, fbx.FbxNodeAttribute.EType.eNull
            )
            if len(self.skeletonObjects) == 0:
                raise ValueError("No skeleton found while importing fbx file")

    def _loadAnimations(self) -> None:
        if len(self.animations) > 0:
            print("Animation data already loaded")
            return

        if len(self.skeletonObjects) == 0:  # Need skeletons to be loaded first
            self._loadSkeletons()

        self._parseSceneAnimations()

    def _parseSceneSkeletons(
        self,
        parentNode: fbx.FbxNode,
        nodeType=fbx.FbxNodeAttribute.EType.eSkeleton,
        skeleton: Optional[Skeleton] = None,
        parentJoint: Optional[fbx.FbxNode] = None,
    ) -> None:
        for i in range(0, parentNode.GetChildCount()):
            node = parentNode.GetChild(i)
            e = FBXImporter._getFbxNodeType(node)
            if nodeType == e:
                name = node.GetName()
                skel = skeleton
                joint: Joint = Joint(name)
                if skel is None:
                    skel = Skeleton(joint)
                    self.skeletonObjects.append(skel)
                    self.skeletonNodes[skel] = [[joint, node]]
                    t = self._getNodeGlobalTransform(node)
                else:
                    t = self._getNodeLocalTransform(node)
                    skel.add(joint, parent=parentJoint)
                    self.skeletonNodes[skel].append([joint, node])
                joint.transform = t
                self._parseSceneSkeletons(node, nodeType, skel, joint)
            else:
                self._parseSceneSkeletons(node, nodeType)

    def _parseSceneAnimations(self) -> None:
        t0 = time.perf_counter()

        nbSkeletons = len(self.skeletonNodes.keys())
        skelid = 1
        for skel in self.skeletonNodes.keys():
            rootNode = self.skeletonNodes[skel][0][1]
            timeSpan = fbx.FbxTimeSpan()

            if rootNode.GetAnimationInterval(timeSpan):
                animationLenght = timeSpan.GetDuration().GetFrameCount(
                    self.scene.GetGlobalSettings().GetTimeMode()
                )
                timeStart = timeSpan.GetStart().GetSecondDouble()
                timeEnd = timeSpan.GetStop().GetSecondDouble()
            else:
                timeStart = 0
                timeEnd = self.stopTime.GetSecondDouble()
                animationLenght = (int)(
                    (timeEnd - timeStart) * self.framerate + 1
                )

            anim = Animation.fromGraph(
                skel, nframes=animationLenght + 1, framerate=self.framerate
            )
            fbxtime = fbx.FbxTime()

            for i in range(animationLenght + 1):
                t = min(timeStart + i / self.framerate, timeEnd)
                fbxtime.SetSecondDouble(t)

                print(
                    "\rFbxImporter - Loading Skeleton {}/{}: frame {}/{} ({}%)".format(
                        skelid,
                        nbSkeletons,
                        i,
                        animationLenght + 1,
                        int(float(i) / float(animationLenght + 1) * 100),
                    ),
                    end="",
                )

                for j in self.skeletonNodes[skel]:
                    anim[i, j[0].id] = self._getNodeLocalTransform(
                        j[1], fbxtime
                    )

            self.animations.append(anim)
            skelid += 1

        print("FbxImporter - Finished: " + str(time.perf_counter() - t0))

    @staticmethod
    def _printNodeNames(self, node: fbx.FbxNode, ind: int = 0) -> None:
        s = FBXImporter._getFbxNodeTypeName(node) + ": " + node.GetName()
        for i in range(0, ind):
            s = "  " + s
        print(s)
        for i in range(0, node.GetChildCount()):
            n = node.GetChild(i)
            self._printNodeNames(n, ind + 1)

    @staticmethod
    def _getFbxNodeType(node: fbx.FbxNode) -> Optional[fbx.FbxNodeAttribute]:
        attribType = node.GetNodeAttribute()
        if attribType is None:
            return None
        return attribType.GetAttributeType()

    @staticmethod
    def _getFbxNodeTypeName(node: fbx.FbxNode) -> str:
        e = FBXImporter._getFbxNodeType(node)
        sType = "Unknown"
        if fbx.FbxNodeAttribute.EType.eNull == e:
            sType = "Null"
        elif fbx.FbxNodeAttribute.EType.eMarker == e:
            sType = "Marker"
        elif fbx.FbxNodeAttribute.EType.eSkeleton == e:
            sType = "Skeleton"
        elif fbx.FbxNodeAttribute.EType.eMesh == e:
            sType = "Mesh"
        elif fbx.FbxNodeAttribute.EType.eNurbs == e:
            sType = "Nurbs"
        elif fbx.FbxNodeAttribute.EType.ePatch == e:
            sType = "Patch"
        elif fbx.FbxNodeAttribute.EType.eCamera == e:
            sType = "Camera"
        elif fbx.FbxNodeAttribute.EType.eLight == e:
            sType = "Light"
        return sType

    def _getNodeLocalTransform(
        self, node: fbx.FbxNode, time: fbx.FbxTime = None
    ) -> Transform:
        t = Transform()
        args = [] if time is None else [time]
        matrix = node.EvaluateLocalTransform(*args)
        for i in range(0, 4):
            for j in range(0, 4):
                t[i, j] = matrix[j][i]

        t[0:3, 3] *= self.systemUnitScale
        return t

    def _getNodeGlobalTransform(
        self, node: fbx.FbxNode, time: fbx.FbxTime = None
    ) -> Transform:
        t = Transform()
        args = [] if time is None else [time]
        matrix = node.EvaluateGlobalTransform(*args)
        for i in range(0, 4):
            for j in range(0, 4):
                t[i, j] = matrix[j][i]

        t[0:3, 3] *= self.systemUnitScale
        return t

    @staticmethod
    @common.ioOption("fixBiped")
    def fixBiped(animation: Animation, boolean: bool = True):
        if not boolean:
            return
        # g2 = j0 * l0 * j1 * l1 * j2 * l2
        # j2' = j1 * j2
        # l2' = (j1 * j2)^-1 * j1 * l1 * j2 * l2
        # g2 = j0 * l0 * j2' * l2'

        # FIXME ensure always called other ways
        if not hasattr(animation.skeleton, "SkeletalPartSwitcher"):
            animation.skeleton.mapHumanoidSkeleton()

        skel = animation.skeleton

        j1 = skel[1]
        j2 = skel.upperBody

        j2.transform = j1.transform @ j2.transform

        animation.joint[j2] = animation.joint[j1] @ animation.joint[j2]

        skel.root.addChild(j2)
