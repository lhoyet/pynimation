import abc
import functools
from typing import TYPE_CHECKING, Any, List

from .common import IO, _getExtension, _ioFactory, io

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation


def handleLoadOptions(load):
    @functools.wraps(load)
    def wrapper(self, filename, **options):
        self.checkOptions(options)
        animations = load(self, filename, **options)
        self._handleOptions(animations, options)
        return animations

    return wrapper


@io(None)
class Importer(abc.ABC, IO):
    """
    Base animation importer class. Subclasses each handle a particular
    animation file format
    """

    @abc.abstractmethod
    def load(self, filename: str, **options: Any) -> List["Animation"]:
        """
        Abstract animation load method.

        Overriding methods must return :class:`~pynimation.anim.Animation`
        objects obtained from `filename`

        Parameters
        ----------
        filename:
            file to import animations from
        options:
            options of the importer, see ImporterOptions, IOOptions and options
            classes of importer implementations

        Returns
        -------
        List[Animation]
            animations present in `filename`

        """
        pass


_importerFactory = functools.partial(_ioFactory, Importer)


def load(filename: str, **options: Any) -> List["Animation"]:
    """
    Generic animation import method.

    Loads animations from `filename`.

    The format of the imported file will be determined by the extension of
    `filename`


    Parameters
    ----------
    filename:
        path of the file to load animations from
    options:
        options of the importer, see ImporterOptions, IOOptions and options
        classes of importer implementations

    Returns
    -------
    List[Animation]
        animations loaded from `filename`
    """
    extension = _getExtension(filename)
    importer = _ioFactory(Importer, extension, **options)
    animations = importer.load(filename, **options)
    return animations
