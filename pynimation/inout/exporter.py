import abc
import functools
from typing import TYPE_CHECKING, Any

from .common import IO, _getExtension, _ioFactory, io

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation


def handleSaveOptions(save):
    @functools.wraps(save)
    def wrapper(self, animation, filename, **options):
        self.checkOptions(options)
        self._handleOptions([animation], options)
        save(self, animation, filename, **options)

    return wrapper


@io(None)
class Exporter(abc.ABC, IO):
    """
    Base animation exporter class. Subclasses each handle a particular
    animation file format
    """

    @abc.abstractmethod
    def save(
        self, animation: "Animation", filename: str, **options: Any
    ) -> None:
        """
        Abstract save method

        Overidding methods should save `animation` to `filename`

        Parameters
        ----------
        animation:
            animation to save
        filename:
            path to save the animation to
        options:
            options of the exporter, see ExporterOptions, IOOptions and options
            classes of exporter implementations
        """
        pass


_exporterFactory = functools.partial(_ioFactory, Exporter)


def save(animation: "Animation", filename: str, **options: Any) -> None:
    """
    Generic animation export method.

    Saves `animation` to `filename`.

    The format of the saved file will be determined by the extension of
    `filename`


    Parameters
    ----------
    animation:
        animation to save
    filename:
        path of the file to save the animation to
    options:
        options of the exporter, see ExporterOptions, IOOptions and options
        classes of exporter implementations
    """
    extension = _getExtension(filename)
    exporter = _ioFactory(Exporter, extension, **options)
    exporter.save(animation, filename, **options)
