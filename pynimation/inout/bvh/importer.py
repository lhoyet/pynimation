import time
from typing import Any, Dict, List, Optional, TextIO

import numpy as np

import pynimation.inout.common as common
from pynimation.anim.animation import Animation
from pynimation.anim.joint import Joint
from pynimation.anim.skeleton import Skeleton
from pynimation.common.rotation import Rotation
from pynimation.inout.importer import Importer, handleLoadOptions

scale = 0.01


@common.io("bvh")
class BVHImporter(Importer):
    """
    Import animations from BVH files
    """

    def __init__(self, **options: Any) -> None:
        common.IO.__init__(self, **options)
        self.clear()

    def clear(self) -> None:
        """ """
        self.skeleton: Optional[Skeleton] = None
        self.animation: Optional[Animation] = None
        self.frameRate: Optional[int] = None
        self.channelPositions: Dict["Joint", str] = dict()
        self.channelRotations: Dict["Joint", str] = dict()

    @handleLoadOptions
    def load(self, filename: str, **options: Any) -> List["Animation"]:
        self.clear()

        file_: TextIO = open(filename, "r")
        self.__skipBlankLines(file_)
        line = file_.readline()
        if "HIERARCHY" not in line:
            raise Exception('expected "HIERARCHY", got ' + line)

        line = file_.readline()
        tokens = line.split()
        if tokens[0] == "ROOT":
            jointname = tokens[1]

        self.__loadJoint(file_, jointname, None)

        self.__skipBlankLines(file_)

        line = file_.readline()
        if "MOTION" not in line:
            raise Exception('expected "MOTION", got ' + line)

        self.__loadAnimation(file_, None)

        file_.close()
        assert self.animation is not None
        return [self.animation]

    # TODO: handle joint rotations
    def __loadJoint(
        self, file: TextIO, jointName: str, parent: Optional["Joint"]
    ) -> None:
        joint = Joint(jointName, parent)
        if self.skeleton is None:
            self.skeleton = Skeleton(joint)
        else:
            self.skeleton.add(joint, parent=parent)

        line = file.readline()
        tokens = line.split()

        while tokens[0] != "}":
            line = file.readline()
            tokens = line.split()

            if tokens[0] in ["ROOT", "JOINT", "End"]:
                childName = tokens[1]
                self.__loadJoint(file, childName, joint)

            elif tokens[0] == "OFFSET":
                joint.transform.position = [
                    float(tokens[1]) * scale,
                    float(tokens[2]) * scale,
                    float(tokens[3]) * scale,
                ]
            elif tokens[0] == "CHANNELS":
                if int(tokens[1]) == 6:
                    self.channelPositions[joint] = (
                        tokens[2][0] + tokens[3][0] + tokens[4][0]
                    ).lower()
                    self.channelRotations[joint] = (
                        tokens[5][0] + tokens[6][0] + tokens[7][0]
                    )
                elif int(tokens[1]) == 3:
                    self.channelRotations[joint] = (
                        tokens[2][0] + tokens[3][0] + tokens[4][0]
                    )

    def __skipBlankLines(self, file: TextIO) -> None:
        if file is None:
            return
        line = ""
        while not line.strip():
            pos = file.tell()
            line = file.readline()
        file.seek(pos)

    def __loadAnimation(
        self, file: TextIO, numberOfFrames=Optional[int]
    ) -> None:
        self.__skipBlankLines(file)
        line = file.readline()
        tokens = line.split()
        if tokens[0] != "Frames:":
            raise Exception("Missing frame number in BVH file format")
        frameNumber = int(tokens[1])

        self.__skipBlankLines(file)
        line = file.readline()
        tokens = line.split()
        if (tokens[0] != "Frame") and (tokens[1] != "Time:"):
            raise Exception("Missing frame time in BVH file format")
        frameTime = float(tokens[2])
        self.frameRate = int(round(1.0 / frameTime))

        self.__skipBlankLines(file)
        if numberOfFrames is not None:
            frameNumber = numberOfFrames
        # self.nextNLines = list(islice(file, frameNumber))

        assert self.skeleton is not None
        # self.animation = Animation(self.frameRate, self.skeleton, frameNumber)
        self.animation = Animation.fromGraph(
            self.skeleton, nframes=frameNumber, framerate=self.frameRate
        )
        t = time.perf_counter() * 1000
        fdata = np.loadtxt(file)
        if len(fdata) != 0:
            fdata = fdata.reshape((fdata.shape[0], -1, 3))
            i = 0
            for j in self.skeleton:
                if j in self.channelPositions:
                    self.channelPositions[j]
                    setattr(
                        self.animation[:, j.id].position,
                        self.channelPositions[j],
                        fdata[:, i] * scale,
                    )
                    i += 1

                if j in self.channelRotations:
                    self.animation[:, j.id].rotation = Rotation.fromEuler(
                        fdata[:, i],
                        order=self.channelRotations[j],
                        degrees=True,
                    )
                    i += 1
                self.animation[:, j.id].position += j.transform.position

        print("Finished: " + str(time.perf_counter() * 1000 - t))
