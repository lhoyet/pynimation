import os
from typing import TYPE_CHECKING, Any, Dict, List, Type

from pynimation.common.meta import _get_classes_rec

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton


def io(extension):
    def wrapper(cls):
        if extension is not None:
            cls.extension = extension

            # register as IO implementation
            baseClasses = cls.__bases__
            if len(baseClasses) > 0:
                baseClass = baseClasses[0]
                if baseClass not in IO.implementations:
                    IO.implementations[baseClass] = {}

                IO.implementations[baseClass][extension] = cls

        # set supportedOptions
        cls.supportedOptions = {}
        for methodname in dir(cls):
            method = getattr(cls, methodname)
            if hasattr(method, "_option"):
                cls.supportedOptions[method._option] = method
        return cls

    return wrapper


def ioOption(optionName):
    def wrapper(func):
        func._option = optionName
        return func

    return wrapper


@io(None)
class IO:
    implementations: Dict[Type["IO"], Dict[str, Type["IO"]]] = {}

    def __init__(self, **options: Any):
        self.options = options

    def _handleOptions(
        self, animations: List["Animation"], moreOptions: Dict[str, Any] = None
    ):
        if moreOptions is None:
            tempopts = self.options
        else:
            tempopts = self.options.copy()
            tempopts.update(moreOptions)
        for o in tempopts:
            for animation in animations:
                self.__class__.supportedOptions[o](animation, tempopts[o])  # type: ignore

    @property
    def options(self) -> Dict[str, Any]:
        return self._options

    @options.setter
    def options(self, newoptions: Dict[str, Any]) -> None:
        self.checkOptions(newoptions)
        self._options = newoptions

    def checkOptions(self, options: Dict[str, Any]):
        """
        Checks if `options` are supported by this IO implementation

        Parameters
        ----------
        options:
            options to check

        Raises
        ------
        NotImplementedError
            if some options are not supported by the implementation
        """
        notSupported = set(options.keys()) - set(
            self.__class__.supportedOptions.keys()  # type: ignore
        )
        if len(notSupported) > 0:
            raise NotImplementedError(
                "No implementation found for options: "
                + ",".join(notSupported)
            )

    @staticmethod
    @ioOption("basePose")
    def basePoseOption(
        animation: "Animation", newskeleton: "Skeleton"
    ) -> None:
        animation.setSkeletonBasePose(newskeleton)


def _ioFactory(ioClass: Any, extension: str, **options) -> Any:
    """

    Parameters
    ----------
    ioClass: typing.Any :

    extension: str :


    Returns
    -------

    """
    m = ioClass.__module__
    m = m[: -m[::-1].index(".") - 1]

    # imports all classes found in pynimation/io/*
    _get_classes_rec(m, [])

    if (
        ioClass in IO.implementations
        and extension in IO.implementations[ioClass]
    ):
        return IO.implementations[ioClass][extension](**options)
    else:
        raise NotImplementedError(
            "No "
            + ioClass.__name__
            + "implementation was found for file extension "
            + extension
        )


def _getExtension(filename: str) -> str:
    """

    Parameters
    ----------
    filename: str :


    Returns
    -------

    """
    _, basename = os.path.splitext(filename)
    return basename.replace(".", "").lower()
