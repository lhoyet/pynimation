import re
from enum import Enum
from typing import Any, List, Optional, Tuple, Union, Dict

from pynimation.common.transform_graph import TransformGraph

from .joint import Joint, JointLike

enumId = 0


def _auto() -> int:
    global enumId
    enumId += 1
    return enumId


class Skeleton(TransformGraph[JointLike]):
    """
    Internal representation of an animation's skeleton. The skeleton is made of
    joints, one for each animated limb

    It is possible to map a skeleton to a standard humanoid skeleton. This
    mapping is based on joint names. For example if a joint is named
    :code:`"Mixamp_Right_Hand"`, the algorithm will detect that it corresponds to the
    right hand of the standard humanoid skeleton. This joint can then be
    accessed directly with its generic name :code:`skeleton.rightHand`

    The standard humanoid skeleton is described by the enum
    :class:`~pynimation.anim.skeleton.Skeleton.SkeletalPart`.
    Names of members of this enum are available generic joint names,
    with the underscore removed, and written in camelCase (e.g.
    :code:`RIGHT_CLAVICLE` becomes :code:`rightClavicle`)

    All available generic joint name can be obtained with
    :attr:`~pynimation.anim.skeleton.Skeleton.partsNames`

    Additionaly, the skeleton has a base pose, consisting of a transform for
    each joint, that indicates the default pose of the skeleton. These
    transforms are handled with
    :attr:`~pynimation.common.transform_graph.TransformGraph.transforms`, and
    are accessible on joints directly, whether they are part of a skeleton or
    not (:code:`skeleton.joints[2].transform == skeleton.basePose[2]`)


    Examples
    --------
    The string representation of a skeleton is similar to
    that of a graph, with generic joint names appended in parentheses:

    >>> import pynimation.common.data as data_
    >>> from pynimation.anim.animation import Animation
    >>> import numpy as np
    >>>
    >>> bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")
    >>>
    >>> skeleton = Animation.load(bvh_file).skeleton
    Finished: 25.414595000213012
    >>>
    >>> skeleton
    [0]Hips (root)
        [1]Chest (upperBody)
            [2]Chest2
                [3]Chest3
                    [4]Neck (neck, headBranch)
                        [5]Head (head)
                            [6]Site
       <BLANKLINE>
                    [7]RightCollar (rightClavicle, rightArm)
                        [8]RightShoulder (rightShoulder)
                            [9]RightElbow (rightElbow)
                                [10]RightWrist (rightWrist)
                                    [11]RightFinger0
                                        [12]Site
       <BLANKLINE>
                    [13]LeftCollar (leftClavicle, leftArm)
                        [14]LeftShoulder (leftShoulder)
                            [15]LeftElbow (leftElbow)
                                [16]LeftWrist (leftWrist)
                                    [17]LeftFinger0
                                        [18]Site
       <BLANKLINE>
        [19]RightHip (rightHip, rightLeg)
            [20]RightKnee (rightKnee)
                [21]RightAnkle (rightAnkle)
                    [22]RightToe (rightToe)
                        [23]Site
       <BLANKLINE>
        [24]LeftHip (leftHip, leftLeg)
            [25]LeftKnee (leftKnee)
                [26]LeftAnkle (leftAnkle)
                    [27]LeftToe (leftToe)
                        [28]Site

    However this representation does not include symmetric part names:

    >>> skeleton.legs == (skeleton.rightLeg, skeleton.leftLeg)
    True

    The :attr:`~pynimation.common.graph.Graph.nodes` is also accessible with
    :attr:`~pynimation.anim.skeleton.Skeleton.joints`

    >>> skeleton.nodes[3] is skeleton.joints[3]
    True

    The base pose is an array of transform, one per joint:

    >>> skeleton.basePose.shape
    (29, 4, 4)

    Joint transforms can also be accessed on joints directly

    >>> np.all(skeleton.joints[2].transform == skeleton.basePose[2])
    IndivLocalGlobal(True)
    """

    class SkeletalPart(Enum):
        """
        Parts of the standard humanoid skeleton

        Used to describe a parts of a skeleton in a generic way

        Each part name (as camelCase with no underscore) can be used to access
        the part directly on the skeleton

        Additional generic joint names are added as aliases and pairs of other
        generic joints
        """

        PROXY_BONE = _auto()
        HEAD = _auto()
        NECK = _auto()
        LEFT_CLAVICLE = _auto()
        LEFT_SHOULDER = _auto()
        LEFT_ELBOW = _auto()
        LEFT_WRIST = _auto()
        RIGHT_CLAVICLE = _auto()
        RIGHT_SHOULDER = _auto()
        RIGHT_ELBOW = _auto()
        RIGHT_WRIST = _auto()
        ROOT = _auto()
        LEFT_HIP = _auto()
        LEFT_KNEE = _auto()
        LEFT_ANKLE = _auto()
        LEFT_TOE = _auto()
        RIGHT_HIP = _auto()
        RIGHT_KNEE = _auto()
        RIGHT_ANKLE = _auto()
        RIGHT_TOE = _auto()

        @classmethod
        def defaultSkeleton(cls) -> List["Skeleton.SkeletalPart"]:
            return list(cls)

        @classmethod
        def _enum2PartName(cls):
            return {
                a: "".join(
                    [a._name_.split("_")[0].lower()]
                    + [b.capitalize() for b in a._name_.split("_")[1:]]
                )
                for a in list(cls)
            }

        @classmethod
        def _genericPartsNames(cls):
            return [
                "upperBody",
                "leftLeg",
                "rightLeg",
                "leftArm",
                "rightArm",
                "headBranch",
            ]

        @classmethod
        def _symmetricPartsNames(cls):
            return [
                "legs",
                "hips",
                "knees",
                "ankles",
                "toes",
                "clavicles",
                "shoulders",
                "elbows",
                "wrists",
            ]

    _symmetricPartsNames = SkeletalPart._symmetricPartsNames()
    _genericPartsNames = SkeletalPart._genericPartsNames()
    _enum2PartName = SkeletalPart._enum2PartName()
    _partsNames = list(_enum2PartName.values())
    partsNames = _partsNames + _genericPartsNames + _symmetricPartsNames
    _leftJointRegexes = ["Left", "L[-_:;# ]", "^L"]
    _rightJointRegexes = ["Right", "R[-_:;# ]", "^R"]

    def __init__(self, root: JointLike) -> None:
        """
        Parameters
        ----------
        root:
            root joint of the skeleton
        """
        self.root: JointLike
        super().__init__(root)
        self._jointNamePrefix = ""

    def __getattr__(self, name: str) -> Any:
        # is called whenever self.name doesn't exist
        if name in Skeleton.partsNames or name in [
            "SkeletalPartSwitcher",
            "partSkeletalSwitcher",
        ]:
            self.mapHumanoidSkeleton()
            return getattr(self, name)
        else:
            raise AttributeError("attribute " + name + " not found")

    @property
    def joints(self) -> Tuple[JointLike, ...]:
        """
        Joints of the skeleton
        """
        return self.nodes

    @property
    def jointsNames(self) -> Tuple[str, ...]:
        """
        Set of joint names
        """
        return tuple(a.name for a in self.nodes)

    @property
    def njoints(self) -> int:
        """
        number of joints of the skeleton (equivalent to :code:`len(skeleton)`
        """
        return len(self._nodes)

    def _initSkeletalParts(self) -> None:
        val: Optional[JointLike] = None
        for a in Skeleton._genericPartsNames + Skeleton._partsNames:
            setattr(self, a, val)

        tup: Tuple[JointLike, ...] = ()
        for b in Skeleton._symmetricPartsNames:
            setattr(self, b, tup)

    def _findJointNamePrefix(self) -> str:
        # finds common prefix of joint names if any
        names = [a.name for a in self.joints]
        return next(
            (
                names[0][: i - 1]
                for i in range(1, len(names[0]))
                for c in names
                if names[0][:i] != c[:i]
            ),
            "",
        )

    def _verifyHumanoidMap(self) -> None:
        missingParts = set(Skeleton._genericPartsNames) - set(
            [
                a
                for a in Skeleton._genericPartsNames
                if getattr(self, a) is not None
            ]
        )

        if len(self.legs) < 2 or len(self.arms) < 2 or len(missingParts) > 1:
            raise ValueError(
                "Could not map humanoid skeleton, following parts are missing: "
                + ",".join(missingParts)
            )

    def getPartFromJoint(
        self, joint: Joint
    ) -> Optional["Skeleton.SkeletalPart"]:
        """
        Get the :class:`~pynimation.anim.skeleton.Skeleton.SkeletalPart`
        associated with :attr:`joint`

        Parameters
        ----------
        joint:
            joint to get the part of

        Returns
        -------
        Optional["Skeleton.SkeletalPart"]:
            The part associated with :attr:`joint`, :code:`None` if no part is
            associated with it
        """
        return next(
            (
                a
                for a in Skeleton.SkeletalPart
                if a in self.SkeletalPartSwitcher
                and self.SkeletalPartSwitcher[a] == joint
            ),
            None,
        )

    def mapHumanoidSkeleton(self) -> None:
        """
        Compute mapping between this skeleton and the standard humanoid
        skeleton described by :class:`~pynimation.anim.skeleton.Skeleton.SkeletalPart`

        Uses joint names to compute this mapping

        This function is called automatically when a humanoid joint is accessed
        (e.g. :code:`skeleton.rightWrist`), and the mapping has not been
        computed already
        """
        # Case where it is not a triangle pelvis (i.e., hips are attached to the first spine node)
        rootBranches: Tuple[JointLike, ...] = self.root.children
        while len(rootBranches) == 1:
            rootBranches = rootBranches[0].children

        self._initSkeletalParts()

        if "Proxy" in self.root.name:
            #: proxy bone of the skeleton if it has one
            self.proxyBone: Optional[JointLike] = self.root

        self._jointNamePrefix = self._findJointNamePrefix()
        self._parseLegsAndUpperBody(rootBranches)
        self._verifyHumanoidMap()

        self.SkeletalPartSwitcher = {
            part: getattr(self, partName)
            for (part, partName) in Skeleton._enum2PartName.items()
        }

        self.partSkeletalSwitcher: Dict[int, List[str]] = {}

        for n in self.partsNames:
            j = getattr(self, n)
            if j is not None and isinstance(j, Joint):
                self.partSkeletalSwitcher[
                    j.id
                ] = self.partSkeletalSwitcher.get(j.id, []) + [n]

    def _parseLegsAndUpperBody(
        self, rootBranches: Tuple[JointLike, ...]
    ) -> None:
        unusedBranches: List[JointLike] = []
        for branch in rootBranches:
            if (
                self.upperBody is None
                and self._searchNamesInBranch(
                    [
                        "spine",
                        "shoulder",
                        "clavicle",
                        "elbow",
                        "wrist",
                        "neck",
                        "head",
                    ],
                    branch,
                )
                is not None
            ):
                #: first spine joint above root joint
                self.upperBody: JointLike = branch
            elif (
                self._searchNamesInBranch(
                    ["leg", "hip", "knee", "ankle", "foot"], branch
                )
                is not None
            ):
                #: first joints of both legs
                self.legs: Tuple[JointLike, ...] = (*self.legs, branch)
            else:
                unusedBranches.append(branch)

        if len(self.legs) > 0:
            # handle fixed biped hierarchy ({root: {upperbody, pelvis:{legs}}}
            while len(self.legs) == 1:
                self.legs = self.legs[0].children

            self._parseLowerBody()

        if self.upperBody is not None:
            self._parseUpperBody()

    @staticmethod
    def _emptyifNone(
        tup: Tuple[Optional[JointLike], Optional[JointLike]]
    ) -> Tuple[JointLike, ...]:
        a: Tuple[JointLike, ...] = ()
        # NEEDED TO MAKE MYPY HAPPY
        if None not in tup:
            a = tuple([b for b in tup if b is not None])
        return a

    def _parseLowerBody(self) -> None:
        for b in self.legs:
            if self.leftLeg is None:
                if (
                    self._searchNamesInBranch(Skeleton._leftJointRegexes, b)
                    is not None
                ):
                    #: left leg joint
                    self.leftLeg: JointLike = b
                    continue
            if self.rightLeg is None:
                if (
                    self._searchNamesInBranch(Skeleton._rightJointRegexes, b)
                    is not None
                ):
                    #: right leg joint
                    self.rightLeg: JointLike = b
                    continue

        if self.rightLeg is None or self.leftLeg is None:
            return  # Not handled yet, look at checking for left/right ortientation of torso?

        #: left hip joint
        self.leftHip: Optional[JointLike] = self.leftLeg
        #: left knee joint
        self.leftKnee: Optional[JointLike] = self._searchNamesInBranch(
            "Knee", self.leftLeg.children[0]
        )
        #: left ankle joint
        self.leftAnkle: Optional[JointLike] = self._searchNamesInBranch(
            ["Ankle", "Foot"], self.leftLeg.children[0]
        )
        #: left toe joint
        self.leftToe: Optional[JointLike] = self._searchNamesInBranch(
            "Toe", self.leftLeg.children[0]
        )

        if self.leftKnee is None:
            if self.leftAnkle is not None:
                self.leftKnee = self.leftAnkle.parent
            else:
                j = self._searchNamesInBranch("Leg", self.leftLeg.children[0])
                if j is not None:
                    self.leftKnee = j
                else:
                    self.leftKnee = self.leftHip.children[0]

        #: right hip joint
        self.rightHip: Optional[JointLike] = self.rightLeg
        #: right knee joint
        self.rightKnee: Optional[JointLike] = self._searchNamesInBranch(
            "Knee", self.rightLeg.children[0]
        )
        #: right ankle joint
        self.rightAnkle: Optional[JointLike] = self._searchNamesInBranch(
            ["Ankle", "Foot"], self.rightLeg.children[0]
        )
        #: right toe joint
        self.rightToe: Optional[JointLike] = self._searchNamesInBranch(
            "Toe", self.rightLeg.children[0]
        )

        if self.rightKnee is None:
            if self.rightAnkle is not None:
                self.rightKnee = self.rightAnkle.parent
            else:
                j = self._searchNamesInBranch("Leg", self.rightLeg.children[0])
                if j is not None:
                    self.rightKnee = j
                else:
                    self.rightKnee = self.rightHip.children[0]

        #: left and right hips
        self.hips: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftHip, self.rightHip)
        )
        #: left and right knees
        self.knees: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftKnee, self.rightKnee)
        )
        #: left and right ankles
        self.ankles: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftAnkle, self.rightAnkle)
        )
        #: left and right toes
        self.toes: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftToe, self.rightToe)
        )

    def _parseUpperBody(self) -> None:
        assert self.upperBody is not None
        uppBodyChildren = [j for j in self.upperBody.children]

        branches = []
        while len(uppBodyChildren) > 0:
            if (
                self._searchNamesInBranch(
                    Skeleton._leftJointRegexes, uppBodyChildren[0]
                )
                is not None
                and self._searchNamesInBranch(
                    Skeleton._rightJointRegexes, uppBodyChildren[0]
                )
                is not None
            ):
                for j in uppBodyChildren[0].children:
                    uppBodyChildren.append(j)
            else:
                branches.append(uppBodyChildren[0])
            uppBodyChildren.pop(0)

        unusedBranches = []
        for b in branches:
            if self.headBranch is None:
                if self._searchNamesInBranch(["Neck", "Head"], b) is not None:
                    self.headBranch: JointLike = b
                    continue
            if self.leftArm is None:
                if (
                    self._searchNamesInBranch(Skeleton._leftJointRegexes, b)
                    is not None
                ):
                    #: left arm joint
                    self.leftArm: JointLike = b
                    continue
            if self.rightArm is None:
                if (
                    self._searchNamesInBranch(Skeleton._rightJointRegexes, b)
                    is not None
                ):
                    #: right arm joint
                    self.rightArm: JointLike = b
                    continue
            unusedBranches.append(b)

        if self.headBranch is not None:
            #:  neck joint
            self.neck = self._searchNamesInBranch("Neck", self.headBranch)
            #:  head joint
            self.head = self._searchNamesInBranch("Head", self.headBranch)

        if self.leftArm is not None:
            #:  left clavicle joint
            self.leftClavicle: Optional[JointLike] = self._searchNamesInBranch(
                ["Clavicle", "Collar"], self.leftArm
            )
            #:  left shoulder joint
            self.leftShoulder: Optional[JointLike] = self._searchNamesInBranch(
                ["Arm", "Shoulder"], self.leftArm
            )
            #:  left elbow joint
            self.leftElbow: Optional[JointLike] = self._searchNamesInBranch(
                ["Elbow", "Forearm"], self.leftArm
            )
            #:  left wrist joint
            self.leftWrist: Optional[JointLike] = self._searchNamesInBranch(
                ["Wrist", "Hand"], self.leftArm
            )

            if (
                self.leftElbow is not None
                and self.leftElbow.parent is not self.leftShoulder
            ):
                self.leftClavicle = self.leftShoulder
                self.leftShoulder = self.leftElbow.parent

        if self.rightArm is not None:
            #:  right clavicle joint
            self.rightClavicle: Optional[
                JointLike
            ] = self._searchNamesInBranch(
                ["Clavicle", "Collar"], self.rightArm
            )
            #:  right shoulder joint
            self.rightShoulder: Optional[
                JointLike
            ] = self._searchNamesInBranch(["Arm", "Shoulder"], self.rightArm)
            #:  right elbow joint
            self.rightElbow: Optional[JointLike] = self._searchNamesInBranch(
                ["Elbow", "Forearm"], self.rightArm
            )
            #:  right wrist joint
            self.rightWrist: Optional[JointLike] = self._searchNamesInBranch(
                ["Wrist", "Hand"], self.rightArm
            )

            if (
                self.rightElbow is not None
                and self.rightElbow.parent is not self.rightShoulder
            ):
                self.rightClavicle = self.rightShoulder
                self.rightShoulder = self.rightElbow.parent

        #:  left and right arm joints
        self.arms: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftArm, self.rightArm)
        )
        #:  left and right shoulder joints
        self.shoulders: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftShoulder, self.rightShoulder)
        )
        #:  left and right clavicle joints
        self.clavicles: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftClavicle, self.rightClavicle)
        )
        #:  left and right elbow joints
        self.elbows: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftElbow, self.rightElbow)
        )
        #:  left and right wrists joints
        self.wrists: Tuple[JointLike, ...] = Skeleton._emptyifNone(
            (self.leftWrist, self.rightWrist)
        )

    def _searchNamesInBranch(
        self, names: Union[List[str], str], joint: JointLike
    ) -> Optional[JointLike]:
        name = joint.name[len(self._jointNamePrefix) :]
        if isinstance(names, list):
            for n in names:
                if re.search(n, name, re.IGNORECASE):
                    return joint
        elif isinstance(names, str):
            if re.search(names, name, re.IGNORECASE):
                return joint

        for c in joint.children:
            j = self._searchNamesInBranch(names, c)
            if j is not None:
                return j
        return None

    def getSkeletalPart(
        self, sp: "Skeleton.SkeletalPart"
    ) -> Optional[JointLike]:
        """
        Get the joint that `sp` refers to

        Parameters
        ----------
        sp:
            skeletal part to get


        Returns
        -------
        Joint:
            joint corresponding to `sp`

        """
        return self.SkeletalPartSwitcher.get(sp, None)

    def getSubSkeleton(
        self,
        skeletonDefinition: Optional[List["Skeleton.SkeletalPart"]] = None,
    ) -> List[int]:
        """
        Get the set of joints that `skeletonDefinition` refers to

        Parameters
        ----------
        skeletonDefinition:
            set of joints to retrieve

        Returns
        -------
        List[int]:
            set of joints corresponding to `skeletonDefinition`. If `skeletonDefinition` is None, then returns all the joints of the skeleton
        """
        if skeletonDefinition:
            return [
                self.joints.index(joint)
                for joint in [
                    self.getSkeletalPart(sp) for sp in skeletonDefinition
                ]
                if joint is not None
            ]
        else:
            return [self.joints.index(j) for j in self.joints]

    def createProxyBone(self) -> None:
        """
        Creates a proxy bone, with the original root as direct child
        """
        self.proxyBone = Joint("Proxy")
        assert self.proxyBone is not None
        r = self.root
        self.root = self.proxyBone
        self.proxyBone.addChild(r)

    def destroyProxyBone(self) -> None:
        """
        Destroy the proxy bone, the original root becomes the new root
        """
        if self.proxyBone is None:
            return

        self.root = self.proxyBone.children[0]
        self.proxyBone = None

    # shortcuts
    basePose = TransformGraph.transforms
    """
    Base pose of the skeleton, could be a T pose, or something else. Included
    by default in the animation when this skeleton has one, but not tied to it.
    Reflecting changes of this attribute to the animation requires to
    removeBasePose of the animation, apply changes, then addBasePose

    Note
    ----
    **Shorthand:** :attr:`~pynimation.anim.skeleton.Skeleton.baseP`,
    :attr:`~pynimation.anim.skeleton.Skeleton.bp`
    """

    baseP = basePose
    bp = basePose
