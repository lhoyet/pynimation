import abc
from typing import Dict, Any, TYPE_CHECKING

import numpy as np

if TYPE_CHECKING:
    from pynimation.anim.joint import Joint


class IK(abc.ABC):
    """
    Base IK class
    """

    @abc.abstractmethod
    def __init__(
        self,
        targetJoint: "Joint",
        endJoint: "Joint",
        settings: Dict[str, Any] = None,
    ):
        """
        Parameters
        ----------

        targetJoint: Joint
            Final joint of the chain that will be positioned to the
            given position by the IK algorithm

        endJoint: Joint
            Initial joint of the chain that will remain static while
            its children are oriented to match the position of the
            :attr:`targetJoint`
        settings: Dict[str, Any]
            Dictionary of settings name and values for the IK algorithm
        """

        self.targetJoint: "Joint"
        """
        Final joint of the chain that will be positioned to the
        given position by the IK algorithm
        """

        self.endJoint: "Joint"
        """
        Initial joint of the chain that will remain static while
        its children are oriented to match the position of the
        :attr:`targetJoint`
        """

        self.settings: Dict[str, Any]
        """
        Dictionary of settings name and values for the IK algorithm
        """

    @abc.abstractmethod
    def solve(self, targetPos: np.ndarray, frame: np.ndarray) -> None:
        """
        Abstract ik solve method
        Overriding methods must solve IK for given parameters

        Parameters
        ----------
        targetPos:
            array of desired positions of the target
        frame:
            frame of the animation the ik algorithm will be applied to
        """
        pass
