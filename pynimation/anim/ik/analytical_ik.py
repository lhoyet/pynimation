from typing import Dict, Any, TYPE_CHECKING, cast

import numpy as np

from pynimation.common.transform import Rotation
from pynimation.common.vec import Vec3
from .ik import IK

if TYPE_CHECKING:
    from pynimation.anim.joint import Joint
    from pynimation.anim.animation import Animation


class AnalyticalIK(IK):
    """
    Implements IK with the analytical method
    Is only applicable to chains of 3 joints


    Accepted settings are:

    * :code:`debug:bool`: whether to print debug indo

    * :code:`backupRotAxis:np.array`: one or ``nframes`` rotation axes to be
    used as new rotation axes for the rotation of :attr:`intermediateJoint`
    """

    defaultSettings = {"debug": False, "backupRotAxis": "X"}

    def __init__(
        self,
        targetJoint: "Joint",
        endJoint: "Joint",
        settings: Dict[str, Any] = None,
    ):
        self.targetJoint = targetJoint
        self.endJoint = endJoint
        if self.endJoint != self.targetJoint.parent.parent:
            raise ValueError("given joint chain must be 3 joint long")
        #: joint between target and end joint
        self.intermediateJoint = self.targetJoint.parent
        self.settings: Dict[str, Any]
        if settings is None:
            self.settings = AnalyticalIK.defaultSettings
        else:
            self.settings = {**AnalyticalIK.defaultSettings, **settings}

    def solve(self, targetPos: np.ndarray, animation: "Animation") -> None:
        """
        Solves IK with the analytical method

        Parameters
        ----------
        targetPos:
            array of desired positions for the target
        animation:
            animation the ik algorithm will be applied to
        """

        js = [
            self.endJoint,
            self.intermediateJoint,
            self.targetJoint,
        ]
        j0, j1, j2 = js

        originalGlobalPositionTarget = animation.joint[j2].globals.position

        L1 = j1.transform.position.norm()
        L2 = j2.transform.position.norm()

        rootPos = animation.joint[j0].globals.position

        def gp(j: "Joint") -> Vec3:
            return animation.joint[j].globals.p

        aa1 = gp(j0) - gp(j1)
        bb1 = gp(j1) - gp(j2)

        rot = Rotation.rotationBetween(
            bb1,
            aa1,
        )

        thetao = np.pi - rot.rotvec.norm()

        d = (targetPos - rootPos).norm()
        theta = np.arccos(
            np.clip((L1**2 + L2**2 - d**2) / (2 * L1 * L2), -1, 1)
        )

        thetaj = thetao - theta

        axisRot: Vec3
        if "axisRot" in self.settings:
            axisRot = Vec3(self.settings["axisRot"])
        else:
            axisRot = Vec3(
                animation.joint[j1].globals.rotation.inverse() @ aa1.cross(bb1)
            ).normalized()

        backupRotAxis = getattr(
            Vec3, cast(str, self.settings["backupRotAxis"]).lower()
        )
        axisRot[(axisRot == [0, 0, 0]).all(axis=1)] = backupRotAxis

        r = Rotation.identityLike(animation.joint[j1].rotation)
        r.rotvec = axisRot * thetaj[:, np.newaxis]
        animation.joint[j1].rotation = animation.joint[j1].rotation @ r

        currentTargetPos = animation.joint[j2].globals.position

        rot = Rotation.rotationBetween(
            currentTargetPos - rootPos, targetPos - rootPos
        )

        animation.joint[j0].rotation = (
            animation.joint[j0.parent].globals.rotation.inverse()
            @ rot
            @ animation.joint[j0].globals.rotation
        )
        newPosTarget = animation.joint[j2].globals.position

        if self.settings["debug"]:
            print("old: " + str(originalGlobalPositionTarget))
            print("new" + str(newPosTarget))
            print(
                "Error: "
                + str(
                    np.linalg.norm(originalGlobalPositionTarget - newPosTarget)
                )
            )
