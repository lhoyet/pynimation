from typing import Dict, Any, TYPE_CHECKING

from .analytical_ik import AnalyticalIK
from .ik import IK

if TYPE_CHECKING:
    from pynimation.anim.joint import Joint


def ikFactory(
    targetJoint: "Joint",
    endJoint: "Joint",
    settings: Dict[str, Any] = None,
) -> IK:
    """
    Factory function for :class:`~pynimation.anim.tools.ik.IK`
    implementations.

    Will construct an :class:`~pynimation.anim.tools.ik.IK` instance according
    to the characteristics of the kinematic chain defined by
    :attr:`targetJoint` and :attr:`endJoint`

    Parameters
    ----------
    targetJoint:
        joint that the :class:`~pynimation.anim.tools.ik.IK` implementation will set to the target position
    endJoint:
        base joint of the kinematic chain
    settings:
        settings to be passed to the :class:`~pynimation.anim.tools.ik.IK` implementation

    Returns
    -------
    IK:
        An instance of one of the implementations of
        :class:`~pynimation.anim.tools.ik.IK` suited to solve IK on the given
        kinematic chain

    Raises
    ------
    NotImplementedError:
        If no implementation was found that is suitable to this kinematic chain
    """
    if targetJoint.parent.parent == endJoint:
        return AnalyticalIK(targetJoint, endJoint, settings)
    else:
        raise NotImplementedError("no IK algorithm for given parameters")
