from typing import TYPE_CHECKING, List, Tuple, Optional, Union

import numpy as np

import pynimation.anim.detect_supports.supports_detecter as supports
from pynimation.anim.joint import Joint

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation


class CircleFitSupportsDetecter(supports.SupportsDetecter):
    """
    Create SupportDetecter instance of :attr:`animation` using
    support circle fit algorithm
    """

    name = "circlefit"

    defaultThreshold: float = 0.05
    defaultCircleWindow: float = 0.1166666666
    defaultMedianWindow: float = 0.1833333333
    defaultBoolMedianWindow: float = 0.1833333333

    def __init__(self, animation: "Animation"):
        self.animation = animation

    def detectSupports(
        self,
        joints: List[Union["Joint", List["Joint"]]],
        threshold: Optional[float] = None,
        circleWindow: Optional[float] = None,
        medianWindow: Optional[float] = None,
        boolMedianWindow: Optional[float] = None,
    ) -> np.ndarray:
        """
        Detect support of :attr:`animation`

        The principle of the algorithm is the following:

        For every ``joint`` of `joints`:

            - Extract position of ``joint`` at each frame
            - For each position, reduce XZ components to
              cumulated past trajectory length on XZ plane,
              and keep Y coordinate
            - Fit circles on points of this vector, selected
              by a moving window of size `circleWindow`,
              return radiuses for each set of points
            - Compute the median of radiuses selected by a
              window of size `medianWindow`
            - Select medians that are under `threshold`
              This step returns a boolean for each frame
            - Smooth the boolean array by a moving median with
              window size `boolMedianWindow`

        If `joints` contains lists of joints rather than
        joints, for each list of joints merge supports of those
        joints using an intersection

        Default window sizes are in seconds in order to be independent
        of animation framerate


        Parameters
        ----------

        joints:
            A list of either joints or joints lists from which to
            compute supports

            If it contains joints lists, merge the supports from
            those joints using an intersection

        threshold:
            Threshold under which median radiuses will be selected as
            contacts

        circleWindow:
            Window size for circle fit step, in seconds

        medianWindow:
            Window size for median of circle fit radiuses, in seconds

        boolMedianWindow:
            Window size for boolean median, in seconds

        Returns
        -------

        np.ndarray:
            shape: (supports_count, frame_count)

            For each joint of `joints` an array of boolean
            indicating if this joint is a support during this frame

        Examples
        --------
        >>> skeleton = animation.skeleton

        >>> # compute supports for left and right toes
        >>> joints = [skeleton.leftToe, skeleton.rightToe]
        >>> supports = CircleFitSupportsDetecter(animation).detectSupports(joints)
        >>> supports.shape
        (2,261)
        >>> supports[:,:10]
        [[ True  True  True  True  True  True  True  True  True  True]
         [False False False False False False False False False False]]

        >>> # a more readable equivalent in ranges of frames
        >>> import pynimation.anim.detect_supports.utils as utils
        >>> utils.supportsToRange(support)
        [[(0, 60), (128, 191)], [(65, 127), (194, 255)]]

        >>> # compute merged supports for toes/ankles
        >>> joints = [
                [skeleton.leftToe,skeleton.leftAnkle],
                [skeleton.rightToe, skeleton.rightAnkle]
            ]
        >>> supports = CircleFitSupportsDetecter(animation).detectSupports(joints)
        >>> supports.shape
        (2,261)
        >>> utils.supportsToRange(support)
        [[(0, 60), (128, 191)], [(65, 127), (194, 255)]]
        """
        threshold_ = threshold or CircleFitSupportsDetecter.defaultThreshold
        framerate = self.animation.framerate
        circleWindow_: int = round(
            (circleWindow or CircleFitSupportsDetecter.defaultCircleWindow)
            * framerate
        )
        medianWindow_: int = round(
            (medianWindow or CircleFitSupportsDetecter.defaultMedianWindow)
            * framerate
        )
        boolMedianWindow_: int = round(
            (
                boolMedianWindow
                or CircleFitSupportsDetecter.defaultBoolMedianWindow
            )
            * framerate
        )
        if len(joints) == 0:
            raise ValueError("Provided an empty joint list to detectSupports")

        if isinstance(joints[0], Joint):
            return self._detectFootSupports(
                joints,
                threshold_,
                circleWindow_,
                medianWindow_,
                boolMedianWindow_,
            )
        elif isinstance(joints[0], list):
            supports = [
                self._detectFootSupports(
                    jointLists,
                    threshold_,
                    circleWindow_,
                    medianWindow_,
                    boolMedianWindow_,
                )
                for jointLists in joints
            ]
            supports_ = np.array(
                [np.logical_or(*support) for support in supports]
            )
            return np.array(
                [
                    self._medianWindowBool(support, boolMedianWindow_)
                    for support in supports_
                ]
            )
        else:
            raise ValueError(
                "Joints argument of detectSupports should be "
                + "joint list of list of joints lists"
            )

    def _detectFootSupports(
        self,
        joints: List["Joint"],
        threshold: float,
        circleWindow: int,
        medianWindow: int,
        boolMedianWindow: int,
    ) -> np.ndarray:
        return np.array(
            [
                self._detectFootSupport(
                    joint,
                    threshold,
                    circleWindow,
                    medianWindow,
                    boolMedianWindow,
                )
                for joint in joints
            ]
        )

    def _detectFootSupport(
        self,
        joint: "Joint",
        threshold: float,
        circleWindow: int,
        medianWindow: int,
        boolMedianWindow: int,
    ) -> np.ndarray:
        pos = self._getPositions(joint)
        xz_y = self._getXZ_Y(pos)
        radiuses = self._circleFitXZ(xz_y, circleWindow)
        medians = self._medianWindow(radiuses, medianWindow)
        underThresh = medians < threshold
        contacts = self._medianWindowBool(underThresh, boolMedianWindow)
        return contacts

    def _getPositions(self, joint: "Joint") -> np.ndarray:
        return self.animation.joint[joint].globals.position

    def _getXZ_Y(self, pos: np.ndarray) -> np.ndarray:
        xz = pos[:, [0, 2]]
        y = pos[:, 1]

        xzNorm = np.cumsum(
            np.linalg.norm(
                np.diff(np.insert(xz, 0, np.zeros(2), axis=0), axis=0), axis=1
            )
        )

        return np.column_stack([xzNorm, y])

    def _circleFitXZ(self, xz_y: np.ndarray, window: int) -> np.ndarray:
        windows = np.squeeze(
            np.lib.stride_tricks.sliding_window_view(xz_y, (window, 2))
        )
        radiuses = np.array(
            [CircleFitSupportsDetecter._circleFit(win)[1] for win in windows]
        )
        # add to the begining and end so that len(radiuses) == len(xy_z)
        radiuses = np.insert(
            radiuses, np.zeros(window // 2, dtype=int), radiuses[0]
        )
        radiuses = np.append(
            radiuses, np.full(window // 2 + (window % 2) - 1, radiuses[-1])
        )

        return radiuses

    @staticmethod
    def _circleFit(points: np.ndarray) -> Tuple[np.ndarray, float]:
        center = np.average(points, axis=0)
        points = points - center
        suu = np.sum(np.power(points[:, 0], 2))
        suv = np.sum(points[:, 0] * points[:, 1])
        svv = np.sum(np.power(points[:, 1], 2))
        suuu = np.sum(np.power(points[:, 0], 3))
        svvv = np.sum(np.power(points[:, 1], 3))
        suvv = np.sum(points[:, 0] * np.power(points[:, 1], 2))
        svuu = np.sum(points[:, 1] * np.power(points[:, 0], 2))

        m = np.linalg.pinv(np.array([[suu, suv], [suv, svv]]))
        uv = np.dot(m, np.array([0.5 * (suuu + suvv), 0.5 * (svvv + svuu)]))
        radius = np.sqrt(np.sum(np.power(uv, 2)) + (suu + svv) / len(points))
        return (uv + center, radius)

    def _medianWindow(self, radiuses: np.ndarray, window: int) -> np.ndarray:
        radiuses = np.insert(
            radiuses, np.zeros(window // 2, dtype=int), radiuses[0]
        )
        radiuses = np.append(
            radiuses, np.full(window // 2 + (window % 2) - 1, radiuses[-1])
        )
        windows = np.squeeze(
            np.lib.stride_tricks.sliding_window_view(radiuses, window)
        )
        return np.median(windows, axis=1)

    def _medianWindowBool(
        self, underThresh: np.ndarray, window: int
    ) -> np.ndarray:
        underThresh = np.insert(
            underThresh, np.zeros(window // 2, dtype=int), underThresh[0]
        )
        underThresh = np.append(
            underThresh,
            np.full(window // 2 + (window % 2) - 1, underThresh[-1]),
        )
        windows = np.squeeze(
            np.lib.stride_tricks.sliding_window_view(underThresh, window)
        )
        return np.median(windows, axis=1) > 0.5
