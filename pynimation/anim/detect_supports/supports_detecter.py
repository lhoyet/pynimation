import abc
from typing import Any, TYPE_CHECKING

import numpy as np

from pynimation.common.meta import _getImplementations

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation


class SupportsDetecter(abc.ABC):
    """
    Base SupportsDetecter class
    """

    @abc.abstractmethod
    def __init__(self, animation: "Animation"):
        """
        Parameters
        ----------

        animation:
            animation to detect support in
        """

        self.animation: "Animation"
        """
        animation to detect support in
        """

    @abc.abstractmethod
    def detectSupports(self, **options: Any) -> np.ndarray:
        """
        Run support detection on :attr:`animation`
        """
        pass


def _supportsDetecterFactory(
    name: str, animation: "Animation"
) -> SupportsDetecter:
    implementations = _getImplementations("pynimation.anim.detect_supports")
    if name not in implementations:
        raise NotImplementedError("no such method " + name)
    else:
        return implementations[name](animation)
