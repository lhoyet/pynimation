from typing import List, Tuple, Optional
import xml.etree.ElementTree as ET
import os

import numpy as np


def readXMLSupports(xmlSupportFile: str) -> np.ndarray:
    """
    Read XML support file and return supports as boolean arrays

    Support files format ressembles:
    :code:`
    <SupportPhases>
        <LeftSupport frame="0" />
        <DoubleSupport frame="40" />
        <RightSupport frame="61" />
        <DoubleSupport frame="104" />
        <LeftSupport frame="126" />
        <DoubleSupport frame="163" />
        <RightSupport frame="187" />
        <DoubleSupport frame="223" />
        <LeftSupport frame="245" />
    </SupportPhases>
    `


    Parameters
    ----------

    xmlSupportFile:
        Path to xml file

    Returns
    -------

    np.ndarray:
        boolean array of supports for each frame obtained
        from xml file
    """

    supportsKinds = ["LeftSupport", "RightSupport", "DoubleSupport"]
    if os.path.exists(xmlSupportFile):
        keyframes = [
            (supportsKinds.index(a.tag), int(a.attrib["frame"]))
            for a in ET.parse(xmlSupportFile).getroot()
            if a.tag in supportsKinds
        ]
    animLength = keyframes[-1][1] + 1
    supports = np.zeros((2, animLength), dtype=bool)
    supportsIndexes = [[0], [1], [0, 1]]
    firstKFKind = keyframes[0][0]
    keyframes.append((firstKFKind, animLength))
    for i in range(len(keyframes) - 1):
        supIdx = supportsIndexes[keyframes[i][0]]
        low = keyframes[i][1]
        high = keyframes[i + 1][1]
        supports[supIdx, low:high] = True
    return supports


def supportsToRange(supports: np.ndarray) -> List[List[Tuple[int, int]]]:
    """
    Convert boolean arrays to integer ranges (as integer pairs)


    Parameters
    ----------

    supports:
        boolean array of supports of shape (n,m)

    Returns
    -------

    List[List[Tuple[int, int]]]:
        for each array of `supports` equivalent
        integer ranges as integer pairs

    Examples
    --------
    >>> import pynimation.anim.detect_supports.utils as utils
    >>> supports = [[True, True, False, False]]
    [[(0, 2)]])
    >>> utils.supportsToRange(supports)

    >>> supports = [
                [True, False, False, True, True, True],
                [False, True, False, True, True, False],
            ]
    >>> utils.supportsToRange(supports)
    [[(0, 1), (3, 6)], [(1, 2), (3, 5)]]
    """
    if supports.dtype != bool:
        raise ValueError("input supports should be boolean")

    def supportToRange(support: np.ndarray) -> List[Tuple[int, int]]:
        range_: List[Tuple[int, int]] = []
        range_ = (
            np.squeeze(
                np.argwhere(
                    np.diff(np.append(np.insert(support, 0, False), False))
                    == 1
                )
            )
            .reshape((-1, 2))
            .tolist()
        )

        return [(a, b) for [a, b] in range_]

    return [supportToRange(support) for support in supports]


def rangeToSupports(
    range: List[List[Tuple[int, int]]], nframes: Optional[int] = None
) -> np.ndarray:
    """
    Convert integer ranges to boolean arrays


    Parameters
    ----------

    range:
        integer ranges (as integer pairs)

    nframes:
        Size of the boolean arrays, if ranges do not span
        the entire array, this argument has to be provided
        to get arrays of the right size

        If not provided will be set to the highest bound of
        the ranges

    Returns
    -------

    np.ndarray:
        for each array ranges an equivalent boolean array

    Examples
    --------
    >>> import pynimation.anim.detect_supports.utils as utils
    >>> ranges = [[(0, 2)]])
    >>> utils.rangeToSupports(ranges)
    [[True, True]]

    >>> ranges = [[(0, 1), (3, 5)], [(1, 2), (3, 5)]]
    >>> # provide nframes argument so that arrays have size nframes
    >>> supports = utils.rangeToSupports(ranges, nframes=6)
    >>> supports
    [
        [True, False, False, True, True, False],
        [False, True, False, True, True, False],
    ]
    """
    nframes_ = nframes or max([a for b in range for c in b for a in c])

    def rangeToSupport(range_: List[Tuple[int, int]]) -> np.ndarray:
        support = np.zeros(nframes_, dtype=bool)
        for r in range_:
            support[r[0] : r[1]] = True
        return support

    return np.array([rangeToSupport(r) for r in range], dtype=bool)


def FScore(gtSupports: np.ndarray, supports: np.ndarray) -> float:
    """
    Compute F-score between `gtSupports` and `supports`


    Parameters
    ----------

    gtSupports:
        ground truth supports

    supports:
        computed supports

    Returns
    -------

    float:
        F-score comparing `gtSupports` and `supports`
    """
    true_positive = np.sum(np.logical_and(gtSupports, supports))
    positive = np.sum(supports)
    relevant = np.sum(gtSupports)
    return 2 * true_positive / (positive + relevant)
