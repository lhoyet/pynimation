from typing import Optional, TYPE_CHECKING, List

import numpy as np

import pynimation.anim.detect_supports.supports_detecter as supports

if TYPE_CHECKING:
    from pynimation.anim.joint import Joint
    from pynimation.anim.animation import Animation


class MenardaisSupportsDetecter(supports.SupportsDetecter):
    """
    Create SupportDetecter instance of :attr:`animation` using
    support detection algorithm [1]_

    Notes
    -----

    .. [1] Ménardais, S. & Kulpa, Richard & Multon, Franck & Arnaldi, B..
       (2004). Synchronization for dynamic blending of motions. 325-335.
       10.1145/1028523.1028567.
    """

    name = "menardais"

    defaultMaxHeight = 0.14
    defaultMaxSpeed = 0.019

    def __init__(self, animation: "Animation"):
        self.animation = animation

    def detectSupports(
        self,
        joints: Optional[List["Joint"]] = None,
        maxHeight: Optional[float] = None,
        maxSpeed: Optional[float] = None,
    ) -> np.ndarray:
        """
        Detect support of :attr:`animation`

        Parameters
        ----------

        maxHeight:
            Maximum height of the foot when it is on the ground, all frames
            where the foot position is below this height will be considered
            supports if they also meet the `maxSpeed` requirement

        maxSpeed:
            Maximum speed of the foot when its position is below
            `maxHeight`, all frames where the foot speed is below this
            speed will be considered supports if they also meet the
            `maxHeight` requirement

        Returns
        -------

        np.ndarray
            shape: (2, frame_count)
            For each foot (left, right) an array of boolean indicating if
            this foot is a support during this frame
        """
        maxHeight = maxHeight or MenardaisSupportsDetecter.defaultMaxHeight
        maxSpeed = maxSpeed or MenardaisSupportsDetecter.defaultMaxSpeed
        joints = joints or self.animation.skeleton.toes
        return np.array(
            [
                self._detectFootSupport(footJoint, maxHeight, maxSpeed)
                for footJoint in joints
            ]
        )

    def _detectFootSupport(
        self,
        joint: "Joint",
        maxHeight: float = None,
        maxSpeed: float = None,
    ) -> np.ndarray:
        tdelta = 1 / self.animation.framerate
        footPositions = self.animation.joint[joint].globals.position
        footHeights = footPositions.y
        footSpeeds = np.linalg.norm(
            np.diff(footPositions, axis=0) / tdelta, axis=1
        )
        # Speed at frame 0 is 0
        footSpeeds = np.insert(footSpeeds, 0, 0)

        contactFrames = np.intersect1d(
            np.argwhere(footHeights < maxHeight),
            np.argwhere(footSpeeds < maxSpeed),
        )

        return np.isin(
            np.arange(self.animation.nframes),
            contactFrames,
            assume_unique=True,
        )
