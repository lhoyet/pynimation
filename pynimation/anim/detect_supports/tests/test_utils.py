import numpy as np

from pynimation.common import data as data_
import pynimation.anim.detect_supports.tests.truths as truths
import pynimation.anim.detect_supports.utils as utils


def testReadXMLSupports():
    xmlPath = data_.getDataPath("data/animations/walking_female.xml")

    supports = utils.readXMLSupports(xmlPath)
    assert np.all(supports == truths.xmlTruth)


def supportToRangeTest(supports, correctRange):
    assert (
        utils.supportsToRange(np.array(supports, dtype=bool)) == correctRange
    )


def rangeToSupportTest(correctSupports, range_):
    assert np.all(
        utils.rangeToSupports(range_, nframes=len(correctSupports[0]))
        == np.array(correctSupports, dtype=bool),
    )


def supportsRangesSimpleTest(testFunc):
    for args in [
        ([[True]], [[(0, 1)]]),
        ([[True, True]], [[(0, 2)]]),
        ([[False]], [[]]),
        ([[False, False]], [[]]),
        ([[True, True, False, False]], [[(0, 2)]]),
        ([[False, False, False, False]], [[]]),
        ([[False, False, True, True]], [[(2, 4)]]),
        ([[True, True, True, True]], [[(0, 4)]]),
        ([[True, False, True, True]], [[(0, 1), (2, 4)]]),
        ([[True, False, True, True]], [[(0, 1), (2, 4)]]),
        ([[True, True, False, True, True, False]], [[(0, 2), (3, 5)]]),
    ]:
        testFunc(*args)


def supportsRangesDoubleTest(testFunc):
    for args in [
        ([[True], [True]], [[(0, 1)], [(0, 1)]]),
        ([[True, True], [True, True]], [[(0, 2)], [(0, 2)]]),
        ([[True, True], [True, False]], [[(0, 2)], [(0, 1)]]),
        ([[True, False], [True, True]], [[(0, 1)], [(0, 2)]]),
        ([[False], [False]], [[], []]),
        ([[True, False], [True, False]], [[(0, 1)], [(0, 1)]]),
        (
            [
                [True, False, False, True, True, True],
                [False, True, False, True, True, False],
            ],
            [[(0, 1), (3, 6)], [(1, 2), (3, 5)]],
        ),
    ]:
        testFunc(*args)


def supportsRangesTripleTest(testFunc):
    for args in [
        ([[True], [True], [True]], [[(0, 1)], [(0, 1)], [(0, 1)]]),
        (
            [[True, True], [True, True], [True, True]],
            [[(0, 2)], [(0, 2)], [(0, 2)]],
        ),
        (
            [[True, True], [True, False], [False, True]],
            [[(0, 2)], [(0, 1)], [(1, 2)]],
        ),
        (
            [[True, False], [True, False], [True, True]],
            [[(0, 1)], [(0, 1)], [(0, 2)]],
        ),
    ]:
        testFunc(*args)


def supportsRangesXMLTest(testFunc):
    testFunc(truths.xmlTruth, truths.xmlRangeTruth)


def testSupportsToRangesSimple():
    supportsRangesSimpleTest(supportToRangeTest)


def testSupportsToRangesDouble():
    supportsRangesDoubleTest(supportToRangeTest)


def testSupportsToRangesTriple():
    supportsRangesTripleTest(supportToRangeTest)


def testSupportsToRangesXML():
    supportsRangesXMLTest(supportToRangeTest)


def testRangesToSupportsSimple():
    supportsRangesSimpleTest(rangeToSupportTest)


def testRangesToSupportsDouble():
    supportsRangesDoubleTest(rangeToSupportTest)


def testRangesToSupportsTriple():
    supportsRangesTripleTest(rangeToSupportTest)


def testRangesToSupportsXML():
    supportsRangesXMLTest(rangeToSupportTest)
