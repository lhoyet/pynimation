import numpy as np

from pynimation.anim.detect_supports.circlefit_supports_detecter import (
    CircleFitSupportsDetecter,
)

nrg = np.random.default_rng(21289328749825)


def circleFitTestRadius(
    radius: float, tol=10e-3, npoints=20, noise_std=None
) -> bool:
    angles = nrg.uniform(0, np.pi * 2, npoints)
    points = np.column_stack([np.cos(angles), np.sin(angles)]) * radius
    if noise_std is not None:
        noise = np.clip(nrg.normal(1, noise_std, npoints), 0, 2)
        noise = noise - (np.average(noise) - 1)
        noise = noise[:, np.newaxis]
        points = points * noise
    foundRadius = CircleFitSupportsDetecter._circleFit(points)[1]

    return np.abs(radius - foundRadius) < tol


def testCircleFitPerfect1to10():
    for r in range(1, 10):
        assert circleFitTestRadius(r)


def testCircleFitNoisy011to10():
    for r in range(1, 10):
        assert circleFitTestRadius(r, noise_std=0.1, tol=10e-2)
