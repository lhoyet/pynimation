from typing import TYPE_CHECKING, Generic, Optional, TypeVar, cast, List

from pynimation.common.transform_graph import TransformNode

if TYPE_CHECKING:
    from .skeleton import Skeleton
    from pynimation.common.transform import Transform

JointLike = TypeVar("JointLike", bound="Joint")


class Joint(Generic[JointLike], TransformNode[JointLike]):
    """
    Joint of a :class:`~pynimation.anim.skeleton.Skeleton`
    """

    @property
    def skeleton(self) -> Optional["Skeleton"]:
        """
        skeleton this joint is part of
        """
        return cast("Skeleton", self._graph)

    def getInverseBindMatrix(self) -> "Transform":
        """
        Computes the inverse of the global transform of the joint
        """
        return self.globalTransform.inverse()

    def _tostring(self, tab="", visited: List["Joint[JointLike]"] = []) -> str:
        if self in visited:
            return "__CYCLE__"
        idstr = ""
        if self.skeleton is not None:
            id_ = self.id
            idstr = "[" + str(id_) + "]"
            partsNames = ""
            if id_ in self.skeleton.partSkeletalSwitcher:
                partsNames = " (%s)" % ", ".join(
                    self.skeleton.partSkeletalSwitcher[id_]
                )
        return (
            tab
            + idstr
            + self.name
            + partsNames
            + "\n"
            + "\n".join(
                [
                    child._tostring(tab + "    ", visited + [self])
                    for child in self.children
                ]
            )
        )
