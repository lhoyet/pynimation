from typing import List

import numpy as np
import pytest

from pynimation.anim.animation import Animation
from pynimation.anim.joint import Joint
from pynimation.anim.skeleton import Skeleton
from pynimation.anim.tests.test_skeleton import genTestSkeleton, skel
from pynimation.common.tests.common_tests import AttrsTest

SHAPE = (10, 10, 4, 4)

FRAMERATE = 60

# TODO: refactor with LocalGlobal tests
SKELS: List["Skeleton"] = [Skeleton(Joint(""))]

for i in range(20):
    o: Joint = Joint("")
    n = o
    for j in range(i):
        n.addChild(Joint(""))
        n = n.children[0]
    SKELS.append(Skeleton(o))


@pytest.fixture
def shape_():
    return SHAPE


@pytest.fixture
def a(shape_):
    return Animation(np.ones(shape_), SKELS[shape_[-3]], FRAMERATE)


@pytest.fixture
def acopy(shape_, a):
    return a.copy()


@pytest.fixture
def arand(shape_):
    return Animation(
        np.random.random_sample(shape_), SKELS[shape_[-3]], FRAMERATE
    )


@pytest.mark.parametrize(
    "obj",
    [
        pytest.lazy_fixture("a"),  # type: ignore
        pytest.lazy_fixture("acopy"),  # type: ignore
    ],
)
@pytest.mark.parametrize(
    ("attr", "type_"),
    [
        ("frate", int),
        ("framerate", int),
        ("duration", float),
        ("nframes", int),
        ("nf", int),
        ("njoints", int),
        ("nj", int),
    ],
)
class TestTransformAttrs(AttrsTest):
    pass


class TestJointNames:
    @pytest.mark.parametrize(
        "anim",
        [lambda a: a, lambda a: a.globals],
    )
    def testRightJoint(self, anim):
        skeleton = genTestSkeleton(skel)
        a = anim(
            Animation.fromSkeleton(
                skeleton,
                nframes=10,
                framerate=60,
            )
        )
        for j in skeleton:
            if hasattr(j, "correctName"):
                assert np.all(getattr(a, j.correctName) == a[..., j.id, :, :])
