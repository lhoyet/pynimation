import numpy as np
import pytest
import copy

from pynimation.anim.joint import Joint
from pynimation.anim.skeleton import Skeleton
from pynimation.common.transform import Transform

skel = {
    "root": {
        "spine": {
            "neck": {"head": {}},
            "_l_clavicle": {"_l_shoulder": {"_l_elbow": {"_l_wrist": {}}}},
            "_r_clavicle": {"_r_shoulder": {"_r_elbow": {"_r_wrist": {}}}},
        },
        "_l_hip": {"_l_knee": {"_l_ankle": {"_l_toe": {}}}},
        "_r_hip": {"_r_knee": {"_r_ankle": {"_r_toe": {}}}},
    }
}


def genTestSkeleton(
    skel,
    sideNames=["Left {0}", "Right {0}"],
    prefix="",
    missing=[],
    spineN=3,
    capitalize=False,
) -> Skeleton:
    skeleton = Skeleton.fromDict(copy.deepcopy(skel), Joint)
    sides = ["l", "r"]

    # remove missing
    for joint in skeleton.joints:
        name = joint.name
        if name[0] == "_":
            name = name[3:]
        if name in missing:
            if joint.parent is not None:
                parent = joint.parent
                parent.removeChild(joint)
                parent.children = (*parent.children, *joint.children)

    # add correct names
    for joint in skeleton.joints:
        name = joint.name
        sidesCorrectNames = ["left", "right"]
        if name[0] == "_":
            side = sides.index(name[1])
            name = sidesCorrectNames[side] + name[3:].capitalize()
        if name == "spine":
            name = "upperBody"

        joint.correctName = name

    # add spine nodes
    spine = next((a for a in skeleton.joints if a.name == "spine"), None)
    if spine is not None:
        children = spine.children
        spine.children = ()
        j = spine
        for i in range(spineN - 1):
            parent = j
            j = Joint("spine" + str(i + 1), parent=parent)
        j.children = children

    # add side Names
    for joint in skeleton.joints:
        name = joint.name
        if name[0] == "_":
            side = sides.index(name[1])
            name = name[3:]
            if capitalize:
                name.capitalize()
            name = sideNames[side].format(name)
        else:
            if capitalize:
                name.capitalize()

        name = prefix + name

        joint.name = name

    return skeleton


def verifySkeleton(skeleton: Skeleton):
    assert (
        all(
            [hasattr(skeleton, j.correctName)]
            for j in skeleton.joints
            if hasattr(j, "correctName")
        )
        and all(
            [
                getattr(skeleton, j.correctName) is not None
                for j in skeleton.joints
                if hasattr(j, "correctName")
            ]
        )
        and all(
            getattr(skeleton, j.correctName) == j
            for j in skeleton.joints
            if hasattr(j, "correctName")
        )
    )


def testMapHumanoidSkeleton():
    skeleton = genTestSkeleton(skel)
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonHipsFirst():
    skeleton = genTestSkeleton(skel)
    branches = skeleton.root.children
    skeleton.root.children = (*branches[1:], branches[0])
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonArmsFirst():
    skeleton = genTestSkeleton(skel)
    spine = skeleton.root.children[0]
    branches = spine.children
    spine = (*branches[1:], branches[0])
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonNoClavicle():
    skeleton = genTestSkeleton(skel, missing=["clavicle"])
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonPrefix():
    skeleton = genTestSkeleton(skel, prefix="Biped: ")
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonOneLetterSideNames():
    skeleton = genTestSkeleton(skel, sideNames=["L {0}", "R {0}"])
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonOneLetterSideNamesWithoutSeparator():
    skeleton = genTestSkeleton(skel, sideNames=["L{0}", "R{0}"])
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonTwoJointsSpine():
    skeleton = genTestSkeleton(skel, spineN=2)
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonFourJointsSpine():
    skeleton = genTestSkeleton(skel, spineN=4)
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonCapitalize():
    skeleton = genTestSkeleton(skel, capitalize=True)
    skeleton.mapHumanoidSkeleton()
    verifySkeleton(skeleton)


def testMapHumanoidSkeletonRaisesExceptionWhenNotMapped():
    skeleton = genTestSkeleton(skel, sideNames=["{0}", "{0}"])
    with pytest.raises(
        ValueError,
        match="Could not map humanoid skeleton, following parts are missing:",
    ):
        skeleton.mapHumanoidSkeleton()


def testMapHumanoidSkeletonRootNotPelvis():
    skeleton = genTestSkeleton(skel)
    skeleton.mapHumanoidSkeleton()
    j = Joint("beforeRoot")
    j.correctName = "root"
    del skeleton.root.correctName
    r = skeleton.root
    skeleton.root = j
    j.addChild(r)
    verifySkeleton(skeleton)


def testProxyBoneIsNewRoot():
    skeleton = genTestSkeleton(skel)
    skeleton.createProxyBone()
    assert skeleton.root == skeleton.proxyBone


def testProxyBoneIsParentOfOldRoot():
    skeleton = genTestSkeleton(skel)
    oldroot = skeleton.root
    skeleton.createProxyBone()
    assert len(skeleton.proxyBone.children) == 1
    assert skeleton.proxyBone.children[0] == oldroot


def testProxyBoneCreatesSingleBone():
    skeleton = genTestSkeleton(skel)
    oldnbones = len(skeleton.nodes)
    skeleton.createProxyBone()
    assert len(skeleton.nodes) == oldnbones + 1


def testProxyBoneHasIdZero():
    skeleton = genTestSkeleton(skel)
    skeleton.createProxyBone()
    assert skeleton.proxyBone.id == 0


def testProxyBoneRootHasIdOne():
    skeleton = genTestSkeleton(skel)
    oldroot = skeleton.root
    skeleton.createProxyBone()
    assert oldroot.id == 1


def testDestroyProxyBoneRemovesProxyBone():
    skeleton = genTestSkeleton(skel)
    skeleton.createProxyBone()
    skeleton.destroyProxyBone()
    assert skeleton.proxyBone is None


def testDestroyProxyBoneOldRootHasIdZero():
    skeleton = genTestSkeleton(skel)
    oldroot = skeleton.root
    skeleton.createProxyBone()
    skeleton.destroyProxyBone()
    assert oldroot.id == 0


def testDestroyProxyBoneKeepsOldRoot():
    skeleton = genTestSkeleton(skel)
    oldroot = skeleton.root
    skeleton.createProxyBone()
    skeleton.destroyProxyBone()
    assert skeleton.root == oldroot


def testDestroyProxyBoneKeepsSameNbOfBones():
    skeleton = genTestSkeleton(skel)
    skeleton.createProxyBone()
    oldnbones = len(skeleton.nodes)
    skeleton.destroyProxyBone()
    assert oldnbones - 1 == len(skeleton.nodes)


def testSymmetricPartsNamesHasRightParts():
    skeleton = genTestSkeleton(skel)
    for a in Skeleton._symmetricPartsNames:
        b = a[:-1]
        assert getattr(skeleton, a) == (
            getattr(skeleton, "left" + b.capitalize()),
            getattr(skeleton, "right" + b.capitalize()),
        )


def testSymmetricPartsIsEmptyIfMissing():
    skeleton = genTestSkeleton(skel, missing=["clavicle"])
    for a in Skeleton._symmetricPartsNames:
        b = a[:-1]
        c = getattr(skeleton, a)
        assert (
            c
            == (
                getattr(skeleton, "left" + b.capitalize()),
                getattr(skeleton, "right" + b.capitalize()),
            )
            or c == ()
        )


def testKeepsTransformsWhenConstructedFromFullHierarchy():
    skeleton = genTestSkeleton(skel)
    n = skeleton.root.children[0]
    n.parent = None
    v = np.random.random_sample((len(n.descendants(True)), 4, 4))
    for vv, nn in zip(v, n.descendants(True)):
        nn.transform = Transform(vv)

    s = Skeleton(n)

    assert all([np.all(vv == nn) for vv, nn in zip(v, s.basePose)])
