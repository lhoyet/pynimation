from typing import Tuple, List, TYPE_CHECKING, Dict, Any

import numpy as np

# from scipy.interpolate import splprep
# from scipy.interpolate import splev

if TYPE_CHECKING:
    from pynimation.anim.skeleton import Skeleton
    from pynimation.anim.animation import Animation
    from pynimation.anim.joint import Joint
import pynimation.anim.retarget.retargeter as retar
from pynimation.anim.ik.ik_factory import ikFactory
from pynimation.anim.ik.ik import IK
from pynimation.common.transform import Transform
from pynimation.common.vec import Vec3
from pynimation.common.rotation import Rotation


class KulpaRetargeter(retar.Retargeter):
    """
    Create Retargeter instance from :attr:`source` animation to :attr:`target`
    skeleton using retargeting algorithm from [1]_

    Notes
    -----

    .. [1] Richard Kulpa, Franck Multon, Bruno Arnaldi. Morphology-independent
       representation of motions for interactive human-like animation. Computer
       Graphics Forum, Wiley, 2005, 24 (3), pp.343-352. ⟨inria-00441139⟩
    """

    name = "kulpa"

    def __init__(
        self,
        source: "Animation",
        target: "Skeleton",
    ):
        self.source = source
        self.target = target

    def retarget(
        self,
        spineCurve: bool = False,
        limbsExtendRatio: bool = True,
        legsRotAxes: Tuple[np.ndarray, np.ndarray] = None,
    ):
        """
        Run retargeting of :attr:`source` animation to :attr:`target` skeleton.

        In some cases, angle differences of the knee joint between source and
        target skeleton can produce a reversed knee in some frames of the
        retargeted animation. To resolve this, you can pass custom axes for
        the knee joints with the legsRotAxes parameter

        Whether the algorithm should be applied to the limbs and spine can be
        controlled with the :attr:`spineCurve` and :attr:`limbsExtendRatio`
        parameters.

        Parameters
        ----------

        spineCurve:
            Apply retargeting to the spine using a spline curve

        limbsExtendRatio:
            Apply retargeting to the limbs to maintain the extension ratio
            between source and resulting animations

        legsRotAxes:
            custom axes of the knee joints, in the form ``(leftKneeAxis,
            rightKneeAxis)``. If one or both of these axes are empty arrays,
            the corresponding axes will be set to axes of the first frame,
            rather than computed at each frame when this parameter is empty
        """

        def getLimbs(
            skel: "Skeleton", skelName, nJoints=3
        ) -> List[List["Joint"]]:
            originsNames: List[str] = [
                "leftShoulder",
                "rightShoulder",
                "leftHip",
                "rightHip",
            ]
            notMapped: List[str] = [
                name for name in originsNames if getattr(skel, name) is None
            ]
            if len(notMapped) > 0:
                raise ValueError(
                    "Unable to retarget, following joints are not mapped in "
                    + skelName
                    + " skeleton:"
                    + ",".join(notMapped)
                )
            origins: List["Joint"] = [
                getattr(skel, name) for name in originsNames
            ]
            assert all([limbOrig is not None for limbOrig in origins])
            return [
                limbOrig.descendants(inclusive=True)[:nJoints]
                for limbOrig in origins
            ]

        def getKCLength(kc: List["Joint"], basePose) -> float:
            return np.sum(basePose.node[kc].position.norm(), axis=-1)

        def getParts(skel: "Skeleton") -> List["Skeleton.SkeletalPart"]:
            return [
                part
                for (part, joint) in skel.SkeletalPartSwitcher.items()
                if joint is not None
            ]

        def rotateAxis(tr, rotation):
            rotvec = tr.rotation.rotvec
            axis = rotvec.normalized()
            angle = rotvec.norm()
            naxis = rotation.rotation @ axis
            ntr = Transform()
            ntr.rotation.rotvec = naxis * angle
            return ntr

        def retargetAllJoints() -> None:
            s, t = mappingSortedLists

            newAnim.joint[t].rotation = (
                target.basePose.node[t].globals.rotation.inverse()
                @ source.basePose.node[s].globals.rotation
                @ sourceAnim.joint[s].rotation
                @ source.basePose.node[s].globals.rotation.inverse()
                @ target.basePose.node[t].globals.rotation
            )

        def retargetLimbs() -> None:
            for lid, (sourceKC, targetKC) in enumerate(
                zip(sourceLimbs, targetLimbs)
            ):
                sourceLen: float = getKCLength(sourceKC[1:], source.basePose)
                targetLen: float = getKCLength(targetKC[1:], target.basePose)
                lenRatio: float = targetLen / sourceLen
                iksettings: Dict[str, Any] = {}
                ik: "IK" = ikFactory(
                    targetKC[-1], targetKC[0], settings=iksettings
                )

                if sourceKC[0] in source.legs and legsRotAxes is not None:
                    legId = source.legs.index(sourceKC[0])
                    ik.settings["axisRot"] = legsRotAxes[legId]

                sourcePos = (
                    sourceAnim.joint[sourceKC[-1]].globals.position
                    - sourceAnim.joint[sourceKC[0]].globals.position
                )
                targetPos = sourcePos * lenRatio
                targetGlobPos = (
                    targetPos + newAnim.joint[targetKC[0]].globals.position
                )
                ik.solve(targetGlobPos, newAnim)

        def retargetRootPosition(lenRatio=None) -> None:
            # adapt root position
            # targetRootPos = sourceRootPos * (targetLegsLength/sourceLegsLength)
            if lenRatio is None:
                sleg, tleg = [
                    [j.id for j in skel.getChain(skel.rightHip, skel.rightToe)]
                    for skel in [source, target]
                ]
                targetLegsLen, sourceLegsLen = [
                    np.sum(
                        # why y and not norm() ?
                        skel.basePose.node[leg].globals.position.y
                        - skel.basePose.node[
                            skel.parents[leg]
                        ].globals.position.y
                    )
                    for skel, leg in [(target, tleg), (source, sleg)]
                ]
                lenRatio = targetLegsLen / sourceLegsLen
            newAnim.root.position = sourceAnim.root.position * lenRatio + Vec3(
                [-0.01, 0, 0]
            )

        # TODO
        # def retargetSpine() -> None:
        #     sourceSpineLen = getKCLength(sourceSpine[1:], source)
        #     targetSpineLen = getKCLength(targetSpine[1:], target)
        #     lenRatio = targetSpineLen / sourceSpineLen
        #     for (i, (nf, f)) in enumerate(zip(newFrames, sourceAnim.frames)):
        #         x = np.array(
        #             [
        #                 f.globalTransforms[j.id].getPosition()
        #                 for j in sourceSpine
        #             ]
        #         )
        #         x = ((x - x[0]) * lenRatio).swapaxes(0, 1)
        #         tck, u = splprep(x)
        #         un = np.cumsum(
        #             [
        #                 np.linalg.norm(j.transform.getPosition())
        #                 for j in targetSpine
        #             ]
        #         )
        #         un = un - un[0]
        #         un = un * (1 / un[-1])
        #         nx = np.swapaxes(splev(un, tck), 0, 1)
        #         nx = (
        #             nx
        #             - nx[0]
        #             + nf.globalTransforms[targetSpine[0].id].getPosition()
        #         )

        #         ox = np.array(
        #             [
        #                 nf.globalTransforms[j.id].getPosition()
        #                 for j in targetSpine
        #             ]
        #         )

        #         for (k, j) in enumerate(targetSpine):
        #             if k == 0 or np.all(nx[k] == ox[k]):
        #                 continue
        #             oj = j.transform.getPosition()

        #             nj = np.dot(
        #                 nf.globalTransforms[j.parent.id].inverse().matrix,
        #                 [*nx[k], 1],
        #             )[:3]
        #             align = Transform.rotationMatrixAlignVectors(
        #                 oj, nj
        #             ).getRotation()
        #             frameRot = nf.localTransforms[j.parent.id]
        #             nf.localTransforms[j.parent.id].setRotation(
        #                 np.dot(frameRot.getRotation(), align)
        #             )

        def alignFeets():
            for lid, (sourceKC, targetKC) in enumerate(
                zip(sourceLimbs, targetLimbs)
            ):
                if (
                    len(targetKC[-1].children) == 1
                    and len(sourceKC[-1].children) == 1
                ):
                    s = sourceKC[-1]
                    sc = s.children[0]
                    t = targetKC[-1]
                    tc = t.children[0]
                    sj = s.globalTransform.rotation @ sc.transform.position
                    tj = t.globalTransform.rotation @ tc.transform.position
                    align = Transform.fromRotation(
                        Rotation.rotationBetween(tj, sj)
                    )
                    t.transform.rotation = (
                        t.transform.rotation
                        @ t.globalTransform.rotation.inverse()
                        @ align.rotation
                        @ t.globalTransform.rotation
                    )

        def alignLimbs():
            for lid, (sourceKC, targetKC) in enumerate(
                zip(sourceLimbs, targetLimbs)
            ):
                align = Transform()
                if sourceKC[0] in [source.leftArm, source.rightArm] and (
                    len(targetKC[-1].children) == 1
                    and len(sourceKC[-1].children) == 1
                ):
                    sourceKC = [*sourceKC, sourceKC[-1].children[0]]
                    targetKC = [*targetKC, targetKC[-1].children[0]]
                for i in range(len(sourceKC) - 1):
                    sj = (
                        sourceKC[i].globalTransform.rotation
                        @ sourceKC[i + 1].transform.position
                    )
                    tj = (
                        targetKC[i].globalTransform.rotation
                        @ targetKC[i + 1].transform.position
                    )
                    align.rotation = Rotation.rotationBetween(tj, sj)
                    targetKC[i].transform.rotation = (
                        targetKC[i].transform.rotation
                        @ targetKC[i].globalTransform.rotation.inverse()
                        @ align.rotation
                        @ targetKC[i].globalTransform.rotation
                    )

        def alignLimbsSecondAxis():
            for (sb, se), (tb, te) in [
                [
                    (j.parent.parent, j)
                    for skel in [source, target]
                    for j in [getattr(skel, ext)]
                ]
                for ext in ["leftAnkle", "rightAnkle"]
            ]:
                scross, tcross = [
                    (
                        Vec3(
                            j.parent.globalTransform.rotation
                            @ j.transform.position
                        ).cross(
                            j.globalTransform.rotation
                            @ j.children[0].transform.position
                        )
                    ).normalized()
                    for j in [se, te]
                ]
                axis = tcross.cross(scross).normalized()
                angle = np.arccos(np.dot(scross, tcross))
                align = Transform()
                align.rotation.rotvec = axis * angle
                tb.transform.rotation = (
                    tb.transform.rotation
                    @ tb.globalTransform.rotation.inverse()
                    @ align.rotation
                    @ tb.globalTransform.rotation
                )

        target = self.target.copy()
        sourceAnim = self.source
        source = sourceAnim.skeleton
        sourceLimbs: List[List["Joint"]] = getLimbs(source, "source")
        targetLimbs: List[List["Joint"]] = getLimbs(target, "target")
        sourceSpine = source.getChain(source.root, source.head.children[0])
        targetSpine = target.getChain(target.root, target.head.children[0])

        sourceParts = getParts(source)
        targetParts = getParts(target)
        notmapped = [a for a in sourceParts if a not in targetParts]

        if len(sourceParts) != len(targetParts) or len(notmapped) > 0:
            raise ValueError(
                "some parts from source skeleton are not mapped in target skeleton"
            )

        mapping = {
            source.SkeletalPartSwitcher[a]: target.SkeletalPartSwitcher[a]
            for a in sourceParts
        }
        spines = list(zip(sourceSpine, targetSpine[1:]))
        mapping.update({a: b for a, b in spines})

        mappingSorted = list(mapping.items())
        mappingSorted = mappingSorted + spines
        mappingSorted = list(set(mappingSorted))
        mappingSorted.sort(key=lambda x: x[1].id)
        mappingSortedLists = [[b[a] for b in mappingSorted] for a in [0, 1]]

        from pynimation.anim.animation import Animation

        newAnim = Animation.fromGraph(
            target, nframes=sourceAnim.nframes, framerate=sourceAnim.framerate
        )
        sourceAnim[...] = source.basePose.inverse() @ sourceAnim

        alignLimbs()
        alignLimbsSecondAxis()
        alignFeets()
        retargetAllJoints()
        sourceAnim[...] = source.basePose @ sourceAnim
        newAnim[...] = target.basePose @ newAnim
        retargetRootPosition()
        # if spineCurve:
        #     retargetSpine()

        if limbsExtendRatio:
            retargetLimbs()

        sourceAnim.skeleton = target
        sourceAnim.set(newAnim)
