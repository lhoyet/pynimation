import abc
from typing import Optional, Any, TYPE_CHECKING

import numpy as np

from pynimation.common.meta import _getImplementations

# from .kulpa_retargeter import KulpaRetargeter

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton


class Retargeter(abc.ABC):
    """
    Base Retargeter class
    """

    @abc.abstractmethod
    def __init__(self, source: "Animation", target: "Skeleton"):
        """
        Parameters
        ----------

        source:
            animation to retarget

        target:
            skeleton :attr:`source` should be retargeted to
        """
        #: animation to retarget
        self.source: "Animation"
        #: skeleton :attr:`source` should be retargeted to
        self.target: "Skeleton"

    @abc.abstractmethod
    def retarget(self, **options: Any) -> Optional[np.ndarray]:
        """
        Run retargeting of :attr:`source` animation to :attr:`target` skeleton
        """
        pass


def _retargeterFactory(
    name: str, source: "Animation", target: "Skeleton"
) -> Retargeter:
    implementations = _getImplementations("pynimation.anim.retarget")
    if name not in implementations:
        raise NotImplementedError("no such method " + name)
    else:
        return implementations[name](source, target)
