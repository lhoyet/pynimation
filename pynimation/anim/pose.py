from typing import Optional, Tuple, Union

import numpy as np

from pynimation.common.localglobal import LocalGlobal
from pynimation.common.transform import Transform

from .skeleton import Skeleton


class PoseTransform(Transform):
    """
    Mutidimensional array of transforms bound to a
    :class:`~pynimation.anim.skeleton.Skeleton` that allows access
    to transforms of joint :code:`j` (i.e. :code:`poseTransform[..., j.id, :, :])
    via its generic name (see :class:`~pynimation.anim.skeleton.SkeletalPart`)

    Examples
    --------
    >>> import pynimation.common.data as data_
    >>> from pynimation.anim.animation import Animation
    >>> from pynimation.anim.pose import Pose
    >>>
    >>> bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")
    >>>
    >>> animation = Animation.load(bvh_file)
    Finished: 25.414595000213012
    >>> isinstance(animation, Pose)
    True
    >>> lw = animation.skeleton.leftWrist
    >>> lw
    [16]LeftWrist (leftWrist)
        [17]LeftFinger0
            [18]Site
       <BLANKLINE>
    >>> np.all(animation.leftWrist == animation.node[lw])
    Animation(True)
    >>> np.all(animation.leftWrist == animation[..., lw.id, :, :]
    Animation(True)
    """

    def __init__(self, array: Optional[np.ndarray] = None, **kwargs):
        self.skeleton = kwargs["graph"] if "graph" in kwargs else None
        super().__init__(array, **kwargs)

    def _jointIdsFromNames(self, name: str) -> Union[Tuple[int, ...], int]:
        j = getattr(self.skeleton, name)
        if isinstance(j, tuple):
            return tuple([jj.id for jj in j])
        else:
            return j.id

    def __getattr__(self, name: str):
        if name in Skeleton.partsNames:
            j = self._jointIdsFromNames(name)
            return self[..., j, :, :].view(Transform)
        else:
            return super().__getattr__(name)

    def __setattr__(self, name: str, value):
        if name in Skeleton.partsNames:
            j = self._jointIdsFromNames(name)
            self[..., j, :, :] = value
        else:
            super().__setattr__(name, value)

    def __array_finalize__(self, obj):
        # print("Pose", "array finalize")

        if hasattr(obj, "skeleton"):
            self.skeleton = obj.skeleton
        else:
            self.skeleton = None

        super().__array_finalize__(obj)


class Pose(PoseTransform, LocalGlobal):
    """
    Multidimensional array of transforms, corresponding to skeleton poses.

    A single pose is a (njoint, 4, 4) shaped array, with global transforms,
    just like :class:`pynimation.common.localglobal.LocalGlobal`.

    Except that it is bound to a :class:`pynimation.anim.skeleton.Skeleton`
    instead of simply a :class:`pynimation.common.graph.Graph`

    This distinction is mainly syntactic and does not provide any functional
    difference

    The main added feature compared to
    :class:`pynimation.common.localglobal.LocalGlobal` is the access to
    transform of a joint via its generic name, through
    :class:`~pynimation.anim.pose.PoseTransform`
    """

    def __new__(
        cls,
        locals_: np.ndarray,
        skeleton: "Skeleton",
        autoUpdate: bool = True,
        autoUpdateGraph: bool = True,
    ):
        obj = super().__new__(
            cls,
            locals_,
            graph=skeleton,
            autoUpdate=autoUpdate,
            autoUpdateGraph=autoUpdateGraph,
        )
        return obj

    def __init__(
        self,
        locals_: np.ndarray,
        skeleton: "Skeleton",
        autoUpdate: bool = True,
        autoUpdateGraph: bool = True,
    ):
        """
        Parameters
        ----------

        locals_:
            numpy array of local transforms, shape ``(len(graph), 4, 4)``
        skeleton:
            skeleton of the pose, indexes transforms and determines global transforms
        autoUpdate:
            whether the global transforms should be updated when accessed, see
            :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdate`
        autoUpdateGraph:
            whether the animation array should be updated to reflect changes in the
            graph topology when they happen, see
            :attr:`~pynimation.common.localglobal.LocalGlobal.updateFromGraph` for more
        """
        self.skeleton = skeleton
        """
        skeleton of this array, indexes transforms and determines global transforms
        """
        super().__init__(
            locals_,
            graph=skeleton,
            autoUpdate=autoUpdate,
            autoUpdateGraph=autoUpdateGraph,
        )

    def __getattr__(self, name: str):
        if name in Skeleton.partsNames:
            j = self._jointIdsFromNames(name)
            return self.joint[j]
        else:
            return super().__getattr__(name)

    def __setattr__(self, name: str, value):
        if name in Skeleton.partsNames:
            j = self._jointIdsFromNames(name)
            self.joint[j] = value
        else:
            super().__setattr__(name, value)

    @property
    def njoints(self) -> int:
        """
        Number of joints of the :attr:`~pynimation.anim.pose.Pose.skeleton `
        of this pose

        Equivalent to :code:`self.shape[-3]`

        Note
        ----
        **Shorthand:** :attr:`~pynimation.anim.pose.Pose.nj`
        """
        return self.shape[-3]

    @classmethod
    def fromGraph(cls, skeleton: "Skeleton", **kwargs) -> "LocalGlobal":
        return cls(
            Transform.identity((len(skeleton),)), skeleton=skeleton, **kwargs
        )

    def _makeGlobals(self, shape: Tuple) -> "Transform":
        if hasattr(self, "skeleton"):
            skel = self.skeleton
        else:
            skel = None
        return PoseTransform.identity(shape[:-2], graph=skel)

    # shortcuts

    skel = LocalGlobal.graph
    skeleton = LocalGlobal.graph

    nj = njoints

    joint = LocalGlobal.node
    j = joint
