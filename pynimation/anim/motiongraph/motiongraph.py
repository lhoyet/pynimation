from typing import TYPE_CHECKING, List, Optional, Dict, Type

import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components

from pynimation.inout.binaryreader import BinaryReader
from pynimation.inout.binarywriter import BinaryWriter
from pynimation.inout.pynim.importer import PynimImporter
from pynimation.inout.pynim.exporter import PynimExporter
from pynimation.common.transform import Transform
from pynimation.common.quaternion import Quaternion

from pynimation.anim.metrics.similarity import Similarity
from pynimation.anim.metrics.global_position_velocity_no_root import (
    GlobalPositionVelocityNoRootMetric as CurrentMetric,
)

from pynimation.anim.pose import Pose

if TYPE_CHECKING:
    from pynimation.anim.metrics.metric import Metric
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton


class MotionGraph:
    """A basic Motion Graph Implementation

    Parameters
    ----------

    metric: Metric
        Implementation of :class:`~pynimation.similarity.metric.Metric` that
        will be used to compare animations to create the motion graph

    anims: Optional["List[Animation]"]
        List of animations to use to create the motion graph (animations can also be added to the motion graph afterwards)

    skeletonMap: Optional[List["Skeleton.SkeletalPart"]]
        Parts of the skeleton that should be included in the comparison

        If ``None``, all the joints of the skeleton will be included

        This is passed to :attr:`metric`'s
        :func:`~pynimation.similarity.metric.Metric.evaluate` function

    framerate: int
        Framerate at which all the animations will be played

    Attributes
    ----------

    metric: Metric
        Implementation of :class:`~pynimation.similarity.metric.Metric` that
        will be used to compare animations to create the motion graph

    anims: List of animations to use to create the motion graph (animations can also be added to the motion graph afterwards)

    skeletonMap: Optional[List["Skeleton.SkeletalPart"]]
        Parts of the skeleton that should be included in the comparison

        If ``None``, all the joints of the skeleton will be included

        This is passed to :attr:`metric`'s
        :func:`~pynimation.similarity.metric.Metric.evaluate` function

    framerate: int
        Framerate at which all the animations will be played

    threshold: float
        Threshold used to detect local minima in the similarity matrix between
        animations

    nodes: List[MotionGraph.MotionGraphNode]
        Nodes of the graph
    """

    class _AnimationData:
        def __init__(
            self, anim: "Animation", dataAnim: List[Quaternion], startIdx: int
        ):
            self.anim = anim
            self.dataAnim = dataAnim
            self.startidx = startIdx

    class MotionGraphNode:
        """
        A Node of :class:`~pynimation.motiongraph.motiongraph.MotionGraph`.
        References the frame of an animation and transitions to other nodes


        Parameters
        ----------

        frame: Frame
            frame referenced by this node

        id: int
            id of the node

        Attributes
        ----------

        frame: Frame
            frame referenced by this node

        id: int
            id of the node

        transitions: Dict[int, Transform]
            dictionary indexing the global transform of the root for each child
            to its id
        """

        def __init__(self, frame: "Animation", id: int):
            self.frame = frame
            self.transitions: Dict[int, Transform] = dict()
            self.id = id

        def addTransition(self, nodeidx: int, deltaTrans: Transform) -> None:
            """
            Add a transition to this node
            """
            self.transitions[nodeidx] = deltaTrans

    def __init__(
        self,
        metric: Optional[Type["Metric"]] = None,
        anims: Optional["List[Animation]"] = None,
        skeletonMap: Optional[List["Skeleton.SkeletalPart"]] = None,
        framerate: int = 60,
        threshold: float = 0.2,
    ):
        if metric is None:
            metric = CurrentMetric
        self.metric: Type["Metric"] = metric
        self.anims: List["Animation"] = []
        self.skeletonMap: Optional[List["Skeleton.SkeletalPart"]] = skeletonMap

        self.nodes: List[MotionGraph.MotionGraphNode] = []
        self._pruning: List[int] = []

        self.framerate: int = framerate

        self.threshold = threshold

        if anims is None:
            return

        self.skeleton = anims[0].skeleton

        for anim in anims:
            self._addMotion(anim)

        self._prune()

    def _addMotion(self, anim: "Animation") -> None:
        anim.setFramerate(self.framerate)
        anim.createProxy()
        anim.setRelativeRoot()

        animData: MotionGraph._AnimationData = MotionGraph._AnimationData(
            anim,
            self.metric.prepare(anim, None, self.skeletonMap),
            len(self.nodes),
        )

        self._addMotionNodes(animData.anim)
        self._addNewTransitions(animData)

        for _animData in self.anims:
            self._addNewTransitions(animData, _animData)

        self.anims.append(animData)

    def _addMotionNodes(self, anim: "Animation") -> None:
        firstNode = lastNode = len(self.nodes)
        for i in range(anim.nframes):
            fr = anim[i].view(Pose).copy()

            fr.root = Transform()
            self.nodes.append(MotionGraph.MotionGraphNode(fr, lastNode))

            if i > 0:
                self.nodes[lastNode - 1].addTransition(
                    lastNode, anim.root[i].view(Transform).copy()
                )

            lastNode += 1

        if anim.isCyclic():
            self.nodes[lastNode - 1].addTransition(
                firstNode + 1, anim.root[1].view(Transform).copy()
            )

    def _addNewTransitions(
        self,
        animData1: "MotionGraph._AnimationData",
        animData2: Optional["MotionGraph._AnimationData"] = None,
    ) -> None:
        if animData2 is None:
            similarity = Similarity(
                metric=self.metric,
                anim1=animData1.anim,
                dataAnim1=animData1.dataAnim,
                skeletonMap=self.skeletonMap,
            )
            animData2 = animData1
        else:
            similarity = Similarity(
                metric=self.metric,
                anim1=animData1.anim,
                dataAnim1=animData1.dataAnim,
                anim2=animData2.anim,
                dataAnim2=animData2.dataAnim,
                skeletonMap=self.skeletonMap,
            )
        similarity.gaussianSmooth()
        similarity.computeLocalMinima()
        similarity.toImage("test.png", self.threshold)

        for minLoc in similarity.getLocalMinima(self.threshold):
            fromAnim1 = animData1.startidx + minLoc[0]
            fromAnim2 = animData2.startidx + minLoc[1]
            deltaFromAnim1 = self.nodes[fromAnim1 - 1].transitions[fromAnim1]
            deltaFromAnim2 = self.nodes[fromAnim2 - 1].transitions[fromAnim2]

            self.nodes[fromAnim1 - 1].addTransition(fromAnim2, deltaFromAnim1)
            self.nodes[fromAnim2 - 1].addTransition(fromAnim1, deltaFromAnim2)

    def _prune(self) -> None:
        # Remove connected components of the graph that are not part of the
        # largest connected component.

        nbNodes = len(self.nodes)
        mgraph = np.zeros((nbNodes, nbNodes), dtype="uint8")
        for i in range(nbNodes):
            for t in self.nodes[i].transitions.keys():
                mgraph[i, t] = 1

        graph = csr_matrix(mgraph)
        n_components, labels = connected_components(
            csgraph=graph,
            directed=True,
            return_labels=True,
            connection="strong",
        )

        count = np.array(
            [np.count_nonzero(labels == i) for i in range(n_components)]
        )
        biggest_SCC = count.argmax()
        self._pruning = list(
            filter(lambda x: labels[x] == biggest_SCC, range(len(labels)))
        )

    def save(
        self,
        filename: str,
    ) -> None:
        """
        Export motion graph to mograph file `filename`

        Parameters
        ----------
        filename:
            file to save motion graph to

        """

        with BinaryWriter(filename) as bn:
            bn.writeUnsignedShortInt(self.framerate)

            PynimExporter.saveSkeleton(self.skeleton, bn)

            bn.writeInt(len(self._pruning))
            for idx in range(len(self._pruning)):
                node = self.nodes[self._pruning[idx]]
                PynimExporter.saveFrame(node.frame, bn)
                bn.writeInt(idx)

                newTrans = dict()
                for t in node.transitions.keys():
                    newTransId = list(
                        filter(
                            lambda x: self._pruning[x] == t,
                            range(len(self._pruning)),
                        )
                    )
                    if len(newTransId) > 0:
                        newTrans[newTransId[0]] = node.transitions[t]

                bn.writeInt(len(newTrans))
                for t in newTrans.keys():
                    bn.writeInt(t)
                    bn.writeVector(newTrans[t].position)
                    bn.writeVector(newTrans[t].rotation.quat)

    def load(
        self,
        filename: str,
    ) -> None:
        """
        Load motion graph from mograph file `filename`

        Parameters
        ----------
        filename:
            mograph file to load

        """

        with BinaryReader(filename) as bn:
            self.framerate = bn.readUnsignedShortInt()
            self.skeleton = PynimImporter.loadSkeleton(bn)
            self.skeleton.mapHumanoidSkeleton()

            nbNodes = bn.readInt()  # len(self.nodes)
            self.nodes = []
            for n in range(nbNodes):
                fr = Pose.identity(
                    (len(self.skeleton),), skeleton=self.skeleton
                )
                PynimImporter.loadFrame(bn, self.skeleton, fr)
                id = bn.readInt()
                self.nodes.append(MotionGraph.MotionGraphNode(fr, id))
                for t in range(bn.readInt()):
                    tIdx = bn.readInt()
                    deltaPos = bn.readVector(3)
                    deltaQuat = bn.readVector(4)
                    trans = Transform()
                    trans.rotation.quat = np.array(deltaQuat)
                    trans.position = deltaPos
                    self.nodes[n].addTransition(tIdx, trans)
