from typing import TYPE_CHECKING, List, Optional

import numpy as np

from .metric import Metric
from pynimation.common.quaternion import Quaternion

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton


class LocalOrientationNoRootMetric(Metric):
    """Similarity metric based on joints local orientation excluding the root joint"""

    @classmethod
    def prepare(
        cls,
        anim: "Animation",
        ids: Optional[np.ndarray] = None,
        skeletonMap: Optional[List["Skeleton.SkeletalPart"]] = None,
    ) -> np.ndarray:
        if ids is None:
            ids = np.arange(anim.nframes)
        else:
            ids = np.array(ids)

        subSkel = np.array(
            [
                j
                for j in anim.skeleton.getSubSkeleton(skeletonMap)
                if j != anim.skeleton.root.id
            ]
        )
        return anim[np.ix_(ids, subSkel)].rotation.quat

    @classmethod
    def evaluate(
        cls, dataFrame1: np.ndarray, dataFrame2: np.ndarray
    ) -> np.ndarray:
        q1i, q2 = Quaternion(dataFrame1).inverse(), Quaternion(dataFrame2)
        s = np.sum(
            np.minimum(
                (q1i * q2).log().norm(),
                (q1i * -q2).log().norm(),
            ),
            axis=-1,
        )
        return s
