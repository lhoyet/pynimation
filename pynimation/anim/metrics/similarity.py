from typing import TYPE_CHECKING, List, Optional, Union, Type, Tuple

import numpy as np
import math
from scipy.ndimage import gaussian_filter
import scipy.ndimage.filters as filters
import scipy.ndimage.morphology as morphology
from PIL import Image, ImageDraw


if TYPE_CHECKING:
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton
    from pynimation.anim.metrics.metric import Metric


class Similarity:
    """
    Uses implementations of :class:`~pynimation.similarity.metric.Metric` to
    compute a similarity metric between :attr:`anim1` and :attr:`anim2`


    Parameters
    ----------

    metric: Metric
        Implementation of :class:`~pynimation.similarity.metric.Metric` that
        will be used to compare animations

    anim1: Animation
        First animation to compare

    anim2: Optional[Animation]
        Second animation to compare. If None, :attr:`anim1` will be compared to
        itself

    dataAnim1: Optional[np.ndarray]
        data returned by
        :func:`~pynimation.similarity.metric.Metric.prepare` for ``anim1``

        If not given will be computed on initialisation

    dataAnim2: Optional[np.ndarray]
        data returned by
        :func:`~pynimation.similarity.metric.Metric.prepare` for ``anim2``

        If not given will be computed on initialisation

    skeletonMap: Optional[List["Skeleton.SkeletalPart"]]
        Parts of the skeleton that should be included in the comparison

        If ``None``, all the joints of the skeleton will be included

        This is passed to :attr:`metric`'s
        :func:`~pynimation.similarity.metric.Metric.evaluate` function

    Attributes
    ----------

    metric: Metric
        Implementation of :class:`~pynimation.similarity.metric.Metric` that
        will be used to compare animations

    anim1: Animation
        First animation to compare

    anim2: Optional[Animation]
        Second animation to compare. If None, :attr:`anim1` will be compared to
        itself

    skeletonMap: Optional[List["Skeleton.SkeletalPart"]]
        Parts of the skeleton that should be included in the comparison

        If ``None``, all the joints of the skeleton will be included

        This is passed to :attr:`metric`'s
        :func:`~pynimation.similarity.metric.Metric.evaluate` function

    similarities: np.ndarray
        Similarity matrix computed by :attr:`computeSimilarity()`

    selfSimilarity: bool
        Does this Similarity instance compare an animation to itself
    """

    def __init__(
        self,
        metric: Type["Metric"],
        anim1: "Animation",
        anim2: Optional["Animation"] = None,
        dataAnim1: Optional[np.ndarray] = None,
        dataAnim2: Optional[np.ndarray] = None,
        skeletonMap: Optional[List["Skeleton.SkeletalPart"]] = None,
    ):
        self.metric = metric
        self.anim1 = anim1
        self.anim2 = anim2
        if dataAnim1 is None:
            self._dataAnim1 = metric.prepare(anim1, None, skeletonMap)
        else:
            self._dataAnim1 = dataAnim1

        # Are we computing self-similarity
        self.selfSimilarity = self.anim2 is None
        if self.selfSimilarity:
            self.anim2 = self.anim1
            self._dataAnim2 = self._dataAnim1
        elif dataAnim2 is None:
            self._dataAnim2 = metric.prepare(anim2, None, skeletonMap)
        else:
            self._dataAnim2 = dataAnim2

        assert self.anim2 is not None

        self.similarities = np.empty(
            (self.anim1.nframes, self.anim2.nframes), dtype="float32"
        )
        axis = self._dataAnim1.shape.index(anim1.nframes)
        self.computeSimilarity(axis)

        self.localMinima: Optional[Tuple[np.ndarray, ...]] = None

    def computeSimilarity(self, axis: int = 0) -> None:
        """
        Compute similarity matrix between :attr:`anim1` and :attr:`anim2`
        according to :attr:`metric`. The resulting matrix is stored in the
        :attr:`similarities` attribute
        """
        for i in range(self.anim1.nframes):
            idx1 = (*[slice(None)] * axis, [i], Ellipsis)
            self.similarities[i, :] = self.metric.evaluate(
                self._dataAnim1[idx1],
                self._dataAnim2[...],
            )

    def gaussianSmooth(self, sigma: Union[int, np.ndarray] = 3) -> None:
        """
        Apply a Gaussian filter to the similarity matrix
        (``self.similarities``)

        Parameters
        ----------
        sigma:
            sigma parameter of the filter
        """
        self.similarities = gaussian_filter(self.similarities, sigma=sigma)

    def computeLocalMinima(self) -> None:
        """
        Compute the local minima of the similarity matrix

        Puts the result in ``self.localMinima``
        """
        # localMinimaX = np.transpose(argrelmin(self.similarities, axis=0))
        # localMinimaY = np.transpose(argrelmin(self.similarities, axis=1))

        # FROM https://stackoverflow.com/questions/3684484/peak-detection-in-a-2d-array/3689710#3689710

        # define a connected neighborhood
        # http://www.scipy.org/doc/api_docs/SciPy.ndimage.morphology.html#generate_binary_structure
        neighborhood = morphology.generate_binary_structure(
            len(self.similarities.shape), 2
        )

        # apply the local minimum filter; all locations of minimum value
        # in their neighborhood are set to 1
        # http://www.scipy.org/doc/api_docs/SciPy.ndimage.filters.html#minimum_filter
        local_min = (
            filters.minimum_filter(
                self.similarities, footprint=neighborhood, mode="constant"
            )
            == self.similarities
        )

        # local_min is a mask that contains the peaks we are looking for, but also the background.
        # In order to isolate the peaks we must remove the background from the mask.
        # we create the mask of the background
        background = self.similarities == 0

        # a little technicality: we must erode the background in order to
        # successfully subtract it from local_min, otherwise a line will
        # appear along the background border (artifact of the local minimum filter)
        # http://www.scipy.org/doc/api_docs/SciPy.ndimage.morphology.html#binary_erosion
        eroded_background = morphology.binary_erosion(
            background, structure=neighborhood, border_value=1
        )

        # we obtain the final mask, containing only peaks,
        # by removing the background from the local_min mask
        detected_minima = local_min ^ eroded_background
        self.localMinima = np.where(detected_minima)

    def getLocalMinima(self, threshold: float = 1.0) -> np.ndarray:
        """
        Return the local minima for a given threshold

        Parameters
        ----------
        threshold:
            threshold above which local minima are returned. Must be inside
            ``[0;1]`` range, corresponding the to ``[0;max(similarities)]`` range

        Returns
        -------
        np.ndarray:
            local minima of the similarity matrix above `threshold`
        """

        if self.localMinima is None:
            self.computeLocalMinima()

        assert self.localMinima is not None

        localMinValues = (
            self.similarities[self.localMinima] / self.similarities.max()
        )
        th = np.argwhere(localMinValues < threshold)

        return np.transpose(self.localMinima)[np.transpose(th)[0]]

    def toImage(
        self,
        imageFileName: Optional[str] = None,
        threshold=1.0,
        dotPercentSize=0.002,
    ) -> None:
        """
        Use PIL to convert similarity matrix to image

        Parameters
        ----------
        imageFileName:
            name of the file the image will be saved to, if None the image
            will be showed
        threshold:
            threshold used to compute the local minima
        dotPercentSize:
            size of the dots representing the local minima on the image
        """
        #
        I8 = (self.similarities / self.similarities.max() * 255).astype(
            np.uint8
        )
        img = Image.fromarray(I8)

        if self.localMinima is not None:
            img = img.convert("RGB")
            draw = ImageDraw.Draw(img)
            ellipseRadiusX = math.floor(
                len(self.similarities) * dotPercentSize
            )
            ellipseRadiusY = math.floor(
                len(self.similarities[0]) * dotPercentSize
            )
            for lMin in self.getLocalMinima(threshold):
                if ellipseRadiusX == 0 or ellipseRadiusY == 0:
                    draw.point((lMin[1], lMin[0]), "red")
                else:
                    draw.ellipse(
                        [
                            lMin[1] - ellipseRadiusY,
                            lMin[0] - ellipseRadiusX,
                            lMin[1] + ellipseRadiusY,
                            lMin[0] + ellipseRadiusX,
                        ],
                        fill="red",
                    )

        if imageFileName is None:
            img.show()
        else:
            img.save(imageFileName)
        img.close()
