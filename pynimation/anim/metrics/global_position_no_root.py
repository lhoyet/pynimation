from typing import TYPE_CHECKING, List, Optional

import numpy as np

from pynimation.common.vec import Vec3
from .metric import Metric

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton


class GlobalPositionNoRootMetric(Metric):
    """Similarity metric based on joints global positions excluding the root joint"""

    @classmethod
    def prepare(
        cls,
        anim: "Animation",
        ids: Optional[np.ndarray] = None,
        skeletonMap: Optional[List["Skeleton.SkeletalPart"]] = None,
    ) -> np.ndarray:
        if ids is None:
            ids = np.arange(anim.nframes)
        else:
            ids = np.array(ids)

        subSkel = np.array(
            [
                j
                for j in anim.skeleton.getSubSkeleton(skeletonMap)
                if j != anim.skeleton.root.id
            ]
        )

        rt = anim.globals[ids].root.inverse()

        gt = anim.globals[np.ix_(ids, subSkel)]

        p = (rt[:, np.newaxis] @ gt).position

        return p

    @classmethod
    def evaluate(
        cls, dataFrame1: np.ndarray, dataFrame2: np.ndarray
    ) -> np.ndarray:
        p1, p2 = Vec3(dataFrame1), Vec3(dataFrame2)
        s = np.sum((p1 - p2).norm(), axis=-1)
        return s
