from typing import TYPE_CHECKING, List, Optional

import numpy as np

from pynimation.common.quaternion import Quaternion

from .metric import Metric

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton


class LocalOrientationVelocityNoRootMetric(Metric):
    """Similarity metric based on joints local orientation and velocity excluding the root joint"""

    @classmethod
    def prepare(
        cls,
        anim: "Animation",
        ids: Optional[np.ndarray] = None,
        skeletonMap: Optional[List["Skeleton.SkeletalPart"]] = None,
    ) -> np.ndarray:
        if ids is None:
            ids = np.arange(anim.nframes)
        else:
            ids = np.array(ids)

        subSkel = np.array(
            [
                j
                for j in anim.skeleton.getSubSkeleton(skeletonMap)
                if j != anim.skeleton.root.id
            ]
        )

        pids, nids = [np.clip(ids + d, 0, anim.nframes - 1) for d in [-1, +1]]

        aq = anim[:, subSkel].rotation.quat
        pq, q, nq = aq[[pids, ids, nids], ...]
        pqi = pq.inverse()
        dq = pqi * nq
        dqneg = pqi * -nq
        sw = dq.log().norm() > dqneg.log().norm()
        dq[sw] = dqneg[sw]
        return np.stack([q, dq])

    @classmethod
    def evaluate(
        cls, dataFrame1: np.ndarray, dataFrame2: np.ndarray
    ) -> np.ndarray:
        q1i, q2 = Quaternion(dataFrame1).inverse(), Quaternion(dataFrame2)
        s = np.sum(
            np.minimum(
                (q1i * q2).log().norm(),
                (q1i * -q2).log().norm(),
            ),
            axis=-1,
        )
        return s[0] * 0.9 + s[1] * 0.1
