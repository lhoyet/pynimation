from typing import TYPE_CHECKING, List, Optional

import numpy as np

from pynimation.common.vec import Vec3
from .metric import Metric

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton


class GlobalPositionVelocityNoRootMetric(Metric):
    """Similarity metric based on joints global positions and velocity excluding the root joint"""

    @classmethod
    def prepare(
        cls,
        anim: "Animation",
        ids: Optional[np.ndarray] = None,
        skeletonMap: Optional[List["Skeleton.SkeletalPart"]] = None,
    ) -> np.ndarray:
        if ids is None:
            ids = np.arange(anim.nframes)
        else:
            ids = np.array(ids)

        subSkel = np.array(
            [
                j
                for j in anim.skeleton.getSubSkeleton(skeletonMap)
                if j != anim.skeleton.root.id
            ]
        )

        pids, nids = [np.clip(ids + d, 0, anim.nframes - 1) for d in [-1, +1]]
        deltat = np.full(pids.shape, 2 / anim.framerate)
        deltat[[0, -1]] /= 2

        art = anim.globals.root.inverse()
        prt, rt, nrt = art[[pids, ids, nids], ...]

        agt = anim.globals[:, subSkel]
        pgt, gt, ngt = agt[[pids, ids, nids], ...]

        p = (rt[:, np.newaxis] @ gt).position
        pp = (prt[:, np.newaxis] @ pgt).position
        np_ = (nrt[:, np.newaxis] @ ngt).position
        v = (np_ - pp) / deltat[:, np.newaxis, np.newaxis]

        return np.stack([p, v])

    @classmethod
    def evaluate(
        cls, dataFrame1: np.ndarray, dataFrame2: np.ndarray
    ) -> np.ndarray:
        v1, v2 = Vec3(dataFrame1), Vec3(dataFrame2)
        s = np.sum((v1 - v2).norm(), axis=-1)
        return 0.9 * s[0] + 0.1 * s[1]
