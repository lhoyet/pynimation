from typing import TYPE_CHECKING, List, Optional

import numpy as np

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation
    from pynimation.anim.skeleton import Skeleton


class Metric:
    """
    Base class for similarity metrics. Can be given to
    :class:`~pynimation.similarity.similarity.Similarity` to compute the
    similarity of two animations according to this metric
    """

    @classmethod
    def prepare(
        cls,
        anim: "Animation",
        ids: Optional[np.ndarray] = None,
        skeletonMap: Optional[List["Skeleton.SkeletalPart"]] = None,
    ) -> np.ndarray:
        """
        Prepare the data to be evaluated for frames :attr:`ids` of :attr:`anim`

        The data returned can then be passed to
        :func:`~pynimation.anim.metrics.metric.evaluated`

        Parameters
        ----------
        anim:
            animation to prepare
        ids:
            index of the frames to prepare in `anim`
        skeletonMap:
            Parts of the skeleton that should be included in the comparison

            If ``None``, all the joints of the skeleton will be included

        Returns
        -------
        The data to be used in the evaluate function of this metric for frames
        :attr:`ids` of :attr:`anim`
        """
        pass

    @classmethod
    def evaluate(
        cls, dataFrame1: np.ndarray, dataFrame2: np.ndarray
    ) -> np.ndarray:
        """
        Evaluate this metric on data from frame :attr:`dataFrame1` and :attr:`dataFrame2`

        Parameters
        ----------
        dataFrame1:
            data of the first frame to compare

        dataFrame2:
            data of the second frame to compare

        Returns
        -------
        The value of this metric for data of the frame :attr:`dataFrame1` and :attr:`dataFrame2`
        """
        pass
