from typing import List, Optional, Union

import math
import numpy as np

from pynimation.common.localglobal import LocalGlobal
from pynimation.common.transform import Transform
from pynimation.common.vec import Vec2
from pynimation.inout.exporter import save
from pynimation.inout.importer import load

from pynimation.anim.metrics.local_orientation_no_root import (
    LocalOrientationNoRootMetric as Metric,
)

from .pose import Pose
from .skeleton import Skeleton


class Animation(Pose):
    """
    Representation of an skeletal animation as an array of transforms of shape
    :code:`(nframes, njoints, 4, 4)`

    The transforms of the array represent the local transforms of each joint of
    the animation, for each frame

    For additional documentation on transform arrays, see
    :class:`~pynimation.common.transform.Transform`

    For additional info on global transforms, how they are updated, and how the
    array is updated when the skeleton changes, see
    :class:`~pynimation.common.localglobal.LocalGlobal`


    Warnings
    --------
    As of ``2.0``, animations derived from other animations will not work
    properly as numpy array do. By derivation, we mean everything that is not a
    view on the original animation, so results of operations on the base
    animation, and other manipulations that do not return a view on the data.
    There are two solutions to this issue, either create a new
    :class:`~pynimation.anim.animation.Animation` object from the derived array
    with the :func:`~pynimation.anim.animation.Animation.sameWith` or with
    the constructor. Or modify the data of the original animation in place, for
    example with :code:`animation[...]` if everything needs to be modified, or
    with :func:`~pynimation.anim.Animation.set` if the shape of the new
    array changes
    For example::

        >>> # animation1 will not work properly
        >>> animation1 = animation + 1

        >>> # solution 1
        >>> animation1 = animation.sameWith(animation+1)
        >>> # or
        >>> animation1 = Animation(animation+1, animation.skeleton,
                animation.framerate)

        >>> # solution 2
        >>> animation[...] = animation + 1
        >>> # or
        >>> animation.set(animation+1)

    The base pose of the skeleton is included in the animation. To remove it,
    use :func:`~pynimation.anim.Animation.removeBasePose`, and to add it
    back :func:`~pynimation.anim.Animation.addBasePose`. Be careful of the
    fact that the global transforms will not be computed correctly while the
    base pose is not included in the animation.

    Examples
    --------
    Load an animation from the animations provided with PyNimation:

    >>> import pynimation.common.data as data_
    >>> from pynimation.anim.animation import Animation
    >>>
    >>> bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")
    >>>
    >>> animation = Animation.load(bvh_file)
    Finished: 25.414595000213012

    Get transforms of legs at frame 0:

    >>> legs = animation.skeleton.legs
    >>> animation[0].legs
    Transform([[[ 0.984469  ,  0.14519467, -0.09868784, -0.085317  ],
                [-0.16337643,  0.9634514 , -0.21229588,  0.        ],
                [ 0.06425671,  0.22512198,  0.97220943,  0.        ],
                [ 0.        ,  0.        ,  0.        ,  1.        ]],
       <BLANKLINE>
               [[ 0.97976551, -0.01473578,  0.1996056 ,  0.085317  ],
                [-0.05925746,  0.93121829,  0.35961236,  0.        ],
                [-0.19117555, -0.3641639 ,  0.91150236,  0.        ],
                [ 0.        ,  0.        ,  0.        ,  1.        ]]])

    Add 10 degrees to the x-axis rotation of the right knee:

    >>> animation.rightKnee.rotation.eulerDeg.x += 10

    Get the global positions of the right knee and the right foot for the 10
    last frames:

    >>> joints = [animation.skeleton.rightKnee, animation.skeleton.rightAnkle]
    >>> animation[-10:].joint[joints].globals.position
    Vec3([[[-0.09675929,  0.45694473,  4.76443035],
           [-0.07263633,  0.13337867,  4.53890634]],
       <BLANKLINE>
          [[-0.0967254 ,  0.45518766,  4.77879293],
           [-0.07291749,  0.13663306,  4.54621146]],
       <BLANKLINE>
          [[-0.09662552,  0.45330248,  4.79429397],
           [-0.07292207,  0.14003428,  4.55462927]],
       <BLANKLINE>
          [[-0.09658413,  0.45128956,  4.81103244],
           [-0.07264365,  0.14354303,  4.5643405 ]],
       <BLANKLINE>
          [[-0.09684886,  0.44919553,  4.82906205],
           [-0.07218704,  0.14731742,  4.57529169]],
       <BLANKLINE>
          [[-0.09763912,  0.44706076,  4.84831965],
           [-0.07176305,  0.15157174,  4.58725518]],
       <BLANKLINE>
          [[-0.09900161,  0.44494864,  4.86881388],
           [-0.07145933,  0.15654209,  4.60011131]],
       <BLANKLINE>
          [[-0.10083015,  0.44290444,  4.890377  ],
           [-0.07129299,  0.16227553,  4.61376723]],
       <BLANKLINE>
          [[-0.10283736,  0.44102775,  4.912793  ],
           [-0.07112981,  0.1685643 ,  4.62837027]],
       <BLANKLINE>
          [[-0.10480213,  0.4393932 ,  4.9358949 ],
           [-0.07079034,  0.17505704,  4.64416286]]])
    """

    def __new__(
        cls,
        locals_: np.ndarray,
        skeleton: "Skeleton",
        framerate: int,
        autoUpdate: bool = True,
        autoUpdateGraph: bool = True,
    ):
        if len(locals_.shape) < 4:
            raise ValueError(
                "%s array should have a minimum of 4 dimensions (%d found)"
                % (
                    cls.__name__,
                    len(locals_.shape),
                )
            )
        obj = super().__new__(
            cls,
            locals_,
            skeleton=skeleton,
            autoUpdate=autoUpdate,
            autoUpdateGraph=autoUpdateGraph,
        )
        return obj

    def __init__(
        self,
        locals_: np.ndarray,
        skeleton: "Skeleton",
        framerate: int,  # default framerate ?
        autoUpdate: bool = True,
        autoUpdateGraph: bool = True,
    ):
        """
        Parameters
        ----------

        locals_:
            numpy array of animation local transforms, shape ``(nframes, njoints, 4, 4)``
        skeleton:
            skeleton of the animation, must have ``njoints`` joints
        framerate:
            framerate of the animation, in frames per second
        autoUpdate:
            whether the global transforms should be updated when accessed, see
            :attr:`~pynimation.anim.animation.Animation.autoUpdate`
        autoUpdateGraph:
            whether the animation array should be updated to reflect changes in the
            skeleton topology when they happen, see
            :attr:`~pynimation.anim.animation.Animation.autoUpdateGraph` for more
        """
        super().__init__(
            locals_,
            skeleton=skeleton,
            autoUpdate=autoUpdate,
            autoUpdateGraph=autoUpdateGraph,
        )
        self._framerate = framerate

    def __hash__(self) -> int:
        return id(self)

    # def __eq__(self, other: object):
    #     return isinstance(other, np.ndarray) and np.all(self == other)

    def __array_finalize__(self, obj):
        if hasattr(obj, "_framerate"):
            self._framerate = obj._framerate

        super().__array_finalize__(obj)

    def copy(self):
        c = self.__class__(
            np.ndarray.copy(self), self.graph.copy(), self.framerate
        )
        c.updateAllGlobals()
        return c

    @property
    def framerate(self) -> int:
        """
        framerate of the animation

        Note
        ----
        **Shorthand:** :attr:`~pynimation.anim.animation.Animation.frate`
        """
        return self._framerate

    @framerate.setter
    def framerate(self, newFrameRate: int) -> None:
        self._framerate = newFrameRate

    @property
    def nframes(self) -> int:
        """
        number of frames of the animation, equivalent to :code:`self.shape[-4]`

        Note
        ----
        **Shorthand:** :attr:`~pynimation.anim.animation.Animation.nf`
        """
        return self.shape[-4]

    @property
    def duration(self) -> float:
        """
        duration of the animation
        """
        return self.nframes / self.framerate

    @classmethod
    def fromSkeleton(
        cls, skel: "Skeleton", nframes: int, framerate: int, **kwargs
    ) -> "Animation":
        """
        Create an animation of :attr:`nframes` from :attr:`skel`, with
        :attr:`framerate`

        Parameters
        ----------
        skel:
            skeleton of the created animation
        nframes:
            number of frames of the created animation
        framerate:
            framerate of the created animation

        Returns
        -------
        Animation:
            Animation of shape :code:`(nframes, len(skel), 4, 4)` and framerate
            :attr:`framerate`
        """
        return cls(
            Transform.identity((nframes, len(skel))), skel, framerate, **kwargs
        )

    def idAt(self, times: np.ndarray) -> np.ndarray:
        """
        Computes which frames of the animation should be played at time
        `times` if the animation is repeated, return their ids

        Parameters
        ----------
        times:
            scalar or array of times for which frame are requested, if `times` exceeds the duration
            of the animation, a frame are returned for ``times % duration``

        Returns
        -------
        int:
            ids of the frames that should be played at `times`

        """
        duration = self.duration
        return np.floor(
            np.where(times < 0 or times > duration, times % duration, times)
            * self.framerate
        ).astype(int)

    def globalCycleIdAt(self, time: float, globalDuration: float) -> int:
        """
        Computes which frame of the animation should be played at time
        `time` if the animation is repeated, return its id in self.frames

        Parameters
        ----------
        time:
            time for which a frame is requested, if `time` exceeds the duration
            of the animation, a frame is returned for ``time % duration``

        globalDuration:
            set a maximum duration cycle

        Returns
        -------
        int
            id of the frame that should be played at `time`

        """
        return self.returnFrameIdInDomain(
            math.trunc(time % globalDuration * self.framerate)
        )

    def timeAt(self, frameId: int) -> float:
        return self.returnFrameIdInDomain(frameId) / self.framerate

    def returnFrameIdInDomain(self, frameId: int):
        if frameId < 0:
            return 0
        if frameId >= self.nframes:
            return self.nframes - 1
        else:
            return frameId

    def at(
        self, times: Union[float, np.ndarray], interpolate: bool = False
    ) -> "Transform":
        """
        Computes which frame of the animation should be played at times
        `times` if the animation is repeated

        Parameters
        ----------
        times:
            times for which frames are requested, if `times` exceeds the duration
            of the animation, frames are returned for ``times % duration``

        interpolate:
            if ``True``, interpolates the frame just before `times`
            and the one right after according to their times and the difference
            with `times`

        Returns
        -------
        Frame:
            the frames that should be played at times `times` if the animation is repeated

        Note
        ----
        Interpolation can be much more resource intensive and might not be
        suited for playing animations in real-times
        """
        times %= self.duration

        fID = times * self.framerate
        if interpolate:
            fIDMin = np.floor(fID).astype(int)
            fIDMax = fIDMin + 1
            f1 = self[..., fIDMin, :, :, :]
            f2 = self[..., fIDMax, :, :, :]
            w = (fIDMax - fID) / (fIDMax - fIDMin)
            # TODO implement
            return Transform.interpolate([f1, f2], np.array([w, 1.0 - w]))
        else:
            return self[..., np.floor(fID).astype("int32"), :, :, :]

    def setFramerate(self, framerate: int) -> None:
        # TODO: explain behaviour difference between cyclic and non-cyclic
        """
        Set the framerate of the animation, while keeping its duration

        Parameters
        ----------
        framerate:
            new framerate
        """

        if self.framerate == framerate:
            return

        oldNframes = self.nframes
        if self.isCyclic():
            # For cyclic motions we want to ensure that we keep the first and
            # last frame the same, and therefore timewarp the motion
            newNframes = (int)(
                round((oldNframes - 1) * framerate / self.framerate)
            ) + 1
            # frameToTime = self.getDuration() / (nbFrames - 1)
        else:
            newNframes = (int)(
                np.floor((oldNframes - 1) * framerate / self.framerate)
            ) + 1

        self.set(self.at(np.arange(newNframes) / framerate))
        self._framerate = framerate

    def isCyclic(self, threshold: float = 0.001) -> bool:
        """
        Determine if the animation is cyclic using
        :class:`~pynimation.anim.metrics.similarity.Similarity` metrics between
        the first and the last frame

        Parameters
        ----------
        threshold:
            similarity threshold

        Returns
        -------
        bool:
            wether the animation is cyclic
        """

        sim = Metric.evaluate(
            Metric.prepare(self, [0]),
            Metric.prepare(self, [-1]),
        )
        return sim < threshold

    def setXZOrigin(self, position: np.ndarray = Vec2.zeros(2)) -> None:
        """
        Set the initial position of the root joint on the XZ plane to
        `position`. The entire animation is shifted so that the root
        joint is at `position` on the first frame

        Parameters
        ----------
        position:
            array of 2 elements, the x and z positions
        """
        self.root.position.xz += position - self.root.position[0].xz

    def cycleOffset(self, time: float) -> "Transform":
        """
        Compute the offset transform of the root joint from the first frame of
        the first cycle to the first frame of current cycle at `time`.

        Essentially, get the offset of the current cycle at `time`

        Parameters
        ----------
        time:
            time for which to compute the offset transformation


        Returns
        -------
        Transform:
            offset transform for time `time` taking cycles into account
        """
        cycle = np.floor(time / self.duration)

        offsetTransform = self.root[-1] @ self.root[0].inverse()

        # TODO: numpy function to do matpow
        offset = Transform.identity()
        for i in range(cycle):
            offset = offset * offsetTransform

        return offset

    def xzFrameOffset(self, time: float) -> "Transform":
        """
        Compute offset transform of the root joint for XZ position and Y
        rotation from the initial frame of the current cycle to `time`.

        Essentially, get the offset of the frame at `time` inside its cycle

        Parameters
        ----------
        time:
            time for which the offset is computed

        Returns
        -------
        Transform:
            offset of the Frame at time `time` relative to the start of
            the cycle
        """
        time %= self.duration

        return Transform.xzOffset(
            self[0].globals.root, self[self.at(time)].globals.root
        )

    def xzCycleOffset(self, time: int) -> Transform:
        """
        Compute offset transform of the root joint for XZ position and Y
        rotation from the first frame of the first cycle to time `time`

        Parameters
        ----------
        time:
            time for which the offset is computed

        Returns
        -------
        Transform:
            offset of the animation at time `time`

        """
        cycle = np.floor(time / self.duration)
        offset = Transform.identity()

        offsetTransform = self.xzFrameOffset(self.duration)
        for i in range(cycle):
            offset = offset * offsetTransform

        return offset

    def removeBasePose(self) -> None:
        """
        Remove the skeleton base pose transforms from this animation's transforms

        Warnings
        --------
        Global transforms of the animation will not be computed correctly while
        the skeleton's base pose is not included in the animation's transforms
        (include it again with
        :func:`~pynimation.anim.animation.Animation.addBasePose`)
        """
        self[...] = self.skeleton.basePose.inverse() @ self

    def addBasePose(self) -> None:
        """
        Add the skeleton base pose transforms to this animation's transforms
        """
        self[...] = self.skeleton.basePose @ self

    def timewarp(self, newNframe: int) -> None:
        """
        Timewarp this animation so that the number of frame is
        `newFrameNumber` while maintaining the framerate

        This method will interpolate frame transforms so as to
        to insert more/fewer frames at relevant times

        Because the framerate stays the same it will effectively
        stretch or condense the animation in time

        Uses :func:`~pynimation.anim.animation.Animation.interp`

        Parameters
        ----------
        newNframe:
            number of frames the timewarped animation should have
        """

        newDuration = newNframe / self.framerate
        kt, nt = [
            np.linspace(0, newDuration, nframes)
            for nframes in (self.nframes, newNframe)
        ]
        newa = self.interp(kt)(nt)
        self.set(newa)

    @staticmethod
    def load(filename: str, **kwargs) -> Optional["Animation"]:
        """
        Load an animation from :attr:`filename`
        Calls :func:`~pynimation.inout.importer.load` and returns
        the first loaded animation if there is multiple (call
        :func:`~pynimation.anim.animation.Animation.loadMultiple` to load
        multiple animations from a single file)

        Parameters
        ----------
        filename:
            Absolute or relative (to current working directory) path to the
            animation file

        Returns
        -------
        Optional["Animation"]:
            animation loaded from :attr:`filename` or :code:`None` if no
            animations were found in this file

        Note
        ----
        To load animation files provided with the pynimation package
        (:code:`pynimation/data/animations`) use
        :func:`pynimation.common.data.getDataPath`
        """
        a = load(filename, **kwargs)
        if a is None or len(a) == 0:
            return None
        else:
            return a[0]

    @staticmethod
    def loadMultiple(filename: str, **kwargs) -> List["Animation"]:
        """
        Load multiple animations from :attr:`filename`
        Calls :func:`~pynimation.inout.importer.load`
        Use :func:`~pynimation.anim.animation.Animation.loadMultiple` to load
        multiple animations from a single file)

        Parameters
        ----------
        filename:
            Absolute or relative (to current working directory) path to the
            animation file

        Returns
        -------
        Optional["Animation"]:
            animation loaded from :attr:`filename` or :code:`None` if no
            animations were found in this file

        Note
        ----
        To load animation files provided with the pynimation package
        (:code:`pynimation/data/animations`) use
        :func:`pynimation.common.data.getDataPath` to get the full path
        """
        a = load(filename, **kwargs)
        if a is None:
            return []
        else:
            return a

    def save(self, filename: str, **kwargs) -> None:
        """
        Save animation to :attr:`filename`
        Calls :func:`~pynimation.inout.importer.save`

        Parameters
        ----------
        filename:
            Absolute or relative (to current working directory) path to save the
            animation file to
        """
        save(self, filename, **kwargs)

    @classmethod
    def fromGraph(  # type: ignore
        cls, skeleton: "Skeleton", nframes: int, **kwargs
    ) -> "LocalGlobal":
        return cls(
            Transform.identity(
                (
                    nframes,
                    len(skeleton),
                )
            ),
            skeleton=skeleton,
            **kwargs
        )

    def createProxy(self) -> None:
        """
        Create a proxy bone and set its animation the the XZ position and Y
        rotation of the current root
        """
        skel = self.skeleton

        with self.holdUpdateGraph() as _:
            with skel.transforms.holdUpdateGraph() as _:
                skel.createProxyBone()

        oroot = skel[1]
        skel.proxyBone.transform = oroot.transform.xzTransform()
        oroot.transform = skel.proxyBone.transform.inverse() @ oroot.transform

        self.proxyBone = self.joint[oroot].xzTransform()
        self.joint[oroot] = self.proxyBone.inv() @ self.joint[oroot]

    def destroyProxy(self) -> None:
        """
        Remove the proxy bone if there is one and apply its animation to the root
        """
        skel = self.skeleton
        oroot = skel[1]

        oroot.transform = skel.proxyBone.transform @ oroot.transform
        self.joint[oroot] = self.proxyBone @ self.joint[oroot]

        with self.holdUpdateGraph() as _:
            with skel.transforms.holdUpdateGraph() as _:
                skel.destroyProxyBone()

    def sameWith(self, array: np.ndarray) -> "Animation":
        return self.__class__(
            array,
            self.graph,
            self.framerate,
            self.autoUpdate,
            self.autoUpdateGraph,
        )

    def interpTime(self, ntimes: np.ndarray) -> "Animation":
        """
        Interpolate animation from current frame times to :attr:`ntimes`

        Parameters
        ----------
        ntimes:
            times to interpolate the animation at

        Return
        ------
        Animation:
            Animation interpolated to :attr:`ntimes`. Shape is
            :code:`(len(ntimes), self.njoints, 4, 4)`
        """
        kt = np.linspace(0, self.duration, self.nframes)
        return self.interp(kt)(ntimes)

    def retarget(
        self, target: "Skeleton", method: Optional[str] = "kulpa", **options
    ):
        """
        Retarget animation to :attr:`target` skeleton using method :`method`.

        Options of the chosen method can be passed directly

        Parameters
        ----------
        target:
            skeleton to retarget the animation to
        method:
            method used to retarget, corresponds to the :code:`name`
            static variable of
            :class:`~pynimation.anim.retarget.retargeter.Retargeter` implementations
        options:
            options to be passed to the retargeter (See documentations of
            :class:`~pynimation.anim.retarget.retargeter.Retargeter`
            implementations)
        """
        import pynimation.anim.retarget.retargeter as retar

        retar._retargeterFactory(method, self, target).retarget(**options)

    def setRelativeRoot(self) -> None:
        """
        Make the root joint global transforms for each frame relative the one
        of the previous frame
        """
        ids = np.arange(1, self.nframes)
        self.root[ids] = self.root[ids - 1].inverse() @ self.root[ids]

    def setAbsoluteRoot(self) -> None:
        """
        Make the root joint global transforms for each frame relative to the
        one of the first frame
        """
        for fr in range(1, self.nframes):
            self.root[fr] = self.root[fr - 1] @ self.root[fr]

    def detectSupports(
        self, method: Optional[str] = "circlefit", *options, **kwoptions
    ) -> np.ndarray:
        """
        Detect supports of animation using method `method`.

        Options of the chosen method can be passed directly

        Parameters
        ----------
        method:
            method used to detect supports, corresponds to the ``name``
            static variable of :class:`~pynimation.anim.detect_supports.supports_detecter.SupportsDetecter` implementations
        options:
            options to be passed to the retargeter (See documentations of
            :class:`~pynimation.anim.detect_supports.supports_detecter.SupportsDetecter`
            implementations)

        Returns
        -------
        np.ndaray
            shape: (supports_count, frame_count)

            For each support joint, an array of boolean
            indicating if this joint is a support during this frame
        """
        import pynimation.anim.detect_supports.supports_detecter as detect_supports

        return detect_supports._supportsDetecterFactory(
            method, self
        ).detectSupports(*options, **kwoptions)

    # shortcuts

    frate = framerate

    nf = nframes
