import abc
import importlib
import inspect
import pkgutil
from typing import Dict, List


def _get_classes_rec(module_name: str, classes: List[Dict[str, str]]):
    module = importlib.import_module(module_name)

    for name, cls in inspect.getmembers(module, inspect.isclass):
        if cls.__module__ == module_name:
            classes.append({"name": name, "module": module_name, "class": cls})

    if (
        module.__spec__ is None
        or module.__spec__.submodule_search_locations is None
    ):
        return
    else:
        search_locations = module.__spec__.submodule_search_locations
    for module_loader, name, ispkg in pkgutil.iter_modules(search_locations):
        if name == "setup":
            continue
        _get_classes_rec(module_name + "." + name, classes)


def _getImplementations(moduleName):
    subclasses = []
    _get_classes_rec(moduleName, subclasses)
    implems = {
        c["class"].name: c["class"]
        for c in subclasses
        if abc.ABC not in c["class"].__bases__ and hasattr(c["class"], "name")
    }
    return implems
