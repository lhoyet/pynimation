from typing import List, Callable, cast

import numpy as np

from .matrix import Matrix
from .rotation import Rotation
from .vec import Vec, Vec3, Vec4


class Transform(Matrix):
    """
    Base class for multi-dimensional arrays of 4x4 transform matrices
    (3d with homogeneous coordinates)

    All operations are performed on the last axis

    Rotation, position and scale of the transform can be access and assigned
    via :attr:`~pynimation.common.transform.Transform.rotation`,
    :attr:`~pynimation.common.transform.Transform.position`,
    :attr:`~pynimation.common.transform.Transform.scale`

    PyNimation uses columns-first representation, so the rotation is the
    left most 3x3 matrix and the homogeneous position is the 4th column

    Examples
    --------

    Create (2,2) shaped array of identity transform matrices:

    >>> from pynimation.common.transform import Transform
    >>> import numpy as np
    >>> np.set_printoptions(suppress=True, precision=6)
    >>>
    >>> t = Transform.identity((2,2))
    >>> t
    Transform([[[[1., 0., 0., 0.],
                 [0., 1., 0., 0.],
                 [0., 0., 1., 0.],
                 [0., 0., 0., 1.]],
       <BLANKLINE>
                [[1., 0., 0., 0.],
                 [0., 1., 0., 0.],
                 [0., 0., 1., 0.],
                 [0., 0., 0., 1.]]],
       <BLANKLINE>
       <BLANKLINE>
               [[[1., 0., 0., 0.],
                 [0., 1., 0., 0.],
                 [0., 0., 1., 0.],
                 [0., 0., 0., 1.]],
       <BLANKLINE>
                [[1., 0., 0., 0.],
                 [0., 1., 0., 0.],
                 [0., 0., 1., 0.],
                 [0., 0., 0., 1.]]]])

    Set the y position of the transforms to 2 (equivalent to :code:`t[...,1,3] = 2`):

    >>> t.position.y = 2
    >>> t.position
    Vec3([[[0., 2., 0.],
           [0., 2., 0.]],
       <BLANKLINE>
          [[0., 2., 0.],
           [0., 2., 0.]]])
    >>> t
    Transform([[[[1., 0., 0., 0.],
                 [0., 1., 0., 2.],
                 [0., 0., 1., 0.],
                 [0., 0., 0., 1.]],
       <BLANKLINE>
                [[1., 0., 0., 0.],
                 [0., 1., 0., 2.],
                 [0., 0., 1., 0.],
                 [0., 0., 0., 1.]]],
       <BLANKLINE>
       <BLANKLINE>
               [[[1., 0., 0., 0.],
                 [0., 1., 0., 2.],
                 [0., 0., 1., 0.],
                 [0., 0., 0., 1.]],
       <BLANKLINE>
                [[1., 0., 0., 0.],
                 [0., 1., 0., 2.],
                 [0., 0., 1., 0.],
                 [0., 0., 0., 1.]]]])

    Set the x euler angle of rotation of the transforms to 90 degrees:

    >>> t.rotation.eulerDeg.x = 90
    >>> t.rotation.eulerDeg
    Vec3([[[90.,  0.,  0.],
           [90.,  0.,  0.]],
       <BLANKLINE>
          [[90.,  0.,  0.],
           [90.,  0.,  0.]]])
    >>> t
    Transform([[[[ 1.,  0.,  0.,  0.],
                 [ 0.,  0., -1.,  2.],
                 [ 0.,  1.,  0.,  0.],
                 [ 0.,  0.,  0.,  1.]],
       <BLANKLINE>
                [[ 1.,  0.,  0.,  0.],
                 [ 0.,  0., -1.,  2.],
                 [ 0.,  1.,  0.,  0.],
                 [ 0.,  0.,  0.,  1.]]],
       <BLANKLINE>
       <BLANKLINE>
               [[[ 1.,  0.,  0.,  0.],
                 [ 0.,  0., -1.,  2.],
                 [ 0.,  1.,  0.,  0.],
                 [ 0.,  0.,  0.,  1.]],
       <BLANKLINE>
                [[ 1.,  0.,  0.,  0.],
                 [ 0.,  0., -1.,  2.],
                 [ 0.,  1.,  0.,  0.],
                 [ 0.,  0.,  0.,  1.]]]])

    See :class:`~pynimation.common.matrix.Matrix` for examples of matrix by
    matrix and matrix by vector multiplication
    """

    _dsize = 4

    @property
    def position(self) -> Vec3:
        """
        Position of the transforms viewed as a :class:`~pynimation.common.vec.Vec3` array

        Equivalent to :code:`self[...,:3,3]`

        This is a view of the transform array, so any modification will affect
        the transform array

        Assigning to this property will assign to the position part of the
        transform array

        Returns
        -------
        Vec3:
            Position of the transforms

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.pos`,
        :attr:`~pynimation.common.transform.Transform.p`
        """
        return self[..., :3, 3].view(Vec3)

    @position.setter
    def position(self, newPosition: np.ndarray) -> None:
        self[..., :3, 3] = newPosition

    @property
    def homPosition(self) -> Vec4:
        """
        Homogeneous position of the transforms viewed as a :class:`~pynimation.common.vec.Vec4` array

        Equivalent to :code:`self[...,3]`

        This is a view of the transform array, so any modification will affect
        the transform array

        Assigning to this property will assign to the 4th column of the
        transform array

        Returns
        -------
        Vec3:
            Homogeneous position of the transforms

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.hpos`,
        :attr:`~pynimation.common.transform.Transform.hp`
        """
        return self[..., 3].view(Vec4)

    @homPosition.setter
    def homPosition(self, newHomPosition: np.ndarray) -> None:
        self[..., 3] = newHomPosition

    @property
    def rotation(self) -> Rotation:
        """
        Rotation of the transforms viewed as a
        :class:`~pynimation.common.rotation.Rotation` array

        Equivalent to :code:`self[...,:3,:3]`

        This is a view of the transform array, so any modification will affect
        the transform array

        Assigning to this property will assign to the left-most 3x3 matrices of the
        transform array

        Returns
        -------
        Rotation:
            rotation of the transforms

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.rot`,
        :attr:`~pynimation.common.transform.Transform.r`
        """
        return self[..., :3, :3].view(Rotation)

    @rotation.setter
    def rotation(self, newRotation: np.ndarray) -> None:
        self[..., :3, :3] = newRotation

    @property
    def scale(self) -> Vec3:
        """
        Scale of the transforms viewed as a
        :class:~pynimation.common.vec.Vec3` array

        Assigning to this property will scale the whole transform array

        Returns
        -------
        Vec3:
            scale of the transforms

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.sca`,
        :attr:`~pynimation.common.transform.Transform.s`
        """
        return Vec3(np.linalg.norm(self.T, axis=-1)[..., :3])

    @scale.setter
    def scale(self, newScale: np.ndarray) -> None:
        self.rotation @= Matrix.asDiag(self.scale).inv()  # type:ignore
        self.rotation @= Matrix.asDiag(newScale)  # type:ignore

    @property
    def homScale(self) -> Vec4:
        """
        Homogeneous scale of the transforms viewed as a
        :class:~pynimation.common.vec.Vec4` array

        Assigning to this property will scale the whole transform array

        Returns
        -------
        Vec3:
            homogeneous scale of the transforms

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.hsca`,
        :attr:`~pynimation.common.transform.Transform.hs`
        """
        return Vec4(np.linalg.norm(self.T, axis=-1))

    @homScale.setter
    def homScale(self, newHomScale: np.ndarray) -> None:
        self[...] = (
            self
            @ Matrix.asDiag(self.homScale).inv()
            @ Matrix.asDiag(newHomScale)
        )

    def positionTransform(self) -> "Transform":
        """
        This array of transforms where the rotation matrices
        have been set to the identity

        Returns
        -------
        Transform:
            An array of transforms from this array's positions

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.posTr`,
        """
        t = self.copy()
        t.rotation.clear()
        return t

    def rotationTransform(self) -> "Transform":
        """
        This array of transforms with positions at 0

        Returns
        -------
        Transform:
            An array of transforms from this array's rotations

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.rotTr`,
        """
        t = self.copy()
        t.hpos = Vec4.w
        return t

    def scaleTransform(self) -> "Transform":
        """
        This array of transforms with only scales

        Returns
        -------
        Transform:
            An array of transforms from this array's scales

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.scaTr`,
        """
        t = self.copy()
        t.clear()
        t.rotation.diag = self.scale
        return t

    def translate(self, trans: np.ndarray, local: bool = False) -> None:
        """
        Translate transforms of this array by vector array :attr:`trans`

        If :attr:`local` is :code:`True`, :attr:`trans` is expressed in world
        coordinates, otherwise in the transforms coordinates

        Parameters
        ----------
        trans:
            array of translation vectors
        local:
            whether :attr:`trans` is expressed in local coordinates
        """
        trans = trans if isinstance(trans, Vec) else Vec(trans)
        if local:
            trans = self.rotation @ trans
        self.position += trans.squeeze()  # type: ignore

    # TODO: verify math
    def rotate(self, rotation: np.ndarray, local: bool = False) -> None:
        """
        Translate transforms of this array by rotation matrix array :attr:`rotation`

        If :attr:`local` is :code:`True`, :attr:`rotation` is expressed in world
        coordinates, otherwise in the transforms coordinates

        Parameters
        ----------
        rotation:
            array of rotation matrices
        local:
            whether :attr:`rotation` is expressed in local coordinates
        """
        if local:
            self.rotation[...] = self.rotation @ rotation.squeeze()
        else:
            self.rotation[...] = rotation.squeeze() @ self.rotation

    @staticmethod
    def fromRotation(rotation: np.ndarray) -> "Transform":
        """
        Create transform array from rotation matrix array :attr:`rotation`

        Parameters
        ----------
        rotation:
            rotation array to create transform array from

        Returns
        -------
        Transform:
            transform array where the rotation is :attr:`rotation`

            shape is :code:`(*rotation.shape[:-2], 4, 4)`

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.fromRot`,
        """
        t = Transform.empty(
            (*rotation.shape[:-2], *(Transform._dsize,) * Transform._nd)
        )
        t.rotation = rotation  # type:ignore
        t.position = 0
        t.rows.w = Vec4.w
        return t

    @staticmethod
    def fromPosition(position: np.ndarray) -> "Transform":
        """
        Create transform array from position vector array :attr:`position`

        Parameters
        ----------
        position:
            position array to create transform array from

            can be array of 3d or 4d vectors

        Returns
        -------
        Transform:
            transform array where the position is :attr:`position`

            shape is :code:`(*position.shape[:-1], 4, 4)`

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.transform.Transform.fromPos`,
        """
        t = Transform.empty(
            (*position.shape[:-1], *(Transform._dsize,) * Transform._nd)
        )
        t.rotation = Rotation.identity()
        if position.shape[-1] == 4:
            t.homPosition = position  # type: ignore
            t.rows.w.xyz = 0
        elif position.shape[-1] == 3:
            t.position = position  # type: ignore
            t.rows.w = Vec4.w
        else:
            raise ValueError(
                "Input positions should be of shape [..., 3] or [..., 4], got [...,%d]"
                % position.shape[-1]
            )
        return t

    @staticmethod
    def xzOffset(tFrom: np.ndarray, tTo: np.ndarray):
        """
        Compute offset transformation from :attr:`tFrom` to
        :attr:`tTo` with regard to the XZ position and Y rotation

        Parameters
        ----------
        tFrom:
            base transforms of the offset

        tTo:
            target transforms of the offset

        Returns
        -------
        Transform:
            offset transforms according to xz position and y rotation
        """
        t: List["Transform"] = [
            (d if isinstance(d, Transform) else Transform(d))
            for d in [tFrom, tTo]
        ]

        xFrom, xTo = [tr.rotation.cols.x.copy() for tr in t]  # type: ignore
        xFrom.y = 0
        xTo.y = 0
        angle = Rotation.angleBetween(xFrom, xTo, False, Vec3.y)

        offPos = (t[0].inverse() @ t[1]).position  # type: ignore
        offPos.y = 0

        tr = Transform.empty(tFrom.shape)
        tr.rotation.euler.y = angle
        tr.position = offPos
        tr.rows.w = Vec4.w
        return tr

    def xzTransform(self) -> "Transform":
        """
        Extract a transform array of XZ positions and Y rotation of this array
        of transforms

        Returns
        -------
        Transform:
            An array of transform with this array's XZ position and Y rotation
        """
        return Transform.xzOffset(Transform.identityLike(self), self)

    # vectorize ?
    def projection(
        self,
        viewWidth: int,
        viewHeight: int,
        fov: int,
        near: float,
        far: int,
    ) -> None:
        """
        Set this transform to the projection matrix for the frustum of
        dimensions `viewWidth`, `viewHeight`, `fov`, `near` and `far`

        Parameters
        ----------
        viewWidth:
            width of the viewport in pixels
        viewHeight:
            height of the viewport in pixels
        fov:
            field of view
        near:
            distance of the near plane
        far:
            distance of the far plane
        """
        aspect = viewWidth / viewHeight
        fovRad = np.radians(fov)
        range = np.tan(fovRad / 2.0) * near

        sx = (2.0 * near) / (range * aspect + range * aspect)
        sy = near / range
        sz = -(far + near) / (far - near)
        pz = -(2.0 * far * near) / (far - near)

        self.clear()
        self[..., 0, 0] = sx
        self[..., 1, 1] = sy
        self[..., 2, 2] = sz
        self[..., 2, 3] = pz
        self[..., 3, 2] = -1.0

    def lookAt(
        self,
        cameraPos: "Vec3",
        targetPos: "Vec3",
        upVector: "Vec3",
    ) -> None:
        """
        Set this transform to a view matrix using the opengl lookAt style. COLUMN ORDER.

        Parameters
        ----------
        cameraPos:
            position of the camera in 3D space, expected as an array of 3
            elements in "XYZ" order
        targetPos:
            position of the target to look at, expected as an array of 3
            elements in "XYZ" order
        upVector:
            vector indicating the roll rotation, expected as an array of 3
            elements in "XYZ" order
        """
        self.clear()
        p = Transform()
        p.position = -cameraPos  # type: ignore
        ori = Transform()
        ori.lookAtOrientation(cameraPos, targetPos, upVector)
        self[...] = ori @ p

    def lookAtOrientation(
        self,
        cameraPos: np.ndarray,
        targetPos: np.ndarray,
        upVector: np.ndarray,
    ) -> None:
        """
        Same as :func:`~pynimation.common.transform.Transform.lookAt`
        but does not set the position
        """
        fwd = Vec3(targetPos - cameraPos).normalized()  # distance vector
        right = fwd.cross(upVector).normalized()  # right vector
        up = right.cross(fwd).normalized()

        self.rotation.columns.x = right
        self.rotation.columns.y = up
        self.rotation.columns.z = -fwd

    def sameWith(self, array) -> "Transform":
        return self.__class__(array)

    def interp(
        self, ktimes: np.ndarray
    ) -> Callable[[np.ndarray], "Transform"]:
        """
        Interpolation of this array of transforms, with every
        transform of the last axis corresponding to a key time of :attr:`ktimes`

        Performs slerp on rotations and lerp on positions

        Returns a function that can be called with new times to interpolate
        transforms at, similar to scipy's :code:`interpolate.interp1d`

        Parameters
        ----------
        ktimes:
            Key times of the transforms

        Returns
        -------
        Callable[[np.ndarray], "Transform"]:
            Function that can be called with times to interpolated transforms at
        """
        lerp = self.homPosition.lerp(ktimes)
        slerp = self.rotation.slerp(ktimes)

        def func(ntimes: np.ndarray) -> "Transform":
            t = self.sameWith(np.empty((len(ntimes), *self.shape[1:])))
            # FIXME use t.w.xyz = 0 when it is fixed
            t[..., 3, :3] = 0
            t.homPosition = cast(Vec4, lerp(ntimes))
            t.rotation = slerp(ntimes)
            return t

        return func

    # shortcuts

    rot = rotation
    r = rotation

    pos = position
    p = position

    hpos = homPosition
    hp = homPosition

    sca = scale
    s = scale

    hsca = homScale
    hs = homScale

    fromPos = fromPosition
    fromRot = fromRotation

    posTr = positionTransform
    rotTr = rotationTransform
    scaTr = scaleTransform
