import functools
import re

import numpy as np
import pytest
from common_tests import AttrsTest, GetSetTest
from scipy.spatial.transform import Rotation as scipyRot
from test_matrix import IdentityTest

import pynimation.common.vec as vec
from pynimation.common.rotation import Rotation

SHAPE = (10, 10, 3, 3)


@pytest.fixture
def r(shape_=SHAPE):
    return Rotation(np.ones(shape_))


@pytest.fixture
def rrand(shape_=SHAPE):
    len_ = functools.reduce(lambda a, b: a * b, shape_[:-2])
    return Rotation(scipyRot.random(len_).as_matrix().reshape(shape_))


@pytest.mark.parametrize(
    "obj",
    [
        pytest.lazy_fixture("r"),  # type: ignore
    ],
)
@pytest.mark.parametrize(
    ("attr", "type_"),
    [
        ("euler", vec.Vec3),
        ("eulerDeg", vec.Vec3),
        ("rotvec", vec.Vec3),
        ("quat", vec.Vec4),
    ],
)
class TestRotationAttrs(AttrsTest):
    pass


@pytest.mark.parametrize(
    "shape",
    [
        (0,),
        (1,),
        (3,),
    ],
)
def testNotEnoughDimsRaisesError(shape):
    with pytest.raises(
        ValueError,
        match=re.escape(
            "Rotation array should have at least %d dimensions (%d found)"
            % (Rotation._nd, len(shape))
        ),
    ):
        Rotation(np.empty(shape))


@pytest.mark.parametrize(
    "shape",
    [
        (1, 1),
        (2, 2),
        (4, 4),
        (5, 5),
        (6, 6),
        (2, 3),
        (2, 4),
        (3, 2),
        (1, 4),
        (2, 2, 2),
        (3, 4, 4),
        (3, 3, 2),
        (3, 1, 3),
        (3, 4, 2),
        (2, 2, 2, 2),
        (2, 2, 4, 4),
        (2, 3, 2, 3),
        (2, 3, 3, 2),
    ],
)
def testWrongShapeRaisesError(shape):
    with pytest.raises(
        ValueError,
        match=re.escape(
            "Rotation array should be of shape (...,3,3) and not %s"
            % (str(shape),)
        ),
    ):
        Rotation(np.empty(shape))


@pytest.mark.parametrize(
    "shape",
    [
        (3, 3),
        (1, 3, 3),
        (2, 3, 3),
        (1, 1, 3, 3),
        (3, 2, 3, 3),
        (1, 1, 1, 3, 3),
        (2, 2, 2, 3, 3),
    ],
)
def testRightShapeDoesntRaiseError(shape):
    try:
        Rotation(np.empty(shape))
    except ValueError as e:
        pytest.fail("raised shape error when it shouldn't have %s" % e)


@pytest.mark.parametrize(
    ("func", "fromFunc"),
    [
        (lambda r: r.euler, lambda r: Rotation.fromEuler(r)),
        (lambda r: r.eulerDeg, lambda r: Rotation.fromEuler(r, degrees=True)),
        (lambda r: r.rotvec, lambda r: Rotation.fromRotVec(r)),
        (lambda r: r.quat, lambda r: Rotation.fromQuat(r)),
    ],
)
@pytest.mark.parametrize(
    "shape_", [(*(10,) * 5, 3, 3)[-i:] for i in range(1, 7 + 1)]
)
class TestConversions:
    def testShape(self, rrand, shape_, func, fromFunc):
        assert func(rrand).shape[:-1] == rrand.shape[:-2]

    def testShapeFrom(self, rrand, shape_, func, fromFunc):
        assert func(rrand).shape[:-1] == rrand.shape[:-2]

    def testEqual(self, rrand, shape_, func, fromFunc):
        assert np.allclose(rrand, fromFunc(func(rrand)))

    def testEqualIndiv(self, rrand, shape_, func, fromFunc):
        a = func(rrand)
        for idx in np.ndindex(rrand.shape[:-2]):
            assert np.allclose(fromFunc(a[idx]), rrand[idx])


@pytest.mark.parametrize(
    "obj",
    [
        pytest.lazy_fixture("r"),  # type: ignore
    ],
)
@pytest.mark.parametrize("shape_", [SHAPE])
class TestRotationGetSet(GetSetTest):
    pass


@pytest.mark.parametrize(
    "inputType",
    [lambda a: np.array(a), lambda a: vec.Vec(np.array(a))],
)
class TestUnsignedAngleBetween:
    @pytest.mark.parametrize(
        ("size"),
        [(), (2,), (2, 2), (2, 2, 2)],
    )
    @pytest.mark.parametrize(
        ("nelem"),
        [2, 3],
    )
    @pytest.mark.parametrize(
        ("degrees"),
        [True, False],
    )
    @pytest.mark.parametrize(
        ("a", "b", "o"),
        [
            ([0, 1, 0], [1, 0, 0], 90),
            ([1, 0, 0], [0, 1, 0], 90),
            ([1, 0, 0], [-1, 0, 0], 180),
            ([0, 1, 0], [0, -1, 0], 180),
            ([1, 0, 0], [np.sqrt(2) / 2, np.sqrt(2) / 2, 0], 45),
            ([0, 1, 0], [np.sqrt(2) / 2, np.sqrt(2) / 2, 0], 45),
        ],
    )
    def testCorrectOutput(self, inputType, a, b, o, degrees, nelem, size):
        a, b = [np.full((*size, nelem), inputType(d)[:nelem]) for d in [a, b]]
        if not degrees:
            o = np.radians(o)
        o = np.full(size, o)
        assert np.allclose(Rotation.angleBetween(a, b, degrees=degrees), o)


@pytest.mark.parametrize(
    "inputType",
    [lambda a: np.array(a), lambda a: vec.Vec(np.array(a))],
)
class TestSignedAngleBetween:
    @pytest.mark.parametrize(
        ("size"),
        [(), (2,), (2, 2), (2, 2, 2)],
    )
    @pytest.mark.parametrize(
        ("degrees"),
        [True, False],
    )
    @pytest.mark.parametrize(
        ("a", "b", "c", "o"),
        [
            ([1, 0, 0], [0, 1, 0], [0, 0, 1], 90),
            ([1, 0, 0], [0, 1, 0], [0, 0, -1], -90),
            ([0, 1, 0], [1, 0, 0], [0, 0, 1], -90),
            ([0, 1, 0], [1, 0, 0], [0, 0, -1], 90),
            ([1, 0, 0], [-1, 0, 0], [0, 0, 1], 180),
            ([0, 1, 0], [0, -1, 0], [0, 0, 1], 180),
            ([1, 0, 0], [np.sqrt(2) / 2, np.sqrt(2) / 2, 0], [0, 0, 1], 45),
            ([0, 1, 0], [np.sqrt(2) / 2, np.sqrt(2) / 2, 0], [0, 0, 1], -45),
        ],
    )
    def testCorrectOutput(self, inputType, a, b, c, o, degrees, size):
        a, b, c = [np.full((*size, 3), inputType(d)) for d in [a, b, c]]
        if not degrees:
            o = np.radians(o)
        o = np.full(size, o)
        assert np.allclose(
            Rotation.angleBetween(a, b, degrees=degrees, rotAxis=c), o
        )


@pytest.mark.parametrize(
    "cls",
    [Rotation],
)
class TestRotationIdentity(IdentityTest):
    pass


@pytest.mark.parametrize(
    "name",
    vec.Vec._elements[:3],
)
@pytest.mark.parametrize("degrees", [True, False])
class TestStaticEuler:
    elements = vec.Vec._elements[:3]

    def testCorrect(self, name, degrees):
        el = TestStaticEuler.elements.index(name)
        name = name + ("Deg" if degrees else "")
        angle = np.random.rand(1)
        r = getattr(Rotation, name)(angle)
        angles = np.zeros(3)
        angles[el] = angle
        assert np.all(r == Rotation.fromEuler(angles, degrees=degrees))


@pytest.mark.parametrize(
    "shape",
    [
        (3,),
        (1, 3),
        (2, 3),
        (1, 1, 3),
        (1, 4, 3),
        (2, 6, 3),
        (1, 1, 1, 3),
        (3, 2, 4, 3),
    ],
)
class TestRotationAlign:
    def verify(a, b, r):
        return np.allclose(b.normalized(), r @ a.normalized())

    def testRandom(self, shape):
        a, b = [vec.Vec(np.random.rand(*shape)) for _ in range(2)]
        r = Rotation.rotationBetween(a, b)
        assert TestRotationAlign.verify(a, b, r)

    @pytest.mark.parametrize(
        "v",
        [vec.Vec3.x, vec.Vec3.y, vec.Vec3.z],
    )
    class TestColinear:
        def testAlone(self, shape, v):
            a, b = [vec.Vec(np.full(shape, v * s)) for s in [1, -1]]
            r = Rotation.rotationBetween(a, b)
            assert TestRotationAlign.verify(a, b, r)

        def testBoth(self, shape, v):
            a, b = [vec.Vec(np.full(shape, v * s)) for s in [1, -1]]
            aa, bb = [c.reshape(-1, a.shape[-1]) for c in [a, b]]
            c = len(aa) // 2
            aa[:c], bb[:c] = [
                vec.Vec.random((c, a.shape[-1])) for _ in range(2)
            ]
            r = Rotation.rotationBetween(a, b)
            assert TestRotationAlign.verify(a, b, r)


def setx(a, x):
    a.x = x


def getx(a):
    return a.x


def set0(a, x):
    a[0] = x


def get0(a):
    return a[0]


def eul(r):
    return r.euler


def eulDeg(r):
    return r.eulerDeg


def rotvec(r):
    return r.rotvec


def quat(r):
    return r.quat


@pytest.mark.parametrize("setfunc", [(setx, getx), (set0, get0)])
@pytest.mark.parametrize(
    "shape_", [(*(2,) * 5, 3, 3)[-i:] for i in range(1, 7 + 1)]
)
class TestConvertedArraySet:
    @pytest.mark.parametrize("func", [eul, eulDeg])
    def testSetEqual(self, rrand, shape_, func, setfunc):
        set_, get = setfunc
        c = func(rrand)
        v = np.random.random_sample(get(c).shape)
        set_(c, v)
        assert np.allclose(get(func(rrand)), v)

    @pytest.mark.parametrize("func", [rotvec, quat])
    def testSetDoesSomething(self, rrand, shape_, func, setfunc):
        set_, get = setfunc
        cp = rrand.copy()
        c = func(rrand)
        v = np.random.random_sample(get(c).shape)
        set_(c, v)
        assert not np.allclose(rrand, cp)


class TestSlerp:
    def test1dim(self):
        x = np.arange(0, 90, 10)
        r = Rotation.identity((x.shape[0],))
        r.eulerDeg.x = x[:, np.newaxis]
        kt = np.linspace(0, len(r), len(r))
        nt = np.linspace(0, len(r), len(r) * 2 - 1)
        nr = r.slerp(kt)(nt)
        assert np.allclose(r.eulerDeg.lerp(kt)(nt), nr.eulerDeg)

    def test2dim(self):
        x = np.arange(0, 90, 10)
        r = Rotation.identity((x.shape[0], 3))
        for i, el in enumerate(["x", "y", "z"]):
            setattr(r[:, i].eulerDeg, el, x[:, np.newaxis])
        kt = np.linspace(0, len(r), len(r))
        nt = np.linspace(0, len(r), len(r) * 2 - 1)
        nr = r.slerp(kt)(nt)
        assert np.allclose(r.eulerDeg.lerp(kt)(nt), nr.eulerDeg)
