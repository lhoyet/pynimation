import pytest


class AttrsTest:
    def testisAttr(self, obj, attr, type_):
        assert hasattr(obj, attr)

    def testattrIsType(self, obj, attr, type_):
        assert isinstance(getattr(obj, attr), type_)


def get_(t, idx=(0)):
    t[idx]


def set_(t, idx=(0), v=1):
    t[idx] = v


def setInPlace(t, idx=(0), v=1):
    t[idx] += v


@pytest.mark.parametrize(
    "tpart",
    [
        lambda t, idx: (t, idx),
        lambda t, idx: (
            t[0],
            idx[1:] if isinstance(idx, tuple) and len(idx) > 1 else idx,
        ),
    ],
)
class MutationTest:
    def testChangedIdEq(self, tpart, f, obj, shape_, idx):
        t, idx = tpart(obj, idx)
        idb = id(t)
        f(t, idx)
        ida = id(t)
        assert ida == idb


@pytest.mark.parametrize(
    "f",
    [set_, setInPlace, get_],
)
@pytest.mark.parametrize(
    "idx",
    [(0, slice(None), 0, 0), (0, 0, 0), (0,), (slice(None), 0, 0), (Ellipsis)],
)
class GetSetTest(MutationTest):
    pass
