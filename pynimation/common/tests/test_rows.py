from itertools import combinations_with_replacement

import numpy as np
import pytest

from pynimation.common.rows import Rows


@pytest.fixture
def r(shape_):
    return Rows(np.random.rand(*shape_))


@pytest.mark.parametrize(
    "shape_",
    [
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (1, 2, 2),
        (1, 3, 3),
        (1, 4, 4),
        (2, 4, 4),
        (3, 2, 2),
        (1, 1, 4, 4),
        (3, 2, 3, 3),
        (1, 1, 1, 2, 2),
        (2, 2, 2, 4, 4),
    ],
)
# TODO: refactor with TestVec
class TestRows:
    elements = ["x", "y", "z", "w"]

    def testHasSingleAttributes(self, r, shape_):
        n = r.shape[-1]
        for i, e in enumerate(TestRows.elements[:n]):
            assert getattr(r, e) is not None
            assert np.all(getattr(r, e) == r[..., [i], :])

    def testHasAllAttributes(self, r, shape_):
        n = r.shape[-1]
        el = TestRows.elements[:n]
        combs = [
            b
            for i in range(1, n)
            for b in combinations_with_replacement(el, i)
        ]
        for c in combs:
            e = "".join(c)
            idx = [el.index(ch) for ch in c]
            assert getattr(r, e) is not None
            assert np.all(getattr(r, e) == r[..., idx, :])

    def testReturnsCorrectClass(self, r, shape_):
        n = r.shape[-1]
        el = TestRows.elements[:n]
        for t in ["".join(el[:i]) for i in range(n)]:
            assert isinstance(getattr(r, t), Rows._cls[r.shape[-1]])
