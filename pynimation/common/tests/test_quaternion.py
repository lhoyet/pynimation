import functools

import numpy as np
import pytest
from scipy.spatial.transform import Rotation

from pynimation.common.quaternion import Quaternion

SHAPE = (10, 10, 10, 4)

RNG = np.random.default_rng()


@pytest.fixture
def qones(shape=SHAPE):
    return Quaternion(np.ones(shape))


@pytest.fixture
def qzeros(shape=SHAPE):
    return Quaternion(np.zeros(shape))


@pytest.fixture
def qrandom(shape=SHAPE, rng=RNG):
    return Quaternion(rng.random(shape))


@pytest.mark.parametrize(
    "q",
    [
        pytest.lazy_fixture("qones"),  # type: ignore
        pytest.lazy_fixture("qzeros"),  # type: ignore
        pytest.lazy_fixture("qrandom"),  # type: ignore
    ],
)
class TestAPI:
    def testQuaternionIsQuaternion(self, q):
        assert isinstance(q, Quaternion)

    @pytest.mark.parametrize(
        "s",
        ["x", "y", "z", "w", "xz", "wxyz"],
    )
    def testSizzle(self, q, s):
        assert hasattr(q, s)


def testWFirstConstructorSetsWLast():
    arr = RNG.random(SHAPE)
    q = Quaternion(arr, wLast=False)
    assert np.all(q[..., -1] == arr[..., 0])


def testWFirstConstructorDoesntTouchOriginalArray():
    arr = RNG.random(SHAPE)
    arrc = arr.copy()
    Quaternion(arr, wLast=False)
    assert np.all(arrc == arr)


def reshapeScipy(f):
    @functools.wraps(f)
    def wrap(*qs):
        re = [q.reshape(-1, 4) for q in qs]
        return f(*re).reshape(qs[0].shape)

    return wrap


class OpsScipy:
    @staticmethod
    @reshapeScipy
    def normalize(qs0):
        return Rotation.from_quat(qs0).as_quat()

    @staticmethod
    @reshapeScipy
    def mult(qs0, qs1):
        return (Rotation.from_quat(qs0) * Rotation.from_quat(qs1)).as_quat()

    @staticmethod
    @reshapeScipy
    def inv(qs0):
        return (Rotation.from_quat(qs0).inv()).as_quat()

    @staticmethod
    def magnitude(qs0):
        return (
            Rotation.from_quat(qs0.reshape(-1, 4))
            .magnitude()
            .reshape(qs0.shape[:-1])
        )

    @staticmethod
    def apply(qs0, qs1):
        return (
            Rotation.from_quat(qs0.reshape(-1, 4))
            .apply(qs1[..., :3].reshape(-1, 3))
            .reshape(*qs0.shape[:-1], 3)
        )

    @staticmethod
    def multbroad(qs0, qs1):
        if qs1.ndim == 1:
            qs1 = qs1.reshape(-1, 4)
        qs1 = qs1[[0]]
        qs1 = np.full(qs0.shape, qs1)
        return (
            (
                Rotation.from_quat(qs0.reshape(-1, 4))
                * Rotation.from_quat(qs1.reshape(-1, 4))
            )
            .as_quat()
            .reshape(qs0.shape)
        )

    # @staticmethod
    # def align(qs0, qs1):
    #     return np.ndarray(
    #         [
    #             Rotation.align_vectors(a, b)[0].as_quat()
    #             for a, b in zip(
    #                 qs0[..., :3].reshape(-1, 3), qs1[..., :3].reshape(-1, 3)
    #             )
    #         ]
    #     ).reshape(qs0.shape)


class OpsQuat:
    @staticmethod
    def normalize(qs0):
        return qs0

    @staticmethod
    def mult(qs0, qs1):
        return qs0 * qs1

    @staticmethod
    def multbroad(qs0, qs1):
        if qs0.ndim > 1:
            qs1 = qs1[[0]]
        return qs0 * qs1

    @staticmethod
    def inv(qs0):
        return qs0.inverse()

    @staticmethod
    def magnitude(qs0):
        return qs0.angle()

    @staticmethod
    def apply(qs0, qs1):
        return qs0 * qs1[..., :3].view(np.ndarray)

    # @staticmethod
    # def align(qs0, qs1):
    #     return Quaternion.between(qs0[..., :3], qs1[..., :3])


@pytest.mark.parametrize(
    "shape", [SHAPE[-i:] for i in range(1, len(SHAPE) + 1)]
)
@pytest.mark.parametrize(
    "op", ["mult", "multbroad", "normalize", "inv", "magnitude", "apply"]
)
class TestOps_:
    def testOp(self, op, shape):
        argcount = getattr(OpsQuat, op).__code__.co_argcount
        qs = [
            Quaternion(RNG.random(shape)).normalized() for _ in range(argcount)
        ]
        scipy = getattr(OpsScipy, op)(*qs)
        quat = getattr(OpsQuat, op)(*qs).view(np.ndarray)
        assert scipy.shape == quat.shape
        assert np.allclose(scipy, quat) or np.allclose(scipy, -quat)
