import pytest
import abc

from pynimation.common.singleton import _Singleton, _SingletonAbstract


class SimpleSingleton(metaclass=_Singleton):
    pass


class AbstractSingleton(metaclass=_SingletonAbstract):
    @abc.abstractmethod
    def test(self):
        pass


class ImplementationSingleton(AbstractSingleton):
    def test(self):
        return


class ImplementationErrorSingleton(AbstractSingleton):
    pass


def testSingleton():
    simple = SimpleSingleton()
    simple2 = SimpleSingleton()

    assert simple is simple2


def testAbstractSingleton():
    implem = ImplementationSingleton()
    implem2 = ImplementationSingleton()

    assert implem is implem2


def testBoth():
    simple = SimpleSingleton()
    implem = ImplementationSingleton()

    assert simple is not implem


def testErrors():
    with pytest.raises(
        TypeError,
        match="Can't instantiate abstract class ImplementationErrorSingleton with abstract method test",
    ):
        ImplementationErrorSingleton()
