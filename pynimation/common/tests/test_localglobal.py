from typing import List
import random

import numpy as np
import pytest
from pytest_mock import MockerFixture
from common_tests import AttrsTest, set_, setInPlace

from pynimation.common.graph import Graph, Node
from pynimation.common.localglobal import LocalGlobal
from pynimation.common.rotation import Rotation
from pynimation.common.transform import Transform
from pynimation.common.vec import Vec3, Vec4, Vec

SHAPE = (3, 3, 3, 4, 4)

GRAPHS: List[Graph] = [Graph(Node(""))]

for i in range(20):
    o: Node = Node("")
    n = o
    for j in range(i):
        n.addChild(Node(""))
        n = n.children[0]
    GRAPHS.append(Graph(o))


@pytest.fixture
def shape_():
    return SHAPE


@pytest.fixture
def graph_(shape_):
    return GRAPHS[shape_[-3]]


@pytest.fixture
def lgAuto(shape_, graph_):
    return createLg(shape_, graph_, autoUpdate_=True)


@pytest.fixture
def lgNoAuto(shape_, graph_):
    return createLg(shape_, graph_, autoUpdate_=False)


@pytest.fixture
def lgRand(shape_, graph_):
    shape_ = shape_ or (1, 4, 4)
    graph_ = graph_ or GRAPHS[shape_[-3]]
    return LocalGlobal.random(shape_, graph=graph_, autoUpdate=False)


@pytest.fixture
def lgNoAutoGraph(shape_, graph_):
    return createLg(shape_, graph_, autoUpdateGraph_=False)


def createLg(
    shape_=None, graph_=None, autoUpdate_=True, autoUpdateGraph_=True
):
    shape_ = shape_ or (1, 4, 4)
    graph_ = graph_ or GRAPHS[shape_[-3]]
    return LocalGlobal.identity(
        shape_[:-2],
        graph=graph_.copy(),
        autoUpdate=autoUpdate_,
        autoUpdateGraph=autoUpdateGraph_,
    )


class TestAttrs:
    @pytest.mark.parametrize(
        "obj",
        [
            pytest.lazy_fixture("lgAuto"),  # type: ignore
            pytest.lazy_fixture("lgNoAuto"),  # type: ignore
        ],
    )
    @pytest.mark.parametrize(
        ("attr", "type_"),
        [
            ("graph", Graph),
            ("globals", Transform),
            ("position", Vec3),
            ("pos", Vec3),
            ("p", Vec3),
            ("homPosition", Vec4),
            ("hpos", Vec4),
            ("hp", Vec4),
            ("rotation", Rotation),
            ("rot", Rotation),
            ("r", Rotation),
        ],
    )
    class TestLocalGlobalAttrs(AttrsTest):
        pass


# class TestShapeValidation:
#     @pytest.mark.parametrize(
#         "shape",
#         [
#             (2, 2, 2),
#             (3, 3, 3),
#             (4, 4, 2),
#             (4, 1, 4),
#             (3, 4, 2),
#             (2, 2, 2, 2),
#             (2, 4, 2, 4),
#             (2, 4, 4, 2),
#         ],
#     )
#     def testRectangeShapeRaisesError(self, shape):
#         with pytest.raises(
#             ValueError,
#             match=re.escape(
#                 "LocalGlobal array should be of shape (...,4,4) and not %s"
#                 % (str(shape),)
#             ),
#         ):
#             LocalGlobal(np.empty(shape), GRAPHS[1])

#     @pytest.mark.parametrize(
#         "shape",
#         [
#             (0,),
#             (1,),
#             (3,),
#             (1, 1),
#             (2, 2),
#             (3, 3),
#             (5, 5),
#             (6, 6),
#             (2, 3),
#             (2, 4),
#             (3, 2),
#             (1, 4),
#         ],
#     )
#     def testMinimumShapeRaisesError(self, shape):
#         with pytest.raises(
#             ValueError,
#             match=re.escape(
#                 "LocalGlobal should have a minimum of 3 dimensions %s found"
#                 % (len(shape),)
#             ),
#         ):
#             LocalGlobal(np.empty(shape), GRAPHS[1])

#     @pytest.mark.parametrize(
#         "shape",
#         [
#             (4, 4),
#         ],
#     )
#     def testShapeLowerThanTwoRaisesError(self, shape):
#         with pytest.raises(
#             ValueError,
#             match=re.escape(
#                 "LocalGlobal array should have a minimum of 3 dimensions (%d found)"
#                 % (len(shape),)
#             ),
#         ):
#             LocalGlobal(np.empty(shape), GRAPHS[1])

#     @pytest.mark.parametrize(
#         "shape",
#         [
#             (2, 4, 4),
#             (3, 4, 4),
#             (4, 4, 4),
#             (4, 4, 4),
#             (3, 4, 4),
#             (2, 2, 4, 4),
#             (2, 4, 4, 4),
#             (2, 4, 4, 4),
#         ],
#     )
#     @pytest.mark.parametrize(
#         "nnodes",
#         [lambda x: x + 1, lambda x: x + 2, lambda x: x - 1, lambda x: 10],
#     )
#     def testShapeDifferentFromNodeCountRaisesError(self, shape, nnodes):
#         with pytest.raises(
#             ValueError,
#             match=re.escape(
#                 "LocalGlobal array dim %d should match number of graph nodes (%d != %d)"
#                 % (len(shape) - 3, shape[-3], nnodes(shape[-3]))
#             ),
#         ):
#             LocalGlobal(np.empty(shape), GRAPHS[nnodes(shape[-3])])

#     @pytest.mark.parametrize(
#         "shape",
#         [
#             (1, 4, 4),
#             (10, 4, 4),
#             (1, 1, 4, 4),
#             (1, 1, 4, 4),
#             (1, 14, 4, 4),
#             (1, 1, 1, 4, 4),
#             (2, 2, 2, 4, 4),
#             (2, 2, 3, 4, 4),
#             (2, 2, 12, 4, 4),
#             (1, 1, 1, 1, 4, 4),
#             (1, 1, 1, 2, 4, 4),
#             (1, 1, 1, 13, 4, 4),
#         ],
#     )
#     def testRightShapeDoesntRaiseError(self, shape):
#         try:
#             LocalGlobal(np.empty(shape), GRAPHS[shape[-3]])
#         except ValueError as e:
#             pytest.fail("raised shape error when it shouldn't have %s" % e)


# @pytest.mark.parametrize(
#     "obj",
#     [
#         pytest.lazy_fixture("lgAuto"),  # type: ignore
#         pytest.lazy_fixture("lgNoAuto"),  # type: ignore
#     ],
# )
# @pytest.mark.parametrize(
#     "shape_", [SHAPE[-i:] for i in range(3, len(SHAPE) + 1)]
# )
# @pytest.mark.parametrize(
#     "f",
#     [set_, setInPlace, get_],
# )
# @pytest.mark.parametrize(
#     "idx",
#     [
#         (0,),
#         (0, 0),
#         (0, 0, 0),
#         (slice(None), 0, 0),
#         (slice(None), slice(None), 0),
#         (0, slice(None), 0),
#         (0, 0, slice(None)),
#         (slice(None), 0, slice(None)),
#         (Ellipsis),
#     ],
# )
# class TestTransformGetSet(MutationTest):
#     pass


# test updateGlobals
# with all
# with parts to Update
# with different shapes


@pytest.mark.parametrize(
    "shape_", [SHAPE[-i:] for i in range(3, len(SHAPE) + 1)]
)
class TestUpdateGlobals:
    def assertGlobalsIsValid(self, lg_):
        globs = np.empty(lg_.shape)
        globs[..., 0, :, :] = lg_[..., 0, :, :]
        for j in lg_._graph.nodes[1:]:
            globs[..., j.id, :, :] = (
                globs[..., j.parent.id, :, :] @ lg_[..., j.id, :, :]
            )
            assert np.allclose(
                globs[..., j.id, :, :], lg_._globals[..., j.id, :, :]
            )

    @pytest.mark.parametrize(
        "f",
        [set_, setInPlace],
    )
    @pytest.mark.parametrize(
        "idx",
        [
            (Ellipsis, 0, slice(None), slice(None)),
            (Ellipsis, 2, slice(None), slice(None)),
            (Ellipsis, slice(None), 0, slice(None)),
            (Ellipsis, 0, 0, slice(None)),
            (Ellipsis,),
        ],
    )
    def testSet(self, shape_, lgAuto, f, idx):
        f(lgAuto, idx)
        lgAuto.globals
        self.assertGlobalsIsValid(lgAuto)

    @staticmethod
    def seteuler(lg, v):
        lg.rotation.euler.x = v

    @staticmethod
    def seteulerdeg(lg, v):
        lg.rotation.eulerDeg.x = v

    @pytest.mark.parametrize(
        "f",
        [seteuler, seteulerdeg],
    )
    def testSetConvertedRotation(self, shape_, lgAuto, f):
        s = (*shape_[:-2], 1)
        v = np.random.random_sample(s)
        f(lgAuto, v)
        lgAuto.globals
        self.assertGlobalsIsValid(lgAuto)


class TestUpdateGraph:
    @pytest.mark.parametrize(
        "lg",
        [
            pytest.lazy_fixture("lgAuto"),  # type: ignore
        ],
    )
    def testCallsWhenSet(self, lg, mocker: MockerFixture):
        assert lg.autoUpdateGraph
        mocker.patch.object(lg, "updateFromGraph")
        lg.graph.root.addChild(Node(""))
        lg.updateFromGraph.assert_called_once()

    @pytest.mark.parametrize(
        "lg",
        [
            pytest.lazy_fixture("lgNoAutoGraph"),  # type: ignore
        ],
    )
    def testDoesntCallWhenNotSet(self, lg, mocker: MockerFixture):
        assert not lg.autoUpdateGraph
        mocker.patch.object(lg, "updateFromGraph")
        lg.graph.root.addChild(Node(""))
        lg.updateFromGraph.assert_not_called()

    @pytest.mark.parametrize(
        "lg",
        [
            pytest.lazy_fixture("lgAuto"),  # type: ignore
            pytest.lazy_fixture("lgNoAuto"),  # type: ignore
        ],
    )
    def testDoesntCallWhenNotSetByUs(self, lg, mocker: MockerFixture):
        lg.autoUpdateGraph = False
        mocker.patch.object(lg, "updateFromGraph")
        lg.graph.root.addChild(Node(""))
        lg.updateFromGraph.assert_not_called()

    @pytest.mark.parametrize(
        "lg",
        [
            pytest.lazy_fixture("lgAuto"),  # type: ignore
            pytest.lazy_fixture("lgNoAuto"),  # type: ignore
        ],
    )
    def testCallsWhenSetByUs(self, lg, mocker: MockerFixture):
        lg.autoUpdateGraph = True
        mocker.patch.object(lg, "updateFromGraph")
        lg.graph.root.addChild(Node(""))
        lg.updateFromGraph.assert_called_once()


@pytest.mark.parametrize(
    "lg",
    [
        pytest.lazy_fixture("lgAuto"),  # type: ignore
        pytest.lazy_fixture("lgNoAuto"),  # type: ignore
    ],
)
class TestHoldUpdate:
    def testHoldsInWith(self, lg, mocker: MockerFixture):
        mocker.patch.object(lg, "updateGlobals")
        with lg.holdUpdate() as lg:
            lg[0] = 1
            lg.globals
            lg.updateGlobals.assert_not_called()

    def testUpdatesAfter(self, lg, mocker: MockerFixture):
        mocker.patch.object(lg, "updateGlobals")
        with lg.holdUpdate() as lg:
            lg[0] = 1
            lg.globals
        lg.updateGlobals.assert_called_once()

    def testRestoresUpdateSetting(self, lg, mocker: MockerFixture):
        o = lg.autoUpdate
        mocker.patch.object(lg, "updateGlobals")
        with lg.holdUpdate() as lg:
            lg[0] = 1
            lg.globals
        assert lg.autoUpdate == o


@pytest.mark.parametrize(
    "lg",
    [
        pytest.lazy_fixture("lgAuto"),  # type: ignore
        pytest.lazy_fixture("lgNoAutoGraph"),  # type: ignore
    ],
)
class TestHoldUpdateGraph:
    def testHoldsInWith(self, lg, mocker: MockerFixture):
        mocker.patch.object(lg, "updateFromGraph")
        with lg.holdUpdateGraph() as lg:
            lg.graph.root.addChild(Node(""))
            lg.updateFromGraph.assert_not_called()

    def testUpdatesAfter(self, lg, mocker: MockerFixture):
        mocker.patch.object(lg, "updateFromGraph")
        with lg.holdUpdateGraph() as lg:
            lg.graph.root.addChild(Node(""))
        lg.updateFromGraph.assert_called_once()

    def testRestoresUpdateSetting(self, lg, mocker: MockerFixture):
        o = lg.autoUpdateGraph
        mocker.patch.object(lg, "updateFromGraph")
        with lg.holdUpdateGraph() as lg:
            lg.graph.root.addChild(Node(""))
        assert lg.autoUpdateGraph == o


@pytest.mark.parametrize(
    "shape_", [SHAPE[-i:] for i in range(4, len(SHAPE) + 1)]
)
class TestResize:
    @pytest.mark.parametrize("delta", [0, 1, -1, 2, -2])
    def testChangesShape(self, shape_, lgNoAuto, delta):
        nshape = (shape_[0] + delta, *shape_[1:])
        lgNoAuto.resize(nshape)
        assert lgNoAuto.shape == nshape

    @pytest.mark.parametrize("delta", [0, 1, -1, 2, -2])
    def testKeepsOldData(self, shape_, lgNoAuto, delta):
        lgNoAuto[...] = np.random.random_sample(lgNoAuto.shape)
        nshape = (shape_[0] + delta, *shape_[1:])
        a = lgNoAuto.copy()
        lgNoAuto.resize(nshape)
        assert np.all(lgNoAuto[: a.shape[0]] == a[: lgNoAuto.shape[0]])


@pytest.mark.parametrize(
    "shape_", [SHAPE[-i:] for i in range(4, len(SHAPE) + 1)]
)
@pytest.mark.parametrize("f", [lambda a: a, lambda a: a.id])
class TestNodeView:
    def testGetSingleItem(self, shape_, lgRand, f):
        g = lgRand._graph
        n = random.choice(g.nodes)
        assert np.all(lgRand.node[f(n)] == lgRand[..., n.id, :, :])

    @pytest.mark.parametrize("ns", list(range(2, 4)))
    def testGetMultiple(self, shape_, lgRand, f, ns):
        g = lgRand._graph
        n = random.sample(g.nodes, ns)
        nn = tuple([f(a) for a in n])
        nni = tuple([a.id for a in n])
        assert np.all(lgRand.node[nn] == lgRand[..., nni, :, :])

    @pytest.mark.parametrize("ns", list(range(2, 4)))
    def testGetMultipleNoTuple(self, shape_, lgRand, f, ns):
        g = lgRand._graph
        n = random.sample(g.nodes, ns)
        nn = tuple([f(a) for a in n])
        nni = tuple([a.id for a in n])
        if ns == 2:
            assert np.all(lgRand.node[nn[0], nn[1]] == lgRand[..., nni, :, :])
        elif ns == 3:
            assert np.all(
                lgRand.node[nn[0], nn[1], nn[2]] == lgRand[..., nni, :, :]
            )

    def testSetSingleItem(self, shape_, lgRand, f):
        g = lgRand._graph
        n = random.choice(g.nodes)
        v = np.random.random_sample((*lgRand.shape[:-3], *lgRand.shape[-2:]))
        lgRand.node[f(n)] = v
        assert np.all(lgRand.node[f(n)] == v)

    @pytest.mark.parametrize("ns", list(range(2, 4)))
    def testSetMultiple(self, shape_, lgRand, f, ns):
        g = lgRand._graph
        n = random.sample(g.nodes, ns)
        v = np.random.random_sample(
            (*lgRand.shape[:-3], len(n), *lgRand.shape[-2:])
        )
        nn = tuple([f(a) for a in n])
        lgRand.node[nn] = v
        assert np.all(lgRand.node[nn] == v)

    @pytest.mark.parametrize("ns", list(range(2, 4)))
    def testSetMultipleNoTuple(self, shape_, lgRand, f, ns):
        g = lgRand._graph
        n = random.sample(g.nodes, ns)
        nn = tuple([f(a) for a in n])
        nni = tuple([a.id for a in n])
        v = np.random.random_sample(
            (*lgRand.shape[:-3], len(n), *lgRand.shape[-2:])
        )
        if ns == 2:
            lgRand.node[nn[0], nn[1]] = v
            assert np.all(lgRand.node[nn[0], nn[1]] == lgRand[..., nni, :, :])
        elif ns == 3:
            lgRand.node[nn[0], nn[1], nn[2]] = v
            assert np.all(
                lgRand.node[nn[0], nn[1], nn[2]] == lgRand[..., nni, :, :]
            )

    @staticmethod
    def setpx(a, x):
        a.p.x = x

    @staticmethod
    def getpx(a):
        return a.p.x

    @staticmethod
    def set0(a, x):
        a[0] = x

    @staticmethod
    def get0(a):
        return a[0]

    @staticmethod
    def setrot(a, x):
        a.rot.euler.y = x

    @staticmethod
    def getrot(a):
        return a.rot.euler.y

    @pytest.mark.parametrize("ns", list(range(2, 4)))
    @pytest.mark.parametrize(
        "h", [(setpx, getpx), (set0, get0), (setrot, getrot)]
    )
    def testSetMultipleConverted(self, shape_, lgRand, f, ns, h):
        g = lgRand._graph
        n = random.sample(g.nodes, ns)
        nn = tuple([f(a) for a in n])
        set_, get = h
        v = np.random.random_sample(get(lgRand.node[nn]).shape)
        set_(lgRand.node[nn], v)
        assert np.allclose(get(lgRand.node[nn]), v)


@pytest.mark.parametrize("attr", ["rows", "cols"])
@pytest.mark.parametrize(
    "shape_",
    [
        (1, 4, 4),
        (2, 4, 4),
        (1, 1, 4, 4),
        (3, 2, 4, 4),
        (1, 1, 1, 4, 4),
        (2, 2, 2, 4, 4),
    ],
)
class TestRowsCols:
    def testCanGetElement(self, lgAuto, shape_, attr):
        m = lgAuto
        el = Vec._elements[: shape_[-1]]
        if attr == "cols":
            elsh = [b.upper() for b in el]
            mm = m.T
        else:
            elsh = el
            mm = m
        for i, esh in enumerate(elsh):
            for j, e in enumerate(el):
                assert np.all(
                    getattr(getattr(m, esh), e.lower()) == mm[..., i, j]
                )


# # test trigger updateglobals
# # with localglobal
# # with sub localglobal
# # when no autoUpdate

# # test globals are updated

# # test no autoUpdate calls right get set finalize
# # test no autoUpdate returns no Obs
