from itertools import combinations_with_replacement

import numpy as np
import pytest
from common_tests import GetSetTest

from pynimation.common.vec import Vec, Vec2, Vec3, Vec4


@pytest.fixture
def vec2(shape_):
    shape_ = (*shape_, 2)
    return Vec2(np.random.rand(*shape_))


@pytest.fixture
def vec3(shape_):
    shape_ = (*shape_, 3)
    return Vec3(np.random.rand(*shape_))


@pytest.fixture
def vec4(shape_):
    shape_ = (*shape_, 4)
    return Vec4(np.random.rand(*shape_))


@pytest.mark.parametrize(
    "v",
    [
        pytest.lazy_fixture("vec2"),  # type: ignore
        pytest.lazy_fixture("vec3"),  # type: ignore
        pytest.lazy_fixture("vec4"),  # type: ignore
    ],
)
@pytest.mark.parametrize("shape_", [(2,), (2, 2), (2, 2, 2)])
class TestVec:
    elements = ["x", "y", "z", "w"]

    def testHasSingleAttributes(self, v, shape_):
        n = v.shape[-1]
        for i, e in enumerate(TestVec.elements[:n]):
            assert getattr(v, e) is not None
            assert np.all(getattr(v, e) == v[..., [i]])

    def testHasAllAttributes(self, v, shape_):
        n = v.shape[-1]
        el = TestVec.elements[:n]
        combs = [
            b
            for i in range(1, n)
            for b in combinations_with_replacement(el, i)
        ]
        for c in combs:
            e = "".join(c)
            idx = [el.index(ch) for ch in c]
            assert getattr(v, e) is not None
            assert np.all(getattr(v, e) == v[..., idx])

    def testReturnsCorrectClass(self, v, shape_):
        n = v.shape[-1]
        el = TestVec.elements[:n]
        for t in ["".join(el[:i]) for i in range(n)]:
            assert isinstance(getattr(v, t), Vec._cls[len(t)])


@pytest.mark.parametrize(
    ("obj", "shape_"),
    [
        (pytest.lazy_fixture("vec2"), (10, 10, 2)),  # type: ignore
        (pytest.lazy_fixture("vec3"), (10, 10, 3)),  # type: ignore
        (pytest.lazy_fixture("vec4"), (10, 10, 4)),  # type: ignore
    ],
)
class TestRotationGetSet(GetSetTest):
    pass


# TODO
def testVecFactory():
    pass


def testVecShapeCheck():
    pass


def testVecNormalize():
    pass
