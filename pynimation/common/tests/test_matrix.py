import re

import numpy as np
import pytest
from common_tests import AttrsTest, GetSetTest

from pynimation.common.matrix import Matrix, Rows
from pynimation.common.vec import Vec, Vec4

SHAPE = (10, 10, 4, 4)


@pytest.fixture
def shape_():
    return SHAPE


@pytest.fixture
def m(shape_):
    return Matrix(np.ones(shape_))


@pytest.fixture
def mrand(shape_):
    return Matrix(np.random.rand(*shape_))


@pytest.mark.parametrize(
    "obj",
    [
        pytest.lazy_fixture("m"),  # type: ignore
    ],
)
@pytest.mark.parametrize(
    ("attr", "type_"),
    [
        ("T", Matrix),
        ("diag", Vec4),
        ("rows", Rows),
        ("columns", Rows),
        ("cols", Rows),
    ],
)
class TestTransformAttrs(AttrsTest):
    pass


@pytest.mark.parametrize(
    "shape",
    [
        (1,),
        (2,),
        (3,),
        (4,),
    ],
)
def testWrongNumberOfDimsRaisesError(shape):
    with pytest.raises(
        ValueError,
        match=re.escape(
            "Matrix array should have at least %d dimensions (%d found)"
            % (Matrix._nd, len(shape))
        ),
    ):
        Matrix(np.empty(shape))


@pytest.mark.parametrize(
    "shape",
    [
        (2, 3),
        (2, 4),
        (3, 2),
        (1, 4),
        (4, 4, 2),
        (4, 1, 4),
        (3, 4, 2),
        (2, 4, 2, 4),
        (2, 4, 4, 2),
    ],
)
def testWrongShapeRaisesError(shape):
    with pytest.raises(
        ValueError,
        match=re.escape(
            "Matrix array should be of shape (...,n,n) and not %s"
            % (str(shape),)
        ),
    ):
        Matrix(np.empty(shape))


@pytest.mark.parametrize(
    "shape",
    [
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (1, 1, 1),
        (1, 2, 2),
        (1, 3, 3),
        (1, 4, 4),
        (2, 4, 4),
        (3, 2, 2),
        (1, 1, 4, 4),
        (3, 2, 3, 3),
        (1, 1, 1, 2, 2),
        (2, 2, 2, 4, 4),
    ],
)
def testRightShapeDoesntRaiseError(shape):
    try:
        Matrix(np.empty(shape))
    except ValueError as e:
        pytest.fail("raised shape error when it shouldn't have %s" % e)


@pytest.mark.parametrize(
    "obj",
    [
        pytest.lazy_fixture("m"),  # type: ignore
    ],
)
@pytest.mark.parametrize("shape_", [SHAPE])
class TestMatrixGetSet(GetSetTest):
    pass


@pytest.mark.parametrize(
    "shape_",
    [
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (1, 2, 2),
        (1, 3, 3),
        (1, 4, 4),
        (2, 4, 4),
        (3, 2, 2),
        (1, 1, 4, 4),
        (3, 2, 3, 3),
        (1, 1, 1, 2, 2),
        (2, 2, 2, 4, 4),
    ],
)
class TestDiag:
    def testShape(self, m, shape_):
        assert m.diag.shape == shape_[:-1]

    def testCorrectIndices(self, m, shape_):
        d = np.random.rand(shape_[-1])
        a = np.zeros(shape_[-2:])
        np.fill_diagonal(a, d)
        m[...] = np.full(shape_, a)
        assert np.all(m.diag == d)

    def testSet(self, m, shape_):
        d = np.random.rand(shape_[-1])
        d = np.full(shape_[:-1], d)
        m.diag = d
        assert np.all(m.diag == d)


@pytest.mark.parametrize(
    "shape_",
    [
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (1, 2, 2),
        (1, 3, 3),
        (1, 4, 4),
        (2, 4, 4),
        (3, 2, 2),
        (1, 1, 4, 4),
        (3, 2, 3, 3),
        (1, 1, 1, 2, 2),
        (2, 2, 2, 4, 4),
    ],
)
class TestTranspose:
    def testShape(self, m, shape_):
        assert m.T.shape == m.shape

    def testTransposeOfTransposeIsOriginal(self, m, shape_):
        assert np.all(m.T.T == m)

    def testSet(self, m, shape_):
        d = Matrix(np.random.rand(*m.shape))
        m.T = d
        assert np.all(m.T == d)
        assert np.all(m == d.T)

    def testCorrectIndices(self, m, shape_):
        a = np.zeros(shape_[-2:])
        a[...] = np.arange(shape_[-1])
        b = np.full(shape_, a.T.copy())
        a = Matrix(np.full(shape_, a))
        assert np.all(a.T == b)


@pytest.mark.parametrize("attr", ["rows", "cols"])
@pytest.mark.parametrize(
    "shape_",
    [
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (1, 2, 2),
        (1, 3, 3),
        (1, 4, 4),
        (2, 4, 4),
        (3, 2, 2),
        (1, 1, 4, 4),
        (3, 2, 3, 3),
        (1, 1, 1, 2, 2),
        (2, 2, 2, 4, 4),
    ],
)
class TestRowsCols:
    def testShape(self, m, shape_, attr):
        a = getattr(m, attr)
        assert a.shape == shape_

    def testRightArrangement(self, m, shape_, attr):
        a = getattr(m, attr)
        if attr == "cols":
            assert np.all(a == m.T)
        else:
            assert np.all(a == m)

    def testEqualsShorthands(self, m, shape_, attr):
        el = Vec._elements[: shape_[-1]]
        a = getattr(m, attr)
        if attr == "cols":
            elsh = [b.upper() for b in el]
        else:
            elsh = el

        for e, esh in zip(el, elsh):
            assert np.all(getattr(a, e) == getattr(m, esh))

    def testCanGetElement(self, m, shape_, attr):
        if shape_[-1] > 4:
            return
        el = Vec._elements[: shape_[-1]]
        if attr == "cols":
            elsh = [b.upper() for b in el]
            mm = m.T
        else:
            elsh = el
            mm = m

        for i, esh in enumerate(elsh):
            assert np.all(getattr(m, esh).x == mm[..., i, 0])

    def testCanSetSubParts(self, mrand, shape_, attr):
        if shape_[-1] > 4:
            return
        m = mrand
        el = Vec._elements[: shape_[-1]]
        if attr == "cols":
            elsh = [b.upper() for b in el]
        else:
            elsh = el

        s = getattr(getattr(m, elsh[0]), el[0]).shape
        v = np.random.random_sample(s)
        for esh in elsh:
            for e in el:
                setattr(getattr(m, esh), e, v)
                assert np.all(getattr(getattr(m, esh), e) == v)

    def testCanSetSubPartsRows(self, mrand, shape_, attr):
        if shape_[-1] > 4:
            return
        m = mrand
        el = Vec._elements[: shape_[-1]]
        m = getattr(m, attr)

        s = getattr(getattr(m, el[0]), el[0]).shape
        v = np.random.random_sample(s)
        for esh in el:
            for e in el:
                setattr(getattr(m, esh), e, v)
                assert np.all(getattr(getattr(m, esh), e) == v)


class IdentityTest:
    def testShortcut(self, cls):
        assert np.all(getattr(cls, "id") == getattr(cls, "identity"))

    def testShape(self, cls):
        assert cls.identity().shape == (cls._dsize, cls._dsize)

    def testIsIdentity(self, cls):
        assert np.all(cls.identity() == np.identity(cls._dsize))
