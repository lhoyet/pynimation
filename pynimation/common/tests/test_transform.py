import re

import numpy as np
import pytest
from common_tests import AttrsTest, GetSetTest
from test_matrix import IdentityTest

from pynimation.common.rotation import Rotation
from pynimation.common.transform import Transform
from pynimation.common.vec import Vec3, Vec4

SHAPE = (10, 10, 4, 4)


@pytest.fixture
def shape_():
    return SHAPE


@pytest.fixture
def t(shape_):
    return Transform(np.ones(shape_))


@pytest.fixture
def trand(shape_):
    return Transform(np.random.rand(*shape_))


@pytest.mark.parametrize(
    "obj",
    [
        pytest.lazy_fixture("t"),  # type: ignore
    ],
)
@pytest.mark.parametrize(
    ("attr", "type_"),
    [
        ("position", Vec3),
        ("pos", Vec3),
        ("p", Vec3),
        ("homPosition", Vec4),
        ("hpos", Vec4),
        ("hp", Vec4),
        ("rotation", Rotation),
        ("rot", Rotation),
        ("r", Rotation),
    ],
)
class TestTransformAttrs(AttrsTest):
    pass


@pytest.mark.parametrize(
    "shape",
    [
        (0,),
        (1,),
        (3,),
    ],
)
def testNotEnoughDimsRaisesError(shape):
    with pytest.raises(
        ValueError,
        match=re.escape(
            "Transform array should have at least %d dimensions (%d found)"
            % (Transform._nd, len(shape))
        ),
    ):
        Transform(np.empty(shape))


@pytest.mark.parametrize(
    "shape",
    [
        (1, 1),
        (2, 2),
        (3, 3),
        (5, 5),
        (6, 6),
        (2, 3),
        (2, 4),
        (3, 2),
        (1, 4),
        (2, 2, 2),
        (3, 3, 3),
        (4, 4, 2),
        (4, 1, 4),
        (3, 4, 2),
        (2, 2, 2, 2),
        (2, 4, 2, 4),
        (2, 4, 4, 2),
    ],
)
def testWrongShapeRaisesError(shape):
    with pytest.raises(
        ValueError,
        match=re.escape(
            "Transform array should be of shape (...,4,4) and not %s"
            % (str(shape),)
        ),
    ):
        Transform(np.empty(shape))


@pytest.mark.parametrize(
    "shape",
    [
        (4, 4),
        (1, 4, 4),
        (2, 4, 4),
        (1, 1, 4, 4),
        (3, 2, 4, 4),
        (1, 1, 1, 4, 4),
        (2, 2, 2, 4, 4),
    ],
)
def testRightShapeDoesntRaiseError(shape):
    try:
        Transform(np.empty(shape))
    except ValueError as e:
        pytest.fail("raised shape error when it shouldn't have %s" % e)


@pytest.mark.parametrize(
    "obj",
    [
        pytest.lazy_fixture("t"),  # type: ignore
    ],
)
@pytest.mark.parametrize("shape_", [SHAPE])
class TestTransformGetSet(GetSetTest):
    pass


@pytest.mark.parametrize(
    "shape_",
    [
        (4, 4),
        (1, 4, 4),
        (2, 4, 4),
        (1, 1, 4, 4),
        (3, 2, 4, 4),
        (1, 1, 1, 4, 4),
        (2, 2, 2, 4, 4),
    ],
)
class TestPositionTransform:
    def testCorrectOutput(self, shape_, trand):
        tpos = trand.positionTransform()
        assert np.all(tpos.pos == trand.pos)
        assert np.all(
            tpos.rotation == np.full((*shape_[:-2], 3, 3), np.identity(3))
        )


@pytest.mark.parametrize(
    "shape_",
    [
        (4, 4),
        (1, 4, 4),
        (2, 4, 4),
        (1, 1, 4, 4),
        (3, 2, 4, 4),
        (1, 1, 1, 4, 4),
        (2, 2, 2, 4, 4),
    ],
)
class TestRotationTransform:
    def testCorrectOutput(self, shape_, trand):
        tpos = trand.rotationTransform()
        assert np.all(tpos.rot == trand.rot)
        p = np.zeros((*shape_[:-2], 4))
        p[..., -1] = 1
        assert np.all(tpos.hpos == p)


@pytest.mark.parametrize(
    "shape_",
    [
        (4, 4),
        (1, 4, 4),
        (2, 4, 4),
        (1, 1, 4, 4),
    ],
)
def testSetScale(trand, shape_):
    trand[..., -1, :] = Vec4.w
    scale = np.random.rand(*(*shape_[:-2], 3))
    trand.scale = scale
    assert np.allclose(trand.scale, scale)


@pytest.mark.parametrize(
    "cls",
    [Transform],
)
class TestRotationIdentity(IdentityTest):
    pass


@pytest.mark.parametrize(
    "shape",
    [
        (4, 4),
        (1, 4, 4),
        (2, 4, 4),
        (1, 1, 4, 4),
        (3, 2, 4, 4),
        (1, 1, 1, 4, 4),
        (2, 2, 2, 4, 4),
    ],
)
class TestFromRotation:
    def testShape(self, shape):
        r = Rotation(np.zeros((*shape[:-2], Rotation._dsize, Rotation._dsize)))
        t = Transform.fromRotation(r)
        assert t.shape == (*r.shape[:-2], Transform._dsize, Transform._dsize)

    def testSetsCorrect(self, shape):
        r = Rotation(
            np.random.rand(*(*shape[:-2], Rotation._dsize, Rotation._dsize))
        )
        t = Transform.fromRotation(r)
        assert np.all(t.rotation == r)

    def testSetsIdentity(self, shape):
        r = Rotation(np.zeros((*shape[:-2], Rotation._dsize, Rotation._dsize)))
        t = Transform.fromRotation(r)
        assert np.all(t[..., -1, -1] == 1)


@pytest.mark.parametrize(
    "shape",
    [
        (4, 4),
        (1, 4, 4),
        (2, 4, 4),
        (1, 1, 4, 4),
        (3, 2, 4, 4),
        (1, 1, 1, 4, 4),
        (2, 2, 2, 4, 4),
    ],
)
@pytest.mark.parametrize(
    "posShape",
    [3, 4],
)
class TestFromPosition:
    def testShape(self, shape, posShape):
        p = np.zeros((*shape[:-2], posShape))
        t = Transform.fromPosition(p)
        assert t.shape == shape

    def testSetsCorrect(self, shape, posShape):
        p = np.random.rand(*(*shape[:-2], posShape))
        t = Transform.fromPosition(p)
        assert np.all(t[..., :posShape, 3] == p)

    def testSetsIdentity(self, shape, posShape):
        p = np.random.rand(*(*shape[:-2], posShape))
        t = Transform.fromPosition(p)
        assert np.all(t.rotation == Rotation.identity())


@pytest.mark.parametrize(
    "shape",
    [
        (4, 4),
        (1, 4, 4),
        (2, 4, 4),
        (1, 1, 4, 4),
        (3, 2, 4, 4),
        (1, 1, 1, 4, 4),
        (2, 2, 2, 4, 4),
    ],
)
class TestXZOffset:
    pass
