import numpy as np

VIEWPORT_HEIGHT = 720
VIEWPORT_WIDTH = 1280
VIEWPORT_NAME = "PyNimation"
VERTEX_SHADER = "data/shaders/diffuse_vs.glsl"
FRAGMENT_SHADER = "data/shaders/diffuse_ps.glsl"
FLOOR_TEXTURE = "data/textures/floor_blue.bmp"
COLOR_PALETTE = [
    [0.223, 0.415, 0.694, 1.0],
    [0.854, 0.486, 0.188, 1.0],
    [0.243, 0.588, 0.317, 1.0],
    [0.8, 0.145, 0.16, 1.0],
    [0.325, 0.317, 0.329, 1.0],
    [0.419, 0.298, 0.603, 1.0],
    [0.572, 0.141, 0.156, 1.0],
    [0.58, 0.545, 0.239, 1.0],
]
INIT_CAMERA_POSITION = np.array([0.0, 0.6, 4])
INIT_CAMERA_ROTATION = np.identity(3)
