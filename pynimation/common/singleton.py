from abc import ABCMeta

from typing import Any, Dict


def call(cls, *args, **kwargs) -> Any:
    if cls not in cls._instances:
        cls._instances[cls] = super(cls.__class__, cls).__call__(
            *args, **kwargs
        )
    return cls._instances[cls]


class _Singleton(type):
    """
    Singleton base class. Classes inheriting from this class will only ever
    have one instance. The first constructor call will create the object and
    return it. Later calls will return the existing instance
    """

    _instances: Dict[Any, Any] = {}

    __call__ = call


class _SingletonAbstract(ABCMeta):
    """
    Singleton base class. Classes inheriting from this class will only ever
    have one instance. The first constructor call will create the object and
    return it. Later calls will return the existing instance
    """

    _instances: Dict[Any, Any] = {}

    __call__ = call
