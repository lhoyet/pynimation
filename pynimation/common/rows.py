from functools import partial
import numpy as np
from pynimation.common.vec import Vec

from .array import ConvertedBehaviour


class Rows(Vec):
    """
    View of a matrix array as vectors of rows or columns

    Allows access and assignment to rows and columns via
    element letters (x,y,z,w)

    See :attr:`~pynimation.common.matrix.Matrix.rows` and
    :attr:`~pynimation.common.matrix.Matrix.columns` for more
    """

    def __getattr__(self, name):
        if all([c in self.__class__._elements for c in name]):
            cl = Vec._cls[self.shape[-1]]
            idx = [self.__class__._elements.index(c) for c in name]
            arr = self[..., idx, :].view(cl)
            if cl is not np.ndarray:
                arr._b = [*self._b, ConvertedBehaviour]
                ConvertedBehaviour._init(
                    arr, {"_convert": partial(self.__setattr__, name)}
                )
            return arr
        else:
            self._raiseAttrError(name)

    def __setattr__(self, name, value):
        if all([c in self.__class__._elements for c in name]):
            idx = [self.__class__._elements.index(c) for c in name]
            self[..., idx, :] = value
        else:
            super().__setattr__(name, value)
