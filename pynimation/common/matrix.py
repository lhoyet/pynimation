from typing import Optional, Tuple

import numpy as np

from pynimation.common.rows import Rows
from pynimation.common.vec import Vec

from .array import Array


class Matrix(Array):
    """
    Base class for multi-dimensional arrays of matrices

    All operations are performed on the last axis

    The size of matrices is defined as 0 for this class, therefore
    any function using this pre-defined size
    (like :attr:`~pynimation.common.matrix.Matrix.identity`)
    cannot be used on this class

    **Swizzling** allows access and assignment to rows and columns of the
    matrices using any combination of letters (x,y,z,w) (See
    :attr:`~pynimation.common.matrix.Matrix.rows` and
    :attr:`~pynimation.common.matrix.Matrix.columns`)

    Examples
    --------

    Create 3x3 Matrix array of shape (2,2):

    >>> from pynimation.common.matrix import Matrix
    >>> import numpy as np
    >>>
    >>> # Cannot use Matrix.identity as Matrix size is 0
    >>> m=Matrix.identityLike(np.empty((2,2,3,3)))
    >>> m
    Matrix([[[[1., 0., 0.],
              [0., 1., 0.],
              [0., 0., 1.]],
        <BLANKLINE>
             [[1., 0., 0.],
              [0., 1., 0.],
              [0., 0., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
            [[[1., 0., 0.],
              [0., 1., 0.],
              [0., 0., 1.]],
        <BLANKLINE>
             [[1., 0., 0.],
              [0., 1., 0.],
              [0., 0., 1.]]]])

    Set 3rd element (:code:`z`) of first row (:code:`x`) to :code:`2`
    (equivalent to :code:`m[...,0,2] = 2`)

    >>> m.x.z = 2
    >>> m
    Matrix([[[[1., 0., 2.],
              [0., 1., 0.],
              [0., 0., 1.]],
        <BLANKLINE>
             [[1., 0., 2.],
              [0., 1., 0.],
              [0., 0., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
            [[[1., 0., 2.],
              [0., 1., 0.],
              [0., 0., 1.]],
        <BLANKLINE>
             [[1., 0., 2.],
              [0., 1., 0.],
              [0., 0., 1.]]]])

    Set 1st element (:code:`x`) of second column (:code:`Y`) to :code:`3` (equivalent to
    :code:`m1[...,0,1] = 3`)

    >>> m1 = Matrix.identityLike(m)
    >>> m1.Y.x  = 3
    >>> m1
    Matrix([[[[1., 3., 0.],
              [0., 1., 0.],
              [0., 0., 1.]],
        <BLANKLINE>
             [[1., 3., 0.],
              [0., 1., 0.],
              [0., 0., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
            [[[1., 3., 0.],
              [0., 1., 0.],
              [0., 0., 1.]],
        <BLANKLINE>
             [[1., 3., 0.],
              [0., 1., 0.],
              [0., 0., 1.]]]])

    Matrix by matrix multiplication of two arrays can be performed with the
    :code:`@` operator

    >>> m @ m1
    Matrix([[[[1., 3., 2.],
              [0., 1., 0.],
              [0., 0., 1.]],
        <BLANKLINE>
             [[1., 3., 2.],
              [0., 1., 0.],
              [0., 0., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
            [[[1., 3., 2.],
              [0., 1., 0.],
              [0., 0., 1.]],
        <BLANKLINE>
             [[1., 3., 2.],
              [0., 1., 0.],
              [0., 0., 1.]]]])

    Matrix by vector multiplication of two arrays can also be performed with
    the :code:`@` operator

    >>> v = m1.x.squeeze() # take vector array of 1st row of m1
    >>> v
    Vec3([[[1., 3., 0.],
           [1., 3., 0.]],
            <BLANKLINE>
          [[1., 3., 0.],
           [1., 3., 0.]]])
    >>>
    >>> m1 @ v
    array([[[10.,  3.,  0.],
            [10.,  3.,  0.]],
        <BLANKLINE>
           [[10.,  3.,  0.],
            [10.,  3.,  0.]]])
    """

    _dsize = 0
    _nd = 2
    _elements = Vec._elements

    def __new__(cls, array: Optional[np.ndarray] = None, **kwargs):
        array_: np.ndarray
        if array is None:
            array_ = cls.identity()
        else:
            array_ = array
        return Array.__new__(cls, array_, **kwargs)

    def __init__(self, array: Optional[np.ndarray] = None, **kwargs):
        super().__init__(self, **kwargs)

    def __imatmul__(self, other: np.ndarray) -> np.ndarray:  # type:ignore
        self[...] = self.__matmul__(other)
        return self

    def __matmul__(self, other: np.ndarray) -> np.ndarray:  # type:ignore
        return self.dot(other)

    def dot(self, other: np.ndarray) -> np.ndarray:  # type: ignore
        """
        Dot product between this array of matrices and :attr:`other`

        :attr:`other` can be a array of matrices, in which case matrix
        multiplication is performed, or an array of vectors, in which
        case vector application is performed

        Parameters
        ----------
        other:
            second operand of the dot product

        Returns
        -------
        np.ndarray:
            the dot product between self and :attr:`other`
        """
        if isinstance(other, Vec):
            return np.einsum("...ij,...j", self, other)
        else:
            return super().__matmul__(other)

    def __getattr__(self, name: str):
        if name.isupper():
            return getattr(self.cols, name.lower())
        elif name.islower():
            return getattr(self.rows, name)

    def __setattr__(self, name, value):
        nl = name.lower()
        if all([c in self.__class__._elements for c in nl]):
            idx = [self.__class__._elements.index(c) for c in nl]

            if name.isupper():
                self.T[..., idx, :] = value
            elif name.islower():
                self[..., idx, :] = value
        else:
            super().__setattr__(name, value)

    # call it reset ?
    def clear(self) -> None:
        """
        Sets all matrices of this array to the identity
        """
        self[...] = np.full(self.shape, np.identity(self.shape[-1]))

    @property
    def T(self) -> "Matrix":
        """
        Transpose of all the matrices of this array

        Returns
        -------
        Matrix:
            This array with every matrix transposed

        Note
        ----
        Each matrix is transposed individually, this is not a transpose of the
        array along other axes
        """
        r = range(self.ndim)
        return (np.transpose(self, axes=(*r[:-2], *r[-2:][::-1]))).view(
            self.__class__
        )

    @T.setter
    def T(self, newT: np.ndarray) -> None:
        if not isinstance(newT, np.ndarray):
            self[...] = newT
        elif len(newT.shape) < 2:
            self[...] = newT.T
        else:
            nt: Matrix = newT if isinstance(newT, Matrix) else Matrix(newT)
            self[...] = nt.T

    def _diagIndices(self) -> Tuple[np.ndarray, ...]:
        ind = np.indices(self.shape)[..., 0]
        ind[-2:] = np.diag_indices(self.shape[-1])[0]
        return tuple(ind)

    @property
    def diag(self) -> "Vec":
        """
        Diagonals of the matrices of this array, as vectors, one for each
        matrix of this array

        Returns
        -------
        Vec:
            Diagonals of all matrices of this array
        """
        return Vec(self[self._diagIndices()])

    @diag.setter
    def diag(self, newDiag: np.ndarray) -> None:
        self[self._diagIndices()] = newDiag

    @staticmethod
    def asDiag(diag: np.ndarray) -> "Matrix":
        """
        Create an array of matrices having :attr:`diag` as diagonals

        Parameters
        ----------
        diag:
            Array of vectors, one for each matrix diagonal

        Returns
        -------
        Matrix:
            An array of matrices of shape :code:`(*diag.shape[:-1], *(diag.shape[-1],) * 2)`

            One matrix for each diagonal vector of :attr:`diag`
        """
        m = Matrix.zeros((*diag.shape[:-1], *(diag.shape[-1],) * 2))
        m.diag = diag
        return m

    def inverse(self) -> "Matrix":
        """
        Inverse of every matrix in this array

        Returns
        -------
        Matrix:
            This array of matrices where every matrix has been inverted

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.matrix.Matrix.inv`
        """
        return np.linalg.inv(self)  # type: ignore

    def pinverse(self) -> "Matrix":
        """
        Pseudo inverse of every matrix in this array

        Returns
        -------
        Matrix:
            This array of matrices where every matrix has been pseudo inverted

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.matrix.Matrix.pinv`
        """
        return np.linalg.pinv(self)  # type: ignore

    @property
    def rows(self) -> "Rows":
        r"""
        Rows of this array as :class:`~pynimation.common.rows.Rows`

        This view enables access via swizzling (See :class:`~pynimation.common.rows.Rows`)

        Rows are accessed by the letters (x,y,z,w). Any combinations of letters
        can be used and they can be repeated

        Returns
        -------
        Rows:
            A view of this array as :class:`~pynimation.common.rows.Rows`

        Note
        ----
        | Swizzling can also be called directly on the :class:`~pynimation.common.matrix.Matrix`
        | Like :code:`m.x` for :code:`m.rows.x`

        Examples
        --------
        >>> from pynimation.common.matrix import Matrix
        >>>
        >>> m = Matrix.zeros((2,2,3,3))
        >>> m
        Matrix([[[[0., 0., 0.],
                  [0., 0., 0.],
                  [0., 0., 0.]],
        <BLANKLINE>
                 [[0., 0., 0.],
                  [0., 0., 0.],
                  [0., 0., 0.]]],
        <BLANKLINE>
        <BLANKLINE>
                [[[0., 0., 0.],
                  [0., 0., 0.],
                  [0., 0., 0.]],
        <BLANKLINE>
                 [[0., 0., 0.],
                  [0., 0., 0.],
                  [0., 0., 0.]]]])

        Assign to the first row (:code:`x`) by calling :code:`rows.x`
        (equivalent to :code:`m[...,0,:]`)

        >>> m.rows.x = 1
        >>> m
        Matrix([[[[1., 1., 1.],
                  [0., 0., 0.],
                  [0., 0., 0.]],
        <BLANKLINE>
                 [[1., 1., 1.],
                  [0., 0., 0.],
                  [0., 0., 0.]]],
        <BLANKLINE>
        <BLANKLINE>
                [[[1., 1., 1.],
                  [0., 0., 0.],
                  [0., 0., 0.]],
        <BLANKLINE>
                 [[1., 1., 1.],
                  [0., 0., 0.],
                  [0., 0., 0.]]]])
        >>> m.rows.x
        Vec3([[[[1., 1., 1.]],
        <BLANKLINE>
               [[1., 1., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
              [[[1., 1., 1.]],
        <BLANKLINE>
               [[1., 1., 1.]]]])

        Swizzling can also be called directly on the matrix

        >>> m.y = 2 # equivalent to m.rows.y
        >>> m
        Matrix([[[[1., 1., 1.],
                  [2., 2., 2.],
                  [0., 0., 0.]],
        <BLANKLINE>
                 [[1., 1., 1.],
                  [2., 2., 2.],
                  [0., 0., 0.]]],
        <BLANKLINE>
        <BLANKLINE>
                [[[1., 1., 1.],
                  [2., 2., 2.],
                  [0., 0., 0.]],
        <BLANKLINE>
                 [[1., 1., 1.],
                  [2., 2., 2.],
                  [0., 0., 0.]]]])
        """
        return self.view(Rows)

    @property
    def columns(self) -> "Rows":
        r"""
        Columns of this array as :class:`~pynimation.common.rows.Rows`

        This view enables access via swizzling (See :class:`~pynimation.common.rows.Rows`)

        Columns are accessed by the letters (x,y,z,w). Any combinations of letters
        can be used and they can be repeated

        Returns
        -------
        Rows:
            A view of this array, transposed, as :class:`~pynimation.common.rows.Rows`

        Note
        ----
        | Swizzling can also be called directly on the :class:`~pynimation.common.matrix.Matrix`
        | **with uppercase letters**
        | Like :code:`m.X` for :code:`m.columns.x`
        |
        | **Shorthand:** :attr:`~pynimation.common.matrix.Matrix.cols`

        Examples
        --------
        >>> from pynimation.common.matrix import Matrix
        >>>
        >>> m = Matrix.zeros((2,2,3,3))
        >>> m
        Matrix([[[[0., 0., 0.],
                  [0., 0., 0.],
                  [0., 0., 0.]],
        <BLANKLINE>
                 [[0., 0., 0.],
                  [0., 0., 0.],
                  [0., 0., 0.]]],
        <BLANKLINE>
        <BLANKLINE>
                [[[0., 0., 0.],
                  [0., 0., 0.],
                  [0., 0., 0.]],
        <BLANKLINE>
                 [[0., 0., 0.],
                  [0., 0., 0.],
                  [0., 0., 0.]]]])

        Assign to the first column (:code:`x`) (equivalent to :code:`m[...,0]`)

        >>> m.columns.x = 1
        >>> m
        Matrix([[[[1., 0., 0.],
                  [1., 0., 0.],
                  [1., 0., 0.]],
        <BLANKLINE>
                 [[1., 0., 0.],
                  [1., 0., 0.],
                  [1., 0., 0.]]],
        <BLANKLINE>
        <BLANKLINE>
                [[[1., 0., 0.],
                  [1., 0., 0.],
                  [1., 0., 0.]],
        <BLANKLINE>
                 [[1., 0., 0.],
                  [1., 0., 0.],
                  [1., 0., 0.]]]])
        >>> m.columns.x
        Vec3([[[[1., 1., 1.]],
        <BLANKLINE>
               [[1., 1., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
              [[[1., 1., 1.]],
        <BLANKLINE>
               [[1., 1., 1.]]]])

        Swizzling can also be called directly on the :class:`~pynimation.common.matrix.Matrix`
        with uppercase letters

        >>> m.Y = 2 # equivalent to m.columns.y
        >>> m
        Matrix([[[[1., 2., 0.],
                  [1., 2., 0.],
                  [1., 2., 0.]],
        <BLANKLINE>
                 [[1., 2., 0.],
                  [1., 2., 0.],
                  [1., 2., 0.]]],
        <BLANKLINE>
        <BLANKLINE>
                [[[1., 2., 0.],
                  [1., 2., 0.],
                  [1., 2., 0.]],
        <BLANKLINE>
                 [[1., 2., 0.],
                  [1., 2., 0.],
                  [1., 2., 0.]]]])
        """
        return self.T.view(Rows)

    @classmethod
    def identity(cls, shape=(), **kwargs):
        """
        Create an array of identity matrices of type :attr:`cls` and shape
        :attr:`shape`. The givent shape should not include the matrix shape
        (:code:`(cls._dsize,) * cls._nd`, see Examples)

        Parameters
        ----------
        shape:
            shape of the array of base objects

        Returns
        -------
        Matrix:
            an array of identity matrices of shape :code:`(*shape,
            *(cls._dsize,) * cls._nd)`

        Note
        ----
        | Additional arguments of the constructor of :attr:`cls` can be passed
        |
        | **Shorthand:** :attr:`~pynimation.common.matrix.Matrix.id`

        Examples
        --------
        >>> from pynimation.common.rotation import Rotation
        >>>
        >>> r = Rotation.identity((2,2))
        >>> r.shape
        (2, 2, 3, 3)
        >>> r
        Rotation([[[[1., 0., 0.],
                    [0., 1., 0.],
                    [0., 0., 1.]],
        <BLANKLINE>
                   [[1., 0., 0.],
                    [0., 1., 0.],
                    [0., 0., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
                  [[[1., 0., 0.],
                    [0., 1., 0.],
                    [0., 0., 1.]],
        <BLANKLINE>
                   [[1., 0., 0.],
                    [0., 1., 0.],
                    [0., 0., 1.]]]])

        """
        shape = (
            *shape,
            *((cls._dsize,) * cls._nd),
        )
        return cls(np.full(shape, np.identity(shape[-1])), **kwargs)

    @classmethod
    def identityLike(cls, obj, **kwargs):
        """
        Create an array of identity matrices of type :attr:`cls` and of the
        the same shape that :attr:`obj`

        Parameters
        ----------
        obj:
            array of which the created array will take the shape

        Returns
        -------
        Matrix:
            an array of identity matrices of the same shape that :attr:`obj`

        Note
        ----
        Additional arguments of the constructor of :attr:`cls` can be passed

        Examples
        --------
        >>> from pynimation.common.matrix import Matrix
        >>>
        >>> a = Matrix.zeros((2,2,3,3))
        >>> m = Matrix.identityLike(a)
        >>> m.shape
        (2, 2, 3, 3)
        >>> m
        Rotation([[[[1., 0., 0.],
                    [0., 1., 0.],
                    [0., 0., 1.]],
        <BLANKLINE>
                   [[1., 0., 0.],
                    [0., 1., 0.],
                    [0., 0., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
                  [[[1., 0., 0.],
                    [0., 1., 0.],
                    [0., 0., 1.]],
        <BLANKLINE>
                   [[1., 0., 0.],
                    [0., 1., 0.],
                    [0., 0., 1.]]]])
        """
        return cls(np.full(obj.shape, np.identity(obj.shape[-1])), **kwargs)

    # shortcuts

    inv = inverse
    pinv = pinverse

    cols = columns

    id = identity
