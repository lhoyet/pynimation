from typing import Tuple
import numpy as np

from .common import returnView
from .vec import Vec3, Vec4


class Quaternion(Vec4):
    """
    Multidimensional arrays of quaternions as
    :class:`~pynimation.common.vec.Vec4` vectors

    Supports addition, subtraction, multiplication,
    division, negation, absolute, all defined
    in terms of quaternion operations such as quaternion
    multiplication.

    The Quaternion class has been desgined such that it
    should support broadcasting and slicing in all of the
    usual ways.

    Warnings
    --------
    The choosen representation is **w last**

    Meaning that quaternions are stored as :code:`[x, y, z, w]` for
    :code:`xi + yj + zk + w`

    It is possible to obtain and set the w first representation (:code:`[w,x,y,z]`)
    with the :attr:`~pynimation.common.quaternion.Quaternion.wFirst` attribute
    (Equivalent to :code:`self.wxyz`)

    References
    ----------

    Greatly inspired by:

    .. [1] Daniel Holden, Jun Saito, and Taku Komura. 2016. A deep learning
       framework for character motion synthesis and editing. ACM Trans. Graph.
       35, 4, Article 138 (July 2016), 11 pages. https://doi.org/10.1145/2897824.2925975
    """

    def __new__(cls, array: np.ndarray, wLast: bool = True, **kwargs):
        obj = Vec4.__new__(cls, array, **kwargs)
        if not wLast:
            obj = obj[..., [1, 2, 3, 0]]
        return obj

    def __init__(self, array: np.ndarray, wLast: bool = True, **kwargs):
        """
        Parameters
        ----------

        array:
            numpy array to create the quaternion array from. Shape ``(..., 4)``
        wLast:
            whether :attr:`array` as the ``w`` element first or last. Will
            convert to last if first
        kwargs:
            additional parameters for the array behaviours
        """
        super().__init__(array, **kwargs)

    @classmethod
    def _broadcast(cls, sqs, oqs, scalar=False):
        if isinstance(oqs, float):
            return sqs, oqs * np.ones(sqs.shape[:-1])

        ss = np.array(sqs.shape) if not scalar else np.array(sqs.shape[:-1])
        os = np.array(oqs.shape)

        if len(ss) != len(os):
            raise TypeError(
                "Quaternion cannot broadcast together shapes %s and %s"
                % (sqs.shape, oqs.shape)
            )

        if np.all(ss == os):
            return sqs, oqs

        if not np.all(
            (ss == os) | (os == np.ones(len(os))) | (ss == np.ones(len(ss)))
        ):
            raise TypeError(
                "Quaternion cannot broadcast together shapes %s and %s"
                % (sqs.shape, oqs.shape)
            )

        sqsn, oqsn = sqs.copy(), oqs.copy()

        for a in np.where(ss == 1)[0]:
            sqsn = sqsn.repeat(os[a], axis=a)
        for a in np.where(os == 1)[0]:
            oqsn = oqsn.repeat(ss[a], axis=a)

        return sqsn, oqsn

    def __add__(self, other):
        return self * other

    def __sub__(self, other):
        return self / other

    def __mul__(self, other):
        """
        Quaternion multiplication has three main methods.

        When multiplying a Quaternion array by Quaternion
        normal quaternion multiplication is performed.

        When multiplying a Quaternion array by a vector
        array of the same shape, where the last axis is 3,
        it is assumed to be a Quaternion by 3D-Vector
        multiplication and the 3D-Vectors are rotated
        in space by the Quaternion.

        When multipplying a Quaternion array by a scalar
        or vector of different shape it is assumed to be
        a Quaternion by Scalars multiplication and the
        Quaternion are scaled using Slerp and the identity
        quaternions.
        """

        """ If Quaternion type do Quaternion * Quaternion """
        if isinstance(other, Quaternion):
            sqs, oqs = Quaternion._broadcast(self, other)
            sqs = sqs.view(np.ndarray)
            oqs = oqs.view(np.ndarray)

            q0 = sqs[..., 3]
            q1 = sqs[..., 0]
            q2 = sqs[..., 1]
            q3 = sqs[..., 2]
            r0 = oqs[..., 3]
            r1 = oqs[..., 0]
            r2 = oqs[..., 1]
            r3 = oqs[..., 2]

            qs = np.empty(sqs.shape)
            qs[..., 3] = r0 * q0 - r1 * q1 - r2 * q2 - r3 * q3
            qs[..., 0] = r0 * q1 + r1 * q0 - r2 * q3 + r3 * q2
            qs[..., 1] = r0 * q2 + r1 * q3 + r2 * q0 - r3 * q1
            qs[..., 2] = r0 * q3 - r1 * q2 + r2 * q1 + r3 * q0

            return Quaternion(qs)

        """ If array type do Quaternion * Vectors """
        if isinstance(other, np.ndarray) and other.shape[-1] == 3:
            vs = Quaternion(
                np.concatenate(
                    [other, np.zeros(other.shape[:-1] + (1,))], axis=-1
                )
            )
            return (self * (vs * self.inverse())).imaginary

        """ If float do Quaternion * Scalars """
        if isinstance(other, np.ndarray) or isinstance(other, float):
            return Quaternion._slerp(Quaternion.id_like(self), self, other)

        raise TypeError(
            "Cannot multiply/add Quaternion with type %s" % str(type(other))
        )

    def __div__(self, other):
        """
        When a Quaternion type is supplied, division is defined
        as multiplication by the inverse of that Quaternion.

        When a scalar or vector is supplied it is defined
        as multiplicaion of one over the supplied value.
        Essentially a scaling.
        """

        if isinstance(other, Quaternion):
            return self * (-other)
        if isinstance(other, np.ndarray):
            return self * (1.0 / other)
        if isinstance(other, float):
            return self * (1.0 / other)
        raise TypeError(
            "Cannot divide/subtract Quaternion with type %s" + str(type(other))
        )

    # def __neg__(self):
    #     """Invert Quaternion"""
    #     return Quaternion(self.view(np.ndarray) * np.array([-1, -1, -1, 1]))

    def __abs__(self):
        """Unify Quaternion To Single Pole"""
        qabs = self.normalized().copy()
        top = np.sum((qabs) * np.array([0, 0, 0, 1]), axis=-1)
        bot = np.sum((-qabs) * np.array([0, 0, 0, 1]), axis=-1)
        qabs[top < bot] = -qabs[top < bot]
        return qabs

    @returnView(np.ndarray)
    def length(self):
        """
        Norm of the quaternions
        """
        return np.sum(self**2.0, axis=-1) ** 0.5

    @property  # type: ignore
    @returnView(np.ndarray)
    def real(self) -> np.ndarray:
        """
        Real part of the quaternions
        """
        return self[..., 3]

    @real.setter
    def real(self, newReal: np.ndarray) -> None:
        self[..., 3] = newReal

    @property  # type: ignore
    @returnView(Vec3)
    def imaginary(self) -> np.ndarray:
        """
        Imaginary part of the quaternions
        """
        return self[..., 0:3]

    @imaginary.setter
    def imaginary(self, newImaginary: np.ndarray) -> None:
        self[..., 0:3] = newImaginary

    @property  # type: ignore
    @returnView(np.ndarray)
    def wFirst(self) -> np.ndarray:
        """
        This array represented with the w coefficient first instead of last
        (:code:`(w,x,y,z)` instead of :code:`(x,y,z,w)`)
        """
        return self.wxyz

    @wFirst.setter
    def wFirst(self, newWfirst: np.ndarray) -> None:
        self.wxyz = newWfirst

    def normalized(self):
        """
        This array of quaternions normalized (divided by its norm)
        """
        return Quaternion(self / self.length()[..., np.newaxis])

    @returnView(np.ndarray)
    def angle(self):
        """
        Angles of the quaternion rotations
        """
        return np.arccos(self.real) * 2

    def inverse(self):
        """
        Inverse of the Quaternion rotations
        """
        return Quaternion(
            (self.np * np.array([-1, -1, -1, 1]))
            / np.sum(np.power(self.np, 2), axis=-1)[..., np.newaxis]
        )

    def log(self):
        """
        Log of the Quaternion rotations
        """
        norm = np.abs(self.normalized())
        imgs = norm.imaginary
        lens = np.sqrt(np.sum(imgs**2, axis=-1))
        lens = np.arctan2(lens, norm.real) / (lens + 1e-10)
        return imgs * lens[..., np.newaxis]

    @classmethod
    def identity(cls, n: Tuple) -> "Quaternion":
        """
        Create an array of identity quaternions of shape :attr:`n`

        Parameters
        ----------
        n:
            shape of the quaternion array to create

        Returns
        -------
        Quaternion:
            an array of identity quaternions of shape :attr:`n`
        """
        qs = Quaternion.zeros(n)
        qs.real = 1.0
        return qs

    @classmethod
    def idLike(cls, a: np.ndarray) -> "Quaternion":
        """
        Create an array of identity quaternions of the same shape that
        :attr:`a`

        Parameters
        ----------
        a:
            array to take the shape from

        Returns
        -------
        Quaternion:
            identity quaternion array of shape :code:`a.shape`
        """
        return Quaternion.identity(a.shape)

    @classmethod
    def exp(cls, ws: np.ndarray) -> "Quaternion":
        """
        Exponentials of quaternions, with weights :code:`ws`

        Parameters
        ----------
        ws:
            weights of the exponential

        Returns
        -------
        Quaternion:
            Exponentials of this array
        """
        ts = np.sum(ws**2.0, axis=-1) ** 0.5
        ts[ts == 0] = 0.001
        ls = np.sin(ts) / ts

        qs = np.empty(ws.shape[:-1] + (4,))
        qs[..., 3] = np.cos(ts)
        qs[..., 0] = ws[..., 0] * ls
        qs[..., 1] = ws[..., 1] * ls
        qs[..., 2] = ws[..., 2] * ls

        return Quaternion(qs).normalized()

    @classmethod
    def between(cls, v0s: np.ndarray, v1s: np.ndarray) -> "Quaternion":
        """
        Compute quaternion rotations between respective vectors of arrays
        :attr:`v0s` and :attr:`v1s`

        Parameters
        ----------
        v0s:
            Source vector array
        v1s:
            Target vector array

        Returns
        -------
        Quaternion:
            quaternion rotations between vectors of :attr:`v0s` and :attr:`v1s`
        """
        a = np.cross(v0s, v1s)
        w = np.sqrt((v0s**2).sum(axis=-1) * (v1s**2).sum(axis=-1)) + (
            v0s * v1s
        ).sum(axis=-1)
        return Quaternion(
            np.concatenate([w[..., np.newaxis], a], axis=-1)
        ).normalized()

    @classmethod
    def _slerp(cls, q0s, q1s, a):
        fst, snd = cls._broadcast(q0s, q1s)
        fst, a = cls._broadcast(fst, a, scalar=True)
        snd, a = cls._broadcast(snd, a, scalar=True)

        len = np.sum(fst * snd, axis=-1)

        neg = len < 0.0
        len[neg] = -len[neg]
        snd[neg] = -snd[neg]

        amount0 = np.zeros(a.shape)
        amount1 = np.zeros(a.shape)

        linear = (1.0 - len) < 0.01
        omegas = np.arccos(len[~linear])
        sinoms = np.sin(omegas)

        amount0[linear] = 1.0 - a[linear]
        amount1[linear] = a[linear]
        amount0[~linear] = np.sin((1.0 - a[~linear]) * omegas) / sinoms
        amount1[~linear] = np.sin(a[~linear] * omegas) / sinoms

        return Quaternion(
            amount0[..., np.newaxis] * fst + amount1[..., np.newaxis] * snd
        )
