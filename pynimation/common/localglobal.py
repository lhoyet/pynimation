import contextlib
from typing import Any, List, Optional, Tuple
from collections.abc import Iterable
from functools import partial

import numpy as np
from reactivex import operators as op
from reactivex.subject import Subject

from .array import GraphViewBehaviour, ObservableBehaviour, ConvertedBehaviour
from .graph import Graph, Node
from .transform import Transform


class LocalGlobal(Transform):
    """
    Multi-dimensional arrays of transforms
    of a :class:`~pynimation.common.graph.Graph` (one for each node).

    Shape must be be :code:`(..., len(graph), 4, 4)`

    Holds transforms of a node relative to its parent (local transforms,
    the array itself) and to the world (global transforms,
    :attr:`~pynimation.common.localglobal.LocalGlobal.globals attribute)

    The transforms of node :code:`n` can be accessed with:
    :code:`self[...,n.id,:,:]`

    Provides methods to compute global transforms from local transforms
    (:func:`~pynimation.common.localglobal.LocalGlobal.updateGlobals`,
    :func:`~pynimation.common.localglobal.LocalGlobal.updateAllGlobals`,
    :func:`~pynimation.common.localglobal.LocalGlobal.updateDirtyGlobals`,
    :func:`~pynimation.common.localglobal.LocalGlobal.updateGlobalsFrames`)

    Changes in local transforms can be tracked with
    :class:`~pynimation.common.array.ObservableBehaviour` so that only invalid
    global transforms can be updated

    Global transforms' updates can be made automatic, everytime the
    :attr:`~pynimation.commmon.localglobal.LocalGlobal.globals` attribute
    is accessed, by setting the :attr:`~pynimation.commmon.localglobal.LocalGlobal.autoUpdate`
    attribute. This behaviour can be temporarily disabled with the
    :attr:`~pynimation.commmon.localglobal.LocalGlobal.holdUpdate` context
    manager

    This class also provides a method to update the array from changes in the
    graph
    (:attr:`~pynimation.commmon.localglobal.LocalGlobal.updateFromGraph`).
    If node ids change, their transforms will be moved accordingly. If nodes
    are removed or added, their transforms will be removed or added.

    Updates from the graph can be made automatic, by settings the
    :attr:`~pynimation.commmon.localglobal.LocalGlobal.autoUpdateGraph`
    attribute. This behaviour can be temporarily disabled with the
    :attr:`~pynimation.commmon.localglobal.LocalGlobal.holdUpdateGraph` context
    manager

    Transforms of multiple nodes can be accessed and assigned with a single
    call to :func:`~pynimation.commmon.localglobal.LocalGlobal.node`, that
    returns a :class:`~pynimation.common.localglobal.Localglobal.NodeView`

    Examples
    --------

    Make a linear graph of depth 3:

    >>> from pynimation.common.localglobal import LocalGlobal
    >>> from pynimation.common.graph import Graph,Node
    >>>
    >>> g = Graph(Node(""))
    >>> for _ in range(3):
    ...     g.nodes[-1].addChild(Node(""))
    ...
    >>> g
    [0]Node_8738211643239
        [1]Node_8738211642525
            [2]Node_8738211482450
                [3]Node_8738198313465

    Make a LocalGlobal array matching the number of nodes of the graph.
    There is a only a single transform per node here, but there could be additional dimensions.
    :class:`~pynimation.common.array.GraphViewBehaviour` causes the array to
    be printed in a graph view (hierarchically), with local transforms on the left and global
    transforms on the right under the node they correspond to, with its id
    (:code:`[0]) and its name (:code:`Node_8738211643239`)

    >>> lg = LocalGlobal.identity((4,), graph=g)
    >>> lg
    [0] Node_8738211643239
        [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
         [0. 1. 0. 0.]     [0. 1. 0. 0.]
         [0. 0. 1. 0.]     [0. 0. 1. 0.]
         [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
        [1] Node_8738211642525
            [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [2] Node_8738211482450
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
                [3] Node_8738198313465
                    [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                     [0. 1. 0. 0.]     [0. 1. 0. 0.]
                     [0. 0. 1. 0.]     [0. 0. 1. 0.]
                     [0. 0. 0. 1.]]    [0. 0. 0. 1.]]

        Transforms of a node can be accessed using the
        :func:`~pynimation.commmon.localglobal.LocalGlobal.node` method.
        Here, this is equivalent to :code:`self[0]` because there is a single
        transform per node, but with additional dimensions, this would be
        equivalent to :code:`self[...,0,:,:]`

        Because we changed the position of the transforom of the root node (id
        0), and the globals were accessed to be printed, they were updated
        automatically so that each global transform is based on the global
        transform of the node's parent

        >>> lg.node[g.root].position.x = 2 # node[] takes a node or a node id
        >>> lg
        [0] Node_8738211643239
            [[1. 0. 0. 2.]    [[1. 0. 0. 2.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8738211642525
                [[1. 0. 0. 0.]    [[1. 0. 0. 2.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
                [2] Node_8738211482450
                    [[1. 0. 0. 0.]    [[1. 0. 0. 2.]
                     [0. 1. 0. 0.]     [0. 1. 0. 0.]
                     [0. 0. 1. 0.]     [0. 0. 1. 0.]
                     [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
                    [3] Node_8738198313465
                        [[1. 0. 0. 0.]    [[1. 0. 0. 2.]
                         [0. 1. 0. 0.]     [0. 1. 0. 0.]
                         [0. 0. 1. 0.]     [0. 0. 1. 0.]
                         [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
        >>>
        >>> lg.node[[1,2]].position.y = 3
        >>> lg
        [0] Node_8738211643239
            [[1. 0. 0. 2.]    [[1. 0. 0. 2.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8738211642525
                [[1. 0. 0. 0.]    [[1. 0. 0. 2.]
                 [0. 1. 0. 3.]     [0. 1. 0. 3.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
                [2] Node_8738211482450
                    [[1. 0. 0. 0.]    [[1. 0. 0. 2.]
                     [0. 1. 0. 3.]     [0. 1. 0. 6.]
                     [0. 0. 1. 0.]     [0. 0. 1. 0.]
                     [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
                    [3] Node_8738198313465
                        [[1. 0. 0. 0.]    [[1. 0. 0. 2.]
                         [0. 1. 0. 0.]     [0. 1. 0. 6.]
                         [0. 0. 1. 0.]     [0. 0. 1. 0.]
                         [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
        >>>
        >>> lg.globals
        Transform([[[1., 0., 0., 2.],
                    [0., 1., 0., 0.],
                    [0., 0., 1., 0.],
                    [0., 0., 0., 1.]],
        <BLANKLINE>
                   [[1., 0., 0., 2.],
                    [0., 1., 0., 3.],
                    [0., 0., 1., 0.],
                    [0., 0., 0., 1.]],
        <BLANKLINE>
                   [[1., 0., 0., 2.],
                    [0., 1., 0., 6.],
                    [0., 0., 1., 0.],
                    [0., 0., 0., 1.]],
        <BLANKLINE>
                   [[1., 0., 0., 2.],
                    [0., 1., 0., 6.],
                    [0., 0., 1., 0.],
                    [0., 0., 0., 1.]]])
    """

    class NodeView:
        """
        Wrapper around a :class:`~pynimation.common.localglobal.LocalGlobal`
        array that can be used to access and assign transforms of one or multiple
        nodes through :code:`__getitem__` and :code:`__setitem__`

        >>> lg.node[node1, node2]
        >>> lg.node[node1.id, node2.id].position.y = 2

        """

        def __init__(self, arr: "LocalGlobal"):
            """
            Parameters
            ----------
            arr:
                array to get the node view of
            """
            #: wrapped array
            self.arr: "LocalGlobal" = arr

        @staticmethod
        def __getids(idx):
            if not isinstance(idx, Iterable):
                return idx.id if isinstance(idx, Node) else idx
            return tuple([a.id if isinstance(a, Node) else a for a in idx])

        def __getitem__(self, idx):
            ids = self.__getids(idx)
            narr = self.arr[..., ids, :, :]
            if isinstance(ids, tuple):
                narr._b = [*self.arr._b, ConvertedBehaviour]
                ConvertedBehaviour._init(
                    narr, {"_convert": partial(self.__setitem__, idx)}
                )
            return narr

        def __setitem__(self, idx, val):
            self.arr[..., self.__getids(idx), :, :] = val

    def __new__(
        cls,
        locals_: np.ndarray,
        graph: Graph,
        autoUpdate: bool = True,
        autoUpdateGraph: bool = True,
    ):
        # FIXME: check in Array, with nd and dsize as tuple ?
        # LocalGlobal._checkSize(graph, locals_.shape)
        obj = super().__new__(cls, locals_)
        return obj

    def __init__(
        self,
        locals_: np.ndarray,
        graph: Graph,
        autoUpdate: bool = True,
        autoUpdateGraph: bool = True,
    ):
        """
        Parameters
        ----------

        locals_:
            numpy array of local transforms, shape ``(len(graph), 4, 4)``
        graph:
            graph of the localglobal array, used to compute global transforms
        autoUpdate:
            whether the global transforms should be updated when accessed, see
            :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdate`
        autoUpdateGraph:
            whether the animation array should be updated to reflect changes in the
            graph topology when they happen, see
            :attr:`~pynimation.common.localglobal.LocalGlobal.updateFromGraph` for more
        """
        self.autoUpdate: bool = autoUpdate
        super().__init__(locals_)
        self._addBehaviour(GraphViewBehaviour, graph=graph)

        self._globals: "Transform" = self._makeGlobals(self.shape)
        assert self._globals.shape == self.shape

        self._graph: Graph = graph
        self._oldnodes: List["Node"] = self._graph._nodes.copy()

        self.autoUpdateGraph: bool = autoUpdateGraph
        """
        whether the animation array should be updated to reflect changes in the
        graph topology when they happen, see
        :attr:`~pynimation.common.localglobal.LocalGlobal.updateFromGraph` for more
        """

        self._obs: Subject = Subject()
        self._subs: List[Any] = []
        self._subs.append(
            self._obs.pipe(
                op.filter(lambda x: (self.autoUpdate and np.any(self._dirty[x])))  # type: ignore
            ).subscribe(on_next=self.updateGlobals)
        )
        self._subs.append(
            self._graph._obs.pipe(
                op.filter(lambda x: self.autoUpdateGraph),  # type: ignore
            ).subscribe(on_next=lambda x: getattr(self, "updateFromGraph")())
        )

    def sameWith(self, array: np.ndarray) -> "LocalGlobal":
        return self.__class__(
            array, self.graph, self.autoUpdate, self.autoUpdateGraph
        )

    def __array_finalize__(self, obj):
        if hasattr(obj, "_globals"):
            self._globals = obj._globals
        else:
            self._globals = self._makeGlobals(obj.shape)

        if hasattr(obj, "_obs"):
            self._obs = obj._obs
        else:
            self._obs = Subject()

        if hasattr(obj, "_oldnodes"):
            self._oldnodes = obj._oldnodes
        else:
            self._oldnodes = []

        if hasattr(obj, "autoUpdateGraph"):
            self.autoUpdateGraph = obj.autoUpdateGraph
        else:
            self.autoUpdateGraph = True

        super().__array_finalize__(obj)

    def __del__(self):
        if hasattr(self, "_subs"):
            for s in self._subs:
                s.dispose()

    def resize(self, shape: Tuple) -> None:  # type: ignore
        LocalGlobal._checkSize(self.graph, shape)
        super().resize(shape, refcheck=False)
        self._globals = self._makeGlobals(self.shape)

    def copy(self):
        c = self.__class__(np.ndarray.copy(self), self.graph.copy())
        c.updateAllGlobals()
        return c

    @classmethod
    def _checkSize(cls, graph, shape) -> None:
        if len(shape) < 3:
            raise ValueError(
                "%s array should have a minimum of 3 dimensions (%d found)"
                % (
                    cls.__name__,
                    len(shape),
                )
            )
        if shape[-3] != len(graph):
            raise ValueError(
                "%s array dim %d should match number of graph nodes (%d != %d)"
                % (
                    cls.__name__,
                    len(shape) - 3,
                    shape[-3],
                    len(graph),
                )
            )

    @staticmethod
    def _pad(lines: str):
        ll = lines.split("\n")
        m = max([len(l_) for l_ in ll])
        return "\n".join([l_ + " " * (m - len(l_)) for l_ in ll])

    @staticmethod
    def _tab(tab: str, *liness: str, delim: str = "   ") -> str:
        return "\n".join(
            [
                tab + delim.join(lines)
                for lines in zip(*[lines.split("\n") for lines in liness])
            ]
        )

    def __getitem__(self, idx, *args, **kwargs):
        # print("LocalGlobal", "getitem")
        arr = super().__getitem__(idx, *args, **kwargs)
        if isinstance(arr, self.__class__):
            arr._globals = self._globals[idx]
            arr._graph = self._graph
        return arr

    def __setitem__(self, idx, item) -> None:
        # print("LocalGlobal", "setitem")
        super().__setitem__(idx, item)

    @property
    def globals(self) -> np.ndarray:
        """
        Global transforms of this array

        When this attribute is accessed, if
        :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdate` is set, global
        transforms are updated

        If :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdate` is
        set, changes to the array are tracked, and global transforms are
        updated where needed. Although checking for changes in the array is not
        an expensive operation, tracking changes consumes additional memory,
        and some performance improvements can be obtained
        by disabling the automatic update and tracking behaviour, particularly if this
        globals are accessed often and no updates are needed, or locals have a
        lot of updates. This can also be done via the
        :attr:`~pynimation.commmon.localglobal.LocalGlobal.holdUpdate`
        context manager

        See the Local and Global transforms section for more general
        informations

        Warnings
        --------
        Setting local transforms from global transforms is not supported

        Meaning that modifying global transforms will have no effect on local
        transforms, modifications will just be overwritten by the next globals
        update

        Locals can be computed from globals by hand by taking the product of the
        inverse of the parent's global transform and the desired global transform


        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.localglobal.LocalGlobal.glob`,
        :attr:`~pynimation.common.localglobal.LocalGlobal.g`
        """
        self._obs.on_next(None)
        return self._globals

    @property
    def autoUpdate(self) -> bool:
        """
        Whether to update :attr:`~pynimation.common.localglobal.LocalGlobal.globals`

        When this attribute is set, global transforms are updated each time they
        are accessed. Changes to local transforms will also be tracked so that
        globals are only recomputed when needed

        This can also be
        done via the :attr:`~pynimation.commmon.localglobal.LocalGlobal.holdUpdate`
        context manager

        See also
        --------
        :attr:`~pynimation.common.localglobal.LocalGlobal.holdUpdate`
        """
        return self._hasBehaviour(ObservableBehaviour)

    @autoUpdate.setter
    def autoUpdate(self, newAutoUpdate: bool) -> None:
        if self.autoUpdate != newAutoUpdate:
            if newAutoUpdate:
                self._addBehaviour(ObservableBehaviour)
            else:
                self._rmBehaviour(ObservableBehaviour)

    @property
    def graph(self) -> Graph:
        """
        The graph bound to this array

        Its number of nodes must correspond to :code:`self.shape[-3]`

        When the topology of the graph changes, and
        :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdateGraph` is
        set,
        :func:`~pynimation.common.localglobal.LocalGlobal.updateFromGraph`
        is called, updating the array to reflect the changes of the graph
        topology
        """
        return self._graph

    @graph.setter
    def graph(self, newGraph) -> None:
        self._graph = newGraph

    @classmethod
    def fromGraph(cls, graph: "Graph", **kwargs) -> "LocalGlobal":
        """
        Create LocalGlobal array full of identity transforms of shape
        :code:`(len(graph), 4, 4)` from :attr:`graph`

        Parameters
        ----------
        graph:
            graph to create the LocalGlobal array from

        Returns
        -------
        LocalGlobal:
            LocalGlobal array of size :code:`(len(graph), 4, 4)`

        Note
        ----
        Additional arguments of the constructor of :attr:`cls` can be passed
        """
        return cls(Transform.identity((len(graph),)), graph=graph, **kwargs)

    def _makeGlobals(self, shape: Tuple) -> "Transform":
        return Transform.identity(shape[:-2])

    def updateGlobals(self, idx=None) -> None:
        """
        Update global transforms, if possible tries to update the minimum
        number of transforms based on previously tracked modifications to
        local transforms, otherwise updates everything

        If :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdate`
        is set, this function will be called automatically

        See also
        --------
        :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdate`
        """
        if self._hasBehaviour(ObservableBehaviour):
            if len(self.shape) == 3:
                self.updateAllGlobals()
            else:
                self.updateDirtyGlobals()
            self._dirty[...] = False
        else:
            self.updateAllGlobals()

    def updateAllGlobals(self, idx=None) -> None:
        """
        Update all global transforms of this array, regardless of if
        they need updates or not
        """
        self.updateGlobalsFrames((Ellipsis,))

    def updateDirtyGlobals(self) -> None:
        """
        Specifically computes which global transforms need updates
        based on previous tracked modifications of local transforms,
        and then updates them

        Does not check whether this computation is actually useful
        based on the size of the array or the state of the tracking
        """
        frames = np.where(
            np.any(self._dirty, axis=tuple(range(1, len(self.shape))))
        ) + (slice(None),) * (len(self.shape) - 4)
        self.updateGlobalsFrames(frames)

    # TODO: benchmark with and without index tuple construction
    def updateGlobalsFrames(self, frames: Tuple) -> None:
        """
        Update globals transforms of index tuple :attr:`frames`

        :attr:`frames` is a tuple of indices, slices, and :code:`Ellipsis`
        that is used to determine which part of the global transforms array
        will be updated

        The sub-array indices are computed with

        >>> idx = (*frames, joint.id, :, :)
        >>> self.globals[idx] = ...

        Parameters
        ----------
        frames:
            tuple of indices used as :code:`self.globals[(*frames, joint.id, :, :)]`
            to determine which part of the array to update
        """
        parents = self._graph.parents

        selfnp = self.np
        globnp = self._globals.np
        end = (slice(None),) * 2

        self._globals[frames + (0,) + end] = selfnp[frames + (0,) + end]

        for i in range(1, self.shape[-3]):
            self._globals[frames + (i,) + end] = (
                globnp[frames + (parents[i],) + end]
                @ selfnp[frames + (i,) + end]
            )

    def updateFromGraph(self, graph: Optional[Graph] = None) -> None:
        """
        Update this array from changes of
        :attr:`~pynimation.common.localglobal.LocalGlobal.graph` (or
        :attr:`graph` if provided) since this function was last called
        (or the array was created)

        This function is called automatically when the graph topology changes
        and :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdateGraph`
        is set

        Based on the list of nodes before and after the update, it will remove,
        add, and move transforms around so that nodes keep their transform
        if their id changes, new nodes get new transforms, and transforms of
        removed nodes are removed

        See also
        --------
        :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdateGraph`

        Examples
        --------
        >>> from pynimation.common.localglobal import LocalGlobal
        >>> from pynimation.common.graph import Graph,Node
        >>>
        >>> g = Graph(Node(""))
        >>> g.root.addChild(Node(""))
        >>>
        >>> lg = LocalGlobal.identity((2,), graph=g)
        >>> lg
        [0] Node_8776428772794
            [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8776428772785
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
        >>>
        >>> g.nodes[1].addChild(Node(""))
        >>> g
        [0]Node_8776428772977
            [1]Node_8776428773043
                [2]Node_8776428772971

        Adding a node to the graph adds a transform to the array

        >>> lg
        [0] Node_8776428772977
            [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8776428773043
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
                [2] Node_8776428772971
                    [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                     [0. 1. 0. 0.]     [0. 1. 0. 0.]
                     [0. 0. 1. 0.]     [0. 0. 1. 0.]
                     [0. 0. 0. 1.]]    [0. 0. 0. 1.]]


        """
        if graph is None:
            graph = self._graph
        else:
            self._graph = graph

        # old -> new map
        onmap = [
            self._oldnodes.index(x) if x in self._oldnodes else None
            for x in graph._nodes
        ]
        assert all(
            [
                n.parent._id < n._id if n.parent is not None else True
                for n in graph._nodes
            ]
        )
        idxs = [i for i in onmap if i is not None]
        if len(idxs) > 0:
            assert min(idxs) >= 0 and max(idxs) < self.shape[-3]
            old = np.append(
                self,
                np.full(
                    (*self.shape[:-3], 1, *self.shape[-2:]), np.identity(4)
                ),
                axis=-3,
            )
            super().resize(
                (*self.shape[:-3], len(onmap), *self.shape[-2:]),
                refcheck=False,
            )
            last = old.shape[-3] - 1
            takelast = [i if i is not None else last for i in onmap]
            self[...] = old[..., takelast, :, :]
        if self.autoUpdate:
            self._dirty = np.ones(self.shape, dtype=bool)
        self._globals = self._makeGlobals(self.shape)
        self._oldnodes = self._graph._nodes.copy()

    @contextlib.contextmanager
    def holdUpdate(self):
        """
        Context manager to temporarily disable global transforms updates and
        call
        :func:`~pynimation.common.localglobal.LocalGlobal.updateGlobals`
        afterwards

        See also
        --------
        :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdate`

        Examples
        --------
        >>> from pynimation.common.localglobal import LocalGlobal
        >>> from pynimation.common.graph import Graph,Node
        >>>
        >>> g = Graph(Node(""))
        >>> g.root.addChild(Node(""))
        >>>
        >>> lg = LocalGlobal.identity((2,), graph=g)
        >>> lg
        [0] Node_8776428772794
            [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8776428772785
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]

        Globals Updates will be disabled inside the with block:

        >>> with lg.holdUpdate() as _:
        ...     lg[0].position.y = 1
        ...     lg[0].position.x = 2
        ...     lg
        ...
        [0] Node_8776428772794
            [[1. 0. 0. 2.]    [[1. 0. 0. 0.]
             [0. 1. 0. 1.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8776428772785
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]

        They are updated and re-enabled when the end of the with block is
        reached:

        >>> lg.autoUpdate = False # show that globals were updated before
        >>> lg
        [0] Node_8776428772794
            [[1. 0. 0. 2.]    [[1. 0. 0. 2.]
             [0. 1. 0. 1.]     [0. 1. 0. 1.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8776428772785
                [[1. 0. 0. 0.]    [[1. 0. 0. 2.]
                 [0. 1. 0. 0.]     [0. 1. 0. 1.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
        """
        o = self.autoUpdate
        try:
            self.autoUpdate = False
            yield self
        finally:
            self.updateGlobals()
            self.autoUpdate = o

    @contextlib.contextmanager
    def holdUpdateGraph(self):
        """
        Context manager to temporarily disable updates from changes in the
        graph topology, and calls
        :func:`~pynimation.common.localglobal.LocalGlobal.updateFromGraph`
        afterwards

        This is particularly useful if you need to make a non-atomic change to the
        graph

        See also
        --------
        :attr:`~pynimation.common.localglobal.LocalGlobal.autoUpdateGraph`

        Examples
        --------
        >>> from pynimation.common.localglobal import LocalGlobal
        >>> from pynimation.common.graph import Graph,Node
        >>>
        >>> g = Graph(Node(""))
        >>> g.root.addChild(Node(""))
        >>>
        >>> lg = LocalGlobal.identity((2,), graph=g)
        >>> lg
        [0] Node_8776428772794
            [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8776428772785
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]

        The array will not be updated from the graph updates while inside the
        with block

        >>> with lg.holdUpdateGraph() as _:
        ...     g.nodes[1].addChild(Node(""))
        ...     r = g.root
        ...     g.root = g.nodes[1]
        ...     g.root.addChild(r)
        ...     g
        ...     lg.shape
        ...
        [0]Node_8776428773043
            [1]Node_8776428773229
        <BLANKLINE>
            [2]Node_8776428772977
        <BLANKLINE>
        (2, 4, 4)
        >>>
        >>> lg # the array is updated when the end of the with block is reached
        [0] Node_8776428773001
            [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8776428773355
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
        <BLANKLINE>
            [2] Node_8776428773205
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
        """
        o = self.autoUpdateGraph
        try:
            self.autoUpdateGraph = False
            yield self
        finally:
            self.updateFromGraph()
            self.autoUpdateGraph = o

    @property
    def node(self) -> "NodeView":
        """
        Get a :class:`~pynimation.common.localglobal.LocalGlobal.NodeView` of
        this array

        With this representation, transforms of one or multiple joints can be
        accessed and modified

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.localglobal.LocalGlobal.n`


        Examples
        --------
        >>> from pynimation.common.localglobal import LocalGlobal
        >>> from pynimation.common.graph import Graph,Node
        >>> import numpy as np
        >>>
        >>> g = Graph(Node(""))
        >>> g.root.addChild(Node(""))
        >>>
        >>> lg = LocalGlobal.identity((2,), graph=g)
        >>> lg
        [0] Node_8776428773373
            [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8776428773334
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
        >>>
        >>> lg.node[0] = 1 # assign to single node with id
        >>> lg.node[g.nodes[0]] # access single node
        LocalGlobal([[1., 1., 1., 1.],
                     [1., 1., 1., 1.],
                     [1., 1., 1., 1.],
                     [1., 1., 1., 1.]])
        >>>
        >>> lg.node[g.nodes[0], g.nodes[1]] = np.identity(4) # assign multipe nodes
        >>> lg.node[0,1] # access multiple nodes with id
        [0] Node_8733103137642
            [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
             [0. 1. 0. 0.]     [0. 1. 0. 0.]
             [0. 0. 1. 0.]     [0. 0. 1. 0.]
             [0. 0. 0. 1.]]    [0. 0. 0. 1.]]
            [1] Node_8733102990156
                [[1. 0. 0. 0.]    [[1. 0. 0. 0.]
                 [0. 1. 0. 0.]     [0. 1. 0. 0.]
                 [0. 0. 1. 0.]     [0. 0. 1. 0.]
                 [0. 0. 0. 1.]]    [0. 0. 0. 1.]]

        """
        return self.NodeView(self)

    # shortcuts

    glob = globals
    g = globals
    n = node
