import functools
from typing import List, Optional, Type, Union, Callable

import numpy as np
from scipy import interpolate

from .array import Array


class _StaticAxes(type):
    def __getattr__(cls, name):
        if all([c in cls._elements for c in name]):
            idx = [cls._elements.index(c) for c in name]
            v = np.zeros(cls._dsize)
            v[idx] = 1
            return v.view(cls)
        else:
            raise AttributeError(name)


class Vec(Array, metaclass=_StaticAxes):
    """
    Base class for multi-dimensional array of vectors of 2, 3 and 4 elements,
    respectively :class:`~pynimation.common.vec.Vec2`,
    :class:`~pynimation.common.vec.Vec3` and :class:`~pynimation.common.vec.Vec4`

    All operations are performed on the last axis

    **Swizzling** allows access and assignment to arrays of elements
    (x,y,z,w) and combinations of elements. Similarly to GLSL vec* types
    (See Examples for details)

    >>> v = Vec4.zeros((2,2,4))
    >>> v.x
    >>> v.xyz = v.zyx

    Every concrete class also has **Swizzling** as static members to create
    instances of 1 dimension where every selected element is set to 1

    >>> Vec4.x
    Vec4([1., 0., 0., 0.])
    >>> Vec3.yz
    Vec3([0., 1., 1.])

    Examples
    --------
    Create array of (2,2) vectors of 4 elements
    by calling the class corresponding to the vector size:

    >>> import numpy as np
    >>> from pynimation.common.vec import Vec,Vec2,Vec3,Vec4
    >>>
    >>> v = Vec4.zeros((2,2,4))
    >>> v
    Vec4([[[0., 0., 0., 0.],
           [0., 0., 0., 0.]],
        <BLANKLINE>
          [[0., 0., 0., 0.],
           [0., 0., 0., 0.]]])

    Or by calling the :class:`~pynimation.common.vec.Vec` class
    directly that will infer the concrete class:

    >>> v = Vec(np.zeros((2,2,4)))
    >>> v
    Vec4([[[0., 0., 0., 0.],
           [0., 0., 0., 0.]],
        <BLANKLINE>
          [[0., 0., 0., 0.],
           [0., 0., 0., 0.]]])

    Any combinations of elements (x,y,z,w) of every vector of the
    array can be accessed and assigned directly with:

    >>> v.x = 1
    >>> v
    Vec4([[[1., 0., 0., 0.],
           [1., 0., 0., 0.]],
        <BLANKLINE>
          [[1., 0., 0., 0.],
           [1., 0., 0., 0.]]])
    >>> v.x
    array([[[1.],
            [1.]],
        <BLANKLINE>
           [[1.],
            [1.]]])
    >>> v.xy
    Vec2([[[1., 0.],
           [1., 0.]],
        <BLANKLINE>
          [[1., 0.],
           [1., 0.]]])
    >>> v.yz = [2,3]
    >>> v
    Vec4([[[1., 2., 3., 0.],
           [1., 2., 3., 0.]],
        <BLANKLINE>
          [[1., 2., 3., 0.],
           [1., 2., 3., 0.]]])

    It can also be used to reorder elements:

    >>> v.xyz = v.zyx
    >>> v
    Vec4([[[3., 2., 1., 0.],
           [3., 2., 1., 0.]],
        <BLANKLINE>
          [[3., 2., 1., 0.],
           [3., 2., 1., 0.]]])

    Or duplicate them

    >>> v.xxx
    Vec3([[[3., 3., 3.],
           [3., 3., 3.]],
        <BLANKLINE>
          [[3., 3., 3.],
           [3., 3., 3.]]])
    """

    _elements = ["x", "y", "z", "w"]
    _cls: List = []
    _nd = 1
    _dsize: int = 0

    def __new__(cls, array: np.ndarray, **kwargs):
        if cls == Vec:
            if not isinstance(array, np.ndarray):
                array = np.array(array)
            cls = Vec._cls[array.shape[-1]]
            if cls is np.ndarray:
                return array.view(np.ndarray)

        return Array.__new__(cls, array, **kwargs)

    def __getattr__(self, name):
        if all([c in self.__class__._elements for c in name]):
            idx = [self.__class__._elements.index(c) for c in name]
            cl = Vec._cls[len(idx)]
            return self[..., idx].view(cl)
        else:
            self._raiseAttrError(name)

    def __setattr__(self, name, value):
        if all([c in self.__class__._elements for c in name]):
            idx = [self.__class__._elements.index(c) for c in name]
            self[..., idx] = value
        else:
            super().__setattr__(name, value)

    def _raiseAttrError(self, name):
        raise AttributeError(
            "'%s' object has no attribute '%s'"
            % (self.__class__.__name__, name)
        )

    def normalized(self) -> "Vec":
        """
        Return this vector array where every vector has been normalized
        (divided by its norm)

        Returns
        -------
        Vec:
            This array of vectors normalized
        """
        norm = np.linalg.norm(self, axis=-1)[..., np.newaxis]
        norm = np.where(norm == 0, 1, norm)
        return self / norm

    def normalize(self) -> None:
        """
        Normalize this array of vectors in place
        """
        self[...] = self.normalized()

    def cross(self, other: np.ndarray) -> "Vec":
        """
        Cross product between every vectors of this array and :attr:`other`

        Parameters
        ----------
        other:
            Array of vectors that will be the second operand of the cross
            product

        Returns
        -------
        Vec:
            Cross product between this array and :attr:`other`
        """
        return Vec(np.cross(self, other))

    def dot(self, other: np.ndarray) -> np.ndarray:  # type: ignore
        """
        Dot product between every vectors of this array and :attr:`other`

        Parameters
        ----------
        other:
            Array of vectors that will be the second operand of the dot
            product

        Returns
        -------
        Vec:
            Dot product between this array and :attr:`other`
        """
        return np.einsum("...j,...j", self, other)

    def norm(self) -> np.ndarray:
        """
        Norm of every vector of this array

        Returns
        -------
        np.ndarray:
            Norm of every vector of this array, shape is :code:`self.shape[:-1]`
        """
        return np.linalg.norm(self, axis=-1)

    def angleWith(
        self,
        other: Union[np.ndarray, "Vec"],
        degrees=False,
        rotAxis: Optional[Union[np.ndarray, "Vec"]] = None,
    ) -> np.ndarray:
        """
        See :class:`~pynimation.common.rotation.Rotation.angleBetween` with
        this array and :attr:`other` as first arguments :code:`a` and :code:`b`
        """
        from .rotation import Rotation

        return Rotation.angleBetween(self, other, degrees, rotAxis)

    def clear(self) -> None:
        """
        Set all vectors of this array to 0
        """
        self[...] = 0

    def lerp(self, ktimes: np.ndarray) -> Callable[[np.ndarray], "Vec"]:
        """
        Linear interpolation of this array of vectors, with every vectors
        of the last axis corresponding to a key time of :attr:`ktimes`

        Returns a function that can be called with new times to interpolate
        vectors at, similar to scipy's :code:`interpolate.interp1d`

        Parameters
        ----------
        ktimes:
            Key times of the vectors

        Returns
        -------
        Callable[[np.ndarray], "Vec"]:
            Function that can be called with times to interpolated vectors at

        Examples
        --------
        >>> from pynimation.common.vec import Vec
        >>> import numpy as np
        >>>
        >>> v = Vec.zeros((4,3))
        >>> v
        Vec3([[0., 0., 0.],
              [0., 0., 0.],
              [0., 0., 0.],
              [0., 0., 0.]])
        >>>
        >>> v[...] = np.arange(4)[:,np.newaxis]
        >>> v
        Vec3([[0., 0., 0.],
              [1., 1., 1.],
              [2., 2., 2.],
              [3., 3., 3.]])
        >>>
        >>> ktimes = np.linspace(0,1,4) # key times
        >>> ktimes
        array([0.        , 0.33333333, 0.66666667, 1.        ])
        >>>
        >>> ntimes = np.linspace(0,1,5) # new times
        >>> ntimes
        array([0.  , 0.25, 0.5 , 0.75, 1.  ])
        >>>
        >>> v.lerp(ktimes)(ntimes)
        Vec3([[0.  , 0.  , 0.  ],
              [0.75, 0.75, 0.75],
              [1.5 , 1.5 , 1.5 ],
              [2.25, 2.25, 2.25],
              [3.  , 3.  , 3.  ]])
        """

        def vec(f) -> Callable[[np.ndarray], "Vec"]:
            @functools.wraps(f)
            def w(*args, **kwargs) -> "Vec":
                return Vec(f(*args, **kwargs))

            return w

        return vec(interpolate.interp1d(ktimes, self, axis=0))


class Vec4(Vec):
    """
    :class:`~pynimation.common.vec.Vec` implementation with 4 elements

    See :class:`~pynimation.common.vec.Vec` for details
    """

    _dsize = 4


class Vec3(Vec):
    """
    :class:`~pynimation.common.vec.Vec` implementation with 3 elements

    See :class:`~pynimation.common.vec.Vec` for details
    """

    _dsize = 3


class Vec2(Vec):
    """
    :class:`~pynimation.common.vec.Vec` implementation with 2 elements

    See :class:`~pynimation.common.vec.Vec` for details
    """

    _dsize = 2


_vecs: List[Type[Vec]] = [Vec2, Vec3, Vec4]
Vec._cls = [*[np.ndarray] * 2, *_vecs, *[np.ndarray] * 10]
for _c in _vecs:
    _c._elements = Vec._elements[: _c._dsize]
del _c
