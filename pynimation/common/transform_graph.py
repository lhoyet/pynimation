from typing import Optional, Tuple, TypeVar

import numpy as np

from pynimation.common.graph import Graph, Node
from pynimation.common.localglobal import LocalGlobal
from pynimation.common.transform import Transform

TransformNodeLike = TypeVar("TransformNodeLike", bound="TransformNode")


class TransformGraph(Graph[TransformNodeLike]):
    """
    A graph with local and global transforms attached to each nodes

    **This differs from :class:`~pynimation.common.localglobal.LocalGlobal`
    as being primarily a graph, and not a transform array**

    Another particularity is that each node only has a single transform,
    meaning that the shape of the transform array will always be
    :code:`(len(graph, 4, 4)`. Additionaly, the transform of a
    :class:`~pynimation.common.transform_graph.TransformNode` is also accessible
    through the node directly (:code:`transformGraph.nodes[2].transform`)
    """

    class _IndivLocalGlobal(LocalGlobal):
        def updateFromGraph(
            self, graph: Optional[Graph[TransformNodeLike]] = None
        ) -> None:
            graph_: Graph = graph or self._graph
            if graph is not None:
                self.graph = graph_
            nodes = graph_._nodes
            added = list(set(nodes) - set(self._oldnodes))
            removed = list(set(self._oldnodes) - set(nodes))
            for n in removed:
                if not hasattr(n, "_transform"):
                    n._transform = Transform()
                n._transform[...] = self[n.id].np.copy()
            super().updateFromGraph(graph_)
            for n in added:
                if hasattr(n, "_transform"):
                    self[n.id] = n._transform.np.copy()

    def copy(self):
        c = super().copy()
        c._transforms = self._IndivLocalGlobal(
            np.ndarray.copy(c._transforms), c
        )
        c._transforms.updateAllGlobals()
        return c

    @property
    def transforms(self) -> "LocalGlobal":
        """
        transforms of this transform graph, one for each node
        """
        return self._transforms

    @transforms.setter
    def transforms(self, ntrans: np.ndarray) -> None:
        self._transforms[...] = ntrans

    def _update(self) -> None:
        if not hasattr(self, "_transforms"):
            self._initTransforms()
            self._transforms._oldnodes = []
        super()._update()

    def _initTransforms(self) -> None:
        self._transforms: "TransformGraph._IndivLocalGlobal" = (
            self._IndivLocalGlobal(
                Transform.identity((len(self),)), graph=self
            )
        )

    # shortcuts

    tr = transforms


class TransformNode(Node[TransformNodeLike]):
    """
    A node of :class:`~pynimation.viewer.transform_graph.TransformGraph`.
    """

    def __init__(
        self,
        name: str,
        parent: Optional[TransformNodeLike] = None,
        children: Optional[Tuple[TransformNodeLike, ...]] = None,
        graph: Optional["TransformGraph"] = None,
    ) -> None:
        super().__init__(name, parent, children, graph)
        if self._graph is None:
            self._transform = Transform()
            self._globalTransform = self._transform

    @property
    def transform(self) -> "LocalGlobal":
        """
        Transform of this node. Points to
        :class:`~pynimation.common.transform_graph.TransformGraph.transforms`
        when this node is part of a graph

        Note
        ----
        **Shorthand:**
        :attr:`~pynimation.common.transform_graph.TransforNode.trans`,
        :attr:`~pynimation.common.transform_graph.TransforNode.t`
        """
        if self._graph is None:
            # raise ValueError("transform node is not bound to a graph")
            return self._transform
        else:
            return self._graph._transforms[self.id]

    @transform.setter
    def transform(self, ntrans) -> None:
        if self._graph is None:
            # raise ValueError("transform node is not bound to a graph")
            self._transform = ntrans
            self._globalTransform = self._transform
        else:
            self._graph._transforms[self.id] = ntrans

    @property
    def globalTransform(self) -> "LocalGlobal":
        """
        Global transform of this node. Points to
        :code:`transformGraph.transforms.globals[node.id]`
        when this node is part of a graph

        Note
        ----
        **Shorthand:**
        :attr:`~pynimation.common.transform_graph.TransforNode.gtrans`,
        :attr:`~pynimation.common.transform_graph.TransforNode.gt`
        """
        if self._graph is None:
            # raise ValueError("transform node is not bound to a graph")
            return self._globalTransform
        else:
            return self._graph._transforms.globals[self.id]

    @globalTransform.setter
    def globalTransform(self, ntrans) -> None:
        if self._graph is None:
            # raise ValueError("transform node is not bound to a graph")
            self._globalTransform = ntrans
            self._transform = self._globalTransform
        else:
            self._graph._transforms[self.id] = (
                self._graph._transforms[self.parent.id].inverse()
                if self.parent is not None
                else np.identity(4)
            ) @ ntrans

    # shortcuts

    trans = transform
    t = transform

    gtrans = globalTransform
    gt = globalTransform
