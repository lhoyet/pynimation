import copy
from typing import Any, Dict, Generic, List, Optional, Tuple, TypeVar

import numpy as np
from reactivex.subject import Subject

NodeLike = TypeVar("NodeLike", bound="Node")


class Graph(Generic[NodeLike]):
    """
    A generic tree class. Manages parent/children relationships and a list of
    nodes through node addition/deletion

    Warnings
    --------
    Despite being called Graph, this class is actually meant to represent trees

    Any cycle in the graph will not be handled correctly by some functions

    Examples
    --------
    Graphs must be created with a root node. Nodes are then assigned an id,
    that is their index in the :code:`nodes` list, that regroups all nodes of
    the graph:
    >>> from pynimation.common.graph import Graph, Node
    >>>
    >>> g = Graph(Node("rootNode"))
    >>> g.root.id
    0
    >>> g.nodes[0] is g.root
    True

    The graph can be modified through the
    :func:`~pynimation.common.graph.Node.addChild`,
    :func:`~pynimation.common.graph.Node.removeChild`
    :func:`~pynimation.common.graph.Graph.remove`
    :func:`~pynimation.common.graph.Graph.add` functions
    and the :attr:`~pynimation.common.graph.Node.children` and
    :attr:`~pynimation.common.graph.Node.parent` attributes:

    >>> g.root.addChild(Node("childNode"))
    >>> g # graphs are printed as an arborescence, with "[nodeid] nodename"
    [0]rootNode
        [1]childNode

    >>> g.root.children
    ([1]childNode
    ,)

    >>> g.nodes[1].parent
    [0]rootNode
        [1]childNode

    >>> g.nodes[1].children = (Node("grandChildNode"),) # modify children
    >>> g
    [0]rootNode
        [1]childNode
            [2]grandChildNode

    >>> g.nodes[2].parent = g.root # modify parent
    >>> g
    [0]rootNode
        [1]childNode
       <BLANKLINE>
        [2]grandChildNode

    Adding a node to the graph adds all of its descendants:

    >>> n = Node("0")
    >>> o = n
    >>> for i in range(1, 4):
    ...     n.addChild(Node(str(i)))
    ...     n = n.children[0]
    ...
    >>>
    >>> len(o.descendants(inclusive=True))
    4
    >>> g.nodes[2].addChild(o)
    >>> g
    [0]rootNode
        [1]childNode
       <BLANKLINE>
        [2]grandChildNode
            [3]0
                [4]1
                    [5]2
                        [6]3

    Removing a node from the graph removes all of its descendants:

    >>> g.remove(g.nodes[3])
    >>> g
    [0]rootNode
        [1]childNode
       <BLANKLINE>
        [2]grandChildNode

    """

    def __init__(self, root: NodeLike) -> None:
        """
        Parameters
        ----------
        root:
            root node of the graph
        """
        self._nodes: List[NodeLike] = []
        self._obs: Subject = Subject()
        self._root: NodeLike = root
        g = self._root._unlink()
        self._add(*self._root.descendants(True))  # type: ignore
        self._update()
        if g is not None and g is not self:
            g._update()

    def __str__(self) -> str:
        return str(self._root)

    def __repr__(self) -> str:
        return str(self._root)

    def __len__(self) -> int:
        return len(self._nodes)

    def _computeHash(self) -> int:
        self.root._updateHash()
        return self.root._hash

    def _update(self) -> None:
        self._updateHash()
        self._updateIds()
        self._updateAdjacency()
        self._updateParents()
        self._updateDescendants()
        self._obs.on_next(None)

    def _updateHash(self) -> None:
        self._hash = self._computeHash()

    def _updateIds(self) -> None:
        for i, n in enumerate(self._nodes):
            n._id = i

    def _updateAdjacency(self) -> None:
        self._adjacency: np.ndarray = np.zeros(
            (len(self._nodes),) * 2, dtype=bool
        )
        for i, n in enumerate(self._nodes):
            self._adjacency[n.id, [ch.id for ch in n.children]] = 1

    def _updateParents(self) -> None:
        self._parents: np.ndarray = np.array(
            [-1]
            + [n.parent.id for n in self.nodes[1:] if n.parent is not None],
            dtype="int32",
        )

    def _updateDescendants(self) -> None:
        self._descendants: np.ndarray = np.zeros(
            (len(self._nodes),) * 2,
            dtype=bool,
        )
        for i, n in enumerate(self._nodes):
            self._descendants[
                n.id, [d.id for d in n.descendants(inclusive=True)]
            ] = True

    def _add(self, *nodes: NodeLike) -> None:
        self._nodes.extend(nodes)
        for n in nodes:
            n._graph = self

    def _remove(self, *nodes: NodeLike) -> None:
        # self._nodes = list(set(self._nodes) - set(nodes))
        self._nodes = [n for n in self._nodes if n not in nodes]
        for n in nodes:
            n._graph = None

    def copy(self) -> "Graph":
        """
        Return a deep copy of this graph

        Warnings
        --------
        :code:`copy.deepcopy(graph)` will not work and this is why this method
        is necessary

        Returns
        -------
        Graph:
            copy of this graph
        """
        o = self._obs
        del self._obs
        c = copy.deepcopy(self)
        c._obs = Subject()
        self._obs = o
        return c

    def hasSameTopology(self, other: object) -> bool:
        """
        Determine if this graph and `other` have the same topology and
        nodes with the same names. This method is relatively inexpensive as it
        uses a hash that is recomputed every time the graph is modified

        Parameters
        ----------
        other:
            graph to compare topology with

        Raises
        ------
        NotImplementedError:
            If other is not a subclass of graph

        Returns
        -------
        boolean:
            does this graph and `other` have the same topology

        """
        # ensure we can access _hash
        if issubclass(other.__class__, Graph):
            return self._hash == other._hash  # type: ignore
        else:
            raise NotImplementedError(
                "cannot compare "
                + self.__class__.__name__
                + " to "
                + other.__class__.__name__
            )

    @property
    def parents(self) -> np.ndarray:
        """
        For each node of this graph, the id of its parent
        """
        return self._parents

    @property
    def adjacency(self) -> np.ndarray:
        """
        The adjacency matrix of the graph

        This is a boolean matrix of shape :code:`(len(graph), len(graph)`
        in which element :code:`[i, j]` is True if node id :code:`i`
        is a parent of node id :code:`j`
        """
        return self._adjacency

    @property
    def descendants(self) -> np.ndarray:
        """
        For each node, wether each other node is its descendant

        This is a boolean matrix of shape :code:`(len(graph), len(graph)`
        in which element :code:`[i, j]` is True if node id :code:`i`
        is an ancestor of node id :code:`j` (i.e. :code:`j` is a descendant
        of :code:`i`)
        """
        return self._descendants

    @property
    def root(self) -> NodeLike:
        """
        The root node of the graph, cannot be removed
        """
        return self._root

    @root.setter
    def root(self, newRoot: NodeLike) -> None:
        # FIXME: do better
        if newRoot is None:
            return
        # TODO: fix mypy
        self._remove(*self._root.descendants(True))  # type: ignore
        assert len(self._nodes) == 0  # sanity check
        g = newRoot._unlink()
        self._root = newRoot
        self._add(*self._root.descendants(True))  # type: ignore
        self._update()
        if g is not None and g is not self:
            g._update()

    @property
    def nodes(self) -> Tuple[NodeLike, ...]:
        """
        list of all nodes of the graph, is updated automatically on node
        addition/deletion
        """
        # return tuple to prevent user from touching the original
        return tuple(self._nodes)

    def add(self, node: NodeLike, parent: Optional[NodeLike] = None) -> None:
        """
        Add `node` to the graph, with `parent` as its parent. if
        `parent` is ``None``, `node` is added to the root

        Parameters
        ----------
        node:
            node to add

        parent:
            if not ``None``, will add `node` as its child, otherwise
            will add to root's children

        Raises
        ------
        ValueError:
            if `parent` is not in the graph
        """
        if node in self._nodes:
            return

        # add to parent's children
        if parent is None:
            parent = self.root
        else:
            if parent not in self._nodes:
                raise ValueError(
                    "parent node "
                    + parent.name
                    + " was not found in the graph, maybe add it first ?"
                )

        parent.addChild(node)

    def remove(self, node: NodeLike) -> None:
        """
        Remove `node` from graph. Will remove its children as well

        Raises
        ------
        ValueError:
            If `node` is not in the graph

        Parameters
        ----------
        node:
            node to remove
        """
        if node not in self._nodes:
            raise ValueError(
                "cannot remove node " + node.name + ", it is not in the graph"
            )
        if node is self._root:
            raise ValueError("cannot remove root")

        assert node.parent is not None

        node.parent.removeChild(node)

    def getNode(self, name: str) -> Optional[NodeLike]:
        """
        Search for node of `name`

        Parameters
        ----------
        name:
            name of the node to search

        Returns
        -------
        Optional[NodeLike]:

        """
        results = [a for a in self.nodes if a.name == name]
        if len(results) > 0:
            return results[0]
        else:
            return None

    def getChain(
        self, startNode: NodeLike, endNode: NodeLike
    ) -> List[NodeLike]:
        """
        Return node chain from `startNode` to `endNode`

        Warning
        -------
        | `endNode` must be a descendant of `startNode`
        | If you need to go up the graph, simply inverse `startNode`
        | and `endNode` in the call to ``getChain`` and
        | :code:`reverse()` the result


        Parameters
        ----------
        startNode:
            start of the chain
        endNode:
            end of the chain

        Returns
        -------
        List[NodeLike]:
            A list of nodes going from `startNode` to `endNode`
            where each node is the parent of the next

        """
        missing = [
            m.name for m in (set([startNode, endNode]) - set(self._nodes))
        ]
        missing.sort()
        missing.reverse()

        if len(missing) > 0:
            raise ValueError(
                "Could not get chain from %s to %s because following nodes are not part of the graph: "
                % (startNode.name, endNode.name)
                + ",".join(missing)
            )

        return self._getChainUnsafe(startNode, endNode)

    def _getChainUnsafe(
        self, startNode: NodeLike, endNode: NodeLike
    ) -> List[NodeLike]:
        if startNode == endNode:
            return [startNode]
        else:
            return [startNode] + next(
                (
                    chain
                    for child in startNode.children
                    for chain in [self._getChainUnsafe(child, endNode)]
                    if endNode in chain
                ),
                [],
            )

    def depth(self) -> int:
        """
        Depth of this graph, i.e. number of nodes in the longest branch
        """
        return Graph._depthRec(self.root) + 1

    @staticmethod
    def _depthRec(node: "Node") -> int:
        return max([Graph._depthRec(c) + 1 for c in node.children] + [0])

    @classmethod
    def fromDict(cls, dict_: Dict[str, Dict], classNode: Any) -> "Any":
        """
        Construct a graph from a nested dictionary with string keys. Mainly for
        testing purposes
        """
        if len(dict_) > 1:
            raise ValueError(
                "dict_ must have a single root (" + str(len(dict_)) + " found)"
            )
        root = classNode.fromDict(
            list(dict_.keys())[0], list(dict_.values())[0], None
        )
        graph = cls(root)
        return graph

    def toDict(self) -> Dict[str, Dict]:
        """
        Get a dictionary representation of the graph. Mainly for
        testing purposes

        Returns
        -------
        Dict[str, Dict]:
            dictionary representation of the graph
        """
        return self._root.toDict()

    def __iter__(self):
        return iter(self.nodes)

    def __getitem__(self, idx) -> "NodeLike":
        return self.nodes[idx]


class Node(Generic[NodeLike]):
    """
    A node of :class:`~pynimation.common.graph.Graph`
    """

    def __init__(
        self,
        name: str,
        parent: Optional[NodeLike] = None,
        children: Optional[Tuple[NodeLike, ...]] = None,
        graph: Optional[Graph] = None,
    ):
        """
        Parameters
        ----------
        name:
            name of the node
        parent:
            parent to attach the newly created node to
        children:
            children to add to the newly created node
        graph:
            graph the newly created node will be attached to
        """
        if len(name) == 0:
            name = self.__class__.__name__ + "_" + str(hash(self))
        #: name of the node
        self.name: str = name
        if children is None:
            children = ()
        # tuple gives immutability
        self._children: Tuple[NodeLike, ...] = children
        self._parent: Optional[NodeLike] = parent
        self._graph: "Optional[Graph]" = graph
        self._id: int = -1
        if self._parent is not None:
            self._graph = self._parent._graph
            self._parent.addChild(self)
        if self._graph is not None:
            self._graph.add(self, self._parent)
        self._updateHash()

    def _updateHash(self) -> None:
        self._hash = self._computeHash()

    # @property
    # def name(self) -> str:
    #     """
    #     name of the node
    #     """
    #     return self._name

    # @name.setter
    # def name(self, newName: str) -> None:
    #     self._name = newName

    def __del__(self):
        # FIXME: we remove root ?
        g = self._unlink()
        for child in self.children:
            child._parent = None
        if g is not None:
            try:
                g._update()
            except ImportError:
                pass

    @property
    def id(self) -> int:
        """
        id of the node in the graph

        Will be updated each time the graph changes

        Nodes not being part of a graph have an id of -1
        """
        return self._id

    @property
    def children(self) -> Tuple[NodeLike, ...]:
        """
        Children of the node

        Note
        ----
        A tuple is used to prevent direct modification of the set of children.

        A child can only be added with `addChild`. The set of children
        can however be replaced by a new tuple: ``self.children = (Node("a"),
        Node("b"))``
        """
        return self._children

    @children.setter
    def children(self, newChildren: Tuple[NodeLike, ...]) -> None:
        added = [n for n in newChildren if n not in self._children]
        removed = [n for n in self._children if n not in newChildren]

        g: List[Optional["Graph"]] = []

        for child in added + removed:
            g.append(child._unlink())

        for child in added:
            child._link(self)

        if self._graph is not None:
            self._graph._update()
        for gg in set(g) - set([self._graph]) - set([None]):
            assert gg is not None
            gg._update()

    @children.deleter
    def children(self):
        del self._children
        self._children = ()

    @property
    def parent(self) -> Optional[NodeLike]:
        """
        parent of the node, can be ``None``.
        Directly setting the parent will the graph accordingly

        Examples
        --------
        >>> # Set node's parent to another node will update the parent's
        >>> # children
        >>> node = Node("a")
        >>> node.parent = graph.root
        >>> node in graph.root.children
        True
        >>> node in graph.nodes
        True

        >>> # Set node's parent to a node that is not in the graph will remove
        >>> # it and its descendants from the graph
        >>> node.parent = Node("a")
        """
        return self._parent

    @parent.setter
    def parent(self, newParent: Optional["NodeLike"]):
        g = self._unlink()
        if newParent is not None:
            self._link(newParent)

        # add to graph
        if self._graph is not None:
            self._graph._update()
        if g is not None and g is not self._graph:
            g._update()

    @parent.deleter
    def parent(self):
        del self._parent
        self._parent = None

    def _addChild(self, child: NodeLike) -> None:
        self._children = (*self._children, child)

    def _removeChild(self, child: NodeLike) -> None:
        self._children = tuple([c for c in self._children if c is not child])

    def _unlink(self) -> Optional["Graph"]:
        if self._parent is not None:
            self._parent._removeChild(self)
            self._parent = None

        g = self._graph

        if self._graph is not None:
            self._graph._remove(*self.descendants(True))

        return g

    def _link(self, parent: NodeLike) -> None:
        parent._addChild(self)
        self._parent = parent

        # add to graph
        if parent._graph is not None:
            parent._graph._add(*self.descendants(True))

    def addChild(self, child: NodeLike) -> None:
        """
        Add `child` to the node's children

        Parameters
        ----------
        child:
            child to add to the node's children
        """
        g = child._unlink()
        child._link(self)

        if self._graph is not None:
            self._graph._update()
        if g is not None and g is not self._graph:
            g._update()

    def removeChild(self, child: NodeLike):
        """
        Remove `child` from children. Updates its parent and the
        graph

        Parameters
        ----------
        child:
            child to remove from node's children

        """
        # FIXME: check if child in self.children ?
        g = child._unlink()

        if self._graph is not None:
            self._graph._update()
        if g is not None and g is not self._graph:
            g._update()

    def descendants(self, inclusive=False) -> List["Node[NodeLike]"]:
        """
        Get the list of descendants, ``self`` is excluded by default

        Parameters
        ----------
        inclusive:
            wether to include ``self`` in the descendants

        Returns
        -------
        List["Node[NodeLike]"]:
            a list of this node's descendants

        """
        descendants: List["Node[NodeLike]"] = []
        for child in self.children:
            child._descendantsRecursive(descendants)
        if inclusive:
            descendants = [self, *descendants]
        return descendants

    def _descendantsRecursive(
        self, descendants: List["Node[NodeLike]"]
    ) -> None:
        descendants.append(self)
        for child in self.children:
            child._descendantsRecursive(descendants)

    def toDict(self) -> Dict[str, Dict]:
        """
        Get representation of this node and its descendants as a dictionary

        Mainly for testing purposes

        Returns
        -------
        Dict[str, Dict]:
            this node and its descendants as a dictionary
        """
        dict_: Dict[str, Dict] = {}
        for child in self.children:
            dict_[child.name] = child.toDict()
        return dict_

    @classmethod
    def fromDict(
        cls,
        name: str,
        dict_: Dict[str, Dict],
        parent: Optional[NodeLike] = None,
    ) -> NodeLike:
        """
        Create a node, with its descendants, from a dictionary

        Mainly for testing purposes

        Parameters
        ----------
        name:
            name of the node
        dict_:
            dictionary to create the node from
        parent:
            parent to attach the node to

        Returns
        -------
        NodeLike:
            a node created from :attr:`dict_`
        """
        node: Any = cls(name)
        if parent is not None:
            parent.addChild(node)
        children = []
        for key in dict_.keys():
            if isinstance(dict_[key], dict) and isinstance(key, str):
                children.append(cls.fromDict(key, dict_[key], node))
        node.children = tuple(children)
        return node

    def __str__(self) -> str:
        return self._tostring("", [])

    def __repr__(self) -> str:
        return str(self)

    def _computeHash(self) -> int:
        if len(self.children) == 0:
            return 1
        else:
            for a in self.children:
                a._updateHash()
            chHash = [str(a._hash) for a in self.children]
            chHash.sort()
            return hash("_".join(chHash))

    def _tostring(self, tab="", visited: List["Node[NodeLike]"] = []) -> str:
        if self in visited:
            return "__CYCLE__"
        id = ""
        if self._graph is not None:
            id = "[" + str(self._graph.nodes.index(self)) + "]"
        return (
            tab
            + id
            + self.name
            + "\n"
            + "\n".join(
                [
                    child._tostring(tab + "    ", visited + [self])
                    for child in self.children
                ]
            )
        )
