import copy
from functools import partial
from typing import TYPE_CHECKING, Any, Callable, Dict, Tuple, List, Type

import numpy as np_

if TYPE_CHECKING:
    from graph import NodeLike


class ArrayBehaviour:
    """
    Abstract class for Behaviours that can be associated
    with an :class:`~pynimation.common.array.Array`

    Classes inheriting from this class can override methods like
    :attr:`_array_finalize`, :attr:`_getitem` etc. Those methods
    will be called with when the corresponding methods of the array
    this behaviour is given to
    (:func:`~pynimation.common.array.Array.__getitem__`,
    :func:`~pynimation.common.array.Array.__array_finalize__`)
    are called. The array will be given as a first argument

    Behaviours are passed to arrays created from the array they are
    initially given to

    Examples
    --------
    >>> from pynimation.common.array import Array
    >>> from pynimation.common.array import GraphViewBehaviour
    >>>
    >>> arr = Array.zeros((10, 10))
    >>> arr._addBehaviour(GraphViewBehaviour)
    >>> arr[0]._hasBehaviour(GraphViewBehaviour)
    True
    """

    _getters: Dict[str, Callable] = {}
    _setters: Dict[str, Callable] = {}
    _methods: Dict[str, Callable] = {}

    @classmethod
    def _init(cls, obj, kwargs=None):
        """
        Called when obj.__init__ is called. Initializes the Behaviour
        """
        pass

    @classmethod
    def _del(cls, obj):
        """
        Called when obj.__del__ is called
        """
        pass

    @classmethod
    def _array_finalize(cls, obj, source):
        """
        Called when obj.__array_finalize__ is called
        """
        pass

    @classmethod
    def _getitem(cls, obj, idx):
        """
        Called when obj.__getitem__ is called
        """
        pass

    @classmethod
    def _setitem(cls, obj, idx, val) -> None:
        """
        Called when obj.__setitem__ is called
        """
        pass

    @classmethod
    def _repr(cls, obj):
        """
        Called when obj.__repr__ is called
        """
        pass

    @classmethod
    def _resize(cls, obj):
        """
        Called when obj.resize is called
        """
        pass


class GraphViewBehaviour(ArrayBehaviour):
    """
    Behaviour given to an array that will display it and arrays created from
    it along with a hierarchical representation of the graph associated

    Examples
    --------
    >>> from pynimation.common.graph import Graph, Node
    >>> from pynimation.common.vec import Vec4
    >>> from pynimation.common.array import GraphViewBehaviour
    >>>
    >>> g = Graph(Node("node1"))
    >>> g.root.addChild(Node("node2"))
    >>> arr = Vec4.zeros((2, 4))
    >>> arr
    Vec4([[0., 0., 0., 0.],
          [0., 0., 0., 0.]])
    >>>
    >>> arr._addBehaviour(GraphViewBehaviour, graph=g)
    >>> arr
    [0] node1
        [0. 0. 0. 0.]
        [1] node2
            [0. 0. 0. 0.]
    """

    @classmethod
    def _init(cls, obj, kwargs=None):
        if kwargs is None or "graph" not in kwargs:
            # raise ValueError("Missing argument graph")
            pass
        else:
            obj._graph = kwargs["graph"]

    @classmethod
    def _del(cls, obj):
        del obj._graph

    @classmethod
    def _array_finalize(cls, obj, source):
        try:
            obj._graph = source._graph
        except AttributeError:
            cls._init(obj)

    @classmethod
    def _repr(cls, obj):
        nd = obj.__class__._nd
        if len(obj.shape) > nd and obj.shape[-nd - 1] == len(obj._graph):
            return GraphViewBehaviour._tostring(obj, obj._graph.root, "")
        else:
            return np_.ndarray.__repr__(obj)

    @classmethod
    def _pad(cls, lines: str):
        ll = lines.split("\n")
        m = max([len(l_) for l_ in ll])
        return "\n".join([l_ + " " * (m - len(l_)) for l_ in ll])

    @classmethod
    def _tab(cls, tab: str, *liness: str, delim: str = "   ") -> str:
        return "\n".join(
            [
                tab + delim.join(lines)
                for lines in zip(*[lines.split("\n") for lines in liness])
            ]
        )

    @classmethod
    def _tostring(
        cls,
        obj,
        n: "NodeLike",
        tab: str = "",
        visited=[],
    ) -> str:
        if n in visited:
            return "__CYCLE__"
        nstr = "%s[%d] " % (tab, n.id)
        rest = (slice(None),) * obj.__class__._nd
        lines = [cls._pad(str(obj.np[(Ellipsis, n.id, *rest)]))]
        if hasattr(obj, "globals"):
            lines.append(
                GraphViewBehaviour._pad(
                    str(obj._globals.np[(Ellipsis, n.id, *rest)])
                )
            )
        return "\n".join(
            [
                nstr + n.name,
                cls._tab(" " * len(nstr), *lines),
                "\n".join(
                    [
                        cls._tostring(obj, child, tab + "    ", visited + [n])
                        for child in n.children
                    ]
                ),
            ]
        )


class ConvertedBehaviour(ArrayBehaviour):
    """
    Behaviour given to an array that is used to set its values
    from another array that is not a view of the first one

    For example when setting the values of converted rotations back
    to the original array

    Examples
    --------

    using advanced indexing does not return a view:

    >>> from functools import partial
    >>> from pynimation.common.vec import Vec4
    >>> from pynimation.common.array import ConvertedBehaviour
    >>>
    >>> arr = Vec4(np.zeros((2, 4)))
    >>> arr
    Vec4([[0., 0., 0., 0.],
          [0., 0., 0., 0.]])
    >>> arr1 = arr[[0]]
    >>> arr1.base is not arr and arr1.base.base is not arr
    True

    meaning that if we modify it, the original will not be modified:

    >>> arr1[0,0] = 1
    >>> arr1
    Vec4([[1., 0., 0., 0.]])
    >>> arr
    Vec4([[0., 0., 0., 0.],
          [0., 0., 0., 0.]])

    ConvertedBehaviour allows to circumvent this behaviour:

    >>> arr1._b = [*arr1._b] # this is a bug
    >>> arr1._addBehaviour(ConvertedBehaviour, _convert=partial(arr.__setitem__, tuple([0])))
    >>> arr1[0,0] = 2
    >>> arr1
    Vec4([[2., 0., 0., 0.]])
    >>> arr
    Vec4([[2., 0., 0., 0.],
          [0., 0., 0., 0.]])
    """

    @classmethod
    def _init(cls, obj, kwargs=None):
        if "_convert" not in kwargs:
            raise ValueError("missing kwargs '_convert'")
        obj._convert = partial(kwargs["_convert"], obj)

    @classmethod
    def _del(cls, obj):
        del obj._convert

    @classmethod
    def _array_finalize(cls, obj, source):
        try:
            obj._convert = source._convert
        except AttributeError:
            cls._init(obj)

    @classmethod
    def _setitem(cls, obj, idx, val) -> None:
        obj._convert()


class ObservableBehaviour(ArrayBehaviour):
    """
    Behaviour given to an array that is used track its changes

    Everytime an element of the array changes, the corresponding element
    of :code:`arr._dirty` will be set to :code`True`

    Because arrays created from this array inherit this behaviour, and a view
    of :code:`arr._dirty` equivalent to their view of :code:`arr`, changes to
    those array are also tracked

    Examples
    --------

    _dirty is initialized to True:

    >>> from pynimation.common.vec import Vec4
    >>> from pynimation.common.array import ObservableBehaviour
    >>>
    >>> arr = Vec4.zeros((2, 4))
    >>> arr._addBehaviour(ObservableBehaviour)
    >>> arr._dirty[...] = False
    >>> arr._dirty
    array([[False, False, False, False],
           [False, False, False, False]])

    setting an element directly causes the corresponding _dirty
    element to be set:

    >>> arr[0,0] = 1
    >>> arr._dirty
    array([[ True, False, False, False],
           [False, False, False, False]])

    arr[0] gets a corresponding view of arr._dirty:

    >>> a = arr[0]
    >>> a._dirty
    array([ True, False, False, False])

    setting one of its element causes the corresponding _dirty
    element to be set:

    >>> a.y = 2
    >>> a._dirty
    array([ True,  True, False, False])

    as well as in the original array:

    >>> arr
    Vec4([[1., 2., 0., 0.],
          [0., 0., 0., 0.]])
    >>> arr._dirty
    array([[ True,  True, False, False],
           [False, False, False, False]])
    """

    @classmethod
    def _init(cls, obj, kwargs=None):
        obj._dirty = np_.ones(obj.shape, dtype="bool")

    @classmethod
    def _del(cls, obj):
        del obj._dirty

    @classmethod
    def _array_finalize(cls, obj, source):
        try:
            obj._dirty = source._dirty
        except AttributeError:
            cls._init(obj)

    @classmethod
    def _getitem(cls, obj, idx):
        obj._dirty = obj._dirty[idx]

    @classmethod
    def _setitem(cls, obj, idx, val) -> None:
        obj._dirty[idx] = True

    @classmethod
    def _resize(cls, obj):
        cls._init(obj)


class ShapeCheckBehaviour(ArrayBehaviour):
    """
    Behaviour given to an array that will check at creation if its size is
    coherent with its class :code:`nd` (number of fixed dimensions) and
    :code:`size` (size of the fixed dimensions)
    """

    @classmethod
    def _init(cls, obj, kwargs=None):
        cls._validateShape(obj)

    @classmethod
    def _validateShape(cls, obj):
        cls = obj.__class__
        shape = obj.shape
        if len(shape) < cls._nd:
            raise ValueError(
                "%s array should have at least %d dimensions (%d found))"
                % (cls.__name__, cls._nd, len(shape))
            )

        if cls._dsize == 0:
            if cls._nd > 1 and shape[-1] != shape[-2]:
                raise ValueError(
                    "%s array should be of shape (...,n,n) and not %s"
                    % (cls.__name__, shape)
                )
        else:
            if not shape[-cls._nd :] == (cls._dsize,) * cls._nd:
                raise ValueError(
                    "%s array should be of shape (...,%s) and not %s"
                    % (
                        cls.__name__,
                        ",".join([str(cls._dsize)] * cls._nd),
                        str(shape),
                    )
                )


class Array(np_.ndarray):
    """
    Base class for multi-dimensional arrays of objects (vectors, transforms,
    etc.)

    Inherits from :code:`numpy.ndarray`

    Can have multiple behaviours (see :class:`~pynimation.common.array.ArrayBehaviour`)
    """

    _nd = 1

    def __new__(cls, array: np_.ndarray, dtype=None, **kwargs):
        # FIXME: should we copy ?
        o = np_.ndarray.copy(np_.asarray(array, dtype=dtype).view(cls))
        return o

    def __init__(
        self,
        array: np_.ndarray,
        behaviours: List[Type["ArrayBehaviour"]] = [ShapeCheckBehaviour],
        **kwargs
    ):
        """
        Parameters
        ----------

        array:
            numpy array to create the Array instance from
        behaviours:
            behaviours the array should have
        kwargs:
            arguments to pass to the behaviours
        """
        super().__init__()
        self._b.extend(behaviours)
        for cls in self._b:
            cls._init(self, kwargs)

    def __array_finalize__(self, obj):
        if obj is None:
            return
        try:
            self._b = obj._b
            for cls in self._b:
                cls._array_finalize(self, obj)
        except AttributeError:
            self._b = []

    def __getitem__(self, idx, *args, **kwargs):
        arr = super().__getitem__(idx, *args, **kwargs)
        if isinstance(arr, self.__class__):
            for cls in arr._b:
                cls._getitem(arr, idx)
        return arr

    def __setitem__(self, idx, val):
        super().__setitem__(idx, val)
        for cls in self._b:
            cls._setitem(self, idx, val)

    def __repr__(self):
        for cls in self._b:
            c = cls._repr(self)
            if c is not None:
                return c
        return super().__repr__()

    def _addBehaviour(self, b, **kwargs):
        if b not in self._b:
            self._b.append(b)
            b._init(self, kwargs)

    def _rmBehaviour(self, b):
        self._b.remove(b)
        b._del(self)

    def _hasBehaviour(self, b):
        return any([issubclass(cls, b) for cls in self._b])

    def resize(self, shape: Tuple[int, ...], refcheck=False) -> None:  # type: ignore
        """
        Resize this array to shape :attr:`shape`

        Parameters
        ----------
        shape:
            new shape

        Warning
        -------
        Attempting to resize an array that does not own its data (if it is a
        view, or a part of another array) will fail. If this is intentional, try
        resizing a copy of the array

        """
        super().resize(shape, refcheck=False)
        for cls in self._b:
            cls._resize(self)

    def set(self, narray: np_.ndarray) -> None:
        """
        Set the content of this array to narray, resizing if needed

        Equivalent of

        >>> self.resize(narray.shape)
        >>> self[...] = narray.copy()

        Parameters
        ----------
        narray:
            new array
        """
        if narray.shape != self.shape:
            self.resize(narray.shape)
        self[...] = narray.copy()

    def copy(self):
        """
        Return a copy of this array

        Uses :code:`copy.deepcopy`

        Returns
        -------
        np.ndarray:
            a copy of this array
        """
        return copy.deepcopy(self)

    @property
    def np(self) -> np_.ndarray:
        """
        This array as a :code:`numpy.ndarray`

        Equivalent of

        >>> self.view(np.ndarray)

        Setting this attribute will set the array, like

        >>> self.set(new_array)

        Returns
        -------
        np.ndarray:
            A view of this array as a :code:`numpy.ndarray`
        """
        return self.view(np_.ndarray)

    @np.setter
    def np(self, newnp: np_.ndarray) -> None:
        self.set(newnp)

    @classmethod
    def zeros(cls, shape: Tuple[int, ...], **kwargs) -> Any:
        """
        Create an array of class :attr:`cls` full of zeros of :attr:`shape`

        Parameters
        ----------
        shape:
            shape of the array to create

        Returns
        -------
        Array:
            An array of class :attr:`cls` and :attr:`shape` full of zeros

        Note
        ----
        Additional arguments of the constructor of :attr:`cls` can be passed
        """
        return cls(np_.zeros(shape), **kwargs)

    @classmethod
    def ones(cls, shape: Tuple[int, ...], **kwargs) -> Any:
        """
        Create an array of class :attr:`cls` full of ones of :attr:`shape`

        Parameters
        ----------
        shape:
            shape of the array to create

        Returns
        -------
        Array:
            An array of class :attr:`cls` and :attr:`shape` full of ones

        Note
        ----
        Additional arguments of the constructor of :attr:`cls` can be passed
        """
        return cls(np_.ones(shape), **kwargs)

    @classmethod
    def empty(cls, shape: Tuple[int, ...], **kwargs) -> Any:
        """
        Create an array of class :attr:`cls` and shape :attr:`shape`,
        without initializing the data

        Values of the array will depend on the data previously at this memory
        location

        Can be slightly faster than creating an array with data initialization

        Parameters
        ----------
        shape:
            shape of the array to create

        Returns
        -------
        Array:
            An array of class :attr:`cls` and :attr:`shape` without data initialization

        Note
        ----
        Additional arguments of the constructor of :attr:`cls` can be passed

        """
        return cls(np_.empty(shape), **kwargs)

    @classmethod
    def random(cls, shape: Tuple[int, ...], **kwargs) -> Any:
        """
        Create an array of class :attr:`cls` and shape :attr:`shape`, full of
        random numbers

        Parameters
        ----------
        shape:
            shape of the array to create

        Returns
        -------
        An array of :attr:`cls` and :attr:`shape` full of random numbers

        Note
        ----
        Additional arguments of the constructor of :attr:`cls` can be passed
        """
        return cls(np_.random.random_sample(shape), **kwargs)

    def shallowCopy(self) -> "Array":
        """
        Return a copy of this array object where only the numpy array data has
        been copied, all other attribute references stay the same

        Returns
        -------
        Array:
            A copy of this array with only numpy array data copied
        """
        return self.sameWith(self.np.copy())

    def sameWith(self, narray: np_.ndarray) -> Any:
        """
        Return a new :class:`~pynimation.common.array.Array` object having with
        the same attributes but with :attr:`narray` as the underlying numpy array data

        Parameters
        ----------
        narray:
            numpy array to create the new Array object from

        Returns
        -------
        Array:
            The newly created :class:`~pynimation.common.array.Array` object
        """
        return self.__class__(narray)
