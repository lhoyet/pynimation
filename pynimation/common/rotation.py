import math
from functools import partial
from typing import TYPE_CHECKING, List, Optional, Union, cast, Callable

import numpy as np
from scipy.spatial.transform import Rotation as scipyRot
from scipy.spatial.transform import Slerp as scipySlerp

from .array import ConvertedBehaviour, ObservableBehaviour
from .matrix import Matrix
from .quaternion import Quaternion
from .vec import Vec, Vec3

if TYPE_CHECKING:
    from .array import Array


class _MetaRotation(type):
    _elements = Vec._elements[:3]

    @staticmethod
    def __fromEulerEl(cls, el, degrees, value):
        angles = np.zeros(3)
        angles[el] = value
        return cls.fromEuler(angles, degrees=degrees)

    def __getattr__(cls, name: str):
        if name[0] in _MetaRotation._elements:
            degrees = name[1:] == "Deg"
            el = _MetaRotation._elements.index(name[0])
            return partial(_MetaRotation.__fromEulerEl, cls, el, degrees)
        else:
            raise AttributeError(name)


class Rotation(Matrix, metaclass=_MetaRotation):
    """
    Multi-dimensional array of rotation matrices (base shape (3,3))

    All operations are performed on the last axis

    Can be converted to and from various representations:
    :attr:`~pynimation.common.rotation.Rotation.euler`,
    :attr:`~pynimation.common.rotation.Rotation.eulerDeg`,
    :attr:`~pynimation.common.rotation.Rotation.rotvec`,
    :attr:`~pynimation.common.rotation.Rotation.quat`

    For each attributes, assignment will convert the values back to the
    original array, including assignment of part of the converted array (See
    Examples)

    Examples
    --------

    >>> from pynimation.common.rotation import Rotation
    >>> import numpy as np
    >>>
    >>> # no scientific notation
    >>> np.set_printoptions(suppress=True, precision=6)
    >>>
    >>> r=Rotation.identity((2,2))
    >>> r
    Rotation([[[[1., 0., 0.],
                [0., 1., 0.],
                [0., 0., 1.]],
        <BLANKLINE>
               [[1., 0., 0.],
                [0., 1., 0.],
                [0., 0., 1.]]],
        <BLANKLINE>
        <BLANKLINE>
              [[[1., 0., 0.],
                [0., 1., 0.],
                [0., 0., 1.]],
        <BLANKLINE>
               [[1., 0., 0.],
                [0., 1., 0.],
                [0., 0., 1.]]]])

    Access euler angles in degrees of this array of rotation matrices:

    >>> r.eulerDeg
    Vec3([[[0., 0., 0.],
           [0., 0., 0.]],
        <BLANKLINE>
          [[0., 0., 0.],
           [0., 0., 0.]]])

    Assigning to the array of euler angles reflects on the rotation
    matrices

    >>> r.eulerDeg.x = 90
    >>> r.eulerDeg
    Vec3([[[90.,  0.,  0.],
           [90.,  0.,  0.]],
        <BLANKLINE>
          [[90.,  0.,  0.],
           [90.,  0.,  0.]]])
    >>> r
    Rotation([[[[ 1.,  0.,  0.],
                [ 0.,  0., -1.],
                [ 0.,  1.,  0.]],
        <BLANKLINE>
               [[ 1.,  0.,  0.],
                [ 0.,  0., -1.],
                [ 0.,  1.,  0.]]],
        <BLANKLINE>
        <BLANKLINE>
              [[[ 1.,  0.,  0.],
                [ 0.,  0., -1.],
                [ 0.,  1.,  0.]],
        <BLANKLINE>
               [[ 1.,  0.,  0.],
                [ 0.,  0., -1.],
                [ 0.,  1.,  0.]]]])

    See :class:`~pynimation.common.matrix.Matrix` for examples of matrix by
    matrix and matrix by vector multiplication
    """

    _dsize = 3

    @property
    def euler(self) -> Vec3:
        """
        Euler angles of each rotation matrix of this array, in radians, as
        order :code:`"xyz"` (See
        :attr:`~pynimation.common.rotation.Rotation.asEulerOrder` for more on
        euler angles order)

        Warnings
        --------
        Modifications of this array will be reflected on the rotation matrices

        Returns
        -------
        Vec3:
            An array of euler angles for each rotation matrix that can be
            edited to modify the rotation matrices

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.rotation.Rotation.eul`,
        :attr:`~pynimation.common.rotation.Rotation.e`
        """
        return self.asEulerOrder()

    @euler.setter
    def euler(self, new: np.ndarray) -> None:
        new = np.array(new)
        self[...] = Rotation.fromEuler(new)

    @property
    def eulerDeg(self) -> Vec3:
        """
        Same as :attr:`~pynimation.common.rotation.Rotation.euler` but in
        degrees

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.rotation.Rotation.eulDeg`,
        :attr:`~pynimation.common.rotation.Rotation.ed`
        """
        return self.asEulerOrder(degrees=True)

    @eulerDeg.setter
    def eulerDeg(self, new: np.ndarray) -> None:
        new = np.array(new)
        self[...] = Rotation.fromEuler(new, degrees=True)

    def asEulerOrder(self, order: str = "xyz", degrees=False) -> "Vec3":
        """
        Euler angles in :attr:`order` and in :attr:`degrees` or not, for each rotation
        matrix of this array

        Parameters
        ----------
        order:
            order of the axes by which the rotations are performed, uppercase
            for extrinsic and lowercase for intrisic

            for more on euler angle order see :code:`seq` argument of `scipy's Rotation.as_euler <https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.transform.Rotation.as_euler.html>`_

        degrees:
            Express the angles in degrees or radians


        Warnings
        --------
        Modifications of this array will be reflected on the rotation matrices

        Returns
        -------
        Vec3:
            An array of euler angles for each rotation matrix that can be
            edited to modify the rotation matrices
        """

        def seteul(a):
            self[...] = Rotation.fromEuler(a, order, degrees)

        return cast(
            Vec3,
            self._converted(
                Vec3,
                self.scipyRot.as_euler(order, degrees).reshape(
                    *self.shape[:-2], 3
                ),
                seteul,
            ),
        )

    def _converted(self, cls, array, func) -> "Array":
        a = cls(array)
        a.__array_finalize__(self)
        a._b = [
            b
            for b in a._b
            if b not in [ConvertedBehaviour, ObservableBehaviour]
        ] + [ConvertedBehaviour]
        ConvertedBehaviour._init(a, {"_convert": func})
        return a

    @staticmethod
    def fromEuler(
        angles: np.ndarray, order: str = "xyz", degrees=False
    ) -> "Rotation":
        """
        Create an array of rotation matrices from euler angles :attr:`angles`
        interpreted in  :attr:`order` and in :attr:`degrees` or not

        Parameters
        ----------
        angles:
            euler angles to create the rotation matrices from

        order:
            order of the axes by which the rotations are performed, uppercase
            for extrinsic and lowercase for intrisic

            for more on euler angle order see :code:`seq` argument of `scipy's Rotation.as_euler <https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.transform.Rotation.as_euler.html>`_

        degrees:
            Interpret the angles in degrees or radians

        Returns
        -------
        Vec3:
            An array of rotation matrices for each euler angles of
            :attr:`angles`
        """
        r = Rotation.empty((*angles.shape[:-1], 3, 3))
        r.scipyRot = scipyRot.from_euler(order, angles.reshape(-1, 3), degrees)
        return r

    @property
    def rotvec(self) -> Vec3:
        """
        Rotation vector of each rotation matrix of this array

        A rotation vector represents a rotation as a 3-dimensional rotation
        axis whose norm is the angle of rotation in radian around it

        Warnings
        --------
        Modifications of this array will be reflected on the rotation matrices

        Returns
        -------
        Vec3:
            An array of rotation vectors for each rotation matrix that can be
            edited to modify the rotation matrices

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.rotation.Rotation.rotv`,
        :attr:`~pynimation.common.rotation.Rotation.rv`
        """
        return self._converted(
            Vec3,
            self.scipyRot.as_rotvec().reshape(*self.shape[:-2], 3),
            partial(Rotation.rotvec.__set__, self),  # type: ignore
        )

    @rotvec.setter
    def rotvec(self, new: np.ndarray) -> None:
        new = np.array(new)
        self[...] = Rotation.fromRotVec(new)

    @staticmethod
    def fromRotVec(rotvec: np.ndarray) -> "Rotation":
        """
        Create an array of rotation matrices from rotation vectors
        :attr:`rotvec`

        Parameters
        ----------
        rotvec:
            rotation vectors to create the rotation matrices from

        Returns
        -------
        Vec3:
            An array of rotation matrices for each rotation vector of
            :attr:`rotvec`
        """
        r = Rotation.empty((*rotvec.shape[:-1], 3, 3))
        r.scipyRot = scipyRot.from_rotvec(rotvec.reshape(-1, 3))
        return r

    @property
    def quat(self) -> Quaternion:
        """
        Quaternion of each rotation matrix of this array

        Warnings
        --------
        Modifications of this array will be reflected on the rotation matrices

        Returns
        -------
        Quaternion:
            An array of quaternions for each rotation matrix that can be
            edited to modify the rotation matrices

        Note
        ----
        **Shorthand:** :attr:`~pynimation.common.rotation.Rotation.q`
        """
        return self._converted(
            Quaternion,
            self.scipyRot.as_quat().reshape(*self.shape[:-2], 4),
            partial(Rotation.quat.__set__, self),  # type: ignore
        )

    @quat.setter
    def quat(self, new: np.ndarray) -> None:
        self[...] = Rotation.fromQuat(new)

    @staticmethod
    def fromQuat(quat: np.ndarray) -> "Rotation":
        """
        Create an array of rotation matrices from quaternions
        :attr:`quat`

        Parameters
        ----------
        quat:
            quaternions to create the rotation matrices from

        Returns
        -------
        Vec3:
            An array of rotation matrices for each quaternions of
            :attr:`rotvec`
        """
        r = Rotation.empty((*quat.shape[:-1], 3, 3))
        r.scipyRot = scipyRot.from_quat(quat.reshape(-1, 4))
        return r

    @staticmethod
    def degrees(radians: Union[np.ndarray, float]) -> Union[np.ndarray, float]:
        """
        Convert :attr:`radians` in degrees

        Parameters
        ----------
        radians:
            Scalar of array of angles to convert in degrees

        Returns
        -------
        Vec3:
            :attr:`radians` converted in degrees
        """
        return np.degrees(radians)

    @staticmethod
    def radians(degrees: Union[np.ndarray, float]) -> Union[np.ndarray, float]:
        """
        Convert :attr:`degrees` in radians

        Parameters
        ----------
        degrees:
            Scalar of array of angles to convert in radians

        Returns
        -------
        Vec3:
            :attr:`degrees` converted in radians
        """
        return np.radians(degrees)

    @staticmethod
    def angleBetween(
        a: Union[np.ndarray, "Vec"],
        b: Union[np.ndarray, "Vec"],
        degrees=False,
        rotAxis: Optional[Union[np.ndarray, "Vec"]] = None,
    ) -> np.ndarray:
        """
        Compute angles between corresponding vector of :attr:`a` and
        :attr:`b`

        Rotate around corresponding axis of :attr:`rotAxis` if provided

        Parameters
        ----------
        a:
            array of vectors to compute angles from
        b:
            array of vectors to compute angles to
        degrees:
            whether the returned angle should be in radian or degrees
        rotAxis:
            rotation axis for every couple of vectors of this array and
            :attr:`other`

            if :code:`None`, the rotation axis will be the cross product of the
            two vectors

        Returns
        -------
        np.ndarray:
            angles between vectors of :attr:`a` and :attr:`b`, shape
            is :code:`a.shape[:-1]`
        """
        c: List["Vec"] = [
            (d if isinstance(d, Vec) else Vec(d)).normalized() for d in [a, b]
        ]
        angle = np.arccos(np.clip(np.sum(c[0] * c[1], axis=-1), -1, 1)).view(
            np.ndarray
        )
        if rotAxis is not None:
            rotAxis = (
                rotAxis if isinstance(rotAxis, Vec) else Vec(rotAxis)
            ).normalized()
            axis = c[0].cross(c[1]).normalized()
            dAxis = np.sum(axis * rotAxis, axis=-1)
            dAxis = np.sign(np.where(dAxis == 0, 1, dAxis))
            angle *= dAxis
        return np.degrees(angle) if degrees else angle

    @staticmethod
    def rotationBetween(
        vFrom: Union[np.ndarray, "Vec"],
        vTo: Union[np.ndarray, "Vec"],
    ) -> "Rotation":
        """
        Compute rotation between vectors of :attr:`vFrom` and :attr:`vTo`

        Parameters
        ----------
        vFrom:
            array of vectors to compute rotations from
        vTo:
            array of vectors to compute rotations to

        Returns
        -------
        Rotation:
            Array of rotation matrices between each vectors of :attr:`a` and
            :attr:`b`, shape is :code:`(*a.shape[:-1], 3, 3)`
        """
        c: List["Vec"] = [
            (d if isinstance(d, Vec) else Vec(d)).normalized()
            for d in [vFrom, vTo]
        ]

        # ensure at least 2d
        is1d = len(c[0].shape) == 1
        if is1d:
            c = [d[np.newaxis, :] for d in c]

        vcls = c[0].__class__
        angle = Rotation.angleBetween(*c)
        rot = Rotation.identity((*c[0].shape[:-1],))
        n = c[0].cross(c[1]).normalized() * angle[..., np.newaxis]

        # check if any are opposite
        zeros = np.logical_and(
            (n == (0, 0, 0)).all(axis=-1), (angle != 0)
        ).nonzero()
        if np.array(zeros).size > 0:
            # try cross product with X
            n[zeros] = c[0][zeros].cross(vcls.x) * np.pi

            zeros = (n == (0, 0, 0)).all(axis=-1).nonzero()
            if np.array(zeros).size > 0:
                # try cross product with Y
                n[zeros] = c[0][zeros].cross(vcls.y) * np.pi

        rot.rotvec = n
        if is1d:
            rot = rot[0]
        return rot

    @classmethod
    def random(cls, shape, **kwargs) -> "Rotation":
        r = Rotation.empty(shape, **kwargs)
        r.scipyRot = scipyRot.random(math.prod(shape[:-2]))
        return r

    @property
    def scipyRot(self) -> scipyRot:
        """
        Get this array of rotations as `scipy's Rotation <https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.transform.Rotation.html>`_

        Because scipy Rotation does not support multi-dimensional arrays, this
        array is flat, assignment of a flat array will reshape back to this
        array's shape

        Returns
        -------
        scipy.spacial.transform.Rotation:
            Array of scipy Rotation for each rotation matrix of this array

            Shape is :code:`(-1, 3, 3)`
        """
        return scipyRot.from_matrix(self.reshape(-1, 3, 3))

    @scipyRot.setter
    def scipyRot(self, new: scipyRot) -> None:
        self[...] = new.as_matrix().reshape(self.shape)

    def slerp(self, ktimes: np.ndarray) -> Callable[[np.ndarray], "Rotation"]:
        """
        Spherical linear interpolation of this array of rotations, with every
        rotation of the last axis corresponding to a key time of :attr:`ktimes`

        Returns a function that can be called with new times to interpolate
        rotations at, similar to scipy's :code:`interpolate.interp1d`

        Parameters
        ----------
        ktimes:
            Key times of the rotations

        Returns
        -------
        Callable[[np.ndarray], "Rotation"]:
            Function that can be called with times to interpolated rotations at

        Examples
        --------
        >>> from pynimation.common.rotation import Rotation
        >>> import numpy as np
        >>>
        >>> r = Rotation.identity((4,))
        >>> r
        Rotation([[[1., 0., 0.],
                   [0., 1., 0.],
                   [0., 0., 1.]],
        <BLANKLINE>
                  [[1., 0., 0.],
                   [0., 1., 0.],
                   [0., 0., 1.]],
        <BLANKLINE>
                  [[1., 0., 0.],
                   [0., 1., 0.],
                   [0., 0., 1.]],
        <BLANKLINE>
                  [[1., 0., 0.],
                   [0., 1., 0.],
                   [0., 0., 1.]]])
        >>>
        >>> r.eulerDeg.x = np.arange(0,40, 10)[:,np.newaxis]
        >>> r.eulerDeg
        Vec3([[ 0.,  0.,  0.],
              [10.,  0.,  0.],
              [20.,  0.,  0.],
              [30.,  0.,  0.]])
        >>>
        >>> ktimes = np.linspace(0,1,4) # key times
        >>> ktimes
        array([0.        , 0.33333333, 0.66666667, 1.        ])
        >>>
        >>> ntimes = np.linspace(0,1,5) # new times
        >>> ntimes
        array([0.  , 0.25, 0.5 , 0.75, 1.  ])
        >>>
        >>> r.slerp(ktimes)(ntimes).eulerDeg
        Vec3([[ 0. ,  0. ,  0. ],
              [ 7.5,  0. ,  0. ],
              [15. ,  0. ,  0. ],
              [22.5,  0. ,  0. ],
              [30. ,  0. ,  0. ]])
        """
        a = self.copy().reshape((self.shape[0], -1, 3, 3)).swapaxes(0, 1)
        fs = [scipySlerp(ktimes, d.scipyRot) for d in a]

        def func(ntimes: np.ndarray) -> "Rotation":
            fshape = (ntimes.shape[0], *self.shape[1:])
            return Rotation(
                np.stack([f(ntimes).as_matrix() for f in fs])
                .swapaxes(0, 1)
                .reshape(fshape)
            )

        return func

    # shortcuts

    e = euler
    eul = euler

    ed = eulerDeg
    eulDeg = eulerDeg

    q = quat

    rv = rotvec
    rotv = rotvec
