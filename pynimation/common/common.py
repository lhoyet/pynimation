import functools


def returnView(cls):
    """
    Decorator for functions returning a subclass of :code:`np.ndarray`

    Modifies the type of the return value by returning a view as :attr:`cls`
    (with :code:`np.ndarray.view`)

    Parameters
    ----------
    cls:
        class the return value should be viewed as
    """

    def dec(f):
        @functools.wraps(f)
        def wrap(*args, **kwargs):
            return f(*args, **kwargs).view(cls)

        return wrap

    return dec
