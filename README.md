[![PyNimation logo](https://gitlab.inria.fr/lhoyet/pynimation/uploads/78f649284078e39a0a8e1347bc35cd39/logo.png)](https://lhoyet.gitlabpages.inria.fr/pynimation/pres)

[![version badge](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/raw/version.svg?job=test)](https://gitlab.inria.fr/lhoyet/pynimation/-/tags)
[![pipeline status](https://gitlab.inria.fr/lhoyet/pynimation/badges/master/pipeline.svg)](https://gitlab.inria.fr/lhoyet/pynimation/-/pipelines/latest)
[![formatting badge](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/raw/formatting.svg?job=test)](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/file/reports/formatting.txt?job=test)
[![linting badge](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/raw/linting.svg?job=test)](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/file/reports/linting.txt?job=test)
[![type check badge](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/raw/type_check.svg?job=test)](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/file/reports/type_check.txt?job=test)
[![tests badge](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/raw/tests.svg?job=test)](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/file/reports/tests.txt?job=test)
[![coverage badge](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/raw/coverage.svg?job=test)](https://lhoyet.gitlabpages.inria.fr/pynimation/coverage)
[![dependencies badge](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/raw/dependencies.svg?job=test)](https://gitlab.inria.fr/lhoyet/pynimation/-/jobs/artifacts/master/file/reports/dependencies.txt?job=test)

PyNimation is a framework for editing, visualizing and studying skeletal 3D animations, it was more particularly designed to process motion capture data. It stems from the wish to utilize Python’s data science capabilities and ease of use for human motion research.

# Installation, Getting Started, Reference

See the relevant sections in the [documentation](https://lhoyet.gitlabpages.inria.fr/pynimation/)    
Follow future updates on [Trello](https://trello.com/b/JJlIvqrR/pynimation)

# License & Copyright
This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
