#version 330

in vec3 vPosition;

out Vertex
{
	vec4 position;
	vec4 color;
} vertex;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform vec4 colorIn;

void main()
{
	vertex.position = proj * view * model * vec4(vPosition, 1.0);
	vertex.color = colorIn;

	gl_Position = vertex.position;
}
