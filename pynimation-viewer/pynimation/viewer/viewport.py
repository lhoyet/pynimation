import os
import sys
from typing import Dict, Optional

import numpy as np
from OpenGL import GL
from PIL import Image

import pynimation.common.defaults as defaults
from pynimation.common.singleton import _Singleton

from .camera import Camera
from .player import Player
from .third_person_camera import ThirdPersonCamera
from .mouse import MouseManager


class Viewport(metaclass=_Singleton):
    """
    Class responsible for initializing and displaying the window

    Parameters
    ----------
    width: int:
        width of the window in pixels
    height: int:
        height of the window in pixels
    camera: Camera:
        camera through which the scene will be displayed
    fs: boolean:
        whether to set the window to full screen
    name: str:
        name displayed as the window title

    Attributes
    ----------
    width: int:
        width of the window in pixels
    height: int:
        height of the window in pixels
    camera: Camera:
        camera through which the scene will be displayed
    fullscreen: boolean:
        whether to set the window to full screen
    name: str:
        name displayed as the window title
    mainCamera: Camera:
        main camera of the viewport, that will be used to display the scene
    cameraTranslationSpeed: float:
        translation speed of the camera, relative to mouse motion
    cameraRotationSpeed: float:
        rotation speed of the camera, relative to mouse motion
    cameraZoomSpeed: float:
        zoom speed of the camera, for cameras supporting zoom
    player: Player:
        player singleton reference
    cameraData: Dict[str, np.ndarray]:
        camera view and projection matrices
    recordingFramerate: int:
        framerate at which to record
    """

    def __init__(
        self,
        width: int = defaults.VIEWPORT_WIDTH,
        height: int = defaults.VIEWPORT_HEIGHT,
        camera: Optional[Camera] = None,
        name: str = defaults.VIEWPORT_NAME,
        fs: bool = False,
    ) -> None:
        self.name = name
        self.fullscreen = fs
        self.width = width
        self.height = height

        self.mainCamera: Camera = camera or Camera(self.width, self.height)
        self._isCameraRotating = False
        self._cameraTranslationalDirection = np.array([0, 0, 0])
        self.cameraTranslationSpeed = 5.0
        self.cameraRotationSpeed = 300.0
        self.cameraZoomSpeed = 0.3

        self._isrecording = False
        self._triggerscreenshot = False
        self.recordingFramerate = 30
        self.player = Player()
        self.cameraData: Dict[str, np.ndarray] = {
            "proj": np.identity(4),
            "view": np.identity(4),
        }

    def startRecording(
        self,
        folder: Optional[str] = None,
        filename: Optional[str] = None,
        fps: int = 30,
    ) -> None:
        """
        Save a capture of the viewport at each frame, to `folder`, with
        prefix `filename`, record at `fps` frames per second

        Parameters
        ----------
        folder:
            folder in which to save the captures
        filename:
            prefix for the image files
        fps:
            framerate at which to record

        Note
        ----
        Capturing in will significantly slow down the render loop. Eventhough
        it appears slower, a constant framerate will be enforced and the
        animation will appear fluid when the captured images are put together.

        Note
        ----
        A video can be created from the set of captured images using
        ``ffmpeg``. `This page
        <https://www.ffmpeg.org/faq.html#How-do-I-encode-single-pictures-into-movies_003f>`_ contains examples
        """
        self._isrecording = True
        self.recordingFramerate = fps
        self.player.framerate = self.recordingFramerate
        self.player.accurate = True

        self.filenamebase = filename or "movie"
        self.filenameId = 0
        folderbase = folder or ""
        folderbase += self.filenamebase
        self.folderRecording = folderbase
        id = 0
        while os.path.isdir(self.folderRecording):
            self.folderRecording = folderbase + "_" + str(id).zfill(3)
            id += 1
        os.mkdir(self.folderRecording)
        self.filenamebase += "_" + str(id - 1).zfill(3)

    def stopRecording(self) -> None:
        """
        Stop a recording
        """
        self._isrecording = False
        self.player.accurate = False
        self.player.framerate = 0

    def startStopRecording(self) -> None:
        """
        Toggle recording on and off
        """
        if self._isrecording:
            self.stopRecording()
        else:
            self.startRecording()

    def triggerScreenshot(self) -> None:
        """
        Trigger a unique screenshot
        """
        self._triggerscreenshot = True

    def setMainCamera(self, camera: Camera) -> None:
        """
        Set main camera to `camera`

        Parameters
        ----------
        camera:
            camera to be set as main

        """
        self.mainCamera = camera

    def initialize(self) -> None:
        """
        Intialize the OpenGL context

        Note
        ----
        Must be called before any :class:`~pynimation.viewer.vbo.VBObject` is
        created, and any shader is compiled.
        Must be called after the creation of a window.
        """
        self.reshape(self.width, self.height)
        GL.glEnable(GL.GL_TEXTURE_2D)
        GL.glEnable(GL.GL_BLEND)
        GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

        GL.glEnable(GL.GL_DEPTH_TEST)
        GL.glDepthFunc(GL.GL_LEQUAL)

    def display(self) -> None:
        """
        Clear the viewport and display the next frame
        """
        GL.glClearColor(1, 1, 1, 1)
        # GL.glClearDepth(0.0)
        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

    def reshape(self, width: int, height: int) -> None:
        """
        Change the size of the openGL context

        Parameters
        ----------
        width:
            new width of the context

        height:
            new height of the context

        """
        GL.glViewport(0, 0, width, height)

    def resize(self, width: int, height: int) -> None:
        """
        Change the size of the window

        Parameters
        ----------
        width:
            new width of the window

        height:
            new height of the window

        """
        self.width = width
        self.height = height
        self.mainCamera.viewWidth = width
        self.mainCamera.viewHeight = height
        self.mainCamera.computeProjectionMatrix()

    def updateCamera(self) -> None:
        """
        Update the view and projection matrices of the camera
        """
        trans = (
            self._cameraTranslationalDirection
            * self.cameraTranslationSpeed
            * self.player.persistentDeltaTime
        )
        if np.linalg.norm(trans):
            self.mainCamera.translate(*trans)
        self.cameraData = {
            "proj": self.mainCamera.projectionMatrix,
            "view": np.linalg.inv(self.mainCamera.globalTransform),
        }

    def _createUniqueScreenshot(
        self,
        folderName: Optional[str] = None,
        filename: Optional[str] = None,
    ) -> None:
        folder = folderName or ""
        filenamebase = filename or "screenshot"
        id = 0
        while os.path.isfile(folder + filenamebase + "_" + str(id) + ".png"):
            id += 1
        self.screenshot(folder + filenamebase + "_" + str(id) + ".png")
        # MISSING DIRECTORY CREATION IF DOES NOT EXIST

    def screenshot(self, filename: str) -> None:
        """
        Capture screenshot

        Parameters
        ----------
        filename:
            path of the captured image
        """
        buffer = (GL.GLubyte * (3 * self.width * self.width))(0)
        GL.glReadPixels(
            0,
            0,
            self.width,
            self.height,
            GL.GL_RGB,
            GL.GL_UNSIGNED_BYTE,
            buffer,
        )

        # Use PIL to convert raw RGB buffer and flip the right way up
        image = Image.frombytes("RGB", (self.width, self.height), buffer)
        image = image.transpose(Image.FLIP_TOP_BOTTOM)
        image.save(filename)

    def handleScreenshots(self) -> None:
        """
        Should be called at the end of the render loop to capture the frame
        """
        if self._isrecording:
            self.screenshot(
                self.folderRecording
                + "/"
                + self.filenamebase
                + "_"
                + str(self.filenameId).zfill(4)
                + ".png"
            )
            self.filenameId += 1

        elif self._triggerscreenshot:
            self._createUniqueScreenshot()
            self._triggerscreenshot = False

    def quit(self) -> None:
        """
        Exit the viewer
        """
        sys.exit(0)

    def setCameraTranslationalDirection(
        self, axis: int, direction: int
    ) -> None:
        """
        Set the translation direction of the :attr:`mainCamera` along `axis` in `direction`
        This information will be used by
        :func:`~pynimation.viewer.viewport.Viewport.updateCamera` to move the
        camera according to :attr:`cameraTranslationSpeed`

        Parameters
        ----------
        axis:
            axis to move the camera along
        direction:
            direction to move the camera in
        """

        self._cameraTranslationalDirection[axis] = direction

    def setCameraIsRotating(self, isRotating: bool) -> None:
        """
        Set wether the :attr:`mainCamera` is rotating according to mouse motion
        This information will be used by
        :func:`~pynimation.viewer.viewport.Viewport.updateCamera` to rotate the
        camera according to :attr:`cameraRotationSpeed` when the mouse moves

        Parameters
        ----------
        isRotating:
            wether the :attr:`mainCamera` should rotate when the mouse moves
        """
        self._isCameraRotating = isRotating

    def zoomCamera(self, way: int) -> None:
        """
        Zoom :attr:`mainCamera` in `way` direction
        with :attr:`cameraZoomSpeed`

        Parameters
        ----------
        way:
            wether to zoom (``1``) or dezoom (``-1``) the camera
        """
        if isinstance(self.mainCamera, ThirdPersonCamera):
            self.mainCamera.distance += self.cameraZoomSpeed * -way

    def toggleThirdPerson(self) -> None:
        if isinstance(self.mainCamera, ThirdPersonCamera):
            m = self.mainCamera
            cam = Camera(
                m.viewWidth,
                m.viewHeight,
                m.near,
                m.far,
                m.fov,
                m.transform.position,
                m.transform.rotation,
            )
        else:
            m = self.mainCamera
            cam = ThirdPersonCamera(
                m.viewWidth,
                m.viewHeight,
                m.near,
                m.far,
                m.fov,
                m.transform.position,
                m.transform.position - m.transform[0:3, 2].copy(),
            )
        self.mainCamera = cam

    def rotateCameraWithMouse(self, mouse: MouseManager) -> None:
        if self._isCameraRotating:
            self.mainCamera.rotateYaw(
                mouse.rel.x / 2 * self.cameraRotationSpeed / self.width
            )
            self.mainCamera.rotatePitch(
                mouse.rel.y / 2 * self.cameraRotationSpeed / self.height
            )
