from pynimation.common.vec import Vec2


class MouseManager:
    def __init__(self):
        self.__pos: Vec2 = Vec2([0, 0], dtype=int)
        self.__opos: Vec2 = Vec2([0, 0], dtype=int)

        self.__active = False

    @property
    def rel(self) -> Vec2:
        return self.__pos - self.__opos

    @property
    def pos(self) -> Vec2:
        return self.__pos

    @pos.setter
    def pos(self, newPos: Vec2) -> None:
        if self.__active:
            self.__opos = self.__pos
        else:
            self.__opos = newPos
            self.__active = True

        self.__pos = newPos

    def idle(self) -> None:
        self.__active = False
