from typing import Optional

import numpy as np

import pynimation.common.defaults as defaults
from pynimation.common.rotation import Rotation
from pynimation.common.transform import Transform

from .scene_graph import SceneNode


class Camera(SceneNode):
    """
    A scene camera

    Parameters
    ----------

    width: int:
        width of the view plane in pixels
    height: int:
        height of the view plane in pixels
    near: float:
        distance from the near plane
    far: int:
        distance from the far plane
    fov: int:
        distance from the far plane
    initPos: np.ndarray:
        initial position of the camera
    initRot: np.ndarray:
        initial rotation of the camera
    name: str:
        name of the camera node

    Attributes
    ----------
    width: int:
        width of the view plane in pixels
    height: int:
        height of the view plane in pixels
    near: float:
        distance from the near plane
    far: int:
        distance from the far plane
    fov: int:
        distance from the far plane
    projectionMatrix: Transform:
        projection matrix of the camera
    """

    def __init__(
        self,
        width: int = defaults.VIEWPORT_WIDTH,
        height: int = defaults.VIEWPORT_HEIGHT,
        near: float = 0.001,
        far: int = 1000,
        fov: int = 30,
        initPos: Optional[np.ndarray] = None,
        initRot: Optional[np.ndarray] = None,
        name: str = "",
    ) -> None:
        super().__init__(name)
        self.viewWidth = width
        self.viewHeight = height
        self.near = near
        self.far = far
        self.fov = fov

        self.projectionMatrix = Transform()
        self.transform = Transform()
        if initRot is None:
            initRot = defaults.INIT_CAMERA_ROTATION
        self.transform.rotation = initRot
        if initPos is None:
            initPos = defaults.INIT_CAMERA_POSITION
        self.transform.position = initPos

        self.computeProjectionMatrix()

    def translate(self, dx: float, dy: float, dz: float) -> None:
        """
        Translate the camera by (`dx`,`dy`,`dz`)

        Parameters
        ----------
        dx:
            translation along X axis
        dy:
            translation along Y axis
        dz:
            translation along Z axis
        """
        self.transform.translate([dx, dy, dz], True)

    def computeProjectionMatrix(self) -> None:
        """
        Compute the projection matrix of the camera according to dimensions
        attributes
        """
        self.projectionMatrix.projection(
            self.viewWidth, self.viewHeight, self.fov, self.near, self.far
        )

    def rotateYaw(self, yaw: float) -> None:
        """
        Rotate camera along Y axis by `yaw` degrees

        Parameters
        ----------
        yaw:
            value to rotate the camera by, in degrees
        """
        rotation = self.transform.rotationTransform()
        position = self.transform.positionTransform()
        rot = Transform.fromRotation(Rotation.yDeg(-yaw))
        self.transform = position @ (rot @ rotation)
        # FIXME: less ops ?
        # self.transform.rotation = Rotation.yDeg(yaw) @ self.transform.rotation

    def rotatePitch(self, pitch: float) -> None:
        """
        Rotate camera along Z axis by `yaw` degrees

        Parameters
        ----------
        pitch:
            value to rotate the camera by, in degrees
        """
        rotation = self.transform.rotationTransform()
        position = self.transform.positionTransform()
        rot = Transform.fromRotation(Rotation.xDeg(-pitch))
        self.transform = position @ (rotation @ rot)

    def lookAt(self, targetPos: np.ndarray) -> None:
        """
        Orient the camera to look at `targetPos`

        Parameters
        ----------
        targetPos:
            Position the camera will look at
        """
        self.transform.lookAtOrientation(
            self.globalTransform.position, targetPos, [0, 1, 0]
        )

    def lookAtNode(self, node: SceneNode) -> None:
        """
        Orient the camera to look at `node`

        Parameters
        ----------
        node:
            node the camera will look at
        """
        self.lookAt(node.globalTransform.position)
