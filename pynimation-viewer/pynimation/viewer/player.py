from operator import attrgetter
from typing import TYPE_CHECKING, List, Dict, Optional

from pynimation.common.singleton import _Singleton

from .clock import Clock

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation


class Player(metaclass=_Singleton):
    """
    Singleton class managing time and animation states

    Parameters
    ----------
    framerate: int
        framerate of the player, the
        :attr:`~pynimation.viewer.player.Player.tick` method will try to
        maintain a framerate below this value, delaying the next frame
        when necessary

    Attributes
    ----------
    framerate: int
        framerate of the player, the
        :attr:`~pynimation.viewer.player.Player.tick` method will try to
        maintain a framerate below this value, delaying the next frame
        when necessary

    animationStates: Dict[Animation, Player.AnimationState]
        current states of the animations: their current frame id, whether they
        are paused

    time: float
        time since the Player was started, as recorded by the last call to
        :attr:`~pynimation.viewer.player.Player.tick`

    deltaTime: float
        time difference between last call to :attr:`tick` and the one before,
        0.0 when the player is paused

    persistentDeltaTime: float
        time difference between last call to :attr:`tick` and the one before,
        even when the player is paused

    speed: float
        speed of the player. 1.0 is normal speed

    """

    class AnimationState:
        def __init__(self, currentFrame: int, paused: bool):
            self.currentFrame = currentFrame
            self.paused = paused

    def __init__(self, framerate: int = 0):
        self._clock = Clock()
        self._accurate = False
        self.framerate = framerate
        self.deltaTime: float = 0
        self.persistentDeltaTime: float = 0
        self._targetDelta: float = 0
        self._time: float = 0
        self.speed: float = 1.0
        self._paused: bool = False
        self.animationStates: Dict["Animation", Player.AnimationState] = {}
        self.longestDuration: Optional[float] = None

    @property
    def animations(self) -> List["Animation"]:
        """
        Get the list of all the animations contained by the player

        Returns
        -------
        List[Animation]
            return all the animation objects contains in animationState
        """
        return list(self.animationStates.keys())

    @property
    def paused(self) -> bool:
        """
        whether the player is paused
        """
        return self._paused

    @paused.setter
    def paused(self, newPaused: bool) -> None:
        self._paused = newPaused
        if self._paused:
            self.deltaTime = 0

    @property
    def time(self) -> float:
        """
        Return the current time of the player
        """
        return self._time

    @time.setter
    def time(self, time: float) -> None:
        self._time = time

    def start(self) -> None:
        """
        Start the player
        """
        self.resume()

    def reset(self) -> None:
        """
        Reset the player time to 0
        """
        self.deltaTime = 0
        self.time = 0

    def stop(self) -> None:
        """
        Stop the player
        """
        self.reset()
        self.pause()

    def resume(self, animation: Optional["Animation"] = None) -> None:
        """
        Resume the player or an animation
        """
        self._updatePaused(False, animation)

    def pause(self, animation: Optional["Animation"] = None) -> None:
        """
        Pause the player
        """
        self._updatePaused(True, animation)

    def togglePause(self, animation: Optional["Animation"] = None) -> None:
        """
        Toggle pause
        """
        self._updatePaused(None, animation)

    def _updatePaused(
        self,
        newPaused: Optional[bool] = None,
        animation: Optional["Animation"] = None,
    ) -> None:
        if animation is not None:
            if newPaused is None:
                newPaused = not self.animationStates[animation].paused
            self.animationStates[animation].paused = newPaused
        else:
            if newPaused is None:
                newPaused = not self.paused
            self.paused = newPaused

    @property
    def accurate(self) -> bool:
        """
        when set to True, :attr:`deltaTime` will always be ``1/framerate``, and
        not the actual time elapsed since last call to
        :attr:`~pynimation.viewer.player.Player.tick`
        """
        return self._accurate

    @accurate.setter
    def accurate(self, newAccurate: bool) -> None:
        if self._accurate == newAccurate:
            return
        else:
            if newAccurate:
                self._targetDelta = 1.0 / self.framerate
            else:
                self._targetDelta = 0

            self._accurate = newAccurate

    def tick(self) -> float:
        """
        Should be called at the end of the main loop

        Returns
        -------
        float
            time elapsed since last call
        """
        deltaTime: float = 0.0
        if self._accurate:
            # delay if we tick too quick
            deltaTime = self._clock.tick(self.framerate)
            # lag if we tick too slow
            if deltaTime > self._targetDelta:
                deltaTime = self._targetDelta
        else:
            deltaTime = self._clock.tick(self.framerate)

        if not self.paused:
            # update the current time
            self.persistentDeltaTime = deltaTime
            self.deltaTime = deltaTime
            self.time += self.deltaTime * self.speed
        # update the view
        self.__updateCurrentFrames()

        return self.deltaTime

    def getDeltaTime(self) -> float:
        """
        Get the time difference between last call to :attr:`tick` and the one before

        Returns
        -------
        float:
            last recorded deltaTime
        """
        return self.deltaTime

    def getTime(self) -> float:
        """
        Get elalpsed time since :attr:`start` was called

        Returns
        -------
        float:
            time since Player start
        """
        return self.time

    def unRegisterAnimation(self, animation: "Animation") -> None:
        """
        Unregister `animation` from the Player. It is a good
        idea to do so when an animation is not being played anymore
        """
        if animation not in self.animationStates:
            return
        del self.animationStates[animation]
        if len(self.animationStates) > 0:
            self.longestDuration = max(
                [anim.duration for anim in self.animationStates]
            )

    def registerAnimation(self, animation: "Animation") -> None:
        """
        Register `animation` to the Player. This is a necessary
        operation to later obtain the current frame id with
        :attr:`~pynimation.viewer.player.Player.getCurrentFrame`, and
        manipulate the state of the animation with
        :attr:`~pynimation.viewer.player.Player.incrementFrame`,
        :attr:`~pynimation.viewer.player.Player.nextFrame`,
        :attr:`~pynimation.viewer.player.Player.previousFame`,
        :attr:`~pynimation.viewer.player.Player.pause`, etc.

        Parameters
        ----------
        animation:
            animation to register
        """
        if animation in self.animationStates:
            return
        self.animationStates[animation] = Player.AnimationState(
            currentFrame=0, paused=False
        )
        self.longestDuration = max(
            [anim.duration for anim in self.animationStates]
        )

    def isAnimationRegistered(self, animation: "Animation") -> bool:
        return self.animationStates.get(animation) is not None

    def __updateCurrentFrames(self) -> None:
        time = self.time
        duration = (
            self.longestDuration if self.longestDuration is not None else 0
        )

        for anim, state in self.animationStates.items():
            if not state.paused:
                state.currentFrame = anim.globalCycleIdAt(time, duration)

    def incrementFrame(
        self, increment: int, animation: Optional["Animation"] = None
    ) -> None:
        """
        Increment the current frame id of `animation` if given,
        otherwise of all registered animations by `increment`
        (based on the lowest framerate)

        Parameters
        ----------
        increment:
            integer that will increment the current frame id
        animation:
            animation whose current frame will be incremented,
            if ``None``, all registered animations will be
            incremented
        """
        animations = (
            [animation]
            if animation is not None
            else list(self.animationStates.keys())
        )

        self.time = (
            increment * (1 / max([anim.framerate for anim in animations]))
            + self.time
        )

    def setCurrentFrame(
        self, frameId: int, animation: Optional["Animation"] = None
    ) -> None:
        """
        set the current frame of `animation` if given,
        otherwise of all registered animations by `frameid`
        (based on the lowest framerate)

        Parameters
        ----------
        frameid:
            integer that will set the current frame id
        animation:
            animation whose current frame will be incremented,
            if ``None``, all registered animations will be
            incremented

        """
        animations = (
            [animation]
            if animation is not None
            else list(self.animationStates.keys())
        )
        currentAnim = max(animations, key=attrgetter("framerate"))

        self.time += currentAnim.returnFrameIdInDomain(
            frameId
        ) - self.animationStates[currentAnim].currentFrame * (
            1 / max([anim.framerate for anim in animations])
        )

    def nextFrame(self, animation: Optional["Animation"] = None):
        """
        Increment the current frame id of `animation` if given,
        otherwise of all registered animations, by 1

        Parameters
        ----------
        animation:
            animation whose current frame will be incremented,
            if ``None``, all registered animations will be
            incremented

        """
        self.incrementFrame(1, animation)

    def previousFrame(self, animation: Optional["Animation"] = None):
        """
        Decrement the current frame id of `animation` if given,
        otherwise of all registered animations, by 1

        Parameters
        ----------
        animation:
            animation whose current frame will be incremented,
            if ``None``, all registered animations will be
            incremented

        """
        self.incrementFrame(-1, animation)

    def getCurrentFrameId(self, animation: "Animation") -> int:
        """
        Get the current frame of `animation` that should be played
        according to the player. Display the frame with the id
        given by this function in order to control it with
        :attr:`~pynimation.viewer.player.Player.incrementFrame`,
        :attr:`~pynimation.viewer.player.Player.nextFrame`,
        :attr:`~pynimation.viewer.player.Player.previousFame`,
        :attr:`~pynimation.viewer.player.Player.pause`, etc.

        Parameters
        ----------
        animation:
            animation whose current frame will be incremented,
            if ``None``, all registered animations will be
            incremented

        """
        if animation not in self.animationStates:
            raise ValueError("Given animation is not registered to the Player")
        return self.animationStates[animation].currentFrame

    def resetSpeed(self) -> None:
        """
        Reset Player speed to 0.0
        """
        self.speed = 1.0

    def varySpeed(self, delta: float) -> None:
        """
        Increase or decrease the speed of the player by `delta`

        Parameters
        ----------
        delta:
            how much the speed is increased/decreased
        """
        self.speed = max(self.speed + delta, 0)
