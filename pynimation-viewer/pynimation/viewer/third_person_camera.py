from typing import Optional

import numpy as np
from pynimation.viewer.camera import Camera

import pynimation.common.defaults as defaults
from pynimation.common.rotation import Rotation
from pynimation.common.transform import Transform


class ThirdPersonCamera(Camera):
    """
    A third person scene camera
    This camera orbits around its :attr:`center` at :attr:`distance`

    Parameters
    ----------

    width: int:
        width of the view plane in pixels
    height: int:
        height of the view plane in pixels
    near: float:
        distance from the near plane
    far: int:
        distance from the far plane
    fov: int:
        distance from the far plane
    initPos: np.ndarray:
        initial position of the camera
    center: np.ndarray:
        initial center of rotation of the camera
    name: str:
        name of the camera node

    Attributes
    ----------
    width: int:
        width of the view plane in pixels
    height: int:
        height of the view plane in pixels
    near: float:
        distance from the near plane
    far: int:
        distance from the far plane
    fov: int:
        distance from the far plane
    projectionMatrix: Transform:
        projection matrix of the camera
    """

    def __init__(
        self,
        width: int = defaults.VIEWPORT_WIDTH,
        height: int = defaults.VIEWPORT_HEIGHT,
        near: float = 0.001,
        far: int = 1000,
        fov: int = 30,
        initPos: Optional[np.ndarray] = None,
        center: Optional[np.ndarray] = None,
        name: str = "",
    ) -> None:
        if initPos is None:
            initPos = defaults.INIT_CAMERA_POSITION
        if center is None:
            center = initPos - np.array([0, 0, 1])
        self._center = center
        self._distance = np.linalg.norm(initPos - self._center).astype(float)
        t = Transform()
        t.lookAtOrientation(initPos, self._center, [0, 1, 0])
        initRot = t.rotation
        super().__init__(
            width, height, near, far, fov, initPos, initRot, name=name
        )

    @property
    def distance(self) -> float:
        """
        distance to the center of rotation of the camera
        """
        return self._distance

    @distance.setter
    def distance(self, newDistance: float) -> None:
        """
        Distance from the camera to :attr:`center`
        """
        self._distance = newDistance
        self.transform.position = (
            self._center + self.transform.rotation.cols.z * self._distance
        )

    @property
    def center(self) -> np.ndarray:
        """
        Center of rotation of the camera, it will orbit around it
        """
        return self._center

    @center.setter
    def center(self, newCenter: np.ndarray) -> None:
        d = self.transform.position - self._center
        self._center = newCenter
        self.transform.position = self._center + d

    def translate(self, dx: float, dy: float, dz: float) -> None:
        """
        Translate the camera by (`dx`,`dy`,`dz`)
        along local XZ axes and global Y axis

        Parameters
        ----------
        dx:
            translation along X local axis
        dy:
            translation along Y global axis
        dz:
            translation along Z local axis
        """
        r = self.transform.rotation.copy()
        r[0:3, 1] = [0, 1, 0]
        d = np.dot(r, [dx, dy, dz])

        self.transform.translate(d, False)
        self._center += np.array(d)

    def rotateYaw(self, yaw: float) -> None:
        """
        Rotate camera along Y axis by `yaw` degrees
        around :attr:`center`

        Parameters
        ----------
        yaw:
            value to rotate the camera by, in degrees
        """
        self.transform.rotation = Rotation.yDeg(-yaw) @ self.transform.rotation
        self.transform.position = (
            self._center + self.transform.rotation.cols.z * self._distance
        )

    def rotatePitch(self, pitch: float) -> None:
        """
        Rotate camera along Z axis by `pitch` degrees
        around :attr:`center`

        Parameters
        ----------
        pitch:
            value to rotate the camera by, in degrees
        """
        self.transform.rotation = self.transform.rotation @ Rotation.xDeg(
            -pitch
        )
        self.transform.position = (
            self._center + self.transform.rotation.cols.z * self._distance
        )

    def lookAt(self, targetPos: np.ndarray) -> None:
        """
        Orient the camera to look at `targetPos`

        Center will be updated to this position

        Camera position will not change

        Parameters
        ----------
        targetPos:
            Position the camera will look at
        """
        self._center = targetPos
        p = self.globalTransform.position
        self._distance = np.linalg.norm(targetPos - p)
        super().lookAt(targetPos)
