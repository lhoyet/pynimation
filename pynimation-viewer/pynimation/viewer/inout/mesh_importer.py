import abc
from typing import TYPE_CHECKING, List, Optional

from pynimation.viewer.character import Character

from pynimation.inout.common import IO, _getExtension, _ioFactory, io
from pynimation.inout.importer import Importer

if TYPE_CHECKING:
    from pynimation.viewer.scene import SceneNode


@io(None)
class MeshImporter(abc.ABC, IO):
    """
    Base mesh importer class. Subclasses each handle a particular
    mesh file format
    """

    @abc.abstractmethod
    def loadMeshes(self, filename: str) -> List["SceneNode"]:
        """
        Load meshes from `filename`

        Parameters
        ----------
        filename:
            file to load meshes from

        Returns
        -------
        List[SceneNode]
            meshes loaded from `filename`
        """
        pass


def loadMeshes(filename: str) -> List["SceneNode"]:
    """
    Generic mesh import method

    Loads meshes from `filename`

    The format of the imported file will be determined by the extension of
    `filename`


    Parameters
    ----------
    filename:
        path of the file to meshes from

    Returns
    -------
    List[SceneNode]
        meshes loaded from `filename`
    """
    mesh_importer = _ioFactory(MeshImporter, _getExtension(filename))
    meshes = mesh_importer.loadMeshes(filename)
    return meshes


def loadCharacters(
    filename: str, modelFilename: Optional[str] = None
) -> List["Character"]:
    """
    Generic character import method

    Loads animations and meshes from `filename` or animations from
    `filename` and meshes from `modelFilename` if the second is
    provided

    If no meshes are found in `filename` and `modelFilename` is not
    provided, a :class:`~pynimation.viewer.stickfigure.StickFigure` mesh will
    be created and used as a mesh for the character

    Parameters
    ----------
    filename:
        file to load animations and meshes from

    modelFilename:
        different file to load meshes from, will have priority over meshes from
        `filename` if provided

    Returns
    -------
    List[Character]
        characters loaded from `filename` and `modelFilename`
    """
    if modelFilename is None:
        modelFilename = ""
    importer = _ioFactory(Importer, _getExtension(filename))
    animations = importer.load(filename)
    meshes = []
    if len(modelFilename) > 0:
        # modelFilename given, load meshes from it
        mesh_importer = _ioFactory(MeshImporter, _getExtension(modelFilename))
        meshes = mesh_importer.loadMeshes(modelFilename)
    else:
        # modelFilename not given, try to load meshes from filename
        try:
            mesh_importer = _ioFactory(MeshImporter, _getExtension(filename))
            meshes = mesh_importer.loadMeshes(filename)
        except NotImplementedError:
            # No MeshImporter for filename, create StickFigure mesh
            pass
    characters = []
    for animation in animations:
        characters.append(Character(animation, meshes))

    return characters
