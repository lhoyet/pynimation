from typing import Any, List, Optional, Union

import numpy as np
from OpenGL import GL
from pynimation.viewer.vbo import VBObject


class Curve(VBObject):
    """
    A 3D curve

    Parameters
    ----------
    points: np.ndarray:
        points of the curve
    color: Optional[List[Union[float, int]]]:
        color of the line
    width: int
        width of the line in world coordinates
    antialiasing: bool:
        enable antialiasing with GL_LINE_SMOOTH

    Attributes
    ----------
    points: np.ndarray:
        points of the curve
    color: List[Union[float, int]]
        color of the line
    width: int
        width of the line in world coordinates
    antialiasing: bool:
        enable antialiasing with GL_LINE_SMOOTH
    """

    def __init__(
        self,
        points: np.ndarray,
        width: int = 1,
        color: Optional[List[Union[float, int]]] = None,
        antialiasing: bool = True,
    ) -> None:
        self.points = np.array(points)
        self.color = color or [0, 0, 1, 1]
        self.width = width
        self.antialiasing = antialiasing
        LIST_VERTICES: List[Any] = self.points.tolist()
        LIST_COLORS: List[Any] = []
        LIST_NORMALS: List[Any] = []

        for _ in points:
            LIST_COLORS.append(self.color)
            LIST_NORMALS.append([0.0, 1.0, 0.0])

        data = {}
        data["vPosition"] = np.array(LIST_VERTICES, dtype="float32")
        data["vColor"] = np.array(LIST_COLORS, dtype="float32")
        data["vNormal"] = np.array(LIST_NORMALS, dtype="float32")

        super(Curve, self).__init__(data, primitive=GL.GL_LINE_STRIP)

    def drawBuffers(self) -> None:
        """ """
        if self.antialiasing:
            GL.glEnable(GL.GL_LINE_SMOOTH)
        GL.glLineWidth(self.width)
        super(Curve, self).drawBuffers()
