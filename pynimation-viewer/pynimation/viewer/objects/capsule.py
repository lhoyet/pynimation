import math
from typing import Any, Dict, List, Optional, Union

import numpy as np
from numpy import float64
from pynimation.viewer.vbo import VBObject
from pynimation.viewer.viewport import Viewport

from pynimation.common import data as data_
from pynimation.common.vec import Vec3


class Capsule(VBObject):
    """
    A 3D capsule mesh for displaying a skeleton's joint

    Parameters
    ----------
    jointID: int:
        id of the joint displayed
    length: float:
        length of the capsule
    radius: float:
        radius of the capsule
    center: bool:
        whether to center the capsule around the bone or align it to the bone
    numberSegments: int:
        number of segments on the cross-section

    Attributes
    ----------
    jointID: int:
        id of the joint displayed
    length: float:
        length of the capsule
    radius: float:
        radius of the capsule
    numberSegments: int:
        number of segments on the cross-section
    """

    def __init__(
        self,
        jointID: int = 0,
        length: float64 = np.float64(1.0),
        radius: float = 1.0,
        center: bool = True,
        color: Optional[List[Union[float, int]]] = None,
        numberSegments: int = 8,
    ) -> None:
        self.nbSegments = numberSegments
        self.radius = radius
        self.length = length
        if color is None:
            color = [0.5, 0.5, 0.5, 1]
        self.color = color
        self.jointID = jointID

        LIST_VERTICES: List[Any] = []
        LIST_NORMALS: List[Any] = []
        LIST_VERTEXINDEX: List[int] = []

        vertSh = "diffusecolor_vs"
        vertFileName = data_.getDataPath("data/shaders/" + vertSh + ".glsl")
        with open(vertFileName) as file:
            VERT = file.read()

        p = np.array([0.0, 0.0, 0.0])
        n = np.array([0.0, 0.0, 0.0])
        nbVertSeg = (int)(self.nbSegments / 2)

        limSeg = (int)(nbVertSeg / 2)
        if center:
            yOffset = -self.length * 0.5
        else:
            yOffset = np.float64(0.0)
        listOfIdx = list(range(-limSeg + 1, 1)) + list(range(0, limSeg))

        LIST_VERTICES.append([0.0, -self.radius + yOffset, 0.0])
        LIST_NORMALS.append([0.0, -1.0, 0.0])

        nbIntermSeg = len(listOfIdx)
        for k in range(0, nbIntermSeg):
            s = listOfIdx[k]
            angle = s / limSeg * math.pi * 0.5
            r = math.cos(angle) * self.radius
            p[1] = math.sin(angle) * radius + yOffset
            n[1] = math.sin(angle) * radius

            if s == 0:
                if center:
                    yOffset = abs(yOffset)
                else:
                    yOffset = self.length

            for i in range(0, self.nbSegments):
                angle = i / self.nbSegments * 2 * math.pi
                p[0] = r * math.cos(angle)
                p[2] = r * math.sin(angle)
                LIST_VERTICES.append(p.copy())
                LIST_NORMALS.append(Vec3([p[0], n[1], p[2]]).normalized())

                if k == 0:
                    self.createFaceIndexes(
                        LIST_VERTEXINDEX,
                        0,
                        i + 1,
                        1 + (i + 1) % self.nbSegments,
                    )
                else:
                    self.createFaceIndexes(
                        LIST_VERTEXINDEX,
                        (k - 1) * self.nbSegments + i + 1,
                        k * self.nbSegments + i + 1,
                        k * self.nbSegments + (i + 1) % self.nbSegments + 1,
                        (k - 1) * self.nbSegments
                        + (i + 1) % self.nbSegments
                        + 1,
                    )

        LIST_VERTICES.append([0.0, self.radius + yOffset, 0.0])
        LIST_NORMALS.append([0.0, 1.0, 0.0])

        # Create top face indexes
        lastVertexIdx = len(LIST_VERTICES) - 1
        for i in range(1, self.nbSegments + 1):
            self.createFaceIndexes(
                LIST_VERTEXINDEX,
                (nbIntermSeg - 1) * self.nbSegments + i,
                lastVertexIdx,
                (nbIntermSeg - 1) * self.nbSegments
                + (i % self.nbSegments)
                + 1,
            )

        data = {}
        data["vPosition"] = np.array(LIST_VERTICES, dtype="float32")
        data["vNormal"] = np.array(LIST_NORMALS, dtype="float32")

        super(Capsule, self).__init__(
            data, np.array(LIST_VERTEXINDEX, dtype="int32"), vertexShader=VERT
        )

        self.colorId = super(Capsule, self).setUndefinedShaderData("colorIn")

    def display(
        self,
        uniformData: Optional[Dict[str, np.ndarray]] = None,
    ) -> None:
        if uniformData is None:
            uniformData = {}
        self.uniformData: Dict[str, np.ndarray] = {
            **uniformData,
            **self.uniformData,
            **Viewport().cameraData,
        }
        self.uniformData["model"] = self.globalTransform
        self.uniformData["colorIn"] = np.array(self.color)
        self.linkShaderData(self.uniformData)
        self.automaticallyLinkShaderUniforms(self.colorId, self.color)
        self.drawBuffers()
