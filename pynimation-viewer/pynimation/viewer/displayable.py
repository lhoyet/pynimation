import abc
from typing import Dict

import numpy as np


class Displayable(abc.ABC):
    """
    Base class for displayable objects
    """

    @abc.abstractmethod
    def display(self, uniformData: Dict[str, np.ndarray] = {}) -> None:
        pass
