from typing import List, Optional

import numpy as np

from pynimation.anim.animation import Animation
from pynimation.common.transform import Transform

from .animatable import Animatable
from .animated import Animated
from .player import Player
from .scene_graph import SceneNode
from .stickfigure import StickFigure


class Character(SceneNode, Animated):
    """
    Representation of an animated 3D character in the scene

    Holds an animation and the mesh it animates

    Responsible for animating the mesh at each frame
    """

    def __init__(
        self,
        animation: Animation,
        meshes: Optional[List[SceneNode]] = None,
        offsetCycle: bool = False,
    ):
        """
        Parameters
        ----------
        animation: Animation
            animation of the character
        meshes: List[SceneNode]
            animated meshes, must be subclass of
            :class:`~pynimation.viewer.animatable.Animatable`
        """
        super().__init__("")

        self.animation: "Animation" = animation
        """
        animation of the character
        """

        self.animation.updateAllGlobals()
        meshes = meshes or []
        if len(meshes) == 0:
            meshes.append(StickFigure(self.animation.skeleton))
        self.children = tuple(meshes)
        self.player = Player()
        self.player.registerAnimation(self.animation)
        self.offsetCycle = offsetCycle
        if self.offsetCycle:
            self._offset = self.computeOffset()
            self._prevCycle = 0

    def computeOffset(self) -> np.ndarray:
        gt0 = self.animation.frames[0].globalTransforms[0]
        gt1 = self.animation.frames[-1].globalTransforms[0]

        off = Transform.computeXZOffset(Transform(), gt0)
        off1 = Transform.computeXZOffset(Transform(), gt1)

        return np.dot(off1.matrix, off.inverse().matrix)

    def __del__(self):
        self.player.unRegisterAnimation(self.animation)

    def animate(self) -> None:
        """
        Animate :attr:`meshes` with :attr:`animation` at the current time
        """
        frame = self.animation[self.player.getCurrentFrameId(self.animation)]
        if self.offsetCycle:
            # FIXME should be able to set globalTransform without it being
            # overwritten
            ncycle = int(self.player.time // self.animation.getDuration())
            if self._prevCycle != ncycle:
                self._prevCycle = ncycle
                off = np.linalg.matrix_power(self._offset, ncycle)
                self.globalTransform.matrix = off

        for mesh in self.children:
            if issubclass(mesh.__class__, Animatable):
                mesh.animate(frame)  # type: ignore

    @staticmethod
    def load(
        filename: str, modelFileName: Optional[str] = None
    ) -> Optional["Character"]:
        from pynimation.viewer.inout import mesh_importer as importer

        c = importer.loadCharacters(filename, modelFileName)
        if c is None or len(c) == 0:
            return None
        else:
            return c[0]

    @staticmethod
    def loadMultiple(
        filename: str, modelFileName: Optional[str] = None
    ) -> List["Character"]:
        from pynimation.viewer.inout import mesh_importer as importer

        c = importer.loadCharacters(filename, modelFileName)
        if c is None or len(c) == 0:
            return []
        else:
            return c
