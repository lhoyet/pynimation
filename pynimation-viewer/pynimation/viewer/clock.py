import time


class Clock:
    def __init__(self):
        self.timeSave = None

    def tick(self, framerate: int) -> float:
        if self.timeSave is None:
            self.timeSave = time.time()

        result = time.time() - self.timeSave
        self.timeSave = time.time()

        return result
