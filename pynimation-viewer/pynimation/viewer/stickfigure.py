import copy
from typing import TYPE_CHECKING, List, Optional, Union

import numpy as np

import pynimation.common.defaults as defaults
from pynimation.anim.skeleton import Skeleton
from pynimation.common.rotation import Rotation
from pynimation.common.transform import Transform
from pynimation.common.vec import Vec3

from .animatable import Animatable
from .objects.capsule import Capsule
from .scene_graph import SceneNode

if TYPE_CHECKING:
    from pynimation.anim.animation import Animation


class StickFigure(SceneNode, Animatable):
    """
    Skeletal 3D figure used to display an animation.

    Parent of the :class:`~pynimation.viewer.objects.Capsule` 3D meshes, that
    are displayed as the skeleton limbs
    """

    def __init__(
        self,
        skeleton: "Skeleton",
        color: Optional[List[Union[float, int]]] = None,
        jointColors: Optional[np.ndarray] = None,
        boneRadius: float = 0.005,
    ) -> None:
        """
        Parameters
        ----------
        skeleton: Skeleton
            skeleton that is displayed
        color: np.ndarray
            color of the whole figure
        boneRadius: float
            radius of the joint meshes
        jointColors: np.ndarray
            RGBA colors of each joint :class:`~pynimation.viewer.objects.Capsule`
        """
        super().__init__("")

        self.skeleton = skeleton
        """
        skeleton that is displayed
        """

        self.begin = 1 + int(self.skeleton.proxyBone is not None)
        """
        index in ``self.skeleton.joints`` where joints start being displayed
        """

        self.boneRadius = boneRadius
        """
        radius of the joint meshes
        """

        self.color = color or copy.deepcopy(defaults.COLOR_PALETTE[0])
        """
        color of the whole figure, setting this attribute will apply
        :attr:`color` to every joint of the
        :class:`~pynimation.viewer.objects.StickFigure`
        """

        self.jointColors = jointColors or np.full(
            (len(skeleton.joints), 4), self.color
        )
        """
        RGBA colors of each joint :class:`~pynimation.viewer.objects.Capsule`
        """

        self._addCapsules(self.color)
        pos = self.skeleton.basePose[self.begin :].position
        # FIXME: broadcast in Rotation.rotationBetween
        y = Vec3(np.full(pos.shape, Vec3.y))
        self._defaultRotations = Transform.fromRotation(
            Rotation.rotationBetween(y, pos)
        )
        # self._defaultRotations = Transform.identityLike(self.skeleton.basePose)
        self._capsuleids = [c.id for c in self.children]

    def _addCapsules(self, color: List[Union[float, int]]) -> None:
        """

        Parameters
        ----------
        jointID: int :

        color: List[Union[float :

        int]] :


        Returns
        -------

        """

        n = self.skeleton.basePose[self.begin :].position.norm()

        self.children = tuple(
            [
                Capsule(
                    j.parent.id,
                    nn,
                    self.boneRadius,
                    False,
                    color,
                )
                for (j, nn) in zip(self.skeleton[self.begin :], n)
            ]
        )

    def animate(
        self,
        frame: "Animation",
    ) -> None:
        """
        Animate the figure with `frame`

        Parameters
        ----------
        frame:
            frame to animated the figure with
        """

        # FIXME: be able to get transform from joint for transform_graph
        self._capsuleids = [c.id for c in self.children]
        self._graph._transforms[self._capsuleids] = (
            frame.globals[self.skeleton.parents[self.begin :]]
            @ self._defaultRotations
        )
        # FIXME: sometimes unnecessary
        for capsule in self.children:
            capsule.color = self.jointColors[capsule.jointID]
