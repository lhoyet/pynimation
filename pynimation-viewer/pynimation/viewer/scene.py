from typing import List, Optional

from .animated import Animated
from .camera import Camera
from .light import LightManager
from .scene_graph import SceneGraph, SceneNode
from .viewport import Viewport


class Scene:
    """
    Representation of a 3D Scene, with its objects, cameras and lights

    Parameters
    ----------

    cameras: Optional[List[Camera]]:
        cameras of the scene, default will be created if :code:`None`
    root: Optional[SceneNode]:
        root of the scene :attr:`graph`, default will be created if ``None``
    light: Optional[LightManager]:
        light manager for the scene, default will be created if ``None``

    Attributes
    ----------

    root: Optional[SceneNode]:
        root of the scene :attr:`graph`
    graph: SceneGraph:
        scene graph of the scene, contains all the objects
    light: Optional[LightManager]:
        light manager for the scene
    """

    def __init__(
        self,
        cameras: Optional[List[Camera]] = None,
        root: Optional[SceneNode] = None,
        light: Optional[LightManager] = LightManager(),
    ):
        self.root = root or SceneNode("root", None)
        self.graph: SceneGraph = SceneGraph(self.root)
        if cameras is not None:
            self.addMultiple(cameras)
        self._mainCamera: Optional[Camera] = None

    @property
    def mainCamera(self) -> Optional[Camera]:
        """
        main camera of the scene, through which it will be displayed
        """
        return self._mainCamera

    @mainCamera.setter
    def mainCamera(self, newMainCamera: Optional[Camera]) -> None:
        self._mainCamera = newMainCamera
        if self._mainCamera is not None:
            if self._mainCamera not in self.graph.nodes:
                self.add(self._mainCamera)
            Viewport().mainCamera = self._mainCamera

    @property
    def cameras(self) -> List[Camera]:
        """
        cameras of the scene
        """
        return [node for node in self.graph.nodes if isinstance(node, Camera)]

    @cameras.setter
    def cameras(*args, **kwargs) -> None:
        raise AttributeError(
            "Cannot set cameras attributes, cameras should be added to the scene directly"
        )

    def addInLine(
        self,
        nodes: List["SceneNode"],
        distance: float = 1.0,
        parent: Optional["SceneNode"] = None,
    ) -> None:
        """Add objects to the scene as children of the root node
        , aligned and separated by distance

        Parameters
        ----------
        nodes:
            nodes to add
        distance:
            distance between them
        parent:
            if not ``None``, will be added to this node's children
        """

        for i, node in enumerate(nodes):
            node.transform.translate([distance * i, 0, 0])
            self.graph.add(node, parent=parent)

    def addMultiple(
        self, nodes: List["SceneNode"], parent: Optional["SceneNode"] = None
    ) -> None:
        """Add multiple nodes to scene

        Parameters
        ----------
        nodes:
            nodes to add to the scene
        parent:
            other node to add `nodes` as children to
        """
        for node in nodes:
            self.graph.add(node, parent=parent)

    def add(
        self, node: "SceneNode", parent: Optional["SceneNode"] = None
    ) -> None:
        """Add node to scene

        Parameters
        ----------
        node:
            node to add to the scene

        parent:
            other node to add `node` as a child to
        """
        self.graph.add(node, parent=parent)

    def remove(self, node: "SceneNode") -> None:
        """Remove node from scene

        Parameters
        ----------
        node:
            node to remove
        """
        self.graph.remove(node)

    def display(self):
        """
        Animate and display all nodes
        """
        for node in self.graph.nodes:
            if issubclass(node.__class__, Animated):
                node.animate()
        for node in self.graph.nodes:
            node.display()
