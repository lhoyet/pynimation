from typing import Optional, Tuple, TypeVar

from pynimation.common.transform_graph import TransformGraph, TransformNode

SceneNodeLike = TypeVar("SceneNodeLike", bound="SceneNode")


class SceneNode(TransformNode):
    """
    A node of :class:`~pynimation.viewer.scene_graph.SceneGraph`.
    """

    def __init__(
        self,
        name: str,
        parent: Optional[SceneNodeLike] = None,
        children: Optional[Tuple[SceneNodeLike, ...]] = None,
        graph: Optional["SceneGraph"] = None,
    ) -> None:
        super().__init__(name, parent, children, graph)
        self._hidden = False

    @property
    def hidden(self) -> bool:
        """
        Whether this node and all its children should be hidden or displayed
        """
        return self._hidden

    @hidden.setter
    def hidden(self, value: bool) -> None:
        if value != self._hidden:
            for child in self.children:
                child.hidden = value
            self._hidden = value

    def display(self):
        """ """
        if self.hidden:
            return
        # TODO display Transform axes


class SceneGraph(TransformGraph[SceneNodeLike]):
    """
    A graph of :class:`~pynimation.viewer.scene_graph.SceneNode`.
    """

    pass
