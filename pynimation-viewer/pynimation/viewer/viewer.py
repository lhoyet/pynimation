import contextlib
from enum import Enum
from functools import partial
from itertools import cycle
from typing import Any, Callable, Dict, List, Optional, Tuple, cast, Union

from pynimation.viewer.objects.reference_system import ReferenceSystem
from pynimation.viewer.stickfigure import StickFigure

import pynimation.common.data as data_
import pynimation.common.defaults as defaults
from pynimation.anim.animation import Animation

from .camera import Camera
from .character import Character
from .objects.plane import Plane
from .player import Player
from .scene import Scene, SceneNode
from .third_person_camera import ThirdPersonCamera
from .viewport import Viewport


def _partialsEq(p1: partial, p2: partial) -> bool:
    return (
        isinstance(p1, partial)
        and isinstance(p2, partial)
        and p1.args == p2.args
        and p1.func == p2.func
        and p1.keywords == p2.keywords
    )


class Viewer:
    """
    Display scenes and objects with a typical main loop implementation

    Parameters
    ----------
    size: Tuple[int, int]
        size of the viewport

    Attributes
    ----------
    viewport: Viewport:
        reference to viewport singleton

    player: Player:
        reference to player singleton

    colorPalette: np.ndarray:
        array of rgba vectors used to color Characters created with
        displayAnimations
    currentScene: Optional[Scene]
        current scene that is displayed
    """

    class CallbackTime(Enum):
        """
        Steps in the main loop when a callback can be called
        """

        AFTER_EVENTS = 0
        BEFORE_DISPLAY = 1
        AFTER_DISPLAY = 2

    class __AnimationsManagement:
        def __init__(self):
            self.animationsToModify: List[Animation] = []
            self.add: bool = False
            self.remove: bool = False

        def addAnimations(self, animations: List[Animation]) -> None:
            self.animationsToModify = animations
            self.add = True

        def removeAnimations(self, animations: List[Animation]) -> None:
            self.animationsToModify = animations
            self.remove = True

    def __init__(
        self,
        size: Tuple[int, int] = (
            defaults.VIEWPORT_WIDTH,
            defaults.VIEWPORT_HEIGHT,
        ),
        firstPerson: bool = False,
    ):
        camera: Camera = cast(
            Camera,
            Camera(*size) if firstPerson else ThirdPersonCamera(*size),
        )
        self.viewport = Viewport(*size, camera=camera)
        self.player = Player()
        self._callbacks: Dict[
            Viewer.CallbackTime, List[Tuple[int, partial]]
        ] = {}
        self.colorPalette = defaults.COLOR_PALETTE
        self.currentScene: Optional[Scene] = None
        self.__animationsManager = self.__AnimationsManagement()

    def callback(
        self,
        args: List[Any] = None,
        kwargs: Dict[str, Any] = None,
        when: CallbackTime = CallbackTime.BEFORE_DISPLAY,
        priority: int = 50,
    ) -> Callable:
        """
        Decorator that adds decorated function as a callback to the viewer's main loop

        Parameters
        ----------
        args:
            arguments of the callback
        kwargs:
            keyword arguments of the callback
        when:
            when in the main loop will the callback be called
        priority:
            callbacks will be called by priority order

        Examples
        --------
        >>> from pynimation.viewer.objects.cube import Cube
        >>> from pynimation.viewer.viewer import Viewer
        >>>
        >>> cube = Cube()
        >>>
        >>> v = Viewer()
        >>>
        >>> # define callback function and register it
        >>> # with Viewer.callback() decorator
        >>> @v.callback(args=[cube])
        >>> def moveCube(cube: Cube):
        >>>     cube.globalTransform.position.y += 0.001
        >>>     cube.globalTransform.position.y %= 2
        """

        def dec(func):
            self.addCallback(func, args, kwargs, when, priority)
            return func

        return dec

    def addCallback(
        self,
        callback: Callable,
        args: List[Any] = None,
        kwargs: Dict[str, Any] = None,
        when: CallbackTime = CallbackTime.BEFORE_DISPLAY,
        priority: int = 50,
    ) -> None:
        """
        Add a callback to the viewer's main loop

        Parameters
        ----------
        callback:
            function to add as callback
        args:
            arguments of the callback
        kwargs:
            keyword arguments of the callback
        when:
            when in the main loop will the callback be called
        priority:
            callbacks will be called by priority order
        """
        args = args or []
        kwargs = kwargs or {}
        partialCallback = partial(callback, *args, **kwargs)
        self.addCallbackPartial(partialCallback, when=when, priority=priority)

    def addCallbackPartial(
        self,
        partialCallback: partial,
        args: List[Any] = None,
        kwargs: Dict[str, Any] = None,
        when: CallbackTime = CallbackTime.BEFORE_DISPLAY,
        priority: int = 50,
    ) -> None:
        """
        Add a callback to the viewer's main loop as a partial object

        Parameters
        ----------
        partialCallback:
            partial to add as callback
        args:
            arguments of the callback
        kwargs:
            keyword arguments of the callback
        when:
            when in the main loop will the callback be called
        priority:
            callbacks will be called by priority order
        """
        args = args or []
        kwargs = kwargs or {}
        callbackEntry = (priority, partialCallback)
        if when not in self._callbacks:
            self._callbacks[when] = [callbackEntry]
        else:
            self._callbacks[when].append(callbackEntry)
            # sort by priority
            self._callbacks[when].sort(key=lambda x: x[0])

    def removeCallback(
        self,
        callback: Callable,
        when: CallbackTime = CallbackTime.BEFORE_DISPLAY,
        args: List[Any] = None,
        kwargs: Dict[str, Any] = None,
    ) -> None:
        """
        Remove a callback from the viewer's main loop

        Parameters
        ----------
        callback:
            function to remove from callbacks
        when:
            when in the main loop is the callback called
        args:
            arguments of the callback
        kwargs:
            keyword arguments of the callback
        """
        args = args or []
        kwargs = kwargs or {}
        partialCallback = partial(callback, args, kwargs)
        self.removeCallbackPartial(partialCallback, when)

    def removeCallbackPartial(
        self,
        partialCallback: partial,
        when: CallbackTime = CallbackTime.BEFORE_DISPLAY,
    ) -> None:
        """
        Remove a callback from the viewer's main loop

        Parameters
        ----------
        partialCallback:
            partial to remove from callbacks
        when:
            when in the main loop is the callback called
        """
        if when in self._callbacks:
            self._callbacks[when] = [
                entry
                for entry in self._callbacks[when]
                if not _partialsEq(entry[1], partialCallback)
            ]

    def _callCallbacks(self, when: CallbackTime) -> None:
        if when in self._callbacks:
            for callbackEntry in self._callbacks[when]:
                callbackEntry[1]()

    @staticmethod
    def defaultScene() -> Scene:
        """
        Contructs an scene with `defaultFloor`

        Returns
        -------
        Scene
            empty scene with `defaultFloor`
        """
        scene = Scene()
        scene.add(Viewer.defaultFloor())
        scene.add(ReferenceSystem(radius=0.01))
        return scene

    def __giveMeScene(self) -> Scene:
        return (
            self.defaultScene()
            if self.currentScene is None
            else self.currentScene
        )

    @staticmethod
    def defaultFloor() -> Plane:
        """
        Construct a :class:`~pynimation.viewer.objects.plane.Plane` with a
        repeating checker pattern

        Returns
        -------
        Plane
            plane object with checker pattern
        """
        return Plane(data_.getDataPath(defaults.FLOOR_TEXTURE))

    def addNode(self, node: SceneNode) -> None:
        """
        Add `node` to a default scene and display the scene

        Parameters
        ----------
        node:
            node to display
        """
        scene = self.__giveMeScene()
        scene.add(node)
        self.currentScene = scene

    def initialize(self) -> None:
        scene = self.defaultScene()
        self.currentScene = scene

    def enterContext(self) -> None:
        """
        Switch current OpenGL context to this viewer's context,
        for Qt applications where there is multiple OpenGL contexts

        This needs to be run before code that calls the OpenGL API outside the
        paintGL, initializeGL and resizeGL functions of ``QOpenGLWidget``

        Basically calls `QOpenGLWidget::makeCurrent() <https://doc.qt.io/qt-6/qopenglwidget.html#makeCurrent>`_

        See also
        --------
        :attr:`~pynimation.viewer.viewer.Viewer.exitContext`,
        :attr:`~pynimation.viewer.viewer.Viewer.inContext`
        """
        if hasattr(self, "_app"):
            viewerComp = self._app.window.viewerComponent  # type: ignore
            if hasattr(viewerComp, "makeCurrent"):
                viewerComp.makeCurrent()

    def exitContext(self) -> None:
        """
        Make this viewer's OpenGL context no longer current,
        for Qt applications where there is multiple OpenGL contexts

        This needs to be run after code that calls the OpenGL API outside the
        paintGL, initializeGL and resizeGL functions of ``QOpenGLWidget``, and
        after the viewer's context has been made current with
        :attr:`~pynimation.viewer.viewer.Viewer.enterContext`

        Basically calls
        `QOpenGLWidget::doneCurrent() <https://doc.qt.io/qt-6/qopenglwidget.html#doneCurrent>`_

        See also
        --------
        :attr:`~pynimation.viewer.viewer.Viewer.enterContext`,
        :attr:`~pynimation.viewer.viewer.Viewer.inContext`
        """
        if hasattr(self, "_app"):
            viewerComp = self._app.window.viewerComponent  # type: ignore
            if hasattr(viewerComp, "doneCurrent"):
                viewerComp.doneCurrent()

    @contextlib.contextmanager
    def inContext(self):
        """
        Context manager for running code while this viewer's OpenGL context is current,
        for Qt applications where there is multiple OpenGL contexts

        Code inside with context manager's block can call the OpenGL API outside the
        paintGL, initializeGL and resizeGL functions of ``QOpenGLWidget``

        Basically calls
        `QOpenGLWidget::makeCurrent() <https://doc.qt.io/qt-6/qopenglwidget.html#makeCurrent>`_
        , runs the code inside the block and calls
        `QOpenGLWidget::doneCurrent() <https://doc.qt.io/qt-6/qopenglwidget.html#doneCurrent>`_

        See also
        --------
        :attr:`~pynimation.viewer.viewer.Viewer.enterContext`,
        :attr:`~pynimation.viewer.viewer.Viewer.exitContext`,
        """
        try:
            self.enterContext()
            yield self
        finally:
            self.exitContext()

    def __updateCharacters(self, inLine=True) -> None:
        if self.__animationsManager.add:
            self.__animationsManager.add = False
            scene = self.__giveMeScene()
            animations = self.__animationsManager.animationsToModify
            characters = [
                Character(animation, [StickFigure(animation.skeleton, color)])
                for (animation, color) in zip(
                    animations, cycle(self.colorPalette)
                )
            ]
            if inLine:
                scene.addInLine(characters)
            else:
                scene.addMultiple(characters)
            self.currentScene = scene

        elif self.__animationsManager.remove:
            self.__animationsManager.remove = False
            animations = self.__animationsManager.animationsToModify
            scene = self.__giveMeScene()
            allCharacters = [
                n for n in scene.graph.nodes if isinstance(n, Character)
            ]

            for anim in animations:
                for char in allCharacters:
                    if char.animation is anim:
                        char.__del__()
                        scene.remove(char)

            self.currentScene = scene

    def addAnimations(self, animations: List[Animation]) -> None:
        self.__animationsManager.addAnimations(animations)

    def removeAnimations(self, animations: List[Animation]) -> None:
        self.__animationsManager.removeAnimations(animations)

    def __returnNodesList(self, args: Any, colors: Any) -> List["SceneNode"]:
        nodes: List["SceneNode"] = []
        for n in args:
            if isinstance(n, List):
                nodes = nodes + (self.__returnNodesList(n, colors))
            elif isinstance(n, SceneNode):
                nodes.append(n)
            else:
                nodes.append(
                    Character(n, [StickFigure(n.skeleton, next(colors))])
                )
        return nodes

    def display(
        self,
        *args: Union[
            "Animation",
            "SceneNode",
            List["SceneNode"],
            List["Animation"],
        ],
        inLine=True
    ) -> None:
        """
        Add Animation or SceneNode and run the application

        Parameters
        ----------
        args:
            Animations or SceneNodes to display
        """
        colors = cycle(self.colorPalette)
        nodes: List["SceneNode"] = self.__returnNodesList(args, colors)

        scene = self.__giveMeScene()
        if inLine:
            scene.addInLine(nodes)
        else:
            scene.addMultiple(nodes)

        self.currentScene = scene

        if hasattr(self, "_app"):
            getattr(self, "_app").run()

    def displayScene(self, scene: Optional[Scene]) -> None:
        """
        Replace the current scene and display it

        scene:
            the Scene to display
        """
        self.currentScene = scene

        if hasattr(self, "_app"):
            getattr(self, "_app").run()

    def processFrame(self) -> None:
        """
        Display `scene` with this viewer
        Uses a typical main loop implementation

        Parameters
        ----------
        scene:
            scene to display
        """
        self._callCallbacks(Viewer.CallbackTime.AFTER_EVENTS)

        self.viewport.updateCamera()

        self._callCallbacks(Viewer.CallbackTime.BEFORE_DISPLAY)

        self.viewport.display()

        if self.currentScene is not None:
            self.currentScene.display()

        self._callCallbacks(Viewer.CallbackTime.AFTER_DISPLAY)

        self.viewport.handleScreenshots()

        self.__updateCharacters()

        self.player.tick()
