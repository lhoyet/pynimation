from typing import List, Dict

from .ui_component import UIComponent


class UIWindow:
    def __init__(
        self,
        components: List[UIComponent],
        name: str = "PyNimation",
        width: int = 1280,
        height: int = 720,
    ):
        self.components = components
        self.name = name
        self.width = width
        self.height = height

    def initUpdateLoops(self) -> None:
        """
        Update all the components here
        """
        for component in self.components:
            component.initUpdateLoop()

    def bindingsDescription(self) -> Dict[str, Dict[str, str]]:
        """
        Get the bindings description for each components of the window

        Returns
        -------
        Dict[str, Dict[str, str]]

            the dictionnary that ossociate all the window components name
            with their bindings description dictionnary
        """
        return {
            type(component).__name__: component.bindings.description
            for component in self.components
            if len(component.bindings.description) > 0
        }
