import inspect

from typing import Any, Callable, Optional, Dict, List, Tuple
from functools import partial


def _partialsEq(p1: partial, p2: partial) -> bool:
    return (
        isinstance(p1, partial)
        and isinstance(p2, partial)
        and p1.args == p2.args
        and p1.func == p2.func
        and p1.keywords == p2.keywords
    )


class Action:
    """
    Contains a handler and its description

    Parameters
    ----------
    handler: partial
        action to realised
    description: str
        action description

    Attributes
    ----------
    handler: partial
        action to realised
    description: str
        action description
    """

    def __init__(
        self,
        handler: partial,
        description: str = None,
    ):
        self.handler = handler
        self.description = description or "".join(
            "Call the "
            + str(handler.func.__name__)
            + ", ".join(
                [
                    args[0] + "=" + str(args[1])
                    for args in zip(
                        inspect.signature(handler.func).parameters.keys(),
                        handler.args,
                    )
                ]
            )
        )

    def __eq__(self, other):
        return isinstance(other, Action) and _partialsEq(
            self.handler, other.handler
        )

    def __str__(self):
        return str((self.handler, self.description))

    def execute(self) -> None:
        """
        Run the handler of this action
        """
        self.handler()


# Action collection
class ActionManager:
    """
    Manage a dictionnary of :class:`~pynimation.viewer.ui.action.Action`
    objects. An Action is an object containing a
    function and its description. The interest of this class is to manage
    the errors caused by a misuse of the actions dictionnary.

    Parameters
    ----------
    actions: Dict[str, Action]
        dictionnary containing the initial actions
    """

    def __init__(self, actions: Dict[str, Action]):
        self.__actions: Dict[str, Action] = actions

    @property
    def actions(self) -> Tuple[Tuple[str, Action], ...]:
        """
        Tuple of tuples with the action name, and the action object
        """
        return tuple(self.__actions.items())

    def get(self, name: str) -> Action:
        """
        Return the action requested if this action exists inside the dictionnary

        Parameters
        ----------
        name:
            action name

        Returns
        -------
        Action
            contain a partial and its description
        """
        action: Optional[Action] = self.__actions.get(name)

        if action is None:
            raise ValueError('"', name, "\" action doesn't exists.")

        return action

    def add(
        self,
        name: str,
        handler: Callable,
        args: List[Any] = None,
        kwargs: Dict[str, Any] = None,
        description: str = None,
    ) -> None:
        """
        Add an action to the dictionnary if the action name does not already
        exist

        Parameters
        ----------
        name:
            action name
        handler:
            action function
        args:
            arguments of the action
        kwargs:
            keyword arguments of the action
        description:
            action description
        """
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}

        handler = partial(handler, *args, **kwargs)
        action: Optional[Action] = self.__actions.get(name)

        if action is None:
            self.__actions[name] = Action(handler, description)
        else:
            raise ValueError('"', name, '" action already exists.')

    def remove(self, name: str) -> None:
        action: Optional[Action] = self.__actions.get(name)

        if action is not None:
            del self.__actions[name]
        else:
            raise ValueError('"', name, "\" action doesn't exists.")

    def modify(
        self,
        name: str,
        handler: Callable,
        args: List[Any] = None,
        kwargs: Dict[str, Any] = None,
        description: str = None,
    ) -> None:
        """
        Modify an action if it's inside the dictionnary

        Parameters
        ----------
        name:
            action name
        handler:
            action function
        args:
            arguments of the action
        kwargs:
            keyword arguments of the action
        description:
            action description
        """
        self.remove(name)
        self.add(name, handler, args, kwargs, description)
