from functools import partial

from pynimation.viewer.ui.action import Action


def getDefaultActions(self):
    return {
        "screenshot": Action(
            partial(self.viewport.triggerScreenshot),
            "Capture a screenshot of the viewport",
        ),
        "recording": Action(
            partial(self.viewport.startStopRecording),
            "Toggle recording",
        ),
        "cameraRotationTrigger": Action(
            partial(self.viewport.setCameraIsRotating, True),
            "Move Camera",
        ),
        "cameraRotationRelease": Action(
            partial(self.viewport.setCameraIsRotating, False),
            "Move Camera",
        ),
        "cameraRotation": Action(
            partial(
                self.viewport.rotateCameraWithMouse,
                self.mouse,
            ),
            "Move Camera",
        ),
        "cameraZoom": Action(
            partial(self.viewport.zoomCamera, 1),
            "Zoom Camera (third person only)",
        ),
        "cameraDezoom": Action(
            partial(self.viewport.zoomCamera, -1),
            "Dezoom Camera (third person only)",
        ),
        "cameraThirdPerson": Action(
            partial(self.viewport.toggleThirdPerson),
            "Toggle between first and third person camera",
        ),
        "pauseplay": Action(
            partial(self.player.togglePause),
            "Play and pause the player",
        ),
        "nextFrame": Action(
            partial(self.player.nextFrame),
            "Display the next frame of all animations",
        ),
        "previousFrame": Action(
            partial(self.player.previousFrame),
            "Display the previous frame of all animations",
        ),
        "increaseSpeed": Action(
            partial(self.player.varySpeed, 0.01),
            "Increase the speed of the player",
        ),
        "decreaseSpeed": Action(
            partial(self.player.varySpeed, -0.01),
            "Decrease the speed of the player",
        ),
        "resetSpeed": Action(
            partial(self.player.resetSpeed),
            "reset the speed of the player to 1.0",
        ),
        "right": Action(
            partial(self.viewport.setCameraTranslationalDirection, 0, 1),
            "Move right",
        ),
        "left": Action(
            partial(self.viewport.setCameraTranslationalDirection, 0, -1),
            "Move left",
        ),
        "forward": Action(
            partial(self.viewport.setCameraTranslationalDirection, 2, -1),
            "Move forward",
        ),
        "backward": Action(
            partial(self.viewport.setCameraTranslationalDirection, 2, 1),
            "Move backward",
        ),
        "up": Action(
            partial(self.viewport.setCameraTranslationalDirection, 1, 1),
            "Move up",
        ),
        "down": Action(
            partial(self.viewport.setCameraTranslationalDirection, 1, -1),
            "Move down",
        ),
        "stopMoveAxisX": Action(
            partial(self.viewport.setCameraTranslationalDirection, 0, 0),
            "Move down",
        ),
        "stopMoveAxisY": Action(
            partial(self.viewport.setCameraTranslationalDirection, 1, 0),
            "Move down",
        ),
        "stopMoveAxisZ": Action(
            partial(self.viewport.setCameraTranslationalDirection, 2, 0),
            "Move down",
        ),
    }
