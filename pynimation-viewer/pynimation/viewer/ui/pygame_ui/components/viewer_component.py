import pygame as pg

from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.pygame_ui.pygame_toolbox import PynimationTools
from pynimation.viewer.ui.event import (
    KeyPressEvent,
    KeyHoldEvent,
    KeyReleaseEvent,
    MouseClickEvent,
    MouseClickReleaseEvent,
    MouseMoveEvent,
    MouseWheelEvent,
    GeneralEvent,
)


class ViewerComponent(UIComponent):
    """
    Base component of the interface.
    It permits to display characters animations in a 3D scene.
    """

    def __init__(self):
        UIComponent.__init__(self, PynimationTools())

    # UIComponent implementation
    def bindingsInit(self) -> None:
        self.bindings.bind(KeyPressEvent(pg.K_s, name="s"), "screenshot")
        self.bindings.bind(KeyPressEvent(pg.K_r, name="r"), "recording")
        self.bindings.bind(
            MouseClickEvent(3, name="right"),
            "cameraRotationTrigger",
        )
        self.bindings.bind(MouseClickReleaseEvent(3), "cameraRotationRelease")
        self.bindings.bind(MouseMoveEvent(), "cameraRotation")
        self.bindings.bind(MouseWheelEvent(1.0), "cameraZoom")
        self.bindings.bind(MouseWheelEvent(-1.0), "cameraDezoom")
        self.bindings.bind(
            KeyPressEvent(pg.K_c, name="c"), "cameraThirdPerson"
        )
        self.bindings.bind(
            KeyPressEvent(pg.K_SPACE, name="space"), "pauseplay"
        )
        self.bindings.bind(KeyHoldEvent("l"), "nextFrame")
        self.bindings.bind(KeyHoldEvent("h"), "previousFrame")
        self.bindings.bind(KeyHoldEvent("k"), "increaseSpeed")
        self.bindings.bind(KeyHoldEvent("j"), "decreaseSpeed")
        self.bindings.bind(KeyPressEvent(pg.K_i, name="i"), "resetSpeed")
        self.bindings.bind(KeyPressEvent(pg.K_RIGHT, name="right"), "right")
        self.bindings.bind(KeyPressEvent(pg.K_LEFT, name="left"), "left")
        self.bindings.bind(KeyPressEvent(pg.K_UP, name="up"), "forward")
        self.bindings.bind(KeyPressEvent(pg.K_DOWN, name="down"), "backward")
        self.bindings.bind(KeyPressEvent(pg.K_PAGEUP, name="page up"), "up")
        self.bindings.bind(
            KeyPressEvent(pg.K_PAGEDOWN, name="page down"), "down"
        )
        self.bindings.bind(KeyReleaseEvent(pg.K_RIGHT), "stopMoveAxisX")
        self.bindings.bind(KeyReleaseEvent(pg.K_LEFT), "stopMoveAxisX")
        self.bindings.bind(KeyReleaseEvent(pg.K_UP), "stopMoveAxisZ")
        self.bindings.bind(KeyReleaseEvent(pg.K_DOWN), "stopMoveAxisZ")
        self.bindings.bind(KeyReleaseEvent(pg.K_PAGEUP), "stopMoveAxisY")
        self.bindings.bind(KeyReleaseEvent(pg.K_PAGEDOWN), "stopMoveAxisY")
        self.bindings.bind(KeyPressEvent(pg.K_q, name="q"), "quit")
        self.bindings.bind(GeneralEvent(pg.QUIT), "quit")

    def __updateComponent(self) -> None:
        while 1:
            while pg.event.peek():
                event = pg.event.poll()
                self.bindings.handleEvent(self.tools.eventTranslator(event))

            self.tools.viewer.processFrame()
            pg.display.flip()

    def initUpdateLoop(self) -> None:
        self.tools.viewport.initialize()
        self.__updateComponent()
