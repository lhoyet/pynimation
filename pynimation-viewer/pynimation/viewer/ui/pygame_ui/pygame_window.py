import os
import pygame as pg

from pynimation.viewer.ui.ui_window import UIWindow
from pynimation.viewer.ui.pygame_ui.components.viewer_component import (
    ViewerComponent,
)


class MainWindow(UIWindow):
    def __init__(self):
        self.viewerComponent = ViewerComponent()
        UIWindow.__init__(self, [self.viewerComponent])

    def show(self) -> None:
        if not pg.get_init():
            pg.init()

        pg.display.set_caption(self.name)
        os.environ["SDL_VIDEO_CENTERED"] = "1"
        SCREEN = pg.display.set_mode(
            (self.width, self.height), pg.HWSURFACE | pg.OPENGL | pg.DOUBLEBUF
        )
        SCREEN.__hash__()
