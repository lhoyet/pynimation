import sys
import pygame as pg

from pynimation.viewer.ui.application import Application


from .pygame_window import MainWindow
from .pygame_toolbox import PynimationTools


class PynimationApp(Application):
    def __init__(self):
        Application.__init__(self, PynimationTools())
        self.tools.viewer._app = self

    def actionsInit(self) -> None:
        self.tools.actions.add("quit", self.quit)

    def windowInit(self) -> None:
        self.window = MainWindow()
        self.descriptionBindings = self.window.bindingsDescription()
        self.window.show()

    def run(self) -> None:
        self.window.initUpdateLoops()

    def quit(self) -> None:
        pg.quit()
        self.tools.viewport.quit()


sys.modules["pynimation.viewer.ui"].PynimationApp = PynimationApp  # type: ignore
