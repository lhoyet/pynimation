from typing import Any

import pygame as pg

from pynimation.common.vec import Vec2
from pynimation.viewer.ui.event import (
    Event,
    KeyPressEvent,
    KeyHoldEvent,
    KeyReleaseEvent,
    MouseClickEvent,
    MouseClickReleaseEvent,
    MouseMoveEvent,
    MouseWheelEvent,
    GeneralEvent,
)
from pynimation.viewer.ui.toolbox import ToolBox


def modifiersChecker(event: Any) -> int:
    if event.mod != pg.KMOD_CAPS and event.mod != pg.KMOD_NUM:
        return event.mod
    else:
        return 0


class PynimationTools(ToolBox):
    def initState(self) -> None:
        pass

    def eventTranslator(self, event: Any) -> Event:
        if event.type == pg.KEYDOWN:
            return KeyPressEvent(event.key, modifiersChecker(event))

        elif event.type == pg.TEXTINPUT:
            return KeyHoldEvent(event.text)

        elif event.type == pg.KEYUP:
            return KeyReleaseEvent(event.key, modifiersChecker(event))

        elif event.type == pg.MOUSEBUTTONDOWN:
            return MouseClickEvent(event.button)

        elif event.type == pg.MOUSEBUTTONUP:
            return MouseClickReleaseEvent(event.button)

        elif event.type == pg.MOUSEMOTION:
            self.mouse.pos = Vec2([event.pos[0], event.pos[1]], dtype=int)
            return MouseMoveEvent()

        elif event.type == pg.MOUSEWHEEL:
            return MouseWheelEvent(event.y)

        elif event.type == pg.QUIT:
            return GeneralEvent(pg.QUIT)
