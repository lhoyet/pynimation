import abc

from pynimation.common.singleton import _SingletonAbstract
from pynimation.viewer.viewer import Viewer

from .toolbox import ToolBox


# One object
# Facade pattern for the entire application
class Application(metaclass=_SingletonAbstract):
    def __init__(self, tools: ToolBox):
        self.tools = tools
        self.actionsInit()
        self.windowInit()

    @property
    def viewer(self) -> Viewer:
        return self.tools.viewer

    @abc.abstractmethod
    def actionsInit(self) -> None:
        """
        Initialize the supplementary actions necessary
        """
        pass

    @abc.abstractmethod
    def windowInit(self) -> None:
        """
        Initialize the main window
        """
        pass

    @abc.abstractmethod
    def run(self) -> None:
        """
        Run pynimation with an UI. Global class wrapper.
        """
        pass

    @abc.abstractmethod
    def quit(self) -> None:
        """
        Quit pynimation with an UI.
        """
        pass
