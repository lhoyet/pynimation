from typing import Dict, Optional, Tuple, List

from pynimation.viewer.ui.action import Action
from pynimation.viewer.ui.action import ActionManager


# Events type
class Event:
    _id: int

    def __hash__(self):
        return hash((self.id, *self.__dict__.values()))

    def __eq__(self, other):
        return hash(self) == hash(other)

    @property
    def id(self) -> int:
        return self.__class__._id


class KeyPressEvent(Event):
    """
    Called when a keyboard key is pressed

    Parameters
    ----------
    key: int
        the id of the key
    mod: int
        the id of the modkey

    Attributes
    ----------
    key: int
        the id of the key
    mod: int
        the id of the modkey
    """

    _id = 0

    def __hash__(self):
        return hash((self.id, self.key, self.mod))

    def __init__(self, key: int = 0, mod: int = 0, name: Optional[str] = None):
        self.key: int = key
        self.mod: int = mod
        # set description
        triggerName = name if name is not None else self.key
        self.description: str = 'Press the "%s" key' % triggerName


class KeyHoldEvent(Event):
    """
    Called when a keyboard key is held
    (inspired by the pygame.TEXTINPUT event)

    Parameters
    ----------
    keys : str
        the keys currently held

    Attributes
    ----------
    keys : str
        the keys currently held
    """

    _id = 1

    def __hash__(self):
        return hash((self.id, self.keys))

    def __init__(self, keys: Optional[str] = None, name: Optional[str] = None):
        self.keys: Optional[str] = keys
        # set description
        triggerName = name if name is not None else self.keys
        self.description: str = 'Hold the "%s" key' % triggerName


class KeyReleaseEvent(Event):
    """
    Called when a keyboard key is released

    Parameters
    ----------
    key: int
        the id of the key
    mod: int
        the id of the modkey

    Attributes
    ----------
    key: int
        the id of the key
    mod: int
        the id of the modkey
    """

    _id = 2

    def __init__(self, key: int = 0, mod: int = 0):
        self.key: int = key
        self.mod: int = mod


class MouseClickEvent(Event):
    """
    Called when a mouse button is pressed

    Parameters
    ----------
    button: int
        the id of the mouse button
    mod: int
        the id of the modkey (not supported by all library)

    Attributes
    ----------
    button: int
        the id of the mouse button
    mod: int
        the id of the modkey (not supported by all library)
    """

    _id = 3

    def __hash__(self):
        return hash((self.id, self.button, self.mod))

    def __init__(
        self, button: int = 0, name: Optional[str] = None, mod: int = 0
    ):
        self.button: int = button
        self.mod: int = mod
        # set description
        triggerName = name if name is not None else self.button
        self.description: str = (
            "Press the %s button of the mouse" % triggerName
        )


class MouseClickReleaseEvent(Event):
    """
    Called when a mouse button is released

    Parameters
    ----------
    button: int
        the id of the mouse button
    mod: int
        the id of the modkey (not supported by all library)

    Attributes
    ----------
    button: int
        the id of the mouse button
    mod: int
        the id of the modkey (not supported by all library)
    """

    _id = 4

    def __init__(self, button: int = 0, mod: int = 0):
        self.button: int = button
        self.mod: int = mod


class MouseMoveEvent(Event):
    """
    Called when the mouse moves
    """

    _id = 5

    def __init__(self):
        # set description
        self.description: str = "Move the mouse"


class MouseWheelEvent(Event):
    """
    Called when the mouse wheel is used

    Parameters
    ----------
    direction: int
        direction of the mouse wheel
    mod: int
        the id of the modkey (not supported by all library)

    Attributes
    ----------
    direction: int
        direction of the mouse wheel
    mod: int
        the id of the modkey (not supported by all library)
    """

    _id = 6

    def __init__(self, direction: int = 0, mod: int = 0):
        self.direction: int = direction
        self.mod: int = mod
        # set description
        self.description: str = "Use the mouse wheel"


class GeneralEvent(Event):
    """
    Called for other type of event

    Parameters
    ----------
    type: Any
        type of the event

    Attributes
    ----------
    type: Any
        type of the event
    """

    _id = 7

    def __init__(self, eventType):
        self.type = eventType


# Bind some Events with some Actions
class BindingManager:
    """
    Manage the bindings. Bind an action to an event. This
    class is agnostic regarding the library that handle the events

    Parameters
    ----------
    actionManager: ActionManager
        the class containing all the actions possible
    """

    def __init__(self, actionManager: ActionManager):
        self.__actionManager: ActionManager = actionManager
        self.__bindings: Dict[Event, List[Action]] = dict()

    @property
    def bindings(self) -> Tuple[Tuple[Event, List[Action]], ...]:
        """
        Returns
        -------
        Tuple[Tuple[Event, List[Action]], ...]:

            Tuple of tuples with the event and the actions associated with it
        """
        return tuple(self.__bindings.items())

    def __getattr(self, a, b):
        try:
            return getattr(a, b)
        except AttributeError:
            return None

    @property
    def description(self) -> Dict[str, str]:
        """
        Returns
        -------
        Dict[str, str]:

            The dictionnary of all the actions and events description
        """
        actionEvents: Dict[str, str] = dict()

        for event, actions in self.__bindings.items():
            for action in actions:
                currentDescription: Optional[str] = actionEvents.get(
                    action.description
                )
                newDescription: Optional[str] = self.__getattr(
                    event, "description"
                )

                if newDescription is not None:
                    if currentDescription is None:
                        actionEvents[action.description] = newDescription
                    else:
                        actionEvents[action.description] = "%s, %s" % (
                            currentDescription,
                            newDescription,
                        )

        return actionEvents

    def bind(self, event: Event, name: str) -> None:
        """
        Bind an event to a single action

        Parameters
        ----------
        event:
            specific event
        name:
            action name
        """
        actions: Optional[List[Action]] = self.__bindings.get(event)

        if actions is None:
            self.__bindings[event] = [self.__actionManager.get(name)]
        else:
            self.__bindings[event].append(self.__actionManager.get(name))

    def bindMultipleAction(self, event: Event, names: List[str]) -> None:
        """
        Bind an event to a list of actions

        Parameters
        ----------
        event:
            specific event
        name:
            list containing the actions names
        """
        for name in names:
            self.bind(event, name)

    def unbind(self, event: Event) -> None:
        """
        Remove an existing bind on an action.

        Parameters
        ----------
        event:
            key to remove from the dictionnary
        """

        actions: Optional[List[Action]] = self.__bindings.get(event)

        if actions is None:
            raise ValueError("This action is not bind.")
        else:
            del self.__bindings[event]

    def rebind(self, event: Event, newEvent: Event) -> None:
        """
        Replace the old event key by a new one

        Parameters
        ----------
        event:
            key to remove from the dictionnary
        newEvent:
            key to replace the old one
        """

        actions: Optional[List[Action]] = self.__bindings.get(event)

        if actions is not None:
            self.unbind(event)
            self.__bindings[newEvent] = []
            self.__bindings[newEvent].extend(actions)
        else:
            raise ValueError("This binding doesn't contains any action.")

    def handleEvent(self, event: Event) -> None:
        """
        execute all the actions binded to an event

        Parameters
        ----------
        event:
            key associated with the action to execute
        """
        actions: Optional[List[Action]] = self.__bindings.get(event)

        if actions is not None:
            for action in actions:
                action.execute()
