import abc

from typing import Any

from pynimation.common.singleton import _SingletonAbstract
from pynimation.viewer.mouse import MouseManager
from pynimation.viewer.viewer import Viewer
from pynimation.viewer.viewport import Viewport
from pynimation.viewer.player import Player
from pynimation.viewer.ui.action import ActionManager
from pynimation.viewer.ui.event import Event

from .default_actions import getDefaultActions


class ToolBox(metaclass=_SingletonAbstract):
    def __init__(self):
        self.viewer: Viewer = Viewer()
        self.mouse: MouseManager = MouseManager()
        self.viewport: Viewport = self.viewer.viewport
        self.player: Player = self.viewer.player
        self.actions: ActionManager = ActionManager(getDefaultActions(self))
        self.initState()

    @abc.abstractmethod
    def initState(self) -> None:
        pass

    @staticmethod
    @abc.abstractmethod
    def eventTranslator(self, event: Any) -> Event:
        """
        Translate an event to a Pynimation event

        Returns
        -------
        Event
            a universal Event type
        """
        pass
