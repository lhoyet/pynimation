from qtpy.QtWidgets import QApplication

from pynimation.viewer.ui.application import Application

from .qt_window import MainWindow
from .qt_toolbox import PynimationTools


class PynimationApp(Application):
    def __init__(self):
        Application.__init__(self, PynimationTools())
        self.tools.viewer._app = self

    def actionsInit(self) -> None:
        pass

    def windowInit(self) -> None:
        self.app = QApplication([])
        self.window = MainWindow()
        # initialize window
        self.window.show()
        # set the current OpenGL context
        self.window.viewerComponent.makeCurrent()

    def run(self) -> None:
        self.window.initUpdateLoops()
        self.app.exec()

    def quit(self) -> None:
        pass
