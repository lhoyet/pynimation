from qtpy.QtCore import Qt
from qtpy.QtWidgets import (
    QApplication,
    QMainWindow,
    QWidget,
    QVBoxLayout,
    QSplitter,
    QTabWidget,
)

import pynimation.common.data as data_
from pynimation.viewer.ui.qt_ui.components.viewer_widget import ViewerWidget
from pynimation.viewer.ui.qt_ui.components.tree_view_widget import (
    TreeViewWidget,
)
from pynimation.viewer.ui.qt_ui.components.joint_list_widget import (
    JointListWidget,
)
from pynimation.viewer.ui.qt_ui.components.player_widget import (
    PlayerWidget,
)
from pynimation.viewer.ui.qt_ui.components.file_menu_widget import (
    FileMenuWidget,
)
from pynimation.viewer.ui.qt_ui.components.console_widget import (
    ConsoleWidget,
)
from pynimation.viewer.ui.qt_ui.components.graph_widget import (
    GraphWidget,
)
from pynimation.viewer.ui.ui_window import UIWindow


class MainWindow(UIWindow, QMainWindow):
    def __init__(self):
        # widgets
        self.viewerComponent = ViewerWidget()
        self.playerComponent = PlayerWidget()
        self.fileMenuComponent = FileMenuWidget()
        self.treeViewComponent = TreeViewWidget()
        self.jointListComponent = JointListWidget()
        self.consoleComponent = ConsoleWidget()
        self.graphComponent = GraphWidget(
            self.treeViewComponent.itemSelectionChanged
        )

        # init
        UIWindow.__init__(
            self,
            [
                self.viewerComponent,
                self.playerComponent,
                self.fileMenuComponent,
                self.treeViewComponent,
                self.jointListComponent,
                self.consoleComponent,
                self.graphComponent,
            ],
        )
        QMainWindow.__init__(self)

        # QMainWindow init
        screen = QApplication.primaryScreen()
        # self.setMinimumSize(1280, 720)
        self.resize(screen.size())
        self.setWindowTitle(self.name)

        # init the UI
        self.ui = QWidget()

        # set the UI
        self.ui.setLayout(self.__uiLayout())
        self.ui.setStyleSheet(self.__uiStyle())
        self.setCentralWidget(self.ui)
        self.__resizeComponents()

        # choose focus
        self.viewerComponent.setFocus(True)

        # name object
        self.ui.setObjectName("ui")

    def __resizeComponents(self) -> None:
        constWidth = 200
        self.right.resize(constWidth, 600)
        self.jointListComponent.resize(constWidth, 600)
        constHeight = 30
        self.playerComponent.resize(self.playerComponent.width(), constHeight)
        self.consoleComponent.resize(
            self.consoleComponent.width(), constHeight
        )
        self.graphComponent.resize(self.graphComponent.width(), constHeight)
        self.bottomTab.resize(self.bottomTab.width(), constHeight)

    def __uiLayout(self) -> QVBoxLayout:
        # layout
        uiLayout = QVBoxLayout()
        rightLayout = QVBoxLayout()
        # setup layout
        rightLayout.setSpacing(0)
        rightLayout.setContentsMargins(0, 0, 0, 0)
        # widgets
        self.right = QWidget()
        self.right.setLayout(rightLayout)
        self.middle = QSplitter(Qt.Orientation.Horizontal)
        self.bottom = QSplitter(Qt.Orientation.Vertical)
        self.bottomTab = QTabWidget()
        # setup layout
        rightLayout.addWidget(self.fileMenuComponent)
        rightLayout.addWidget(self.treeViewComponent)
        self.middle.addWidget(self.right)
        self.middle.addWidget(self.viewerComponent)
        self.middle.addWidget(self.jointListComponent)
        self.bottom.addWidget(self.middle)
        self.bottomTab.addTab(self.playerComponent, "player")
        self.bottomTab.addTab(self.consoleComponent, "console")
        self.bottomTab.addTab(self.graphComponent, "graph")
        self.bottom.addWidget(self.bottomTab)
        # merge and return layout
        self.bottomTab.resize(self.bottom.width(), 100)
        self.middle.resize(self.middle.width(), 200)
        uiLayout.addWidget(self.bottom)
        return uiLayout

    def closeEvent(self, event) -> None:
        QApplication.closeAllWindows()
        event.accept()

    def __uiStyle(self) -> str:
        icons_folder = (
            "%s/pynimation/pynimation-viewer/pynimation/viewer/ui/qt_ui/icons/"
            % (data_.getDataPath("").split("\\pynimation")[0])
        )
        icons_folder = icons_folder.replace("\\", "/")
        return """
                QWidget {
                    border:0;
                    color:white;
                    background:#3C3C3C;
                }

                QWidget#ui {
                    background:#282828;
                }

                QSplitter::handle {
                    background:#282828;
                }

                QPushButton {
                    background:#282828;
                    padding:5px;
                }

                QLineEdit {
                    width:30;
                    background:#686868;
                    padding:5px;
                }

                QTabWidget::pane {
                    border:1px solid #585858;
                }

                QTabBar::tab::selected {
                    background:#585858;
                }

                QLabel {
                    font-size:15px;
                }

                QTabBar::tab {
                    background:#282828;
                }

                QComboBox {
                    width:30;
                    background:#686868;
                    padding:5px;
                    font-size:15px;
                }

                QTreeView::branch:open:has-children:!has-siblings{image:url(%sarrow_rotate.png)}
                QTreeView::branch:closed:has-children:!has-siblings{image:url(%sarrow.png)}
                QTreeView::branch:open:has-children{image:url(%sarrow_rotate.png)}
                QTreeView::branch:closed:has-children{image:url(%sarrow.png)}
                QTreeView::branch:open:{image:url(%sarrow_rotate.png)}
                QTreeView::branch:closed:{image:url(%sarrow.png)}
            """ % (
            icons_folder,
            icons_folder,
            icons_folder,
            icons_folder,
            icons_folder,
            icons_folder,
        )
