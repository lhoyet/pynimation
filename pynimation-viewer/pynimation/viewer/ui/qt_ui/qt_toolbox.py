from typing import Any, List, Optional

from qtpy.QtCore import Qt, QEvent

from pynimation.anim.animation import Animation
from pynimation.viewer.ui.event import (
    Event,
    KeyPressEvent,
    KeyHoldEvent,
    KeyReleaseEvent,
    MouseClickEvent,
    MouseClickReleaseEvent,
    MouseMoveEvent,
    MouseWheelEvent,
)
from pynimation.viewer.ui.toolbox import ToolBox


def modifiersChecker(event: Any) -> Any:
    if event.modifiers() == Qt.KeyboardModifier.NoModifier:
        return 0
    else:
        return event.modifiers()


class JointData:
    def __init__(
        self, animation: Animation, animId: int, jointId: int, name: str
    ):
        self.animation: Animation = animation
        self.animId: int = animId
        self.id: int = jointId
        self.name: str = name


class PynimationTools(ToolBox):
    def initState(self) -> None:
        # joints data
        self.selectedJoints: List[JointData] = []
        # animations data
        self.preciseAnimation: Optional[Animation] = None
        self.largestAnimation: Optional[Animation] = None

    def eventTranslator(self, event: Any) -> Event:
        if event.type() == QEvent.Type.KeyPress:
            if not event.isAutoRepeat():
                return KeyPressEvent(event.key(), modifiersChecker(event))
            else:
                return KeyHoldEvent(str(event.key()))

        elif event.type() == QEvent.Type.KeyRelease:
            if not event.isAutoRepeat():
                return KeyReleaseEvent(event.key(), modifiersChecker(event))

        elif event.type() == QEvent.Type.MouseButtonPress:
            return MouseClickEvent(event.button(), modifiersChecker(event))

        elif event.type() == QEvent.Type.MouseButtonRelease:
            return MouseClickReleaseEvent(
                event.button(), modifiersChecker(event)
            )

        elif event.type() == QEvent.Type.MouseMove:
            return MouseMoveEvent()

        elif event.type() == QEvent.Type.Wheel:
            k = abs(event.angleDelta().y())

            if k == 0:
                k = 1

            return MouseWheelEvent(
                event.angleDelta().y() / k,
                modifiersChecker(event),
            )
