from typing import Any, List

from qtpy.QtCore import Qt, QTimer
from qtpy.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QScrollArea,
)

from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.qt_ui.qt_toolbox import (
    PynimationTools,
    JointData,
)
from pynimation.viewer.ui.qt_ui.components.joint_data_widget import (
    JointDataWidget,
)
from pynimation.viewer.ui.qt_ui.components.common.detachable_widget import (
    DatachableWidget,
)


class JointListWidget(UIComponent, QWidget):
    """
    Show the list of JointDataWidget components.

    Attributes
    ----------
    joints: List[JointData]
        contains all the joints to add to the widget list
    """

    def __init__(self):
        QWidget.__init__(self)
        UIComponent.__init__(self, PynimationTools())

        self.__joints: List[JointData] = []
        self.__jointWidgetList: List[DatachableWidget] = []
        self.__scroller = QScrollArea()
        self.__scroller.setWidgetResizable(True)
        self.__listLayout = QVBoxLayout()
        self.__listWidget = QWidget()

        self.__listLayout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.__listLayout.setContentsMargins(0, 0, 0, 0)
        self.__listWidget.setLayout(self.__listLayout)
        self.__scroller.setWidget(self.__listWidget)
        layout = QVBoxLayout()
        layout.addWidget(self.__scroller)
        self.setLayout(layout)

        self.setMaximumWidth(800)
        self.setMinimumWidth(0)

    def __clearListWidget(self):
        for i in reversed(range(self.__listLayout.count())):
            self.__listLayout.itemAt(i).widget().setParent(None)

    def __initUI(self):
        tempJointWidgetList = self.__jointWidgetList.copy()

        # remove missing joints
        for jointWidget in tempJointWidgetList:
            if jointWidget.parent() is not None and not (
                jointWidget.childrenWidget.joint in self.__andJoints
            ):
                self.__jointWidgetList.remove(jointWidget)
                jointWidget.setParent(None)
                jointWidget.deleteLater()
            elif jointWidget.parent() is None and jointWidget.isClosed:
                self.__jointWidgetList.remove(jointWidget)

        # add new joints
        for joint in self.__notJoints:
            jointWidget = JointDataWidget(joint)
            detachable = DatachableWidget(
                jointWidget,
                self.__listLayout,
            )
            detachable.setMaximumHeight(750)
            detachable.setMinimumWidth(150)
            self.__jointWidgetList.append(detachable)
            self.__listLayout.addWidget(self.__jointWidgetList[-1])

        # set the widget visibility
        for i in range(self.__listLayout.count()):
            jointWidget = self.__listLayout.itemAt(i).widget().childrenWidget
            if i == 0:
                jointWidget.canCollapse = False
            else:
                jointWidget.canCollapse = True
            jointWidget.changeVisibility()

    def __updateWidget(self) -> None:
        if self.__joints != self.tools.selectedJoints:
            # get the XOR list between the current selected joints and the old one
            self.__notJoints = [
                n for n in self.tools.selectedJoints if n not in self.__joints
            ]
            # get the AND list between the current selected joints and the old one
            self.__andJoints = [
                n for n in self.tools.selectedJoints if n in self.__joints
            ]
            self.__initUI()
            self.__joints = self.tools.selectedJoints.copy()

    def initUpdateLoop(self) -> None:
        self.timer = QTimer()
        signal: Any = self.timer.timeout
        signal.connect(self.__updateWidget)
        self.timer.start(16)
