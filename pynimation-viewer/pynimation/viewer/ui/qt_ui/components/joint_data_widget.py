import numpy as np
from typing import Any
from operator import attrgetter

from qtpy.QtCore import Qt, QTimer
from qtpy.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QTabWidget,
    QPushButton,
)
from qtpy.QtGui import QIcon

import pynimation.common.data as data_
from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.qt_ui.qt_toolbox import (
    PynimationTools,
    JointData,
)
from pynimation.viewer.ui.qt_ui.components.matrix_input_widget import (
    MatrixInputWidget,
)


class JointDataWidget(UIComponent, QWidget):
    """
    Show all the important data concerning a joint.

    Parameters
    ----------
    joint: JointData
        all the data concerning the current joint

    Attributes
    ----------
    joint: JointData
        all the data concerning the current joint
    frame: int
        current frame of the joint
    """

    class __GlobLocBaseMatrices(QWidget):
        def __init__(
            self,
            loc: MatrixInputWidget,
            glob: MatrixInputWidget,
            base: MatrixInputWidget,
            name: str,
            layoutType: str,
        ):
            QWidget.__init__(self)

            self.loc = loc
            self.glob = glob
            self.base = base
            self.name = name
            self.layoutType = layoutType

            self.__initUI()

        def __initUI(self) -> None:
            title = QLabel()
            locLabel = QLabel()
            globLabel = QLabel()
            baseLabel = QLabel()

            title.setText(self.name)
            locLabel.setText("Local :  ")
            globLabel.setText("Global :")
            baseLabel.setText("Base :   ")

            layout = QVBoxLayout()
            layout.addWidget(title)

            if self.layoutType == "vertical":
                layout = self.__verticalLayout(
                    layout, locLabel, globLabel, baseLabel
                )
            elif self.layoutType == "horizontal":
                layout = self.__horizontalLayout(
                    layout, locLabel, globLabel, baseLabel
                )

            self.setLayout(layout)

        def __verticalLayout(
            self,
            layout: QVBoxLayout,
            locLabel: QLabel,
            globLabel: QLabel,
            baseLabel: QLabel,
        ) -> QVBoxLayout:
            layout.addWidget(locLabel)
            layout.addWidget(self.loc)

            layout.addWidget(globLabel)
            layout.addWidget(self.glob)

            layout.addWidget(baseLabel)
            layout.addWidget(self.base)

            return layout

        def __horizontalLayout(
            self,
            layout: QVBoxLayout,
            locLabel: QLabel,
            globLabel: QLabel,
            baseLabel: QLabel,
        ) -> QVBoxLayout:
            locLayout = QHBoxLayout()
            locLayout.addWidget(locLabel)
            locLayout.addWidget(self.loc)
            locWidget = QWidget()
            locWidget.setLayout(locLayout)

            globLayout = QHBoxLayout()
            globLayout.addWidget(globLabel)
            globLayout.addWidget(self.glob)
            globWidget = QWidget()
            globWidget.setLayout(globLayout)

            baseLayout = QHBoxLayout()
            baseLayout.addWidget(baseLabel)
            baseLayout.addWidget(self.base)
            baseWidget = QWidget()
            baseWidget.setLayout(baseLayout)

            layout.addWidget(locWidget)
            layout.addWidget(globWidget)
            layout.addWidget(baseWidget)

            return layout

        def isModified(self):
            return (
                self.loc.isModified()
                or self.glob.isModified()
                or self.base.isModified()
            )

    def __init__(self, joint: JointData):
        QWidget.__init__(self)
        UIComponent.__init__(self, PynimationTools())

        self.setAutoFillBackground(True)
        self.installEventFilter(self)

        # data
        self.__joint = joint
        self.__frame: int = 0
        self.canCollapse: bool = True
        # widgets
        self.__titleWidget = QLabel()

        self.__initUI()
        self.__initSignal()
        self.initUpdateLoop()

    @property
    def joint(self) -> JointData:
        """
        Get the joint data

        Returns
        -------
        JointData
            the joint data
        """
        return self.__joint

    def __initWidget(self) -> None:
        transLoc = self.__joint.animation[self.__frame, self.__joint.id]
        transGlob = self.__joint.animation.globals[
            self.__frame, self.__joint.id
        ]
        transBase = self.__joint.animation.skeleton.basePose[self.__joint.id]

        self.__editTransform = self.__GlobLocBaseMatrices(
            MatrixInputWidget(transLoc),
            MatrixInputWidget(transGlob, False),
            MatrixInputWidget(transBase),
            "Transform :",
            "vertical",
        )
        self.__editPosition = self.__GlobLocBaseMatrices(
            MatrixInputWidget(transLoc.position),
            MatrixInputWidget(transGlob.position, False),
            MatrixInputWidget(transBase.position),
            "Position :",
            "horizontal",
        )
        self.__editRotationEuler = self.__GlobLocBaseMatrices(
            MatrixInputWidget(transLoc.rotation.euler),
            MatrixInputWidget(transGlob.rotation.euler, False),
            MatrixInputWidget(transBase.rotation.euler),
            "Euler rotation :",
            "horizontal",
        )
        self.__editRotationQuat = self.__GlobLocBaseMatrices(
            MatrixInputWidget(transLoc.rotation.quat),
            MatrixInputWidget(transGlob.rotation.quat, False),
            MatrixInputWidget(transBase.rotation.quat),
            "Quaternion rotation :",
            "horizontal",
        )
        self.__editRotationVec = self.__GlobLocBaseMatrices(
            MatrixInputWidget(transLoc.rotation.rotvec),
            MatrixInputWidget(transGlob.rotation.rotvec, False),
            MatrixInputWidget(transBase.rotation.rotvec),
            "Vector rotation :",
            "horizontal",
        )
        self.__tabGeneral = QTabWidget()
        self.__tabRotation = QTabWidget()

    def __getIconPath(self) -> str:
        icons_folder = (
            "%s/pynimation/pynimation-viewer/pynimation/viewer/ui/qt_ui/icons/"
            % (data_.getDataPath("").split("\\pynimation")[0])
        )
        icons_folder = icons_folder.replace("\\", "/")
        return icons_folder

    def __initUI(self) -> None:
        self.__initWidget()

        self.__tabRotation.addTab(self.__editRotationEuler, "euler")
        self.__tabRotation.addTab(self.__editRotationQuat, "quaternion")
        self.__tabRotation.addTab(self.__editRotationVec, "vector")

        positionLayout = QVBoxLayout()
        positionLayout.addWidget(self.__editPosition)
        positionLayout.addWidget(self.__tabRotation)
        positionLayout.setContentsMargins(0, 0, 0, 0)
        positionWidget = QWidget()
        positionWidget.setLayout(positionLayout)

        self.__tabGeneral.addTab(positionWidget, "Transform vectors")
        self.__tabGeneral.addTab(self.__editTransform, "Transform matrix")

        collapsibleLayout = QVBoxLayout()
        collapsibleLayout.addWidget(self.__titleWidget)
        collapsibleLayout.addWidget(self.__tabGeneral)
        collapsibleLayout.setContentsMargins(0, 0, 0, 0)

        self.__collapsible = QWidget()
        self.__collapsible.setLayout(collapsibleLayout)

        self.__buttonCollapse = QPushButton()
        self.__buttonCollapse.setText(
            "Animation : %s Joint %s"
            % (self.__joint.animId, self.__joint.name)
        )
        self.__buttonCollapse.setIcon(
            QIcon("%sarrow_rotate.png" % self.__getIconPath())
        )

        layout = QVBoxLayout()
        layout.addWidget(self.__buttonCollapse)
        layout.addWidget(self.__collapsible)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(Qt.AlignmentFlag.AlignTop)

        self.setLayout(layout)

    def __initSignal(self) -> None:
        collapseButtonClicked: Any = self.__buttonCollapse.clicked
        collapseButtonClicked.connect(self.changeVisibility)

        generalChanged: Any = self.__tabGeneral.currentChanged
        generalChanged.connect(self.__updateAllMatrices)

        rotationChanged: Any = self.__tabRotation.currentChanged
        rotationChanged.connect(self.__updateAllMatrices)

    def changeVisibility(self) -> None:
        """
        Change the visibility of the widget
        """
        self.canCollapse = not self.canCollapse

        self.__collapsible.setVisible(self.canCollapse)
        if self.canCollapse:
            self.__buttonCollapse.setIcon(
                QIcon("%sarrow_rotate.png" % self.__getIconPath())
            )
        else:
            self.__buttonCollapse.setIcon(
                QIcon("%sarrow.png" % self.__getIconPath())
            )

    def __attrsetter(self, obj, attr, val):
        pre, _, post = attr.rpartition(".")
        return setattr(attrgetter(pre)(obj) if pre else obj, post, val)

    def __updateMatrix(self, widget, attribute: str = None):
        transLoc = self.__joint.animation[self.__frame, self.__joint.id]
        transGlob = self.__joint.animation.globals[
            self.__frame, self.__joint.id
        ]
        transBase = self.__joint.animation.skeleton.basePose[self.__joint.id]

        if attribute is not None:
            widget.loc.arrayData = attrgetter(attribute)(transLoc)
            widget.glob.arrayData = attrgetter(attribute)(transGlob)
            widget.base.arrayData = attrgetter(attribute)(transBase)
        else:
            widget.loc.arrayData = transLoc
            widget.glob.arrayData = transGlob
            widget.base.arrayData = transBase

    def __updateAllMatrices(self) -> None:
        if self.__tabGeneral is None and self.__tabRotation is None:
            return

        if self.__tabGeneral.currentIndex() == 0:
            self.__updateMatrix(self.__editPosition, "position")

            if self.__tabRotation.currentIndex() == 0:
                self.__updateMatrix(self.__editRotationEuler, "rotation.euler")
            elif self.__tabRotation.currentIndex() == 1:
                self.__updateMatrix(self.__editRotationQuat, "rotation.quat")
            elif self.__tabRotation.currentIndex() == 2:
                self.__updateMatrix(self.__editRotationVec, "rotation.rotvec")

        elif self.__tabGeneral.currentIndex() == 1:
            self.__updateMatrix(self.__editTransform)

    def __setData(self, widget, attribute: str = None) -> None:
        if widget.isModified():
            transLoc = self.__joint.animation[self.__frame, self.__joint.id]
            transGlob = self.__joint.animation.globals[
                self.__frame, self.__joint.id
            ]
            transBase = self.__joint.animation.skeleton.basePose[
                self.__joint.id
            ]

            newTransLoc = widget.loc.getModifiedArrayData()
            newTransBase = widget.base.getModifiedArrayData()

            if attribute is not None:
                currentDataLoc = attrgetter(attribute)(transLoc)
                # currentDataGlob = attrgetter(attribute)(transGlob)
                currentDataBase = attrgetter(attribute)(transBase)

                if not np.array_equal(currentDataLoc, newTransLoc):
                    self.__attrsetter(
                        self.__joint.animation[self.__frame, self.__joint.id],
                        attribute,
                        newTransLoc,
                    )

                elif not np.array_equal(currentDataBase, newTransBase):
                    self.__attrsetter(
                        self.__joint.animation.skeleton.basePose[
                            self.__joint.id
                        ],
                        attribute,
                        newTransBase,
                    )

                self.__joint.animation.updateGlobalsFrames((self.__frame,))
                widget.glob.arrayData = attrgetter(attribute)(transGlob)

            else:
                self.__joint.animation[
                    self.__frame, self.__joint.id
                ] = newTransLoc
                self.__joint.animation.skeleton.basePose[
                    self.__joint.id
                ] = newTransBase

    def __setNewData(self) -> None:
        if self.__tabGeneral is None and self.__tabRotation is None:
            return

        if self.__tabGeneral.currentIndex() == 0:
            self.__setData(self.__editPosition, "position")

            if self.__tabRotation.currentIndex() == 0:
                self.__setData(self.__editRotationEuler, "rotation.euler")
            elif self.__tabRotation.currentIndex() == 1:
                self.__setData(self.__editRotationQuat, "rotation.quat")
            elif self.__tabRotation.currentIndex() == 2:
                self.__setData(self.__editRotationVec, "rotation.rotvec")

        elif self.__tabGeneral.currentIndex() == 1:
            self.__setData(self.__editTransform)

    def __updateWidget(self) -> None:
        if self.canCollapse and self.tools.player.isAnimationRegistered(
            self.__joint.animation
        ):
            updateMod = self.__frame != self.tools.player.getCurrentFrameId(
                self.__joint.animation
            )
            self.__frame = self.tools.player.getCurrentFrameId(
                self.__joint.animation
            )

            if updateMod:
                # update mod
                self.__updateAllMatrices()
            else:
                # edit mod
                self.__setNewData()

            self.__titleWidget.setText("Frame : %s" % (self.__frame))

    def initUpdateLoop(self) -> None:
        self.timer = QTimer()
        signal: Any = self.timer.timeout
        signal.connect(self.__updateWidget)
        self.timer.start(16)
