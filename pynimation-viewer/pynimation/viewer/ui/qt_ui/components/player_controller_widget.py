import math
from typing import List, Any, Callable
from dataclasses import dataclass, field

from qtpy.QtCore import Qt, QRectF, QPointF
from qtpy.QtWidgets import (
    QHBoxLayout,
    QGraphicsItem,
    QSlider,
    QGraphicsScene,
    QGraphicsView,
    QWidget,
)
from qtpy.QtGui import (
    QPolygonF,
    QColor,
    QBrush,
    QPen,
)

from pynimation.viewer.ui.qt_ui.qt_toolbox import PynimationTools
from pynimation.viewer.ui.qt_ui.components.common.edit_numbers import (
    EditNumbers,
)


class PlayerControllerWidget(QWidget):
    class __ControlPanel(QGraphicsView):
        @dataclass
        class Colors:
            background: QColor = field(
                default_factory=lambda: QColor(80, 80, 80)
            )
            button: QColor = field(
                default_factory=lambda: QColor(200, 200, 200)
            )
            buttonHighlight: QColor = field(
                default_factory=lambda: QColor(255, 255, 255)
            )

        class __ShapeData:
            def __init__(self):
                self.size = 20

            def __triangle(self, x, y, size) -> QPolygonF:
                return QPolygonF(
                    [
                        QPointF(x, y),
                        QPointF(x + size, y + self.size / 2),
                        QPointF(x, y + self.size),
                    ]
                )

            def camera(self, x, y) -> QPolygonF:
                flashWidth: int = 10
                flashHeight: int = 3
                return QPolygonF(
                    [
                        QPointF(x, y + flashHeight),
                        QPointF(
                            x + self.size / 2 - flashWidth / 2, y + flashHeight
                        ),
                        QPointF(x + self.size / 2 - flashWidth / 2, y),
                        QPointF(x + self.size / 2 + flashWidth / 2, y),
                        QPointF(
                            x + self.size / 2 + flashWidth / 2, y + flashHeight
                        ),
                        QPointF(x + self.size, y + flashHeight),
                        QPointF(x + self.size, y + self.size),
                        QPointF(x, y + self.size),
                    ]
                )

            def usualTriangle(self, x, y) -> QPolygonF:
                return self.__triangle(x, y, self.size)

            def invertedTriangle(self, x, y) -> QPolygonF:
                return self.__triangle(x, y, -self.size)

            def bar(self, x, y) -> QRectF:
                return QRectF(x, y, self.size / 4, self.size)

            def mask(self, x, y, k) -> QRectF:
                return QRectF(x - k, y, self.size + k, self.size)

        class __Shapes:
            def __init__(self, x: int, y: int):
                self.x = x
                self.y = y
                self.list: List[Any] = []

            def setVisible(self, value: bool) -> None:
                for shape in self.list:
                    shape.setVisible(value)

            def setBrush(self, value: QBrush) -> None:
                for shape in self.list:
                    shape.setBrush(value)

            def setPen(self, value: QPen) -> None:
                for shape in self.list:
                    shape.setPen(value)

            def isUnderMouse(self) -> bool:
                for shape in self.list:
                    if shape.isUnderMouse():
                        return True
                return False

        class __Button:
            def __init__(self, action, mask):
                self.__state: int = 0
                self.action: Callable = action
                self.underMouse = False

                # QGraphicsItem
                self.mask: Any = mask
                self.states: Any = []

            @property
            def state(self) -> int:
                return self.__state

            @state.setter
            def state(self, state: int) -> None:
                # loop the states
                self.states[self.__state].setVisible(False)
                if state >= 0 and state < len(self.states):
                    self.__state = state
                elif state < 0:
                    self.__state = len(self.states) - 1
                elif state >= len(self.states):
                    self.__state = 0
                self.states[self.__state].setVisible(True)

            @property
            def currentState(self) -> Any:
                return self.states[self.state]

            def incrState(self) -> None:
                self.state = self.state + 1

            def isUnderMouse(self, normalColor, highlightColor) -> bool:
                if self.mask.isUnderMouse():
                    self.currentState.setBrush(QBrush(highlightColor))
                    self.currentState.setPen(QPen(highlightColor))
                    self.underMouse = True
                else:
                    self.currentState.setBrush(QBrush(normalColor))
                    self.currentState.setPen(QPen(normalColor))
                    self.underMouse = False
                return self.underMouse

        def __init__(self, tools):
            QGraphicsView.__init__(self)

            self.setMouseTracking(True)

            self.defaultColors = self.Colors()

            # objects
            self.scene: QGraphicsScene = QGraphicsScene(self)
            self.tools: PynimationTools = tools
            self.player = self.tools.player
            # shapes
            self.shapeData = self.__ShapeData()
            self.nextFrame = self.__Shapes(self.rightPosition(1), 0)
            self.shapePause = self.__Shapes(0, 0)
            self.shapePlay = self.__Shapes(0, 0)
            self.previousFrame = self.__Shapes(self.leftPosition(1), 0)
            self.shapeScreenshot = self.__Shapes(self.leftPosition(4), 0)
            self.shapeRecord = self.__Shapes(self.leftPosition(3), 0)
            self.shapeNotRecord = self.__Shapes(self.leftPosition(3), 0)
            # buttons
            self.buttons = []
            self.currentItem = None

            # var
            self.play = True
            self.record = False

            # config
            self.setScene(self.scene)

            # setup scene
            self.__initPlayPause()
            self.__initNext()
            self.__initPrevious()
            self.__initScreenshot()
            self.__initRecord()

        # initialization
        def leftPosition(self, k: int) -> float:
            return -(self.shapeData.size) * 2 * k - self.shapeData.size / 4

        def rightPosition(self, k: int) -> float:
            return self.shapeData.size * 2 * k

        # shape functions
        def __addMask(self, x, y) -> QGraphicsItem:
            mask = self.scene.addRect(
                self.shapeData.mask(x, y, 10),
                QPen(self.defaultColors.button),
                QBrush(self.defaultColors.button),
            )
            mask.setVisible(False)

            return mask

        def __addUsualTriangle(self, x, y) -> QGraphicsItem:
            return self.scene.addPolygon(
                self.shapeData.usualTriangle(x, y),
                QPen(self.defaultColors.button),
                QBrush(self.defaultColors.button),
            )

        def __addInvertedTriangle(self, x, y) -> QGraphicsItem:
            return self.scene.addPolygon(
                self.shapeData.invertedTriangle(x, y),
                QPen(self.defaultColors.button),
                QBrush(self.defaultColors.button),
            )

        def __addBar(self, x, y) -> QGraphicsItem:
            return self.scene.addRect(
                self.shapeData.bar(x, y),
                QPen(self.defaultColors.button),
                QBrush(self.defaultColors.button),
            )

        def __addCamera(self, x, y) -> QGraphicsItem:
            return self.scene.addPolygon(
                self.shapeData.camera(x, y),
                QPen(self.defaultColors.button),
                QBrush(self.defaultColors.button),
            )

        def __addEllipse(self, x, y) -> QGraphicsItem:
            return self.scene.addEllipse(
                x,
                y,
                self.shapeData.size,
                self.shapeData.size,
                QPen(self.defaultColors.button),
                QBrush(self.defaultColors.button),
            )

        def __addSquare(self, x, y) -> QGraphicsItem:
            return self.scene.addRect(
                x,
                y,
                self.shapeData.size,
                self.shapeData.size,
                QPen(self.defaultColors.button),
                QBrush(self.defaultColors.button),
            )

        # buttons functions
        def __initPlayPause(self) -> None:
            self.shapePlay.list.append(
                self.__addUsualTriangle(self.shapePlay.x, self.shapePlay.y)
            )
            self.shapePlay.setVisible(False)

            self.shapePause.list.append(
                self.__addBar(self.shapePause.x, self.shapePause.y)
            )
            self.shapePause.list.append(
                self.__addBar(self.shapePause.x + 14, self.shapePause.y)
            )
            self.shapePause.setVisible(True)

            self.playPauseButton = self.__Button(
                self.player.togglePause,
                self.__addMask(self.shapePlay.x, self.shapePause.y),
            )
            self.playPauseButton.states.append(self.shapePause)
            self.playPauseButton.states.append(self.shapePlay)
            self.buttons.append(self.playPauseButton)

        def __initNext(self) -> None:
            self.nextFrame.list.append(
                self.__addUsualTriangle(self.nextFrame.x, self.nextFrame.y)
            )
            self.nextFrame.list.append(
                self.__addBar(
                    self.nextFrame.x + self.shapeData.size, self.nextFrame.y
                )
            )

            self.nextFrameButton = self.__Button(
                self.player.nextFrame,
                self.__addMask(self.nextFrame.x, self.nextFrame.y),
            )
            self.nextFrameButton.states.append(self.nextFrame)
            self.buttons.append(self.nextFrameButton)

        def __initPrevious(self) -> None:
            self.previousFrame.list.append(
                self.__addInvertedTriangle(
                    -self.shapeData.size, self.previousFrame.y
                )
            )
            self.previousFrame.list.append(
                self.__addBar(self.previousFrame.x, self.previousFrame.y)
            )

            self.previousFrameButton = self.__Button(
                self.player.previousFrame,
                self.__addMask(self.previousFrame.x, self.previousFrame.y),
            )
            self.previousFrameButton.states.append(self.previousFrame)
            self.buttons.append(self.previousFrameButton)

        def __initScreenshot(self) -> None:
            self.shapeScreenshot.list.append(
                self.__addCamera(
                    self.shapeScreenshot.x, self.shapeScreenshot.y
                )
            )
            self.scene.addRect(
                self.shapeScreenshot.x + self.shapeData.size / 3,
                self.shapeScreenshot.y + self.shapeData.size / 2.5,
                self.shapeData.size / 2.5,
                self.shapeData.size / 2.5,
                QPen(self.defaultColors.background),
                QBrush(self.defaultColors.background),
            )

            self.screenshotButton = self.__Button(
                self.tools.viewport.triggerScreenshot,
                self.__addMask(self.shapeScreenshot.x, self.shapeScreenshot.y),
            )
            self.screenshotButton.states.append(self.shapeScreenshot)
            self.buttons.append(self.screenshotButton)

        def __initRecord(self) -> None:
            self.shapeRecord.list.append(
                self.__addEllipse(self.shapeRecord.x, self.shapeRecord.y)
            )
            self.shapeRecord.setVisible(True)

            self.shapeNotRecord.list.append(
                self.__addSquare(self.shapeRecord.x, self.shapeRecord.y)
            )
            self.shapeNotRecord.setVisible(False)

            self.recordButton = self.__Button(
                self.tools.viewport.startStopRecording,
                self.__addMask(self.shapeRecord.x, self.shapeRecord.y),
            )
            self.recordButton.states.append(self.shapeRecord)
            self.recordButton.states.append(self.shapeNotRecord)
            self.buttons.append(self.recordButton)

        # event
        def mouseMoveEvent(self, event) -> None:
            self.currentItem = None

            for button in self.buttons:
                if button.isUnderMouse(
                    self.defaultColors.button,
                    self.defaultColors.buttonHighlight,
                ):
                    self.currentItem = button

        def mousePressEvent(self, event) -> None:
            if (
                event.button() == Qt.MouseButton.LeftButton
                and self.currentItem is not None
            ):
                self.currentItem.action()

        # update
        def updateWidget(self) -> None:
            if (
                not self.tools.player.paused
                and self.playPauseButton.state == 1
            ):
                self.playPauseButton.incrState()
            elif self.tools.player.paused and self.playPauseButton.state == 0:
                self.playPauseButton.incrState()

            if (
                not self.tools.viewport._isrecording
                and self.recordButton.state == 1
            ):
                self.recordButton.incrState()
            elif (
                self.tools.viewport._isrecording
                and self.recordButton.state == 0
            ):
                self.recordButton.incrState()

    def __init__(self, tools):
        QWidget.__init__(self)

        # config
        self.setFixedHeight(70)
        self.setAttribute(
            Qt.WidgetAttribute.WA_StyledBackground,
            True,
        )

        # objects
        self.controlPanel = self.__ControlPanel(tools)
        self.controlSpeed = QWidget()
        self.inputSpeed = EditNumbers()
        self.sliderSpeed: QSlider = QSlider(Qt.Orientation.Horizontal)
        self.player = tools.player

        # config
        self.controlPanel.setFixedWidth(300)
        self.controlSpeed.setFixedWidth(300)
        self.sliderSpeed.setFixedWidth(200)
        self.inputSpeed.setFixedWidth(90)

        # init speed edition
        self.sliderSpeed.setValue(
            round(self.__playerToSlider(self.player.speed))
        )
        self.inputSpeed.setText("{:.3f}".format(self.player.speed))
        self.inputSpeed.setCursorPosition(0)

        # var
        self.speed: float = self.player.speed
        self.sliderValue: float = self.sliderSpeed.value()

        # speed layout
        speedLayout = QHBoxLayout()
        speedLayout.addWidget(self.inputSpeed)
        speedLayout.addWidget(self.sliderSpeed)
        self.controlSpeed.setLayout(speedLayout)

        # set layout
        layout = QHBoxLayout()
        layout.addWidget(self.controlPanel, Qt.AlignmentFlag.AlignCenter)
        layout.addWidget(self.controlSpeed, Qt.AlignmentFlag.AlignCenter)
        self.setLayout(layout)

        # style
        self.setStyleSheet(
            """
                background:#4a4a4a;
            """
        )
        self.inputSpeed.setStyleSheet(
            """
                background:#3C3C3C;
                margin-right:10px;
            """
        )

    def __strIsFloat(self, strToCheck: str) -> bool:
        try:
            float(strToCheck)
            return True
        except ValueError:
            return False

    def __sliderToPlayer(self, value: float) -> float:
        return math.exp(value / 10 - 5)

    def __playerToSlider(self, value: float) -> float:
        return (math.log(value) + 5) * 10 if value > 0 else 0

    def __sliderHasChanged(self) -> bool:
        result = self.sliderValue != self.sliderSpeed.value()
        if result:
            self.player.speed = self.__sliderToPlayer(self.sliderSpeed.value())
        return result

    def __inputHasChanged(self) -> bool:
        result = self.inputSpeed.isModifiedWidthDigit
        if result and self.__strIsFloat(self.inputSpeed.text()):
            self.player.speed = float(self.inputSpeed.text())
        return result

    def __setSliderSpeed(self) -> None:
        self.sliderSpeed.setValue(
            round(self.__playerToSlider(self.player.speed))
        )
        self.sliderValue = self.sliderSpeed.value()
        self.speed = self.player.speed

    def __setInputSpeed(self) -> None:
        self.inputSpeed.setText("{:.3f}".format(self.player.speed))
        self.inputSpeed.setCursorPosition(0)
        self.sliderValue = self.sliderSpeed.value()
        self.speed = self.player.speed

    def __speedHasChanged(self) -> bool:
        result = self.speed != self.player.speed
        if self.__sliderHasChanged():
            self.__setInputSpeed()
        elif self.__inputHasChanged():
            self.__setSliderSpeed()
        elif result:
            self.__setInputSpeed()
            self.__setSliderSpeed()
        return result

    def updateWidget(self) -> None:
        self.controlPanel.updateWidget()
        self.__speedHasChanged()
