from typing import Any

from qtpy.QtCore import Qt, QTimer
from qtpy.QtWidgets import (
    QWidget,
    QVBoxLayout,
)

from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.qt_ui.qt_toolbox import PynimationTools
from pynimation.viewer.ui.qt_ui.components.timeline_widget import (
    TimelineWidget,
)
from pynimation.viewer.ui.qt_ui.components.player_controller_widget import (
    PlayerControllerWidget,
)
from pynimation.viewer.ui.event import (
    KeyPressEvent,
    KeyHoldEvent,
)


class PlayerWidget(UIComponent, QWidget):
    """
    Provide buttons and speed slider to control the player.
    Provide a timeline to visualise each frames of each animations.
    """

    def __init__(self):
        QWidget.__init__(self)
        UIComponent.__init__(self, PynimationTools())

        # event management
        self.installEventFilter(self)

        # objects
        self.__playerControllerWidget = PlayerControllerWidget(self.tools)
        self.__timelineWidget = TimelineWidget(self.tools)

        self.__setLayout()

    def __setLayout(self) -> None:
        layout = QVBoxLayout()

        # config layout
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        # merge widgets
        layout.addWidget(self.__playerControllerWidget)
        layout.addWidget(self.__timelineWidget)
        self.setLayout(layout)
        self.resize(self.width(), 200)

    # Events
    def eventFilter(self, source, event):
        self.bindings.handleEvent(self.tools.eventTranslator(event))
        return super().eventFilter(source, event)

    # UIComponent implementation

    def bindingsInit(self) -> None:
        # default bindings
        # KeyPress
        self.bindings.bind(KeyPressEvent(Qt.Key.Key_L, name="l"), "nextFrame")
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_H, name="h"), "previousFrame"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_K, name="k"), "increaseSpeed"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_J, name="j"), "decreaseSpeed"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Space, name="space"), "pauseplay"
        )
        # KeyHold
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_L), name="l"), "nextFrame"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_H), name="h"), "previousFrame"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_K), name="k"), "increaseSpeed"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_J), name="j"), "decreaseSpeed"
        )

        # custom bindings
        # KeyPress
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Right, name="right"), "nextFrame"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Left, name="left"), "previousFrame"
        )
        # KeyHold
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_Right), name="right"), "nextFrame"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_Left), name="left"), "previousFrame"
        )

    def initUpdateLoop(self) -> None:
        self.timer = QTimer()
        signal: Any = self.timer.timeout

        signal.connect(self.__playerControllerWidget.updateWidget)
        signal.connect(self.__timelineWidget.updateWidget)
        self.timer.start(16)
