import math
import numpy as np
from typing import Tuple, List, Any

from qtpy.QtWidgets import (
    QWidget,
    QGridLayout,
)
from pynimation.viewer.ui.qt_ui.components.common.edit_numbers import (
    EditNumbers,
)


class MatrixInputWidget(QWidget):
    """
    Display an ordered set of "text input" depending of a tensor.

    Parameters
    ----------
    arrayData: np.ndarray
        the tensor input
    enable: bool
        set the read only mod

    Attributes
    ----------
    enable: bool
        set the read only mod
    """

    def __init__(self, arrayData: np.ndarray, enable: bool = True):
        QWidget.__init__(self)

        self.__arrayData: np.ndarray = arrayData
        self.__arrayWidget: List[Any] = []
        self.enable = enable
        self.__initEditData()

    def __shapeToGridPos(self, n, shape) -> Tuple[int, int]:
        i: int = 0
        j: int = 0
        # compute the position of a text input
        for x, dim in enumerate(shape):
            if x == len(shape) - 1:
                j = n % dim
            elif x == len(shape) - 2:
                i = dim if i == 0 else i * dim
                i = math.trunc(n / i)
            else:
                i *= dim

        return (i, j)

    def __initEditData(self) -> None:
        flatArrayData = self.__arrayData.flatten()

        layout = QGridLayout()
        self.__arrayWidget = [None for n in range(flatArrayData.shape[0])]

        for n in range(flatArrayData.shape[0]):
            edit = EditNumbers()
            edit.setText("{:.4f}".format(flatArrayData[n]))
            edit.setCursorPosition(0)
            edit.setEnabled(self.enable)
            self.__arrayWidget[n] = edit
            pos = self.__shapeToGridPos(n, self.__arrayData.shape)
            layout.addWidget(self.__arrayWidget[n], pos[0], pos[1])

        self.setLayout(layout)

    def __strIsFloat(self, strToCheck: str) -> bool:
        try:
            float(strToCheck)
            return True
        except ValueError:
            return False

    @property
    def arrayData(self) -> np.ndarray:
        return self.__arrayData

    @arrayData.setter
    def arrayData(self, arrayData) -> None:
        self.__arrayData = arrayData
        flatArrayData = self.__arrayData.flatten()

        for n in range(flatArrayData.shape[0]):
            self.__arrayWidget[n].setText("{:.4f}".format(flatArrayData[n]))
            self.__arrayWidget[n].setCursorPosition(0)

    def getModifiedArrayData(self) -> np.ndarray:
        size = np.prod(self.__arrayData.shape)
        result = np.arange(size, dtype=float)

        for n in range(size):
            currentValue = self.__arrayWidget[n].text()
            if self.__strIsFloat(currentValue):
                result[n] = float(currentValue)

        result.shape = self.__arrayData.shape
        return result

    def isModified(self) -> bool:
        for textBox in self.__arrayWidget:
            if textBox.isModifiedWidthDigit:
                textBox.isModifiedWidthDigit = False
                return True
        return False
