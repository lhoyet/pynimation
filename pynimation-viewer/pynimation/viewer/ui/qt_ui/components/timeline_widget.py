import math
from typing import Any, List, Optional
from dataclasses import dataclass, field

from qtpy.QtCore import (
    Qt,
    QRectF,
    QPointF,
    QLineF,
    QSizeF,
)
from qtpy.QtGui import (
    QColor,
    QBrush,
    QPen,
    QPolygonF,
)
from qtpy.QtWidgets import (
    QSizePolicy,
    QGraphicsScene,
    QGraphicsView,
    QFrame,
)

from pynimation.common.vec import Vec2
from pynimation.viewer.ui.qt_ui.components.common.utils import (
    npArrayContentsCompare,
)


class TimelineWidget(QGraphicsView):
    @dataclass
    class __Camera:
        def __init__(self):
            # coordinates
            self.x: float = 0
            self.y: float = 0

            self.width: float = 1
            self.globalWidth: float = 1

            self.height: float = 1
            self.globalHeight: float = 1

            # utils
            self.oldX: float = 0
            self.frameHeight: float = 9
            self.globalFrameWidth: float = 0
            self.scaleFramerate: Optional[float] = None

            self.init: bool = False

            # graphic items
            self.verticalGlob: Any = None
            self.verticalLoc: Any = None
            self.horizontalGlob: Any = None
            self.horizontalLoc: Any = None

        @property
        def zoomCoeff(self) -> float:
            if self.width > 0:
                return self.globalWidth / self.width

            return 1

        @property
        def globalHorizontalOrigin(self) -> float:
            return (self.x - (self.width / 2)) * self.zoomCoeff

        @property
        def globalVerticalOrigin(self) -> float:
            return self.y - (self.height / 2)

        @property
        def origin(self) -> float:
            return self.x - self.width / 2

        def frameWidth(self, framerate: float) -> float:
            return (
                (self.globalFrameWidth)
                / (
                    framerate / self.scaleFramerate
                    if self.scaleFramerate is not None
                    else 1
                )
                * self.zoomCoeff
            )

    @dataclass
    class __Cursor:
        def __init__(self):
            self.x: float = 0
            self.y: float = 0

            # const
            self.precision: int = 5000

            # graphic items
            self.arrow: Any = None
            self.line: Any = None

    @dataclass
    class Colors:
        # widget
        backgroundColor: QColor = field(
            default_factory=lambda: QColor(60, 60, 60)
        )
        # frames
        frameBackground: QColor = field(
            default_factory=lambda: QColor(50, 50, 255)
        )
        frameLine: QColor = field(default_factory=lambda: QColor(50, 50, 180))
        frameBackgroundHighlight: QColor = field(
            default_factory=lambda: QColor(255, 255, 255)
        )
        frameLineHighlight: QColor = field(
            default_factory=lambda: QColor(255, 255, 255)
        )
        # text
        frameId: QColor = field(default_factory=lambda: QColor(220, 220, 220))
        # scrollbars
        bar: QColor = field(default_factory=lambda: QColor(100, 100, 100))
        scrollBar: QColor = field(
            default_factory=lambda: QColor(200, 200, 200)
        )
        # cursor
        cursorColor: QColor = field(
            default_factory=lambda: QColor(200, 200, 200)
        )

    def __init__(self, tools):
        # init
        QGraphicsView.__init__(self)

        self.defaultColors = self.Colors()

        # QGraphicsView init
        self.setAutoFillBackground(True)
        self.setFocusPolicy(Qt.FocusPolicy.StrongFocus)
        self.setSizePolicy(
            QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Minimum
        )
        self.setMinimumHeight(0)
        self.setFrameShape(QFrame.Shape.NoFrame)
        self.installEventFilter(self)
        self.setMouseTracking(True)

        # UI Component init
        self.tools = tools
        self.player = self.tools.player
        self.__currentAnims = None

        # UI init
        self.__initUI()
        self.__initColor()

    # QWidget implementation

    # Initialization
    # add all of the items in the scene
    def __needInit(self) -> bool:
        needInit: bool = not npArrayContentsCompare(
            self.__currentAnims, self.tools.player.animations
        )
        if needInit:
            self.__currentAnims = self.tools.player.animations
        return needInit

    def __initUI(self) -> None:
        self.initScene: bool = False

        # event
        self.leftPressed: bool = False
        self.rightPressed: bool = False
        self.verticalScroll: bool = False
        self.verticalScrollTrigger: bool = False
        self.horizontalScrollTrigger: bool = False

        # data
        self.camera = self.__Camera()
        self.cursorTimeline = self.__Cursor()
        self.currentHighlight: List[int] = []

        # const
        self.scrollBarSize: int = 15
        self.frameHeight: float = self.camera.frameHeight
        self.animationBoxHeight: float = self.frameHeight * 6
        self.originCorrection = -10

        # object
        self.scene: Any = QGraphicsScene(self)
        self.setScene(self.scene)

    def __initColor(self) -> None:
        self.scene.setBackgroundBrush(
            QBrush(self.defaultColors.backgroundColor)
        )

    def __initFrames(self) -> None:
        initRect: QRectF = QRectF(0, 0, 0, 0)

        for i, animation in enumerate(self.__currentAnims):
            for i in range(animation.nframes):
                currentFrame = self.scene.addRect(
                    initRect,
                    QPen(self.defaultColors.frameLine),
                    QBrush(self.defaultColors.frameBackground),
                )
                currentTextId = self.scene.addText(str(i))
                currentTextId.setDefaultTextColor(self.defaultColors.frameId)
                currentTextId.setVisible(False)
                self.frames.append(currentFrame)
                self.framesTextId.append(currentTextId)

    def __initScrollbars(self) -> None:
        initRect: QRectF = QRectF(0, 0, 0, 0)

        self.camera.horizontalGlob = self.scene.addRect(
            initRect,
            QPen(self.defaultColors.bar),
            QBrush(self.defaultColors.bar),
        )
        self.camera.horizontalLoc = self.scene.addRect(
            initRect,
            QPen(self.defaultColors.scrollBar),
            QBrush(self.defaultColors.scrollBar),
        )
        self.camera.verticalGlob = self.scene.addRect(
            initRect,
            QPen(self.defaultColors.bar),
            QBrush(self.defaultColors.bar),
        )
        self.camera.verticalLoc = self.scene.addRect(
            initRect,
            QPen(self.defaultColors.scrollBar),
            QBrush(self.defaultColors.scrollBar),
        )

    def __initCursor(self) -> None:
        self.cursorTimeline.arrow = self.scene.addPolygon(
            QPolygonF(
                [
                    QPointF(-10, 0),
                    QPointF(10, 0),
                    QPointF(0, 20),
                ]
            ),
            QPen(self.defaultColors.cursorColor),
            QBrush(self.defaultColors.cursorColor),
        )
        self.cursorTimeline.line = self.scene.addLine(
            QLineF(QPointF(0, 0), QPointF(0, 0)),
            QPen(self.defaultColors.cursorColor),
        )

    def __initScene(self) -> None:
        self.scene.clear()

        # graphic objects
        self.frames: List[Any] = []
        self.framesTextId: List[Any] = []

        self.__initFrames()
        self.__initCursor()
        self.__initScrollbars()

    # update functions
    # change the state of all the items in the scene
    def __updateFrames(self) -> None:
        k: int = 15
        currentId: int = 0

        if self.camera.init:
            for i, animation in enumerate(self.__currentAnims):
                # compute vertical transforms
                verticalOrigin = self.height() / 2 - (
                    len(self.player.animations) * self.animationBoxHeight / 2
                )
                verticalOrigin = (
                    verticalOrigin if verticalOrigin > k else self.height() / 7
                )

                y = (
                    verticalOrigin
                    + i * self.animationBoxHeight
                    - self.camera.globalVerticalOrigin
                )
                height = self.frameHeight * 2

                for j in range(animation.nframes):
                    # compute horizontal coordinates
                    x = (
                        j * (self.camera.frameWidth(animation.framerate))
                        - self.camera.globalHorizontalOrigin
                    )
                    width = self.camera.frameWidth(animation.framerate)

                    # set frame coordinates
                    currentFrame = self.frames[currentId]
                    currentFrame.setRect(
                        QRectF(QPointF(x, y + k), QSizeF(width, height))
                    )

                    # set id frame coordinates
                    freq = round(
                        animation.nframes
                        / self.camera.frameWidth(animation.framerate)
                        / self.camera.zoomCoeff
                    )
                    freq = freq if freq >= 1 else 1
                    currentText = self.framesTextId[currentId]
                    if j % freq == 0:
                        currentText.setPos(QPointF(x, y - k))
                        currentText.setVisible(True)
                    else:
                        currentText.setVisible(False)

                    currentId = currentId + 1

    def __updateScrollbars(self) -> None:
        if self.camera.init:
            self.camera.horizontalGlob.setRect(
                QRectF(
                    self.originCorrection,
                    self.height() - self.scrollBarSize,
                    self.width() - self.originCorrection,
                    self.scrollBarSize,
                )
            )
            self.camera.horizontalLoc.setRect(
                QRectF(
                    self.camera.globalHorizontalOrigin / self.camera.zoomCoeff
                    + self.originCorrection,
                    self.height() - self.scrollBarSize,
                    self.camera.width,
                    self.scrollBarSize,
                )
            )
            self.camera.verticalGlob.setRect(
                QRectF(
                    self.width() - self.scrollBarSize,
                    self.originCorrection,
                    self.scrollBarSize,
                    self.height() - self.originCorrection,
                )
            )
            self.camera.verticalLoc.setRect(
                QRectF(
                    self.width() - self.scrollBarSize,
                    self.camera.globalVerticalOrigin + self.originCorrection,
                    self.scrollBarSize,
                    self.camera.height,
                )
            )

    def __updateTime(self) -> None:
        if (
            self.tools.preciseAnimation is not None
            and self.tools.largestAnimation is not None
        ):
            self.cursorTimeline.x = (
                self.player.time
                % self.tools.largestAnimation.duration
                * self.tools.preciseAnimation.framerate
                * self.camera.frameWidth(self.tools.preciseAnimation.framerate)
                - self.camera.globalHorizontalOrigin
            )

    # update cursor and highlight of the frames
    def __clearOldHighlight(self) -> None:
        for frameId in self.currentHighlight:
            if frameId >= 0 and frameId < len(self.frames):
                currentFrame = self.frames[frameId]
                currentFrame.setBrush(self.defaultColors.frameBackground)
                currentFrame.setPen(self.defaultColors.frameLine)
        self.currentHighlight.clear()

    def __updateCursor(self) -> None:
        self.__updateTime()
        self.cursorTimeline.arrow.setPos(
            QPointF(
                (self.cursorTimeline.x),
                (self.cursorTimeline.y + self.originCorrection),
            )
        )
        self.cursorTimeline.line.setLine(
            QLineF(
                QPointF(self.cursorTimeline.x, 0),
                QPointF(self.cursorTimeline.x, self.height()),
            )
        )
        # frame highlight
        nbFrames: int = 0
        self.__clearOldHighlight()
        for i, animation in enumerate(self.__currentAnims):
            currentId = self.player.getCurrentFrameId(animation) + nbFrames
            self.currentHighlight.append(currentId)
            currentFrame = self.frames[currentId]
            currentFrame.setBrush(self.defaultColors.frameBackgroundHighlight)
            currentFrame.setPen(self.defaultColors.frameLineHighlight)
            nbFrames += animation.nframes

    def __updateCamera(self) -> None:
        if self.tools.largestAnimation is not None:
            self.camera.globalFrameWidth = (
                self.camera.globalWidth / self.tools.largestAnimation.nframes
            )

        if self.camera.zoomCoeff <= 1:
            self.camera.width = self.camera.globalWidth

        if not self.camera.init:
            self.__resizeCamera()
            self.camera.init = True

    def __resizeCamera(self) -> None:
        self.camera.globalWidth = self.width()
        self.camera.globalHeight = self.height()
        self.camera.width = self.camera.globalWidth
        self.camera.height = (
            self.camera.globalHeight
            * self.camera.globalHeight
            / (
                (self.animationBoxHeight) * len(self.player.animations)
                + self.frameHeight
            )
        )

        self.camera.x = self.camera.globalWidth / 2
        self.camera.y = self.camera.height / 2

    def __updateAnimsData(self) -> None:
        currentLargest = None
        currentPrecision = None
        scaleFramerate = None

        for animation in self.player.animations:
            if (
                currentLargest is None
                or animation.nframes / animation.framerate
                > currentLargest.nframes / currentLargest.framerate
            ):
                currentLargest = animation
                scaleFramerate = currentLargest.framerate

            if (
                currentPrecision is None
                or currentPrecision.framerate > currentPrecision.framerate
            ):
                currentPrecision = animation

        self.tools.largestAnimation = currentLargest
        self.tools.preciseAnimation = currentPrecision
        self.camera.scaleFramerate = scaleFramerate

    def __updateScene(self) -> None:
        self.__updateAnimsData()
        self.__updateCamera()
        self.__updateFrames()
        self.__updateScrollbars()
        self.__updateCursor()

    def updateWidget(self) -> None:
        if self.height() < (self.animationBoxHeight) * len(
            self.player.animations
        ):
            self.verticalScroll = True

        if self.__needInit():
            self.__initScene()
            self.__updateScene()
        else:
            self.__updateAnimsData()
            self.__updateCamera()
            self.__updateCursor()

    # Actions
    def __setTime(self, posX: float) -> None:
        globPosX: float = posX + self.camera.globalHorizontalOrigin
        currentFrame: int = math.trunc(
            globPosX / (self.camera.frameWidth(self.cursorTimeline.precision))
        )
        self.player.time = currentFrame / self.cursorTimeline.precision

    def __setZoom(self, direction: float):
        zoom = self.camera.width - direction * 300 * (
            1 / self.camera.zoomCoeff
        )
        zoom = zoom if zoom >= 0 else 0
        zoom = (
            zoom
            if zoom <= self.camera.globalWidth
            else self.camera.globalWidth
        )
        self.camera.width = zoom

    # Events
    def __mousePress(self, event) -> None:
        if event.button() == Qt.MouseButton.LeftButton:
            if event.pos().x() >= self.width() - self.scrollBarSize:
                self.camera.y = event.pos().y()
                self.verticalScrollTrigger = True
            elif event.pos().y() >= self.height() - self.scrollBarSize:
                self.camera.x = event.pos().x()
                self.horizontalScrollTrigger = True
            else:
                self.__setTime(event.pos().x())

            self.leftPressed = True

        if event.button() == Qt.MouseButton.RightButton:
            self.rightPressed = True

    def __mouseRelease(self, event) -> None:
        if event.button() == Qt.MouseButton.LeftButton:
            self.leftPressed = False
            self.horizontalScrollTrigger = False
            self.verticalScrollTrigger = False

        if event.button() == Qt.MouseButton.RightButton:
            self.rightPressed = False
            self.tools.mouse.idle()

    def __mouseMove(self, event) -> None:
        self.tools.mouse.pos = Vec2(
            [event.pos().x(), event.pos().y()], dtype=int
        )

        if self.leftPressed:
            if self.verticalScrollTrigger:
                self.camera.y = event.pos().y()
            elif self.horizontalScrollTrigger:
                self.camera.x = event.pos().x()
            else:
                self.__setTime(event.pos().x())

        if self.rightPressed:
            self.camera.x -= self.tools.mouse.rel.x[0] / self.camera.zoomCoeff

        self.camera.oldX = event.pos().x()

    def __mouseWheel(self, event) -> None:
        k = (
            abs(event.angleDelta().y())
            if abs(event.angleDelta().y()) > 0
            else 1
        )
        direction = event.angleDelta().y() / k

        if event.modifiers() & Qt.KeyboardModifier.ControlModifier:
            self.camera.y -= direction * 20
        else:
            self.__setZoom(direction)

    def mousePressEvent(self, event) -> None:
        self.__mousePress(event)
        self.__updateScene()

    def mouseReleaseEvent(self, event) -> None:
        self.__mouseRelease(event)
        self.__updateScene()

    def mouseMoveEvent(self, event) -> None:
        self.__mouseMove(event)
        self.__updateScene()

    def wheelEvent(self, event) -> None:
        self.__mouseWheel(event)
        self.__updateScene()

    def resizeEvent(self, event) -> None:
        self.scene.setSceneRect(
            QRectF(
                QPointF(0, 0), QSizeF(self.width() - 10, self.height() - 10)
            )
        )
        self.__resizeCamera()
        if self.__needInit():
            self.__initScene()
        self.__updateScene()
