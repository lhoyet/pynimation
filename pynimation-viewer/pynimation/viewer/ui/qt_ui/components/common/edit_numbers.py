from qtpy.QtCore import Qt, QEvent
from qtpy.QtWidgets import (
    QLineEdit,
)


class EditNumbers(QLineEdit):
    def __init__(self):
        QLineEdit.__init__(self)
        self.installEventFilter(self)
        self.isModifiedWidthDigit = False
        self.digitKeys = [
            Qt.Key.Key_0,
            Qt.Key.Key_1,
            Qt.Key.Key_2,
            Qt.Key.Key_3,
            Qt.Key.Key_4,
            Qt.Key.Key_5,
            Qt.Key.Key_6,
            Qt.Key.Key_7,
            Qt.Key.Key_8,
            Qt.Key.Key_9,
            Qt.Key.Key_Enter,
            Qt.Key.Key_Control,
            Qt.Key.Key_V,
            Qt.Key.Key_Z,
        ]

    def eventFilter(self, source, event):
        super().eventFilter(source, event)

        if event.type() == QEvent.Type.KeyPress:
            self.isModifiedWidthDigit = event.key() in self.digitKeys

        return False

    def focusInEvent(self, event):
        self.setSelection(0, self.maxLength())

    def mousePressEvent(self, event):
        self.setSelection(0, self.maxLength())
