import numpy as np


def npArrayContentsCompare(a1: np.ndarray, a2: np.ndarray) -> bool:
    if a1 is None or a2 is None:
        return False
    if len(a1) != len(a2):
        return False
    else:
        length = len(a1)
        for i in range(length):
            if len(a1[i]) != len(a2[i]):
                return False
    return True
