from typing import Any

from qtpy.QtWidgets import (
    QWidget,
    QLayout,
    QVBoxLayout,
    QPushButton,
)


class DatachableWidget(QWidget):
    def __init__(self, childrenWidget, parentLayout, neverRemove=False):
        QWidget.__init__(self)

        self.__neverRemove: bool = neverRemove
        self.childrenWidget: QWidget = childrenWidget
        self.parentLayout: QLayout = parentLayout

        self.__stateButton = QPushButton()
        self.__stateButton.setText("detach")
        self.__stateButton.clicked.connect(self.__setState)

        self.__state: bool = False

        self.__layout: Any = QVBoxLayout()
        self.__layout.setSpacing(0)
        self.__layout.setContentsMargins(0, 0, 0, 0)
        self.__layout.addWidget(self.__stateButton)
        self.__layout.addWidget(self.childrenWidget)
        self.setLayout(self.__layout)

        self.isClosed: bool = False

    def __setState(self) -> None:
        self.__state = not self.__state
        if self.__state:
            newParent: Any = None
            self.setParent(newParent)
            self.show()
            self.move(0, 0)
            self.__stateButton.setText("attach")
        else:
            self.parentLayout.addWidget(self)
            self.__stateButton.setText("detach")

    def closeEvent(self, event) -> None:
        if self.__neverRemove:
            self.__setState()
            event.ignore()
        else:
            self.isClosed = True
