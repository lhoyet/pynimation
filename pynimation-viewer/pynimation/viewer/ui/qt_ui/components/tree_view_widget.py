from typing import Any

from qtpy.QtCore import QTimer
from qtpy.QtWidgets import (
    QTreeWidget,
    QTreeWidgetItem,
    QAbstractItemView,
)

from pynimation.anim.animation import Animation
from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.qt_ui.qt_toolbox import (
    PynimationTools,
    JointData,
)
from pynimation.viewer.ui.qt_ui.components.common.utils import (
    npArrayContentsCompare,
)


class TreeViewWidget(UIComponent, QTreeWidget):
    """
    Show all the joints of all the animations present in the current context.
    You can select multiple joints at the same time.
    """

    class __TreeAnimationItem(QTreeWidgetItem):
        def __init__(self, animation, animId, idJoint, jointName):
            QTreeWidgetItem.__init__(self, QTreeWidgetItem.ItemType.UserType)
            self.animation: Animation = animation
            self.animId: int = animId
            self.id: int = idJoint
            self.name: str = jointName

    def __init__(self):
        QTreeWidget.__init__(self)
        UIComponent.__init__(self, PynimationTools())

        self.setMaximumWidth(800)
        self.setMinimumWidth(0)
        self.setAutoFillBackground(True)
        self.setHeaderHidden(True)
        self.setSelectionMode(
            QAbstractItemView.SelectionMode.ExtendedSelection
        )

        self.__currentAnims = None

    def __jointName(self, node, listMap) -> str:
        result = "[%s] %s" % (node.id, node.name)

        if listMap is not None:
            parts = ",".join(listMap)
            result += " (%s)" % parts

        return result

    def __exploreTree(self, animation, root, node, lastItem, idAnim) -> None:
        listMap = animation.skeleton.partSkeletalSwitcher.get(node.id)
        jointName = self.__jointName(node, listMap)
        if node is root:
            currentItem = lastItem
            currentItem.setText(0, "Animation %s : %s" % ((idAnim), jointName))
        else:
            currentItem = self.__TreeAnimationItem(
                animation,
                idAnim,
                node.id,
                jointName,
            )
            currentItem.setText(0, jointName)
            lastItem.addChild(currentItem)

        if len(node.children) > 0:
            childrens = node.children
            for currentNode in childrens:
                self.__exploreTree(
                    animation, root, currentNode, currentItem, idAnim
                )

    def __initWidget(self) -> None:
        self.clear()
        for i, animation in enumerate(self.__currentAnims):
            node = animation.skeleton.root
            listMap = animation.skeleton.partSkeletalSwitcher.get(node.id)
            jointName = self.__jointName(node, listMap)
            currentRoot = self.__TreeAnimationItem(
                animation,
                i,
                node.id,
                jointName,
            )
            self.addTopLevelItem(currentRoot)
            self.__exploreTree(
                animation,
                node,
                node,
                currentRoot,
                i,
            )

    def __updateWidget(self) -> None:
        if not npArrayContentsCompare(
            self.__currentAnims, self.tools.player.animations
        ):
            self.__currentAnims = self.tools.player.animations.copy()
            self.__initWidget()

    def __getNbFrames(self, animId) -> int:
        result = 0
        for i, anim in enumerate(self.tools.player.animations):
            if animId == i:
                return result
            result = result + anim.nframes
        return result

    def __updateSelection(self) -> None:
        selection: Any = self.selectedItems()
        tempToolsJoint = self.tools.selectedJoints.copy()

        selectedJointId = [
            j.id + self.__getNbFrames(j.animId) for j in selection
        ]
        toolsJointId = [
            j.id + self.__getNbFrames(j.animId)
            for j in self.tools.selectedJoints
        ]

        for j in tempToolsJoint:
            if j.id + self.__getNbFrames(j.animId) not in selectedJointId:
                self.tools.selectedJoints.remove(j)

        for j in selection:
            if j.id + self.__getNbFrames(j.animId) not in toolsJointId:
                self.tools.selectedJoints.append(
                    JointData(j.animation, j.animId, j.id, j.name)
                )

    def keyPressEvent(self, event) -> None:
        super().keyPressEvent(event)
        self.__updateSelection()

    def initUpdateLoop(self) -> None:
        self.timer = QTimer()
        signal: Any = self.timer.timeout
        signal.connect(self.__updateWidget)
        self.timer.start(16)

        itemsChange: Any = self.itemSelectionChanged
        itemsChange.connect(self.__updateSelection)
