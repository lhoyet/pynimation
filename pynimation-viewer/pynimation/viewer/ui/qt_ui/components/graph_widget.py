import numpy as np
import random
from typing import Any, List
from operator import attrgetter

from qtpy.QtCore import (
    Qt,
    QTimer,
    QSize,
)
from qtpy.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QComboBox,
    QLabel,
    QScrollArea,
    QPushButton,
    QRadioButton,
)
from qtpy.QtGui import (
    QColor,
    QBrush,
)

import pyqtgraph as pg

import pynimation.common.defaults as defaults
from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.qt_ui.qt_toolbox import (
    PynimationTools,
    JointData,
)
from pynimation.viewer.ui.qt_ui.components.common.detachable_widget import (
    DatachableWidget,
)
from pynimation.viewer.ui.event import (
    KeyPressEvent,
    KeyHoldEvent,
)


class GraphWidget(UIComponent, QWidget):
    """
    Create a graph widget to visualize the joints data.

    Parameters
    ----------
    selectionChange: pyqtSignal
        the signal is used to determine when the widget needs an update
    """

    class __JointGraphs(QWidget):
        # class which contains multiple graph for a joint
        class __JointGraph(QWidget):
            # class to represent a single graph
            def __init__(
                self,
                joint: JointData,
                graph: pg.PlotWidget,
                legend: pg.LegendItem,
                tools: Any,
            ):
                QWidget.__init__(self)

                self.tools = tools
                self.joint = joint
                self.graph = graph
                self.legend = legend
                self.scaleFramerate: int = 120

                currentId = len(self.legend.items)
                if currentId < len(defaults.COLOR_PALETTE):
                    currentColor = [
                        int(defaults.COLOR_PALETTE[currentId][0] * 255),
                        int(defaults.COLOR_PALETTE[currentId][1] * 255),
                        int(defaults.COLOR_PALETTE[currentId][2] * 255),
                    ]
                else:
                    currentColor = [
                        random.randint(0, 100000) % 255,
                        random.randint(0, 100000) % 255,
                        random.randint(0, 100000) % 255,
                    ]
                self.color = QColor(
                    currentColor[0],
                    currentColor[1],
                    currentColor[2],
                )
                self.menu = QWidget()
                self.selector = QComboBox()
                self.plotItem = None

                representations: List[str] = [
                    "position.x",
                    "position.y",
                    "position.z",
                    "rotation.euler.x",
                    "rotation.euler.y",
                    "rotation.euler.z",
                    "rotation.quat.x",
                    "rotation.quat.y",
                    "rotation.quat.z",
                    "rotation.quat.w",
                    "rotation.rotvec.x",
                    "rotation.rotvec.y",
                    "rotation.rotvec.z",
                ]
                for item in representations:
                    self.selector.addItem(item)

                menuLayout = QHBoxLayout()
                menuLayout.setContentsMargins(0, 0, 0, 0)
                self.menu.setLayout(menuLayout)

                self.radioLocal = QRadioButton("Local")
                self.radioLocal.setChecked(True)
                menuLayout.addWidget(self.radioLocal)

                self.radioGlobal = QRadioButton("Global")
                menuLayout.addWidget(self.radioGlobal)

                # trigger an update when the user click on a button
                signal: Any = self.selector.currentIndexChanged
                signal.connect(self.updateGraphState)
                signal = self.radioLocal.toggled
                signal.connect(self.updateGraphState)
                signal = self.radioGlobal.toggled
                signal.connect(self.updateGraphState)

                layout = QVBoxLayout()
                layout.addWidget(self.menu)
                layout.addWidget(self.selector)
                layout.setContentsMargins(0, 0, 0, 0)

                self.setLayout(layout)

                self.plotItem = pg.PlotDataItem(
                    np.eye(2, 2), name="temp", symbolBrush=QBrush(self.color)
                )
                self.graph.addItem(self.plotItem)
                self.updateGraphState()

            def __convertJointDataToGraphData(
                self, data: np.ndarray
            ) -> np.ndarray:
                return np.stack(
                    [
                        np.arange(len(data))
                        * (
                            self.scaleFramerate
                            / self.joint.animation.framerate
                        ),
                        data,
                    ],
                    axis=1,
                )

            def updateGraphState(self):
                # get the representation of the data (position, rotation etc.)
                representation: str = self.selector.currentText()

                # set the new data in the plot item
                if self.radioGlobal.isChecked():
                    data = attrgetter(representation)(
                        self.joint.animation.globals
                    )
                else:
                    data = attrgetter(representation)(self.joint.animation)
                self.plotItem.setData(
                    self.__convertJointDataToGraphData(
                        data[:, self.joint.id, 0],
                    ),
                )

                # write the legend
                name: str = "animation %s : %s (%s)" % (
                    self.joint.animId,
                    self.joint.name.split("(", 1)[0],
                    representation,
                )
                self.legend.getLabel(self.plotItem).setText(name)

        def __init__(
            self,
            joint: JointData,
            graph: pg.PlotWidget,
            legend: pg.LegendItem,
            tools: PynimationTools,
        ):
            QWidget.__init__(self)

            self.joint = joint
            self.graph = graph
            self.legend = legend
            self.tools = tools

            self.graphList: List[Any] = []

            # configure the layout
            label = QLabel("Animation %s : %s" % (joint.animId, joint.name))

            menu = QWidget()
            menuLayout = QHBoxLayout()
            menu.setLayout(menuLayout)

            self.buttonAdd = QPushButton()
            self.buttonAdd.setText("Add")
            signal: Any = self.buttonAdd.clicked
            signal.connect(self.addGraph)

            self.buttonRemove = QPushButton()
            self.buttonRemove.setText("Remove")
            signal = self.buttonRemove.clicked
            signal.connect(self.removeGraph)

            menuLayout.addWidget(self.buttonAdd)
            menuLayout.addWidget(self.buttonRemove)

            self.listWidget = QWidget()
            self.listLayout: QVBoxLayout = QVBoxLayout()
            self.listWidget.setLayout(self.listLayout)

            layout: QVBoxLayout = QVBoxLayout()
            layout.addWidget(label)
            layout.addWidget(menu)
            layout.addWidget(self.listWidget)
            self.setLayout(layout)

            menuLayout.setContentsMargins(0, 0, 0, 0)
            self.listLayout.setContentsMargins(0, 0, 0, 0)
            layout.setContentsMargins(0, 0, 0, 0)

            self.addGraph()

        def addGraph(self):
            self.graphList.append(
                self.__JointGraph(
                    self.joint,
                    self.graph,
                    self.legend,
                    self.tools,
                )
            )
            self.listLayout.addWidget(self.graphList[-1])

        def removeGraph(self):
            if len(self.graphList) > 0:
                self.graph.removeItem(self.graphList[-1].plotItem)
                self.graphList.remove(self.graphList[-1])
                self.listLayout.itemAt(
                    self.listLayout.count() - 1
                ).widget().deleteLater()

    class __GraphWidget(UIComponent, pg.PlotWidget):
        def __init__(self):
            pg.PlotWidget.__init__(self, background=QColor(74, 74, 74))
            UIComponent.__init__(self, PynimationTools())
            self.scaleFramerate: int = 120
            self.__playerCursor: Any = pg.InfiniteLine([0, 0])
            self.addItem(self.__playerCursor)
            self.setLimits(xMin=0)

        def __updateWidget(self):
            # only update the player cursor when the viewer is on pause
            # due to performance issue with pyqtgraph
            # (it needs to do a graph.update() each time we call this function)
            if (
                self.tools.player.paused
                and self.tools.largestAnimation is not None
            ):
                self.__playerCursor.setValue(
                    [
                        (self.tools.player.time * self.scaleFramerate)
                        % (
                            self.tools.largestAnimation.duration
                            * self.scaleFramerate
                        ),
                        0,
                    ]
                )

        # pg.PlotWidget implementation
        def eventFilter(self, source, event):
            # set a filter to translate qt event in pynimation event
            self.bindings.handleEvent(self.tools.eventTranslator(event))
            return super().eventFilter(source, event)

        def sizeHint(self):
            return QSize(self.size().width(), 100)

        # UIComponent implementation
        def bindingsInit(self) -> None:
            pass

        def initUpdateLoop(self) -> None:
            self.timer = QTimer()
            signal: Any = self.timer.timeout
            signal.connect(self.__updateWidget)
            self.timer.start(320)

    def __init__(self, selectionChange: Any):
        QWidget.__init__(self)
        UIComponent.__init__(self, PynimationTools())

        self.setAttribute(
            Qt.WidgetAttribute.WA_StyledBackground,
            True,
        )
        self.installEventFilter(self)

        self.__initGraph()
        self.__initLayout()
        self.__initSignal()

        self.__selectionChange: Any = selectionChange
        self.__joints: List[JointData] = []
        self.__notJoints: List[JointData] = []
        self.__andJoints: List[JointData] = []
        self.__jointGraphsList: List[Any] = []

    # initialization
    def __initGraph(self) -> None:
        self.__graph: pg.PlotWidget = self.__GraphWidget()
        self.__graph.setLabel("left", "values")
        self.__graph.setLabel("bottom", "frames")
        self.__legend: pg.LegendItem = self.__graph.addLegend()

    def __initLayout(self) -> None:
        self.__layout: Any = QVBoxLayout()
        self.__layout.setSpacing(0)
        self.__layout.setContentsMargins(0, 0, 0, 0)

        self.__jointsList = QWidget()
        self.__jointsListLayout = QVBoxLayout()
        self.__jointsListLayout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.__jointsList.setLayout(self.__jointsListLayout)
        self.__scroller = QScrollArea()
        self.__scroller.setWidgetResizable(True)
        self.__scroller.setFixedWidth(400)
        self.__scroller.setWidget(self.__jointsList)

        self.__jointsAndGraph = QWidget()
        jointsAndGraphLayout = QHBoxLayout()
        jointsAndGraphLayout.setContentsMargins(0, 0, 0, 0)
        self.__jointsAndGraph.setLayout(jointsAndGraphLayout)

        jointsAndGraphLayout.addWidget(self.__graph)
        jointsAndGraphLayout.addWidget(self.__scroller)

        self.__detach = DatachableWidget(
            self.__jointsAndGraph, self.__layout, True
        )

        menuWidget = QWidget()
        menuLayout = QHBoxLayout()
        menuLayout.setContentsMargins(0, 0, 0, 0)
        menuWidget.setLayout(menuLayout)

        label = QLabel("Scale the x axis : ")
        menuLayout.addWidget(label)

        self.__framerateSelection = QComboBox()
        menuLayout.addWidget(self.__framerateSelection)

        self.__jointsListLayout.addWidget(menuWidget)

        self.__layout.addWidget(self.__detach)
        self.setLayout(self.__layout)

    def __initSignal(self) -> None:
        signal: Any = self.__framerateSelection.currentIndexChanged
        signal.connect(self.__updateFramerateScale)

    # functions
    def __updateGraphData(self) -> None:
        tempJointGraphsList = self.__jointGraphsList.copy()

        # remove missing jointGraphs
        for jointGraphs in tempJointGraphsList:
            if not (jointGraphs.joint in self.__andJoints):
                self.__jointGraphsList.remove(jointGraphs)
                for i in range(jointGraphs.listLayout.count()):
                    jointGraphs.removeGraph()
                jointGraphs.setParent(None)
                jointGraphs.deleteLater()

        # add new jointGraphs
        for joint in self.__notJoints:
            jointGraphs = self.__JointGraphs(
                joint,
                self.__graph,
                self.__legend,
                self.tools,
            )
            self.__jointGraphsList.append(jointGraphs)
            self.__jointsListLayout.addWidget(self.__jointGraphsList[-1])

    def __updateFrameratesSelector(self) -> None:
        self.__framerateSelection.clear()
        framerates: List[str] = []
        currentValue: str = ""
        for joint in self.__joints:
            currentValue = "framerate: %s" % joint.animation.framerate
            if not (currentValue in framerates):
                framerates.append(currentValue)
        framerates.sort()
        for item in framerates:
            self.__framerateSelection.addItem(item)

    def __updateFramerateScale(self) -> None:
        split: List[str] = self.__framerateSelection.currentText().split(
            "framerate: "
        )
        if len(split) > 1:
            scaleFramerate: int = int(split[1])
            self.__graph.setLabel(
                "bottom", "frames (base framerate : %sfps)" % scaleFramerate
            )
            self.__graph.scaleFramerate = scaleFramerate
            for graphs in self.__jointGraphsList:
                for graph in graphs.graphList:
                    graph.scaleFramerate = scaleFramerate
                    graph.updateGraphState()

    def __updateWidget(self) -> None:
        if self.__joints != self.tools.selectedJoints:
            # get the XOR list between the current selected joints and the old one
            self.__notJoints = [
                n for n in self.tools.selectedJoints if n not in self.__joints
            ]
            # get the AND list between the current selected joints and the old one
            self.__andJoints = [
                n for n in self.tools.selectedJoints if n in self.__joints
            ]
            self.__updateGraphData()
            self.__joints = self.tools.selectedJoints.copy()
            self.__updateFrameratesSelector()

    def eventFilter(self, source, event):
        # set a filter to translate qt event in pynimation event
        self.bindings.handleEvent(self.tools.eventTranslator(event))
        return super().eventFilter(source, event)

    # UIComponent implementation
    def bindingsInit(self) -> None:
        # default bindings
        # KeyPress
        self.bindings.bind(KeyPressEvent(Qt.Key.Key_L, name="l"), "nextFrame")
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_H, name="h"), "previousFrame"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_K, name="k"), "increaseSpeed"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_J, name="j"), "decreaseSpeed"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Space, name="space"), "pauseplay"
        )
        # KeyHold
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_L), name="l"), "nextFrame"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_H), name="h"), "previousFrame"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_K), name="k"), "increaseSpeed"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_J), name="j"), "decreaseSpeed"
        )

        # custom bindings
        # KeyPress
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Right, name="right"), "nextFrame"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Left, name="left"), "previousFrame"
        )
        # KeyHold
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_Right), name="right"), "nextFrame"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_Left), name="left"), "previousFrame"
        )

    def initUpdateLoop(self) -> None:
        self.__selectionChange.connect(self.__updateWidget)
        self.__graph.initUpdateLoop()
