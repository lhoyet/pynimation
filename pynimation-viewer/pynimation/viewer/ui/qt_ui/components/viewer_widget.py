from typing import Any

from qtpy.QtCore import Qt, QEvent
from qtpy.QtWidgets import QOpenGLWidget

from pynimation.common.vec import Vec2
from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.qt_ui.qt_toolbox import PynimationTools
from pynimation.viewer.ui.event import (
    KeyPressEvent,
    KeyHoldEvent,
    KeyReleaseEvent,
    MouseClickEvent,
    MouseClickReleaseEvent,
    MouseMoveEvent,
    MouseWheelEvent,
)


class ViewerWidget(UIComponent, QOpenGLWidget):
    """
    Base component of the interface.
    It permits to display characters animations in a 3D scene.
    """

    def __init__(self):
        # init
        QOpenGLWidget.__init__(self)
        UIComponent.__init__(self, PynimationTools())

        self.setMouseTracking(True)
        # enable keyboard events
        self.setFocusPolicy(Qt.FocusPolicy.StrongFocus)
        # events management
        self.installEventFilter(self)

    # QOpenGLWidget implementation
    def initializeGL(self):
        self.tools.viewport.initialize()

    def resizeGL(self, w, h):
        self.tools.viewport.resize(self.width(), self.height())

    def paintGL(self):
        self.tools.viewer.processFrame()

    def eventFilter(self, source, event):
        if event.type() == QEvent.Type.MouseMove:
            self.tools.mouse.pos = Vec2(
                [event.pos().x(), event.pos().y()], dtype=int
            )

        self.bindings.handleEvent(self.tools.eventTranslator(event))
        return super().eventFilter(source, event)

    # UIComponent implementation
    def bindingsInit(self) -> None:
        # KeyPress
        self.bindings.bind(KeyPressEvent(Qt.Key.Key_S, name="s"), "screenshot")
        self.bindings.bind(KeyPressEvent(Qt.Key.Key_R, name="r"), "recording")
        self.bindings.bind(KeyPressEvent(Qt.Key.Key_L, name="l"), "nextFrame")
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_H, name="h"), "previousFrame"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_K, name="k"), "increaseSpeed"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_J, name="j"), "decreaseSpeed"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Space, name="space"), "pauseplay"
        )
        self.bindings.bind(KeyPressEvent(Qt.Key.Key_Left, name="left"), "left")
        self.bindings.bind(KeyPressEvent(Qt.Key.Key_Up, name="up"), "forward")
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Down, name="down"), "backward"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_PageUp, name="page up"), "up"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_PageDown, name="page down"), "down"
        )
        # KeyHold
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_L), name="l"), "nextFrame"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_H), name="h"), "previousFrame"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_K), name="k"), "increaseSpeed"
        )
        self.bindings.bind(
            KeyHoldEvent(str(Qt.Key.Key_J), name="j"), "decreaseSpeed"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_Right, name="right"), "right"
        )
        self.bindings.bind(
            KeyPressEvent(Qt.Key.Key_C, name="c"), "cameraThirdPerson"
        )
        # KeyRelease
        self.bindings.bind(KeyReleaseEvent(Qt.Key.Key_Right), "stopMoveAxisX")
        self.bindings.bind(KeyReleaseEvent(Qt.Key.Key_Left), "stopMoveAxisX")
        self.bindings.bind(KeyReleaseEvent(Qt.Key.Key_Up), "stopMoveAxisZ")
        self.bindings.bind(KeyReleaseEvent(Qt.Key.Key_Down), "stopMoveAxisZ")
        self.bindings.bind(KeyReleaseEvent(Qt.Key.Key_PageUp), "stopMoveAxisY")
        self.bindings.bind(
            KeyReleaseEvent(Qt.Key.Key_PageDown), "stopMoveAxisY"
        )
        # Mouse
        self.bindings.bind(
            MouseClickEvent(Qt.MouseButton.RightButton, name="right"),
            "cameraRotationTrigger",
        )
        self.bindings.bind(
            MouseClickReleaseEvent(Qt.MouseButton.RightButton),
            "cameraRotationRelease",
        )
        self.bindings.bind(MouseMoveEvent(), "cameraRotation")
        self.bindings.bind(MouseWheelEvent(1.0), "cameraZoom")
        self.bindings.bind(MouseWheelEvent(-1.0), "cameraDezoom")

    def initUpdateLoop(self) -> None:
        signal: Any = self.frameSwapped
        signal.connect(self.update)
