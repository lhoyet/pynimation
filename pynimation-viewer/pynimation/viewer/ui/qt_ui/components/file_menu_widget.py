import traceback
from typing import (
    # Any,
    List,
)

from qtpy.QtWidgets import (
    QWidget,
    QGridLayout,
    QPushButton,
    QFileDialog,
)

from pynimation.anim.animation import Animation
from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.qt_ui.qt_toolbox import PynimationTools


class FileMenuWidget(UIComponent, QWidget):
    """
    This goal of this menu is to manage the animations.
    """

    def __init__(self):
        QWidget.__init__(self)
        UIComponent.__init__(self, PynimationTools())

        self.__importButton = QPushButton()
        self.__removeButton = QPushButton()
        self.__exportButton = QPushButton()

        self.__importButton.setText("Import")
        self.__removeButton.setText("Remove")
        self.__exportButton.setText("Export")

        self.__importButton.clicked.connect(self.importAnimations)
        self.__removeButton.clicked.connect(self.removeAnimations)
        self.__exportButton.clicked.connect(self.exportAnimations)

        layout = QGridLayout()
        layout.addWidget(self.__importButton, 0, 0)
        layout.addWidget(self.__removeButton, 0, 1)
        layout.addWidget(self.__exportButton, 0, 2)

        self.setLayout(layout)

    def __animationsSelected(self) -> List[Animation]:
        # remove duplicate
        return list(set([n.animation for n in self.tools.selectedJoints]))

    def __importAnimations(self, paths: List[str]) -> None:
        animations = []
        for path in paths:
            try:
                animations.append(Animation.load(path))
            except NotImplementedError:
                traceback.print_exc()
                self.importAnimations()
        if len(animations) > 0:
            self.tools.viewer.addAnimations(animations)

    def importAnimations(self) -> None:
        """
        Open a dialog box to choose the animations you want to import
        """
        path = QFileDialog.getOpenFileNames(
            self,
            "Import a file",
            "",
            "AllFile (*.*);; BVH (*.bvh);; FBX (*.fbx);; NPZ (*.npz);; MVNX (*.mvnx);; PYNIM (*.pynim)",
        )

        if path != ("", ""):
            self.__importAnimations(path[0])

    def removeAnimations(self) -> None:
        animationsSelected = self.__animationsSelected()
        self.tools.selectedJoints.clear()
        self.tools.viewer.removeAnimations(animationsSelected)

    def __exportAnimation(self, animation: Animation) -> None:
        path = QFileDialog.getSaveFileName(
            self, "Export a file", "", ".fbx;; .npz;; .pynim"
        )

        if path != "":
            try:
                animation.save("%s%s" % (path[0], path[1]))
            except NotImplementedError:
                traceback.print_exc()

    def exportAnimations(self) -> None:
        """
        Open a dialog to choose where the animations you want to export will be
        """
        animationsSelected = self.__animationsSelected()

        for animation in animationsSelected:
            self.__exportAnimation(animation)

    def updateWidget(self) -> None:
        pass

    def bindingsInit(self) -> None:
        pass

    def initUpdateLoop(self) -> None:
        pass
