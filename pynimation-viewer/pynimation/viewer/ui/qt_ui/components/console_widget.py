import importlib
from typing import Dict, List, Any

from qtpy.QtCore import Qt
from qtpy.QtWidgets import (
    QWidget,
    QVBoxLayout,
)

from pyqtconsole.console import PythonConsole
import pyqtconsole.highlighter as hl

from pynimation.common.meta import _get_classes_rec
from pynimation.viewer.character import Character
from pynimation.viewer.ui.ui_component import UIComponent
from pynimation.viewer.ui.qt_ui.qt_toolbox import PynimationTools
from pynimation.viewer.ui.qt_ui.components.common.detachable_widget import (
    DatachableWidget,
)

try:  # PyQt >= 5.11
    QueuedConnection = Qt.ConnectionType.QueuedConnection  # type: ignore
except AttributeError:  # PyQt < 5.11
    QueuedConnection = Qt.QueuedConnection  # type: ignore


class ConsoleWidget(UIComponent, QWidget):
    """
    Python console integrated in the interface
    with every contextual data you need in order
    to use the PyNimation library within it.
    """

    def __init__(self):
        QWidget.__init__(self)
        UIComponent.__init__(self, PynimationTools())

        self.setAttribute(
            Qt.WidgetAttribute.WA_StyledBackground,
            True,
        )

        self.commands: Dict[str, Any] = {
            "tools": self.tools,
            "viewer": self.tools.viewer,
            "player": self.tools.player,
            "animations": [],
            "characters": [],
        }

        self.__initConsole()
        self.__initStyle()
        self.__initLayout()

    # initialization
    def __initConsole(self) -> None:
        self.console = PythonConsole(
            formats={
                "keyword": hl.format("#e3b96b", "bold"),
                "operator": hl.format("#fff87a"),
                "brace": hl.format("silver"),
                "defclass": hl.format("#cc9074", "bold"),
                "string": hl.format("#45c456"),
                "string2": hl.format("#45c456"),
                "comment": hl.format("grey", "italic"),
                "self": hl.format("#ff7a88", "italic"),
                "numbers": hl.format("#f59551"),
                "inprompt": hl.format("#cc9074", "bold"),
                "outprompt": hl.format("#ff7a88", "bold"),
            }
        )
        self.console.edit.zoomIn(5)
        for name, obj in self.commands.items():
            self.console.push_local_ns(name, obj)
        self.helpStr = "help"
        # set the help function in the console
        self.console.push_local_ns(self.helpStr, self.helpFunc)
        # set all classes of the pynimation api in the console
        self.addAllPackageClasses("pynimation")
        self.addAllPackageClasses("pynimation.viewer")
        self.console.interpreter.exec_signal.connect(self.updateContext)
        self.console._insert_output_text(self.greeting())
        self.console._more = False
        self.console._update_ps(False)
        self.console._show_ps()
        viewer = PynimationTools().viewer

        def exec_(*args, **kwargs):
            with viewer.inContext() as _:
                ret = self.console.interpreter.exec_(*args, **kwargs)
            return ret

        self.console.interpreter.exec_signal.connect(exec_, QueuedConnection)

    def __initStyle(self) -> None:
        self.console.setStyleSheet(
            """
                background:#4a4a4a;
                color:white;
            """
        )

    def __initLayout(self) -> None:
        self.layout: Any = QVBoxLayout()
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.detach = DatachableWidget(self.console, self.layout, True)
        self.layout.addWidget(self.detach)
        self.setLayout(self.layout)

    # functions
    def greeting(self) -> str:
        return (
            "Hello and welcome to PyNimation ! This console is a python console where you can use all the classes and objects of the current context of pynimation. Type %s() if you need help."
            % self.helpStr
        )

    def helpFunc(self) -> None:
        print("Here are the current objects you can use :")
        for name, obj in self.commands.items():
            if isinstance(obj, List):
                print("\t%s : List of all the current %s" % (name, name))
            else:
                print("\t%s : Object" % name)
        print("You can use all the classes provided by the pynimation api.")

    def addAllPackageClasses(self, module: str) -> None:
        allClasses: List[Dict[str, str]] = []
        _get_classes_rec(module, allClasses)
        for currentClass in allClasses:
            self.console.push_local_ns(
                currentClass["name"],
                getattr(
                    importlib.import_module(currentClass["module"]),
                    "%s" % (currentClass["name"]),
                ),
            )

    def updateContext(self) -> None:
        scene = self.tools.viewer.currentScene
        self.console.push_local_ns("animations", self.tools.player.animations)
        self.console.push_local_ns(
            "characters",
            (
                [n for n in scene.graph.nodes if isinstance(n, Character)]
                if scene is not None
                else []
            ),
        )

    # UIComponent implementation
    def updateWidget(self) -> None:
        pass

    def bindingsInit(self) -> None:
        pass

    def initUpdateLoop(self) -> None:
        pass
