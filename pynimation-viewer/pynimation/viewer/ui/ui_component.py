import abc

from .toolbox import ToolBox
from pynimation.viewer.ui.event import BindingManager


class UIComponent:
    def __init__(self, tools: ToolBox):
        self.tools: ToolBox = tools
        self.bindings: BindingManager = BindingManager(self.tools.actions)
        self.bindingsInit()

    @abc.abstractmethod
    def bindingsInit(self) -> None:
        """
        Initialization of the bindings here
        """
        pass

    @abc.abstractmethod
    def initUpdateLoop(self) -> None:
        """
        Execution of the component computation here
        """
        pass
