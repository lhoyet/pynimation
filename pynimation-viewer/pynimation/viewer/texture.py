from OpenGL import GL
from PIL import Image
import numpy as np


class Texture:
    """
    Encapsulate an OpenGL texture object

    Parameters
    ----------
    filename: str
        path to the texture image

    Parameters
    ----------
    image: pg.image
        pygame image object of the texture image
    imageData: bytes
        RGBA buffer of the texture image
    texID: int
        OpenGL internal texture ID
    """

    def __init__(self, filename: str) -> None:
        self.image = Image.open(filename).convert("RGBA")
        width, height = self.image.size
        self.imageData = np.array(list(self.image.getdata()), np.uint8)

        self.texID = GL.glGenTextures(1)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
        GL.glTexImage2D(
            GL.GL_TEXTURE_2D,
            0,
            GL.GL_RGBA,
            width,
            height,
            0,
            GL.GL_RGBA,
            GL.GL_UNSIGNED_BYTE,
            self.imageData,
        )
        GL.glTexParameteri(
            GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR
        )
        GL.glTexParameteri(
            GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR
        )
        GL.glTexParameteri(
            GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT
        )
        GL.glTexParameteri(
            GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT
        )
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)

    def use(self, textureUnit: int) -> None:
        """
        Use this texture as OpenGL `textureUnit`

        Parameters
        ----------
        textureUnit:
        """
        GL.glActiveTexture(GL.GL_TEXTURE0 + textureUnit)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texID)
