from functools import partial

from pynimation.viewer.ui.action import Action, ActionManager


def computation() -> int:
    return 1


def harderComputation() -> int:
    return 2


def testGetAction():
    actionTest = Action(partial(computation))
    actionManager = ActionManager({"action": actionTest})

    assert actionTest is actionManager.get("action")


def testAddAction():
    actionTest = Action(partial(computation))
    actionManager = ActionManager({})
    actionManager.add("action", computation)

    assert actionTest == actionManager.get("action")


def testRemoveAction():
    actionTest = Action(partial(computation))
    actionManager = ActionManager({"action": actionTest})
    actionManager.remove("action")

    assert ("action", actionTest) not in actionManager.actions


def testModifyAction():
    actionTest = Action(partial(computation))
    actionTestHard = Action(partial(harderComputation))
    actionManager = ActionManager({"action": actionTest})
    actionManager.modify("action", harderComputation)

    assert actionTestHard == actionManager.get("action")
