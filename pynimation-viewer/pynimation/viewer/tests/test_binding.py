from functools import partial

from pynimation.viewer.ui.action import Action, ActionManager
from pynimation.viewer.ui.event import (
    KeyPressEvent,
    KeyReleaseEvent,
    BindingManager,
)


def computation() -> int:
    return 1


def harderComputation() -> int:
    return 2


def defaultActions():
    return {
        "computation": Action(partial(computation)),
        "harderComputation": Action(partial(harderComputation)),
    }


def testBind():
    event = KeyPressEvent()
    actionManager = ActionManager(defaultActions())
    bindingManager = BindingManager(actionManager)
    bindingManager.bind(event, "computation")

    result = (event, [actionManager.get("computation")])

    assert result in bindingManager.bindings


def testBindMultipleAction():
    event = KeyPressEvent()
    actionManager = ActionManager(defaultActions())
    bindingManager = BindingManager(actionManager)
    bindingManager.bindMultipleAction(
        event, ["computation", "harderComputation"]
    )

    result = (
        event,
        [
            actionManager.get("computation"),
            actionManager.get("harderComputation"),
        ],
    )

    assert result in bindingManager.bindings


def testUnbind():
    event = KeyPressEvent()
    actionManager = ActionManager(defaultActions())
    bindingManager = BindingManager(actionManager)
    bindingManager.bind(event, "computation")
    bindingManager.unbind(event)

    result = (event, [actionManager.get("computation")])

    assert result not in bindingManager.bindings


def testRebind():
    event = KeyPressEvent()
    newEvent = KeyReleaseEvent()
    actionManager = ActionManager(defaultActions())
    bindingManager = BindingManager(actionManager)
    bindingManager.bindMultipleAction(
        event, ["computation", "harderComputation"]
    )
    bindingManager.rebind(event, newEvent)

    result = (
        newEvent,
        [
            actionManager.get("computation"),
            actionManager.get("harderComputation"),
        ],
    )
    oldResult = (
        event,
        [
            actionManager.get("computation"),
            actionManager.get("harderComputation"),
        ],
    )

    assert (
        result in bindingManager.bindings
        and oldResult not in bindingManager.bindings
    )
