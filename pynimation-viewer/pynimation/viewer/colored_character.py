from typing import List, Optional, Union, cast

import numpy as np

import pynimation.common.defaults as defaults
from pynimation.anim.animation import Animation
from pynimation.anim.joint import Joint
from .stickfigure import StickFigure
from .character import Character
from pynimation.viewer.objects.sphere import Sphere


class ColoredCharacter(Character):
    """
    :class:`~pynimation.viewer.stickfigure.StickFigure` animated
    :class:`~pynimation.viewer.character.Character` whose joints can
    independently be colored for each frame
    """

    def __init__(
        self,
        animation: Animation,
        colors: Optional[np.ndarray] = None,
        color: Optional[np.ndarray] = None,
        *args,
        **kwargs
    ):
        """
        Parameters
        ----------
        animation: Animation
            animation of the character
        meshes: List[SceneNode]
            animated meshes, must be subclass of
            :class:`~pynimation.viewer.animatable.Animatable`
        colors: np.ndarray
            Array of colors (RGBA) that will be applied to joints for every frame.
            Shape should be (frame_count, joint_count, 4)
        color: np.ndarray
            default RGBA color of this character, will be applied to every frame
            and joint
        """
        colorsShape = (
            animation.nframes,
            animation.njoints,
            4,
        )
        defaultColor = color or defaults.COLOR_PALETTE[0]
        defaultColor = np.array(defaultColor)
        if colors is None:
            self.colors: np.ndarray = np.full(
                colorsShape,
                defaultColor,
            )
            """
            default RGBA color of this character, will be applied to every frame
            and joint
            """
        else:
            if colors.shape != colorsShape:
                raise ValueError(
                    "colors argument of ColoredCharacter should "
                    + "have shape %s instead of %s"
                    % (str(colorsShape), str(colors.shape))
                )
            else:
                self.colors = colors
        super().__init__(
            animation,
            [StickFigure(animation.skeleton, defaultColor.tolist())],
            *args,
            **kwargs
        )
        assert len(self.children) == 1
        self.stickFigure: StickFigure = self.children[0]
        """
        StickFigure mesh of this character
        """

    def animate(self) -> None:
        fid = self.player.getCurrentFrameId(self.animation)
        self.stickFigure.jointColors = self.colors[fid]
        if hasattr(self, "_spheres"):
            for i, sphere in enumerate(self._spheres):
                sphere.color = self._colors[fid, i]
        super().animate()

    def colorSupportsJoints(
        self,
        supports: np.ndarray,
        supportsColors: Optional[np.ndarray] = None,
        coloredJoints: Optional[
            Union[List[List["Joint"]], List[List[int]]]
        ] = None,
    ):
        """
        Color each set of `coloredJoints` in `supportsColor`
        during the corresponding set of `supports` phases.

        Parameters
        ----------
        supports:
            Array containing masks (boolean arrays) for frames corresponding
            to a support phase for each support

        supportsColors:
            Either single RGBA color which will be applied to all colored
            joints during their support phases

            Or an array of colors, one for each `coloredJoints`, that
            will be applied to them during their support phases

        coloredJoints:
            For each support, a list of joints or joint ids that will be
            colored during support phases

            Will default to whole left and right legs if length of first
            dimension of `supports` is 2

        """

        defaultColor = self.stickFigure.color

        if supportsColors is None:
            supportsColors = np.array(
                [1.0 - c for c in defaultColor[:-1]] + [1.0]
            )
        if len(supportsColors.shape) == 1:
            supportsColors = np.full(
                (supports.shape[0], supportsColors.shape[0]), supportsColors
            )
        elif supportsColors.shape[0] != supports.shape[0]:
            raise ValueError(
                "There should be as much supportsColors as there "
                + " are supports, had %d supportsColors and %d supports"
                % (supportsColors.shape[0], supports.shape[0])
            )

        if coloredJoints is None:
            if supports.shape[0] != 2:
                raise ValueError(
                    "coloredJoints must be supplied if supports.shape[0] != 2"
                )
            else:
                coloredJoints = [
                    [j.id for j in hip.descendants(inclusive=True)]
                    for hip in self.animation.skeleton.hips
                ]
        else:
            if isinstance(coloredJoints[0][0], Joint):
                listJoints: List[List[Joint]] = cast(
                    List[List[Joint]], coloredJoints
                )
                coloredJoints = [
                    [j.id for j in support] for support in listJoints
                ]

            if len(coloredJoints) != supports.shape[0]:
                raise ValueError(
                    "There should be as much coloredJoints as there "
                    + " are supports, had %d coloredJoints and %d supports"
                    % (len(coloredJoints), supports.shape[0])
                )

        colorsShape = (
            self.animation.nframes,
            self.animation.njoints,
            4,
        )

        self.colors = np.full(colorsShape, defaultColor)

        for supportsFoot, jointsFoot, supportColorFoot in zip(
            supports, coloredJoints, supportsColors
        ):
            for i, support in enumerate(supportsFoot):
                if support:
                    self.colors[i, jointsFoot, :] = supportColorFoot

    def colorSupportsSpheres(
        self,
        supports: np.ndarray,
        joints: Optional[Union[List["Joint"], List[int]]] = None,
        colors: Optional[np.ndarray] = None,
        defaultColors: Optional[np.ndarray] = None,
        radius: float = 0.02,
    ):
        """
        Add spheres at the ends of each `joints` and
        color them in `colors` during the corresponding
        set of `supports` phases.

        Parameters
        ----------
        supports:
            Array containing masks (boolean arrays) for frames corresponding
            to a support phase for each support

        joints:
            Joints to attach spheres to

            Support phases of these joints are described by `supports`

            Will default to left and right toes if length of first
            dimension of `supports` is 2

            Will default to [leftToe, leftAnkle, rightToe, rightAnkle]
            dimension of `supports` is 4


        colors:
            Either single RGBA color, for all spheres, or an array of colors,
            one for each sphere, that will be applied to them during the support
            phases of their corresponding joint

        defaultColors:
            Either single RGBA color, for all spheres, or an array of colors,
            one for each sphere, that will be applied to them during by default
            and overwritten by `colors` during support phases

        radius:
            Radius of the Spheres, defaults to 0.05

        """

        defaultColor = self.stickFigure.color

        if colors is None:
            colors = np.array([1.0 - c for c in defaultColor[:-1]] + [1.0])

        if len(colors.shape) == 1:
            colors = np.full((supports.shape[0], colors.shape[0]), colors)
        elif colors.shape[0] != supports.shape[0]:
            raise ValueError(
                "If there is more than one colors"
                + "there should be as much colors as there "
                + " are supports, had %d colors and %d supports"
                % (colors.shape[0], supports.shape[0])
            )

        if defaultColors is None:
            defaultColors = np.array(defaultColor)

        if len(defaultColors.shape) == 1:
            defaultColors = np.full(
                (supports.shape[0], defaultColors.shape[0]),
                defaultColors,
            )
        elif defaultColors.shape[0] != supports.shape[0]:
            raise ValueError(
                "If there is more than one defaultColors"
                + "there should be as much defaultColors as there "
                + " are supports, had %d defaultColors and %d supports"
                % (defaultColors.shape[0], supports.shape[0])
            )

        joints_: List["Joint"]
        if joints is None:
            if supports.shape[0] not in [2, 4]:
                raise ValueError(
                    "joints must be supplied if supports.shape[0] != 2"
                )
            else:
                if supports.shape[0] == 2:
                    joints_ = list(self.animation.skeleton.toes)
                else:
                    s = self.animation.skeleton
                    joints_ = [
                        s.leftToe,
                        s.leftAnkle,
                        s.rightToe,
                        s.rightAnkle,
                    ]

        else:
            if isinstance(joints[0], int):
                listJoints: List[int] = cast(List[int], joints)
                joints_ = [self.animation.joints[j] for j in listJoints]
            else:
                joints_ = joints

            if len(joints) != supports.shape[0]:
                raise ValueError(
                    "There should be as much joints as there "
                    + " are supports, had %d joints and %d supports"
                    % (len(joints), supports.shape[0])
                )

        animlen = self.animation.nframes

        self._colors: np.ndarray = np.array(
            [
                np.full((animlen, 4), defaultColor)
                for defaultColor in defaultColors
            ]
        ).swapaxes(0, 1)

        self._spheres: List[Sphere] = []

        for i, (support, sphereJoint, sphereColor, defaultColor) in enumerate(
            zip(supports, joints_, colors, defaultColors)
        ):
            sphere = Sphere(radius=radius, color=defaultColor)
            self.stickFigure.children[sphereJoint.id].addChild(sphere)
            self._spheres.append(sphere)
            for j, supportFrame in enumerate(support):
                if supportFrame:
                    self._colors[j, i, :] = sphereColor
