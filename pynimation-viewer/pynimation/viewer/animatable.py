import abc

import numpy as np


class Animatable(abc.ABC):
    """
    Base class for animatable objects
    """

    @abc.abstractmethod
    def animate(self, frame: np.ndarray) -> None:
        """
        Animate this object with the transforms from `frame`

        Parameters
        ----------
        frame:
            frame to animate this object with
        """
        pass
