import setuptools

# with open("../README.md", "r") as fh:
#     long_description = fh.read()

setuptools.setup(
    name="pynimation",
    version="2.1",
    author="Ludovic Hoyet",
    author_email="ludovic.hoyet@inria.fr",
    description="3D character animation framework in python",
    # long_description=long_description,
    # long_description_content_type="text/markdown",
    url="https://gitlab.inria.fr/lhoyet/pynimation",
    packages=setuptools.find_namespace_packages(
        include=["pynimation.*", "pynimation"],
    ),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: GNU Affero General Public License v3",
    ],
    license="GNU Affero General Public License v3",
    include_package_data=True,
    python_requires=">=3.7",
    install_requires=[
        "fbxsdkpy>=2020.1.post2",
        "numpy",
        "scipy",
        "reactivex",
        "sip",
    ],
)
