import os
import sys
import importlib
from unittest.mock import create_autospec
from typing import Dict, List

# mock test
from pynimation.viewer.viewport import Viewport
from pynimation.viewer.player import Player
from pynimation.viewer.mouse import MouseManager
from pynimation.viewer.ui.action import ActionManager
from pynimation.viewer.ui.default_actions import getDefaultActions
from pynimation.viewer.ui.event import BindingManager
from pynimation.common.meta import _get_classes_rec


BINDINGS_DOCS_FILE = "default_bindings.rst"
BINDINGS_DOCS_HEADER = """
Default bindings
================

Here is the list of the default bindings for all the PyNimation components :

"""
BINDINGS_QT_HEADER = """
Qt Bindings
-----------"""
BINDINGS_PYGAME_HEADER = """
Pygame Bindings
---------------"""

repo_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, os.path.abspath(repo_dir))
sys.path.insert(0, os.path.abspath(repo_dir + "/pynimation-viewer"))

bindings_docs_path = repo_dir + "/docs/source/" + BINDINGS_DOCS_FILE


class MockTools:
    def __init__(self):
        self.mouse = create_autospec(MouseManager)()
        self.viewport = create_autospec(Viewport)()
        self.player = create_autospec(Player)()
        self.actions = ActionManager(getDefaultActions(self))

    def __getattr__(self, name):
        return None


class MockApp:
    def __init__(self, tools):
        self.tools = tools
        self.window = None
        self.bindingsDescription = None

    def quit(self):
        pass

    def __getattr__(self, name):
        return None


class MockComponent:
    def __init__(self, actions):
        self.bindings = BindingManager(actions)

    def __getattr__(self, name):
        return None


def getAttrAug(cls, attr):
    try:
        return getattr(cls, attr)
    except AttributeError:
        return None


def generateRows(dict) -> str:
    ret = ""
    for a, b in dict.items():
        ret = "%s\n   * - %s\n     - %s" % (ret, a, b)
    return ret


def generateArray(bindingsDesc) -> str:
    table = ""

    for component, descDict in bindingsDesc.items():
        table = (
            "%s\n\n.. list-table:: Default bindings of the %s\n   :widths: 50 50\n   :header-rows: 1\n"
            % (table, component)
        )
        table = "\n%s  %s" % (
            table,
            generateRows({"Actions of %s" % component: "Events"}),
        )
        table = "%s %s" % (table, generateRows(descDict))

    return table


def generate_dict(implem):
    pathApp = "pynimation.viewer.ui.%s_ui.%s_application" % (implem, implem)
    pathComponents = "pynimation.viewer.ui.%s_ui.components" % implem

    QtApp = getattr(
        importlib.import_module(pathApp),
        "PynimationApp",
    )

    tools = MockTools()
    app = MockApp(tools)

    allClasses: List[Dict[str, str]] = []
    _get_classes_rec(pathComponents, allClasses)

    allDesc: Dict[str, Dict[str, str]] = {}

    for className in allClasses:
        currentClass = getAttrAug(
            importlib.import_module(className["module"]),
            "%s" % (className["name"]),
        )

        if getAttrAug(currentClass, "bindingsInit") is not None:
            QtApp.actionsInit(app)
            component = MockComponent(app.tools.actions)
            currentClass.bindingsInit(component)
            descDict = component.bindings.description

            if len(descDict) > 0:
                allDesc[className["name"]] = descDict

    return allDesc


def generate_bindings_docs():
    qtBindings = generate_dict("qt")
    pygameBindings = generate_dict("pygame")

    bindingsDocsQt = "%s%s" % (BINDINGS_QT_HEADER, generateArray(qtBindings))
    bindingsDocsPygame = "%s%s" % (
        BINDINGS_PYGAME_HEADER,
        generateArray(pygameBindings),
    )

    bindingsDocs = "%s %s\n%s" % (
        BINDINGS_DOCS_HEADER,
        bindingsDocsQt,
        bindingsDocsPygame,
    )

    with open(bindings_docs_path, "w") as f:
        f.write(bindingsDocs)


if __name__ == "__main__":
    generate_bindings_docs()
