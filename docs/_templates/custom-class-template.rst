{{ fullname | escape | underline}}

.. currentmodule:: {{ module }}

.. autoclass:: {{ objname }}
   :members:                                    
   :undoc-members:
   :no-special-members:
   :show-inheritance:                           

   {% block methods %}

   {% if methods %}
   .. rubric:: {{ _('Methods') }}

   .. autosummary::
   {% for item in methods%}
     ~{{ name }}.{{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}

   {% block attributes %}
   {% if attributes %}
   .. rubric:: {{ _('Attributes') }}

   .. autosummary::
   {% for item in attributes%}
     ~{{ name }}.{{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}

   {% block inherited_methods %}
   {% if inh_methods %}
   .. rubric:: {{ _('Inherited Methods') }}

   .. autosummary::
   {% for item in inh_methods %}
     ~{{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}

   {% block inh_attr %}
   {% if inh_attr %}
   .. rubric:: {{ _('Inherited Attributes') }}

   .. autosummary::
   {% for item in inh_attr %}
     ~{{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}
