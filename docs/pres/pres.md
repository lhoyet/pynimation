[![PyNimation logo](https://gitlab.inria.fr/lhoyet/pynimation/uploads/78f649284078e39a0a8e1347bc35cd39/logo.png)](https://gitlab.inria.fr/lhoyet/pynimation/)

[![Viewer preview](https://gitlab.inria.fr/lhoyet/pynimation/uploads/8c294b31f42cb0fa0b8a195cf60819a2/viewer.gif)](https://gitlab.inria.fr/lhoyet/pynimation/)

PyNimation is a framework for editing, visualizing and studying skeletal 3D animations, it was more particularly designed to process motion capture data. It stems from the wish to utilize Python’s data science capabilities and ease of use for human motion research.

## Features
* Import animations from formats:
    - BVH
    - FBX
    - MVNX
* Export animations to formats:
    - FBX
* Access and modify joint transforms as numpy arrays
* Common tools to manipulate rotations and spatial transforms
* Animation tools like IK, support detection, retargeting, etc.
* Interactive OpenGL viewer for animations and objects
* Animate skinned meshes

## Links

- [Gitlab repository](https://gitlab.inria.fr/lhoyet/pynimation/)
- [Documentation](https://lhoyet.gitlabpages.inria.fr/pynimation/)
