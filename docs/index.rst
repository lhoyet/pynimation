.. pynimation documentation master file, created by
   sphinx-quickstart on Wed Sep 30 16:27:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pynimation's documentation!
======================================

.. autosummary::
  :recursive:

.. toctree::
   :maxdepth: 1
   :caption: Introduction

   static/introduction/introduction
   static/introduction/installation

.. toctree::
   :maxdepth: 1
   :caption: Overview

   static/overview/pynimation
   static/overview/pynimation-viewer
   static/overview/migrating_2.0
   static/overview/troubleshooting

.. toctree::
   :maxdepth: 1
   :caption: Development

   static/development/requirements
   static/development/coding_style
   static/development/type_hints
   static/development/testing
   static/development/documentation
   static/development/cicd
   Coverage report <https://lhoyet.gitlabpages.inria.fr/pynimation/coverage>

.. toctree::
   :maxdepth: 1
   :caption: API reference

   source/api/index




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
