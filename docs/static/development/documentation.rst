Documentation
=============

Documentation Style
-------------------
| PyNimation uses numpy style docstrings (`example <https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html>`_)
| Here is an example of how a typical class can be documented:

::
  
  class Example():
    """
    This is an example class, it does various things

    Examples
    --------
    >>> e = Example([1])
    >>> e.strs
    ["1"]
    >>> e.dostuff(1,[])
    "stuff has been done"

    Warnings
    --------
    This is only an example
    """

    clstuf = 2
    """
    an int used by this class to do stuff          <----- docstrings of class attributes (TYPES NOT NEEDED)
    """

    def __init__(self, ints: List[int]) -> None:
      """
      Parameters          <----- parameters of the constructor (TYPES NOT NEEDED)
      ----------
      ints:
        a list of int
      """

      self.ints = ints
      """
      a list of int          <----- docstrings of instance attributes
      """

      #: a list of strings          <----- docstrings of instance attributes can also be written like this
      self.strs = [ str(i) for i in ints ]

    def dostuff(self, input: int, otherinput: str) -> str:
      """
      Does stuff with input

      Parameters      <----- parameters of the function (TYPES NOT NEEDED)
      ----------
      input:
        input int used to do stuff

      otherinput:
        input str used to do other stuff

      Returns
      -------
      str:           <---- return type
        what stuff has been done

      Examples
      --------
      >>> e = Example([1])
      >>> e.dostuff(1,[])
      "stuff has been done"

      Note
      ----
      You need input and otherinput to be such that ... for stuff to be done correctly
      """

| Indicating the types attributes and parameters in the docstring is not necessary, if written like above, :code:`sphinx` reads them from type hints
| Writing a docstring for the constructor rather than with a ``Parameters`` section in the class docstring allows the docstring of the constructor to be passed to subclasses that do not define one for themselves. A similar mechanism occurs for class and instance attributes, documenting them with their own docstring rather than with an ``Attributes`` section in the class constructor makes it possible to have them documented in subclasses without having to put them in the subclass ``Attributes`` section.

Generation
----------
| PyNimation uses `Sphinx <https://www.sphinx-doc.org/en/master/>`_ to generate HTML documentation from :code:`rst` files and docstrings inside the source code, these files and the configuration for :code:`sphinx` can be found in the :code:`docs` directory.
| The autosummary plugin of sphinx is used to summarize all methods, attributes, inherited methods and inherited attributes of all classes, at the top of their API documentation, with link to their complete documentation. Because the original plugin did provide all these features, a `modified version <https://gitlab.inria.fr/radili/sphinx_pynimation_autosummary>`_ was written for PyNimation
| To generate the list of bindings of the documentation:

::

  python docs/generate_bindings_docs.py

| To generate the documentation, generate the html with:

::

  sphinx-build -b html docs public

| Note that :code:`sphinx` also supports other output formats like LaTeX for compiling PDFs
