Testing
=======

PyNimation uses `pytest <https://pytest.org>`_ as a test framework

Running tests
-------------

Tests can be run using::
    
    pytest

.. warning:: PyNimation must be in your path, see :ref:`Add to Python path`

Writing tests
-------------

One test file should be written for every module (python file). Tests files
should be put in a :code:`tests` directory that is in the same directory as the module.
Example project tree:
::

    pynimation
    ├── anim
    │   ├── animation.py
    │   ├── frame.py
    │   ├── joint.py
    │   ├── skeleton.py
    │   └── tests
    │       ├── test_frame.py
    │       └── test_skeleton.py
    └── common
        ├── data.py
        ├── graph.py
        ├── tests
        │   ├── test_graph.py
        │   ├── test_transform_graph.py
        │   └── test_transform.py
        ├── transform_graph.py
        └── transform.py
