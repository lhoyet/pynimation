CI/CD
=====

PyNimation uses `Gitlab CI <https://docs.gitlab.com/ee/ci/>`_ for Continuous Integration and Continuous Deployment. It allows running tests and building and deploying documentation and packages automatically when a commit is pushed to the repository

Each time a commit is pushed to any branch of the repository :

- Formatting is verified with `black <https://github.com/psf/black>`_
- Coding style is verified with `flake8 <https://flake8.pycqa.org>`_
- The code is type checked with `Mypy <https://github.com/python/mypy>`_
- Tests are run with pytest


You can check the details of the output, including error messages, in the :code:`CI/CD` tab of Gitlab's sidebar 



Each time a **tagged commit** is pushed to any branch of the repository :

  - Documentation is generated and deployed to github pages
  - Packages are built and pushed to the package registry

.. warning:: Please avoid tagging commits that are not releases
