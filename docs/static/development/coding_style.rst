Coding Style
============

Naming
------

Conventions
***********

Here is a table summarizing the naming conventions used for the various components of
pynimations:

+--------------------+--------------------+------------------------------------------------------------------+
| component          | naming convention  | examples                                                         |
+====================+====================+==================================================================+
| variable, function | :code:`mixedCase`  | :code:`variable`, :code:`myVariable`, :code:`mySuperVariable`    |
+--------------------+--------------------+------------------------------------------------------------------+
| constant           | :code:`ALL_CAPS`   | :code:`CONSTANT`, :code:`MY_CONSTANT`, :code:`MY_SUPER_CONSTANT` |
+--------------------+--------------------+------------------------------------------------------------------+
| class              | :code:`CamelCase`  | :code:`Class`, :code:`MyClass`, :code:`MySuperClass`             |
+--------------------+--------------------+------------------------------------------------------------------+
| module (file)      | :code:`snake_case` | :code:`module`, :code:`my_module`, :code:`my_super_module`       |
+--------------------+--------------------+------------------------------------------------------------------+

Private members
***************

Classes, attributes and methods should begin with a single :code:`_` if they are
private. A member or class that is not part of the API and is not intented to
be used by end users should be private


Formatting
----------

The standard code format of the project can be achieved by running :code:`black --line-length 79 .` in the root of the project. See `black <https://github.com/psf/black>`_ for installation and integration with your editor



Line Endings
************

Existing conventions
++++++++++++++++++++

Windows and Mac OS/Linux differ in their line endings conventions (i.e. the characters that are placed to signal the end of a line)

- Windows uses Carriage Return followed by Line Feed:

::
  
  This is a line on Windows\r\n

- Linux and Mac OS uses Carriage Return followed by Line Feed:

::
  
  This is a line on Linux or Mac OS\n

Convention in PyNimation
++++++++++++++++++++++++

| PyNimation chooses to use the **Windows convention** with lines ending in :code:`\r\n`
| For Windows users, this means that their editor does not need additional configuration
| For Linux and Mac OS users however, their editor should be configured to end lines with :code:`\r\n`


Import Statements
-----------------

Types of imports
****************

- imports from the Python standard library are the ones that do not need any
  packages installed, these include:

::
    
    import os
    import sys
    from typing import List

- imports from external packages: 

::
    
    import pygame as pg
    import numpy as np
    from pyquaternion import Quaternion


- imports from PyNimation:

::
  
    from pynimation.anim import Animation
    from .viewer import Viewer
    import pynimation.common.data as data_

Conventions in PyNimation
*************************

Imports style
+++++++++++++

- Avoid importing multiple packages in one line like: :code:`import os,sys`
- Avoid using :code:`*` to import multiple modules like: :code:`from pynimation.anim import *`
- Rename the :code:`numpy` module as :code:`np` when importing it: :code:`import numpy as np`
- Rename the :code:`pygame` module as :code:`pg` when importing it: :code:`import pygame as pg`
- Only place import statements at the top of the file

Order
+++++

Import statements should be placed in the following order, with an empty line
between each category:

- Python standard library
- external packages
- PyNimation

For example:
::

    import os
    import sys
    from typing import List
    
    import pygame as pg
    import numpy as np
    from pyquaternion import Quaternion

    from pynimation.anim import Animation
    from .viewer import Viewer
    import pynimation.common.data as data_
    
