This sections describes how to contribute to the project, from installing requirements to conforming to the conventions adopted

Requirements
============

With poetry
-----------

Poetry is a dependency management and packaging tool for python projects. It uses the :code:`pyproject.toml` file to define dependencies and configuration for tools and plugins. The PyNimation project uses it for package managment and configuration of :code:`mypy`, :code:`black` and other tools. Installing dependencies with poetry is done with a simple:
::

    poetry install

For further information on how to use poetry and the :code:`pyproject.toml` file, see the `poetry documentation <https://python-poetry.org/>`_


Without poetry
--------------

Poetry can be cumbersome and complex to run and install for the uninitiated. For those cases, a :code:`requirements.txt` file is provided, from which dependencies can be installed with:

::

    pip install -r requirements.txt



Add to Python path
------------------

| While examples can be run as is when pynimation is installed, you might not want to install your development version each time you need to run tests or examples.
| The solution is to add the path to both :code:`pynimation` and :code:`pynimation-viewer` packages to the Python path
| From the root of the repository:


* from python (effective until the program exits)::

    import sys
    import os
    sys.path.insert(0, os.path.abspath("."))
    sys.path.insert(0, os.path.abspath("./pynimation-viewer"))

* from Linux shell:

  You can either define an environment variable for a single shell session or for a single command

  * shell session::

      export PYTHONPATH="${PYTHONPATH-}${PYTHONPATH+":"}$(pwd):$(pwd)/pynimation-viewer"

  * single command::

      PYTHONPATH="${PYTHONPATH-}${PYTHONPATH+":"}$(pwd):$(pwd)/pynimation-viewer" python examples/myexample.py

.. warning:: Having :code:`PYTHONPATH` set to :code:`""` will result in errors with some Python versions that don't accept leading :code:`:` in :code:`PYTHONPATH`. If this is the case, avoid setting it if possible or run :code:`unset PYTHONPATH` to unset it before using the above commands



* from Windows cmd::
    
    set PYTHONPATH=[absolute path to pynimation];[absolute path to pynimation-viewer]

