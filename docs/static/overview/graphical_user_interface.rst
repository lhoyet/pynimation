Graphical user interface
========================

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/88e704ba797d362a6e4ac505ffcd3702/application.png

   overview of the graphical user interface

| PyNimation provides a Qt graphical user interface to facilitate the interaction between the user and the library.
| In order to run the interface, you need to import the Qt version of the :class:`~pynimation.viewer.ui.application.PynimationApp` class (see :ref:`Visualize animations`).

.. note:: The Pygame version of :class:`~pynimation.viewer.ui` only contains the viewer component (see :ref:`Visualize animations`).

| This interface is composed of many components, most of them can be resized and hidden, some can be detached by clicking the ``detach`` button
| There are independent keyboard and mouse bindings for each of component, see :ref:`Default bindings`.


Viewer
++++++

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/ff1fce013a27ad6035c51e9f7078fa59/viewer.png

   A preview of the interface with the viewer component highlighted in red

| Base component of the interface. It shows character animations in a 3D scene. You can navigate through it using the default bindings: :ref:`Default bindings`

Animations manager
++++++++++++++++++

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/5a5ecf0849c2e734d0c0b4add24dad01/animations_manager.png
   
   A preview of the interface with the animations manager component highlighted in red

| The goal of this menu is to manage the animations. The features of this component are:

* **Import new animations**: you can import all the files that PyNimation support (bvh, fbx, etc. see :ref:`Features`).

* **Remove animations**: delete animations that already exist in the current context. Select animations within the tree view and press remove to delete them from PyNimation.

* **Export animations**: you can export the animations existing in the current context. Select animations within the tree view and press export. 

Tree view
+++++++++

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/0db029df9b9315874629df152f402266/tree_view.png

   A preview of the interface with the tree view component highlighted in red

| For each animation present in the current context, shows their skeleton as an expandable hierarchy of joints. This view is the mean to select joints for other components like the :ref:`Joint data` component
| Multiple joints can be selected at the same time with:

* Maintaning the right click.

* Ctrl + click

* Shift + click

Joint data
++++++++++

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/c2f5dd6660bc905488166970029901d5/joint_data.png

   A preview of the interface with the joint data component highlighted in red

| Shows rotation and position data of joints selected in the :ref:`Tree view`, for the current frame. Available data and representations:

* vector representation:

	* position: ``x, y, z``

	* euler angles: ``x, y, z``

	* quaternions: ``x, y, z, w``

	* vector rotation: ``x, y, z``

* matrix representation:

	* just shows the transform matrix.

| each of these representations show the local (editable) and global (read only) transforms of the selected joints for the current frame, and the base (editable) transform of the skeleton.

.. note:: You can detach each of these component to see theme in different windows that allow to compare them.

Player
++++++

| The player component controls and shows the time of the animations

Player buttons
--------------

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/e403cce758fd657644fc1099b6c4977e/player_buttons.png

   A preview of the interface with the player buttons highlighted in red

* Play or pause the animations. 
* Go to the next frame or to the previous frame (only during pause). 
* Record a video of the scene. 
* Take a screenshot of the scene.

Speed slider
------------

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/ea40ee841a9f186c62daef23201a1223/slider.png

   A preview of the interface with the speed slider highlighted in red

| Control the slider to change speed of the player. You can set a ratio of the original speed in the text input to change the speed manually.

Timeline
--------

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/43c540eac7546ace998912a817cb6438/timeline.png

   A preview of the interface with the timeline component highlighted in red

| Representation of animations through time, as a series of frames, with a cursor indicating the current frame displayed by the :ref:`Viewer`
| Use right click to scroll through the timeline, and left click to select a specific moment.

Console
+++++++

.. figure:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/e0006a3e70e8a0e77aa6b75dbbd1ecdc/console.png

   A preview of the interface with the console component open and highlighted in red

| Python console integrated in the interface with every contextual data needed to use the PyNimation library within it. 
| Type ``help()`` in it to get more informations.

Graph
+++++

| Visualise the evolution of multiple joint data in a graph.
| Select the data to visualise (for example the x position for the current selection) and the graph shows the curve that corresponds to the selected data.

| You have at your disposition many way to manipulate the graph.
* Use the right button to move.
* Use the left button and move your mouse to resize the curves.
* Use the A button in the bottom left of the component to resize the graph.

| Like the timeline, the graph component display a cursor that indicate the current time of the animations.
* you can use h and l to go backward and forward, as well as left and right.
* you can use j and k to decrease or increase speed.
* you can use space to toggle play or pause. 

| You have many option to customize the displayed graphs :
* You can choose the "base framerate" of the abscissa axis. This option will be useful if you work with animations that have different framerate.
For example if you want to see the frame for animations that have 60 fps instead of 120, you can simply change it using the menu option on the right, the x-axis will show the frame for that framerate.
* You can create as many graph as you want for one joint. 
For each of them, you can choose the global or local transform and the type of representation you want.

.. note:: For each of these components, custom bindings can be created thanks to a python script.
