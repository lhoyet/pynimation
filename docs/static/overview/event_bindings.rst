Event bindings
==============

Event bindings are used to register functions that will be called on user events like mouse motion, mouse click, key presses, etc.

Events
------

| :code:`Event` is a class containing an id. The goal of this class is to give to PyNimation the possibility to use events regardless of the library that implements the user interface.
| There are different classes of event, each of them are inherited from the main class :code:`Event`. There is an event that fits every software need :

*  :class:`~pynimation.viewer.ui.event.Event`
*  :class:`~pynimation.viewer.ui.event.KeyPressEvent`
*  :class:`~pynimation.viewer.ui.event.KeyHoldEvent`
*  :class:`~pynimation.viewer.ui.event.KeyReleaseEvent`
*  :class:`~pynimation.viewer.ui.event.MouseClickEvent`
*  :class:`~pynimation.viewer.ui.event.MouseClickReleaseEvent`
*  :class:`~pynimation.viewer.ui.event.MouseMoveEvent`
*  :class:`~pynimation.viewer.ui.event.MouseWheelEvent`
*  :class:`~pynimation.viewer.ui.event.GeneralEvent`

| For instance, these events can be used as `Pygame events <https://www.pygame.org/docs/ref/event.html#pygame.event.Event>`_ as well as `Qt events <https://doc.qt.io/qt-5/qt.html#Key-enum>`_ .

Example with Qt :
+++++++++++++++++
| With the event :code:`KeyPressEvent`.

::

  key = KeyPressEvent(Qt.Key.Key_A)

.. note:: Pressing the :code:`A` key with any modifier will not trigger this event

| To match the :code:`A` key being pressed while the :code:`CTRL` modifier is pressed, we can write:

::

  key = KeyPressEvent(Qt.Key.Key_A, Qt.KeyboardModifier.ControlModifier)


Example with Pygame :
+++++++++++++++++++++
| With the event :code:`KeayReleaseEvent`.

::

  key = KeyPressEvent(pg.K_a)

Adding bindings
---------------

| The :class:`~pynimation.viewer.ui.event.BindingManager` class is responsible for keeping current event bindings, listening to user events and calling event handlers appropriately.
| Bindings can be be modified either with the :func:`~pynimation.viewer.ui.event.BindingManager.unbind` and :func:`~pynimation.viewer.ui.event.BindingManager.rebind` functions.
| The user interface of PyNimation can be composed of multiple component. Each component has its own :code:`bindings`.

With bind()
++++++++++++

| With the :func:`~pynimation.viewer.events.EventManager.bind` function, a single binding can be added with:

::

  # event handler
  def translateCharacter(character: Character, trans: np.ndarray=[0,0,0]) -> None:
      character.globalTransform.translate(trans)


  pynimation = PynimationApp()

  # register the new action
  pynimation.tools.actions.add("translateCharacter", translateCharacter, args=[character], kwargs={"trans":, [0.1, 0, 0]})

  # bind this action to the "h" key for the viewer component
  pynimation.window.viewerComponent.bindings.bind(KeyPressEvent(pg.K_h), "translateCharacter")

| This code binds the :code:`translateCharacter` action to the :code:`h` key press. The function will be called with :code:`character` as a first argument and :code:`[0.1,0,0]` as a value for the :code:`trans` keyword argument. It will be called anytime a the user presses the :code:`h`  key.


With the :code:`bindings` attribute
+++++++++++++++++++++++++++++++++++

| Like specified above, each components of the user interface has its own attribute :code:`bindings`. This attribute is public, you can modify it whenever you want.
| In order to that, you need to create a new :class:`~pynimation.viewer.ui.event.BindingManager` :

::

  # event handler
  def translateCharacter(character: Character, trans: np.ndarray=[0,0,0]) -> None:
      character.globalTransform.translate(trans)


  pynimation = PynimationApp()

  # register the new action
  pynimation.tools.actions.add("translateCharacter", translateCharacter, args=[character], kwargs={"trans":, [0.1, 0, 0]})

  # create a new BindingManager with the already existing ActionManager
  newBindings = BindingManager(pynimation.tools.actions)
  # bind this action to the "h" key
  newBindings.bind(KeyPressEvent(pg.K_h), "translateCharacter")  

  # replace the old BindingManager by the new one for the viewer component
  pynimation.window.viewerComponent.bindings = newBindings

| You can also create a new ActionManager in order to completely seperate a user interface component from the rest of the software actions :

::

  # event handler
  def translateCharacter(character: Character, trans: np.ndarray=[0,0,0]) -> None:
      character.globalTransform.translate(trans)


  pynimation = PynimationApp()

  # create a new action manager
  newActions =  ActionManager(
    {
      "translateCharacter" : Action(
        partial(translateCharacter, character, [0.1, 0, 0])
      )
    }
  )

  # create a new BindingManager with the already existing ActionManager
  newBindings = BindingManager(newActions)
  # bind an action to the "h" key
  newBindings.bind(KeyPressEvent(pg.K_h), "translateCharacter")  

  # replace the old BindingManager by the new one for the viewer component
  pynimation.window.viewerComponent.bindings = newBindings


Example
-------

.. literalinclude:: ../../../examples/custom_bindings.py

