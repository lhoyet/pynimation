Visualize animations
--------------------

| PyNimation comes with an interactive OpenGL visualization engine that can display multiple objects and animations arranged in a scene, we will refer to it simply as the viewer. This capability is available with the optional :code:`pynimation-viewer` package.

| The viewer offers a very flexible API, objects and animations can be displayed in one call, they can be precisely arranged in a scene, or the main loop can be partially re-written to fit the user's needs. We will be reviewing the simplest API here, see :ref:`Viewer` for more.

| The viewer comes with two interfaces:

* The default one is a :ref:`Graphical user interface` that offers a visual way to manipulate the animations and interact with the viewer through various tools (a timeline, an animation data inspector, etc.). This  interface is implemented with `PyQt5 <https://doc.qt.io/qtforpython/>`_. It can be launched with:

::

  from pynimation.viewer.ui import PynimationApp

  PynimationApp().viewer.display(animation)


.. image:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/88e704ba797d362a6e4ac505ffcd3702/application.png


* The lite one  only contains the OpenGL visualization: you can use it by importing the :class:`~from pynimation.viewer.ui.pygame_ui.pygame_application.PynimationApp` class from the :mod:`~pynimation.viewer.ui.pygame_ui.pygame_application` module. This interface is implemented with `pygame <https://www.pygame.org/docs/>`_. It can be launched with:

::

  from pynimation.viewer.ui.pygame_ui.pygame_application import PynimationApp

  PynimationApp().viewer.display(animation)

.. image:: https://gitlab.inria.fr/lhoyet/pynimation/uploads/ff1fce013a27ad6035c51e9f7078fa59/viewer.png
