In this section, most of the mechanisms related to basic access and manipulation of animations and related data structures will be detailed

Loading
-------

| :class:`~pynimation.anim.animation.Animation` objects can be imported using the :func:`~pynimation.anim.animation.Animation.load` function, it takes the path to an animation file in one of the supported formats, and returns an animation in the internal format. For loading multiple animations, see :func:`~pynimation.anim.animation.Animation.loadMultiple`.

::

  >>> import numpy as np

  >>> from pynimation.anim.animation import Animation
  >>> from pynimation.viewer.viewer import Viewer
  >>> from pynimation.common import data

  >>> # obtain absolute path to provided example BVH animation file
  >>> # (this is only necessary for files provided with PyNimation)
  >>> bvh_file = data.getDataPath("data/animations/walk_loop.bvh")

  >>> # load the animation
  >>> animation: Animation = Animation.load(bvh_file)

  >>> # keep a copy of the original
  >>> animationPost = animation.copy()

.. note:: Here, the :code:`data.getDataPath` function is used to get the runtime path of the example animations provided with PyNimation, unless you import one of the assets provided with PyNimation, you won't need to use this function.

| We also make a copy of the original animation to later compare it to its modified version

Animation objects
-----------------

PyNimation handles per-frame skeletal animations. An :class:`~pynimation.anim.animation.Animation` object is a `numpy array <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ of transforms of shape ``(nframes, njoints, 4, 4)``: one transform for each joint of each frame.

::
  
  >>> # animations are numpy arrays of transforms
  >>> isinstance(animation, np.ndarray)
  True

  >>> # of shape is (nframes, njoints, 4, 4)
  >>> animation.shape
  (261, 29, 4, 4)

.. note:: :class:`~pynimation.anim.animation.Animation` being a subtype `numpy array <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ means that every function of the ``numpy`` API is available to animation objects

Animation and Skeleton
----------------------

Joint ids
*********
  
An :class:`~pynimation.anim.animation.Animation` object also references a :attr:`~pynimation.anim.skeleton.Skeleton` that describes the skeletal structure to animate. In this skeleton, all joints have an id (displayed below between square brackets) corresponding to their index in the :attr:`~pynimation.anim.skeleton.Skeleton.joints` list. 

::

  >>> # print skeleton hierarchy
  >>> animation.skeleton
  [0]Hips (root)
      [1]Chest (upperBody)
          [2]Chest2
              [3]Chest3
                  [4]Neck (neck, headBranch)
                      [5]Head (head)
                          [6]Site

                  [7]RightCollar (rightClavicle, rightArm)
                      [8]RightShoulder (rightShoulder)
                          [9]RightElbow (rightElbow)
                              [10]RightWrist (rightWrist)
                                  [11]RightFinger0
                                      [12]Site

                  [13]LeftCollar (leftClavicle, leftArm)
                      [14]LeftShoulder (leftShoulder)
                          [15]LeftElbow (leftElbow)
                              [16]LeftWrist (leftWrist)
                                  [17]LeftFinger0
                                      [18]Site

      [19]RightHip (rightHip, rightLeg)
          [20]RightKnee (rightKnee)
              [21]RightAnkle (rightAnkle)
                  [22]RightToe (rightToe)
                      [23]Site

      [24]LeftHip (leftHip, leftLeg)
          [25]LeftKnee (leftKnee)
              [26]LeftAnkle (leftAnkle)
                  [27]LeftToe (leftToe)
                      [28]Site

  >>> # get right hip joint
  >>> animation.skeleton[19].name
  'RightHip'

Animation array structure
*************************

A joint id also indexes the transforms that animates this joint, in the second dimension of the animation array.

::

  >>> # get animation transforms of right hip joint (id 19) for each frame
  >>> animation[:, 19]
  Animation([[[ 0.984469  ,  0.14519467, -0.09868784, -0.085317  ],
              [-0.16337643,  0.9634514 , -0.21229588,  0.        ],
              [ 0.06425671,  0.22512198,  0.97220943,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             [[ 0.98159976,  0.15975675, -0.104593  , -0.085317  ],
              [-0.17697174,  0.96684466, -0.18409892,  0.        ],
              [ 0.07171413,  0.19922146,  0.97732691,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             [[ 0.97909371,  0.17214126, -0.10836468, -0.085317  ],
              [-0.18770944,  0.96986661, -0.1553188 ,  0.        ],
              [ 0.07836251,  0.17241273,  0.98190283,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             ...,

             [[ 0.99029694,  0.11307017, -0.08079051, -0.085317  ],
              [-0.13070951,  0.95528991, -0.26520976,  0.        ],
              [ 0.04719104,  0.2731965 ,  0.96080002,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             [[ 0.98749588,  0.1291379 , -0.09041727, -0.085317  ],
              [-0.14742818,  0.95961989, -0.23957168,  0.        ],
              [ 0.05582843,  0.2499061 ,  0.96665926,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             [[ 0.984469  ,  0.14519467, -0.09868784, -0.085317  ],
              [-0.16337643,  0.9634514 , -0.21229588,  0.        ],
              [ 0.06425671,  0.22512198,  0.97220943,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]]])

  >>> # rather than use animation[:, 19] we can use the 'joint' attribute
  >>> # that can take multiple ids or joint objects
  >>> rightHip, leftHip = animation.skeleton[19], animation.skeleton[24]
  >>> np.all(animation[:, [19, 24]] == animation.joint[rightHip, leftHip])
  True

Humanoid skeleton mapping
*************************

Next to the joint names, in the skeleton representation printed earlier, in parentheses, are the humanoid joint mappings. Once created, joints of :attr:`~pynimation.anim.skeleton.Skeleton` objects are mapped to humanoid skeleton joints with generic names like ``rightHip``, ``legs``, ``leftToe``. With these generic names, joints can be refered to regardless of the exact structure of the animation's skeleton or the joints' names, thus making it possible to generalize algorithms to various skeleton structures and formats.

::

  >>> # get the right hip joint from its generic name
  >>> animation.skeleton.rightHip.name
  'RightHip'

  >>> # get the transforms of the right hip joint for every frame
  >>> animation.rightHip
  Transform([[[ 0.984469  ,  0.14519467, -0.09868784, -0.085317  ],
              [-0.16337643,  0.9634514 , -0.21229588,  0.        ],
              [ 0.06425671,  0.22512198,  0.97220943,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             [[ 0.98159976,  0.15975675, -0.104593  , -0.085317  ],
              [-0.17697174,  0.96684466, -0.18409892,  0.        ],
              [ 0.07171413,  0.19922146,  0.97732691,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             [[ 0.97909371,  0.17214126, -0.10836468, -0.085317  ],
              [-0.18770944,  0.96986661, -0.1553188 ,  0.        ],
              [ 0.07836251,  0.17241273,  0.98190283,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             ...,

             [[ 0.99029694,  0.11307017, -0.08079051, -0.085317  ],
              [-0.13070951,  0.95528991, -0.26520976,  0.        ],
              [ 0.04719104,  0.2731965 ,  0.96080002,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             [[ 0.98749588,  0.1291379 , -0.09041727, -0.085317  ],
              [-0.14742818,  0.95961989, -0.23957168,  0.        ],
              [ 0.05582843,  0.2499061 ,  0.96665926,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],

             [[ 0.984469  ,  0.14519467, -0.09868784, -0.085317  ],
              [-0.16337643,  0.9634514 , -0.21229588,  0.        ],
              [ 0.06425671,  0.22512198,  0.97220943,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]]])

  >>> # get the transforms of the two hip joints at frame 1
  >>> animation.legs[1]
  Transform([[[ 0.98159976,  0.15975675, -0.104593  , -0.085317  ],
              [-0.17697174,  0.96684466, -0.18409892,  0.        ],
              [ 0.07171413,  0.19922146,  0.97732691,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]],
  
             [[ 0.97938026, -0.00150205,  0.20201992,  0.085317  ],
              [-0.07081002,  0.93398447,  0.35022699,  0.        ],
              [-0.18920953, -0.35731043,  0.9146196 ,  0.        ],
              [ 0.        ,  0.        ,  0.        ,  1.        ]]])

  >>> # equivalent ways of writing the last statement
  >>> legs = animation.skeleton.legs
  >>> legsIds = [leg.id for leg in legs]

  >>> eq = [
      animation[1].legs,
      animation.legs[1],
      animation[1].joint[legs],
      animation.joint[legs][1],
      animation[1].joint[legsIds],
      animation.joint[legsIds][1],
      animation[1, legsIds],
  ]
  >>> all([np.all(eq[0] == b) for b in eq[1:]])
  True

Base pose
*********

Traditionally, a skeletal animation is applied to a skeleton that already
defines joint length and sometimes rotation. This is called the **base pose**
of the skeleton. In PyNimation, the base pose of the skeleton is included in the animation's
local transforms. However, it is possible to access it, remove it and add it
back

::

  >>> # access the whole basePose array
  >>> animation.skeleton.basePose.shape
  (29, 4, 4)

  >>> # access individual base transforms (here only joint length)
  >>> animation.skeleton.rightHip.transform
  _IndivLocalGlobal([[ 1.    ,  0.    ,  0.    , -0.0853],
                     [ 0.    ,  1.    ,  0.    ,  0.    ],
                     [ 0.    ,  0.    ,  1.    ,  0.    ],
                     [ 0.    ,  0.    ,  0.    ,  1.    ]])

  >>> # joint length is included in animation
  >>> animation[1].rightHip
  Transform([[ 0.9816,  0.1598, -0.1046, -0.0853],
             [-0.177 ,  0.9668, -0.1841,  0.    ],
             [ 0.0717,  0.1992,  0.9773,  0.    ],
             [ 0.    ,  0.    ,  0.    ,  1.    ]])

  >>> # if we remove the base pose, it is not included anymore
  >>> animation.removeBasePose()
  >>> animation[1].rightHip
  Transform([[ 0.9816,  0.1598, -0.1046,  0.    ],
             [-0.177 ,  0.9668, -0.1841,  0.    ],
             [ 0.0717,  0.1992,  0.9773,  0.    ],
             [ 0.    ,  0.    ,  0.    ,  1.    ]])

  >>> animation.addBasePose()
  >>> animation[1].rightHip
  Transform([[ 0.9816,  0.1598, -0.1046, -0.0853],
             [-0.177 ,  0.9668, -0.1841,  0.    ],
             [ 0.0717,  0.1992,  0.9773,  0.    ],
             [ 0.    ,  0.    ,  0.    ,  1.    ]])

Locals and Globals
------------------

Transforms of a joint in the animation are local to this joint, meaning they are 
relative to its parent's transforms, which are themselves relative to its 
parent's parent's transforms. The global transforms are the joint transforms 
in world coordinates, and not in parent coordinates. So for every joint ``j`` we have

::

    j.globalTransform = j.parent.globalTransform @ j.transform


Where ``@`` is the matrix multiplication operator (like in PyNimation)

.. note:: For the root, which doesn't have a parent, ``root.globalTransform =
   root.transform``

Global transforms of the animation are accessible with ``animation.globals``,
which is an array of the same size that the ``animation`` array. By default,
global transforms are automatically recomputed when accessed, if the local
transforms changed. This mecanism can be turned off.

::

  >>> # the globals array has the same shape that the locals array
  >>> animation.globals.shape
  (261, 29, 4, 4)

  >>> # globals can be accessed the same way than locals
  >>> animation[1].rightHip
  Transform([[ 0.9816,  0.1598, -0.1046, -0.0853],
             [-0.177 ,  0.9668, -0.1841,  0.    ],
             [ 0.0717,  0.1992,  0.9773,  0.    ],
             [ 0.    ,  0.    ,  0.    ,  1.    ]])
  >>> animation.globals[1].rightHip
  Transform([[ 0.9699,  0.0874, -0.2274, -0.0727],
             [-0.1133,  0.9882, -0.1035,  0.8212],
             [ 0.2156,  0.1262,  0.9683,  2.0157],
             [ 0.    ,  0.    ,  0.    ,  1.    ]])

  >>> # update rightHip position at frame 1
  >>> animation[1].rightHip[1, 3] = 1

  >>> # globals are automatically updated
  >>> animation.globals[1].rightHip
  Transform([[ 0.9699,  0.0874, -0.2274, -0.118 ],
             [-0.1133,  0.9882, -0.1035,  1.8157],
             [ 0.2156,  0.1262,  0.9683,  1.9213],
             [ 0.    ,  0.    ,  0.    ,  1.    ]])

Transforms
----------

As mentionned before, an animation is an array of 4x4 homogeneous transform matrices. 
An homogeneous transform matrix includes rotation and position, as a 3x3 rotation matrix
and a 3d vector. Both can be accessed and assigned with properties ``rotation``
and ``position``. This is of course true for transform arrays of any shape.

::

  >>> animation[1].rightHip
  Transform([[ 0.9816,  0.1598, -0.1046, -0.0853],
             [-0.177 ,  0.9668, -0.1841,  1.    ],
             [ 0.0717,  0.1992,  0.9773,  0.    ],
             [ 0.    ,  0.    ,  0.    ,  1.    ]])

  >>> # access rotation
  >>> animation[1].rightHip.rotation
  Rotation([[ 0.9816,  0.1598, -0.1046],
            [-0.177 ,  0.9668, -0.1841],
            [ 0.0717,  0.1992,  0.9773]])

  >>> # access position
  >>> animation[1].rightHip.position
  Vec3([-0.0853,  1.    ,  0.    ])

  >>> # assign rotation
  >>> animation[1].rightHip.rotation = animation[1].leftHip.rotation

  >>> # assign position
  >>> animation[1].rightHip.position = animation[1].leftHip.position

  >>> # access global positions of legs on frame 1 to 4
  >>> animation.globals[1:5].legs.position
  Vec3([[[-0.118 ,  1.8157,  1.9213],
         [ 0.0962,  0.831 ,  2.038 ]],
  
        [[-0.0709,  0.8213,  2.0286],
         [ 0.098 ,  0.8325,  2.05  ]],
  
        [[-0.0694,  0.8221,  2.0411],
         [ 0.0995,  0.8339,  2.0617]],
  
        [[-0.0681,  0.8231,  2.0534],
         [ 0.1009,  0.8355,  2.0733]]])

Vectors
-------

Arrays of vectors of 2, 3 and 4 elements are handled by
:class:`~pynimation.common.vec.Vec2`, :class:`~pynimation.common.vec.Vec3` and
:class:`~pynimation.common.vec.Vec4`. These are used for positions, euler
angles, etc. Similarly to GLSL vec classes, the ``x`` elements of all vectors of the array can be accessed and assigned with ``vectors.x``, and combinations of elements can be made (e.g. ``vectors.xy``)

::

  >>> # access positions of legs on frame 1 to 4
  >>> animation[1:5].legs.position
  Vec3([[[-0.0853,  1.    ,  0.    ],
         [ 0.0853,  0.    ,  0.    ]],
  
        [[-0.0853,  0.    ,  0.    ],
         [ 0.0853,  0.    ,  0.    ]],
  
        [[-0.0853,  0.    ,  0.    ],
         [ 0.0853,  0.    ,  0.    ]],
  
        [[-0.0853,  0.    ,  0.    ],
         [ 0.0853,  0.    ,  0.    ]]])

  >>> # access x positions of legs on frame 1 to 4
  >>> animation[1:5].legs.position.x
  array([[[-0.0853],
          [ 0.0853]],
  
         [[-0.0853],
          [ 0.0853]],
  
         [[-0.0853],
          [ 0.0853]],
  
         [[-0.0853],
          [ 0.0853]]])

  >>> animation[1:5].legs.position.xy += 1
  >>> animation[1:5].legs.position
  Vec3([[[0.914683, 2.      , 0.      ],
         [1.085317, 1.      , 0.      ]],
  
        [[0.914683, 1.      , 0.      ],
         [1.085317, 1.      , 0.      ]],
  
        [[0.914683, 1.      , 0.      ],
         [1.085317, 1.      , 0.      ]],
  
        [[0.914683, 1.      , 0.      ],
         [1.085317, 1.      , 0.      ]]])

  >>> # set root xz origin at (1,1)
  >>> self.root.position.xz += Vec2(1,1) - self.root.position[0].xz

Rotations
---------

Rotations are represented by default as 3x3 rotation matrices, but they can be
converted to and from euler angles, quaternions and angle-axis representations 
whith properties :attr:`~pynimation.common.rotation.Rotation.euler`, :attr:`~pynimation.common.rotation.Rotation.rotvec` and :attr:`~pynimation.common.rotation.Rotation.quat`.
When setting these properties or part of them, the result is converted back
to the original rotation matrix array

::
  
  >>> animation[1].rightHip.rotation
  Rotation([[ 0.9816,  0.1598, -0.1046],
            [-0.177 ,  0.9668, -0.1841],
            [ 0.0717,  0.1992,  0.9773]])

  >>> # get euler angles
  >>> animation[1].rightHip.rotation.euler
  Vec3([ 0.2011, -0.0718, -0.1784])

  >>> # modify euler angles
  >>> animation[1].rightHip.rotation.euler.x += 0.2
  >>> animation[1].rightHip.rotation.euler
  Vec3([ 0.4011, -0.0718, -0.1784])

  >>> # rotation matrices are modified accordingly
  >>> animation[1].rightHip.rotation
  Rotation([[ 0.9816,  0.1358, -0.1342],
            [-0.177 ,  0.911 , -0.3725],
            [ 0.0717,  0.3894,  0.9183]])

  >>> # this works for multidimensional arrays
  >>> animation[1:4].legs.rotation.quat
  Quaternion([[[ 0.1952, -0.0528, -0.0801,  0.9761],
               [-0.1808,  0.1   , -0.0177,  0.9783]],
  
              [[ 0.0827, -0.0471, -0.0908,  0.9913],
               [-0.1756,  0.1011, -0.0231,  0.979 ]],
  
              [[ 0.0683, -0.0485, -0.094 ,  0.992 ],
               [-0.1697,  0.1027, -0.0263,  0.9798]]])

Export animations
-----------------

Exporting works in a similar way as importing, it is done through a single :func:`~pynimation.anim.animation.Animation.save` function, common to all formats. For now, only :code:`FBX`, ``PYNIM`` and ``NPZ`` are supported for exporting.
::

    # export the modified animation
    animation.save("modified.npz")
