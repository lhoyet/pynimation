Multidimensional data structures
================================

Several base data structures of PyNimation are essentially augmented numpy arrays. By subclassing numpy arrays, they allow manipulation by functions of the numpy and scipy API, but also with functions of PyNimations's API, designed for specific tasks related to these structures

Also take a look at :ref:`Vectorizing` for an example of how to use these structures compared to the previous iterative methods

.. note:: For clarity's sake, methods and attributes inherited from ``numpy.ndarray`` are not shown in the API documentation


Base Structures
---------------

These are basic multidimensional structures for spatial information (vectors, rotation, transform), based on numpy arrays

* :class:`~pynimation.common.vec.Vec2`
* :class:`~pynimation.common.vec.Vec3`
* :class:`~pynimation.common.vec.Vec4`
* :class:`~pynimation.common.rotation.Rotation`
* :class:`~pynimation.common.quaternion.Quaternion`
* :class:`~pynimation.common.transform.Transform`

And although their name suggest that they hold a single element, they are actually designed to be used as multidimensional arrays of these elements

For example a ``2x2`` array of 3-dimensional vectors can be created as a :class:`~pynimation.common.vec.Vec3` object.
::

  >>> from pynimation.common.vec import Vec3
  >>> v = Vec3.random((2,2,3))
  >>> v.shape
  (2,2,3)

Many operations can be performed on every vectors of the array without a loop. For example normalizing, or cross product, etc. (see :class:`~pynimation.common.vec.Vec3`)
::

  >>> v.normalize()
  >>> v1 = Vec3.random((2,2,3))
  >>> v2 = v.cross(v1)
  >>> v2.shape
  (2,2,3)

As another example a :class:`~pynimation.common.transform.Transform` object ``t`` can be created as a ``2x2`` array of transforms. Multiplication of every transforms of this array and another is possible with a single operation:
::

  >>> from pynimation.common.transform import Transform
  >>> t = Transform.identity((2,2))
  >>> t.shape
  (2,2,4,4)
  >>> t1 = Transform.identity((2,2))
  >>> t1.rotation.eulerDeg.x = 10
  >>> # multiply t and t1 transform-wise
  >>> t2 = t @ t1
  
See the documentation of every structure for more

Animation related structures
----------------------------

From these base structures are derived a number of structures dedicated to representing animation, skeletal, and more generally graph based spatial data

* :class:`~pynimation.common.localglobal.LocalGlobal`
* :class:`~pynimation.anim.pose.Pose`
* :class:`~pynimation.anim.animation.Animation`

As with base structures, because they are essentially augmented numpy arrays, they are designed for vectorized operations. For example, you can multiply a whole animation by a pose, if the number of joint is the same:
::

    >>> import pynimation.common.data as data_
    >>> from pynimation.anim.animation import Animation
    >>>
    >>> bvh_file = data_.getDataPath("data/animations/walk_loop.bvh")
    >>>
    >>> animation = Animation.load(bvh_file)
    Finished: 25.414595000213012
    >>> animation.shape
    (261, 29, 4, 4)
    >>> animation.skeleton.basePose.shape
    (29, 4, 4)
    >>> # multiply
    >>> animation[...] = animation.skeleton.basePose.inverse() @ animation

