Migrating to 2.0
================

From objects to Numpy arrays
----------------------------

The motivation for the ``2.0`` version, and the major evolution compared to ``1.0`` is the data structure used for animation data. The new animation data structure is a numpy array, rather than nested objects holding a single numpy array for every transform. This structural change allows a shift of paradigm for the development of algorithms, from iterative to parallel. An example of how to parallelize code with these new data structures is provided in :ref:`Vectorizing`

New data structures
*******************

In the new version, an :class:`~pynimation.anim.animation.Animation` is a subclass of ``numpy.ndarray``, parent classes of :class:`~pynimation.anim.animation.Animation` provide methods to manipulate and access components of the array. For more, see :ref:`Multidimensional data structures`

Vectorizing
***********

Algorithms can be vectorized with the help of both PyNimation and Numpy. PyNimation provides the most common vectorized operations related to linear algebra and 3d geometry. While the numpy API is the most complete and flexible set of tools for general array manipulation. Much information can be found on vectorizing algorithms in numpy and scipy documentations. We provide an example below for how to use PyNimation's ``2.0`` API to vectorize iterative algorithms

This algorithm sets the origin position of the animation on the XZ plane, because the root position at each frame is absolute and not relative to the previous frame, it has to be shifted by the displacement vector for the new origin position at each frame.

This is the iterative PyNimation ``1.2.post2`` implementation, where a for loop was needed:
::

    def setOriginRootXZPosition(
        self, position: List[float] = [0.0, 0.0]
    ) -> None:
        originalPosition = self.frames[0].localTransforms[0].getPosition()
        originalPosition[0] -= position[0]
        originalPosition[2] -= position[1]

        rootID = self.skeleton.root.id
        for i in range(self.getFrameNumber()):
            fr = self.frames[i]
            pos = fr.getRootPosition()
            fr.setBonePosition(
                rootID,
                np.array(
                    [
                        pos[0] - originalPosition[0],
                        pos[1],
                        pos[2] - originalPosition[2],
                    ]
                ),
            )

Below is the PyNimation ``2.0`` implementation, where all operations can be performed via array manipulation.
::

    def setXZOrigin(self, position: np.ndarray = Vec2.zeros(2)) -> None:
        self.root.position.xz += position - self.root.position[0].xz

Here, :code:`self.root.position.xz` basically does :code:`self[:,0,[0,2], 3]` where ``0`` is the id of the root joint, ``[0,2]`` grabs the ``x`` and ``z`` lines, and ``3`` grabs the 3rd column for the position
  
You can check other functions and modules between ``1.2.post2`` and ``2.0`` for
how to vectorize more complex algorithms


API Changes
-----------

Namespaces
**********
| In PyNimation ``1.0``, the namespaces were organised such that it was possible to import all module members (classes, functions, etc.) from the parent package (e.g. :code:`from pynimation.anim import Animation` instead of :code:`from pynimation.anim.animation import Animation`)
| Because it created circular imports very easily and caused lots of unecessary imports, it is no longer the case in PyNimation ``2.0``.

PynimationApp
*************
| A :ref:`Graphical user interface` is now available in version ``2.0``, and while the old ``pygame`` implementation is still available, the API for launching the viewer and now the UI has evolved.
| Where it could started with :code:`Viewer().display()` before, it now requires a :code:`PynimationApp().tools.viewer.display()`

Frames and transform lists
**************************
As mentionned in the :ref:`From objects to Numpy arrays` section, animations are now longer composed of ``Frame`` objects, themselves composed of lists of ``Transforms``, but an animation is itself a numpy array. Operations can be performed on this array as a whole, with the help on numpy functions, and :ref:`Multidimensional data structures`.

Missing Tools
*************
Some tools are not yet implemented in this version, specifically the full body IK algorithm, and the cyclification algorithm
