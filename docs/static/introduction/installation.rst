Installation
************

These instructions will help you install PyNimation as a package, with ``pip``

Release versions
================

To install the latest **release version**, run the following commands:

pynimation and pynimation-viewer
--------------------------------
::

  pip install pynimation pynimation-viewer --extra-index-url https://gitlab.inria.fr/api/v4/projects/18034/packages/pypi/simple 


only pynimation
---------------
::

  pip install pynimation --extra-index-url https://gitlab.inria.fr/api/v4/projects/18034/packages/pypi/simple 


Non-release versions
====================

In order to install **non-release versions** (that don't end in :code:`.number`, e.g. :code:`1.1.dev1` or :code:`1.1.rc1`), the :code:`--pre` option must be given to pip after :code:`install`
::

  pip install --pre pynimation pynimation-viewer --extra-index-url https://gitlab.inria.fr/api/v4/projects/18034/packages/pypi/simple 
