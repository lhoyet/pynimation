Introduction
============

Description
-----------

PyNimation is a framework for editing, visualizing and studying skeletal 3D animations, it was more particularly designed to process motion capture data. It stems from the wish to utilize Python's data science capabilities and ease of use for human motion research. 

Features
--------


* Import animations from formats:

   * BVH
   * FBX
   * MVNX

* Export animations to formats:

   * FBX

* Access and modify joint transforms as numpy arrays
* Common tools to manipulate rotations and spatial transforms
* Animation tools like IK, support detection, retargeting, etc.
* Interactive OpenGL viewer for animations and objects
* Interactive graphical interface with timeline, interactive console, joint data, etc.
* Animate skinned meshes

Out-of-tree development
-----------------------

If your code is not destined to be integrated into pynimation, you might want to keep it separate from the pynimation source tree and use pynimation as a library. This will facilitate future updates and allows you to version your code separately. To do so, install pynimation as a package using the standard installation method (with ``pip``, see :ref:`Installation`), or add your pynimation folder to the python path, and import the modules using :code:`pynimation.[module]`

Packages
--------

PyNimation is split into 2 packages:
  * the core :code:`pynimation` package contains everything needed to import, process and export animations, it can be used independently of :code:`pynimation-viewer`
  * the optional :code:`pynimation-viewer` package contains everything needed to visualize animations, it provides the :code:`pynimation.viewer` module
