# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from datetime import date

pynimation_path = os.path.abspath("..")
sys.path.insert(0, pynimation_path)
sys.path.insert(0, "%s/pynimation-viewer" % pynimation_path)


# -- Project information -----------------------------------------------------

project = "pynimation"
copyright = "%d, Ludovic Hoyet" % date.today().year
author = "Ludovic Hoyet"

# The full version, including alpha/beta/rc tags
release = "2.0"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.coverage",
    "sphinx.ext.autodoc",
    "sphinx_pynimation_autosummary",
    # "sphinx.ext.autosummary",
    "sphinx.ext.napoleon",
    "sphinx.ext.doctest",
    "sphinx_autodoc_typehints",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.linkcode",
]

set_type_checking_flag = False

autosummary_generate = True  # Turn on sphinx.ext.autosummary
templates_path = ["_templates"]

autoclass_content = "both"

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
html_theme_options = {"collapse_navigation": False, "sticky_navigation": False}


def autodoc_skip_member(app, what, name, obj, skip, options):
    if type(obj) == property:
        realname = obj.fget.__qualname__.split(".")[1]
        return realname != name and realname[0] == name[0]


def linkcode_resolve(domain, info):
    def find_source():
        # try to find the file and line number, based on code from numpy:
        # https://github.com/numpy/numpy/blob/master/doc/source/conf.py#L286
        obj = sys.modules[info["module"]]
        for part in info["fullname"].split("."):
            obj = getattr(obj, part)
        import inspect
        import os

        fn = inspect.getsourcefile(obj)
        fn = os.path.relpath(fn, start=pynimation_path)
        source, lineno = inspect.getsourcelines(obj)
        return fn, lineno, lineno + len(source) - 1

    if domain != "py" or not info["module"]:
        return None
    try:
        filename = "%s#L%d-L%d" % find_source()
    except Exception:
        filename = info["module"].replace(".", "/") + ".py"
    tag = "master"
    return "https://gitlab.inria.fr/lhoyet/pynimation/blob/%s/%s" % (
        tag,
        filename,
    )


def setup(app):
    app.connect("autodoc-skip-member", autodoc_skip_member)
