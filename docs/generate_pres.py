import os

import markdown

repo_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
pres_dir = repo_dir + "/docs/pres"
doc_title = "PyNimation"
with open(pres_dir + "/pres.md", "r") as f:
    html = markdown.markdown(f.read())
    output = [
        """<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <link href="github-markdown.css" type="text/css" rel="stylesheet" />
        <title>%(title)s</title>
        <style>
                .markdown-body {
                        box-sizing: border-box;
                        min-width: 200px;
                        max-width: 980px;
                        margin: 0 auto;
                        padding: 45px;
                }

                @media (max-width: 767px) {
                        .markdown-body {
                                padding: 15px;
                        }
                }
        </style>
      </head>

    <body class="markdown-body">
    """
        % {"title": doc_title}
    ]

    output.append(html)

    output.append(
        """

    </body>

    </html>
    """
    )
    with open(pres_dir + "/index.html", "w") as f1:
        f1.write("".join(output))
