import argparse
import importlib
import os
import runpy
import sys
import time

repoDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
exampleDir = repoDir + "/examples"
sys.path.insert(0, os.path.abspath(repoDir))
sys.path.insert(0, os.path.abspath(repoDir + "/pynimation-viewer"))

parser = argparse.ArgumentParser(
    """
    Run example files from pynimation/examples without having pynimation in
PYTHONPATH
    """
)
parser.add_argument(
    "file",
    metavar="example_file.py",
    type=str,
    help="the example file from pynimation/examples to run",
)
parser.add_argument(
    "--benchmark",
    "-b",
    action="store_true",
    help="Record execution time of this example, exit when Viewer() is run",
)

args = parser.parse_args(sys.argv[1:])


def isPythonScript(filename: str):
    return len(filename) > 3 and os.path.splitext(filename)[1] == ".py"


def exampleFullPath(filename: str):
    return exampleDir + "/" + filename


def mockViewer(benchEnd):
    def exit_(*args, **kwargs):
        benchEnd(time.time())
        sys.exit(0)

    viewerCls = importlib.import_module("pynimation.viewer.viewer").Viewer

    setattr(viewerCls, "displayScene", exit_)


def benchmark(filename):
    times = []

    def benchBegin(time):
        times.append(time)

    def benchEnd(time):
        times.append(time)
        print("%f" % (times[1] - times[0]))

    mockViewer(benchEnd)

    benchBegin(time.time())
    runpy.run_path(filename)


if args.file is not None:
    if isPythonScript(args.file):
        fpath = exampleFullPath(args.file)
        if args.benchmark:
            benchmark(fpath)
        else:
            runpy.run_path(fpath)
